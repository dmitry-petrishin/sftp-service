﻿using System;
using System.Runtime.Loader;
using System.Threading;

using Deloitte.Cortex.SftpService.Server;

namespace Deloitte.Cortex.SftpService
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = StartupServer
                .GetInstance()
                .SftpServerHost;
            service.Run();

            // Catch SIGINT and SIGTERM and process them
            var ended = new ManualResetEventSlim();
            var starting = new ManualResetEventSlim();

            Console.CancelKeyPress += (object sender, ConsoleCancelEventArgs cancelArgs) =>
            {
                Console.WriteLine("SIGINT caught");
                starting.Set();
                ended.Wait();
            };

            AssemblyLoadContext.Default.Unloading += (ctx) =>
            {
                Console.WriteLine("Unloading fired");
                starting.Set();
                ended.Wait();
            };

            starting.Wait();
            service.Stop();
            ended.Set();
        }
    }
}
