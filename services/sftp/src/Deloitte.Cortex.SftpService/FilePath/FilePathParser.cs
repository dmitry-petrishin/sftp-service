﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Deloitte.Cortex.SftpService.Enums;

namespace Deloitte.Cortex.SftpService.FilePath
{
    /// <summary> File path parser. </summary>
    public class FilePathParser : IFilePathParser
    {
        /// <summary> Parsing dictionary is used instead of switch statement to parse either bundle file path or data request file path. </summary>
        private readonly IDictionary<ServiceEnum, Action> parsingDictionary;

        /// <summary> Id parsed from file path. </summary>
        private string id;

        /// <summary> Default received path from constructor. </summary>
        private string path;

        /// <summary> Initializes a new instance of the <see cref="FilePathParser"/> class. </summary>
        /// <param name="path"> File path. </param>
        public FilePathParser(string path)
        {
            this.path = path;

            this.Service = path.IndexOf("_datarequest", StringComparison.OrdinalIgnoreCase) > -1
                               ? ServiceEnum.DataRequest
                               : ServiceEnum.Bundle;

            this.parsingDictionary = 
                new Dictionary<ServiceEnum, Action>
                {
                    { ServiceEnum.Bundle, this.ParseBundleFilePath },
                    { ServiceEnum.DataRequest, this.ParseDataRequestFilePath }
                };
        }

        /// <inheritdoc />
        public string FileName { get; private set; }

        /// <inheritdoc />
        /// <summary>
        /// Sets id and parsed id flag to true. 
        /// </summary>
        public string Id
        {
            get => this.id;
            private set
            {
                this.id = value;
                this.IsIdParsed = true;
            }
        }

        /// <inheritdoc />
        public bool IsIdParsed { get; private set; }

        /// <inheritdoc />
        public ServiceEnum Service { get; }

        /// <inheritdoc />
        public void Parse()
        {
            this.parsingDictionary[this.Service]();
        }

        /// <summary> Parse bundle file. </summary>
        private void ParseBundleFilePath()
        {
            // Clean up.
            Regex regex = new Regex(@"^[\/]+");

            this.path = regex.Replace(this.path, string.Empty);

            // Parsing.
            int index = this.path.IndexOf("_", StringComparison.Ordinal);

            if (index > -1)
            {
                this.Id = this.path.Substring(0, index);
                this.FileName = this.path.Substring(index + 1);
            }
        }

        /// <summary> Parse data request file path. </summary>
        private void ParseDataRequestFilePath()
        {
            string[] splitFileName = this.path.Split("/");

            if (splitFileName.Length > 2)
            {
                this.Id = splitFileName[splitFileName.Length - 2];
                this.FileName = splitFileName[splitFileName.Length - 1];
            }
        }
    }
}