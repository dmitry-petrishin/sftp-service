﻿using Deloitte.Cortex.SftpService.Enums;

namespace Deloitte.Cortex.SftpService.FilePath
{
    /// <summary> File path parser interface. </summary>
    public interface IFilePathParser
    {
        /// <summary> Gets id. </summary>
        string Id { get; }

        /// <summary> Gets <see cref="ServiceEnum"/>. </summary>
        ServiceEnum Service { get; }

        /// <summary> Gets parsed file name. </summary>
        string FileName { get; }

        /// <summary> Gets a value indicating whether is <see cref="Id"/> has been parsed. </summary>
        bool IsIdParsed { get; }

        /// <summary> Parses the path received in constructor. </summary>
        void Parse();
    }
}