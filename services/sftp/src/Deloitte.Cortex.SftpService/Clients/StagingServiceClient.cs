﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

using Deloitte.Cortex.SftpService.Clients.Models.StagingServiceClient;
using Deloitte.Cortex.SftpService.Models.Configuration;
using Deloitte.Cortex.Shared.Authorization;
using Deloitte.Cortex.Shared.Clients;

using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.SftpService.Clients
{
    /// <inheritdoc cref="IStagingServiceClient" />
    public class StagingServiceClient : BaseClient, IStagingServiceClient
    {
        /// <summary> Instance of the <see cref="IAuthenticationService"/>. </summary>
        private readonly IAuthenticationService authenticationService;

        /// <summary> Initializes a new instance of the <see cref="StagingServiceClient"/> class. </summary>
        /// <param name="servicesConfig"> The services config. </param>
        /// <param name="authenticationService"> Instance of the <see cref="IAuthenticationService"/>. </param>
        /// <param name="logger"> Instance of the <see cref="IStagingServiceClient"/>. </param>
        public StagingServiceClient(
            Services servicesConfig, 
            IAuthenticationService authenticationService,
            ILogger<BaseClient> logger)
            : base(servicesConfig.Staging, logger)
        {
            this.authenticationService = authenticationService;
            this.Timeout = TimeSpan.FromMinutes(60);
        }

        /// <inheritdoc />
        public async Task<SftpOutputPayload> Authenticate(string clientId, SftpPassword password)
        {
            await this.UseServiceAuthTokenAsync(this.authenticationService);
            this.logger.LogInformation($"Authenticating in Staging by ClientId: \"{clientId}\"");
            return await this.PostAsync<SftpOutputPayload>($"/sftp/auth/{clientId}", password);
        }

        /// <inheritdoc />
        public async Task<SftpOutputPayload> GetPathByBundleId(string bundleId)
        {
            await this.UseServiceAuthTokenAsync(this.authenticationService);
            this.logger.LogInformation($"Getting of path from Staging by BundleId: \"{bundleId}\"");
            return await this.PostAsync<SftpOutputPayload>($"/sftp/pathByBundle/{bundleId}", null);
        }

        /// <inheritdoc />
        public async Task<SftpOutputPayload> GetPathByDataRequestId(string dataRequestId)
        {
            await this.UseServiceAuthTokenAsync(this.authenticationService);
            this.logger.LogInformation($"Getting of path from Staging by DataRequestId: \"{dataRequestId}\"");
            return await this.PostAsync<SftpOutputPayload>($"/sftp/pathByDataRequest/{dataRequestId}", null);
        }

        /// <inheritdoc />
        public async Task Upload(string targerFolderNodeId, string fileName, Stream stream)
        {
            await this.UseServiceAuthTokenAsync(this.authenticationService);
            this.logger.LogInformation($"Sending of file with Name: \"{fileName}\", Length: \"{stream.Length}\" to NodeId: \"{targerFolderNodeId}\" in staging service.");

            MultipartFormDataContent content = new MultipartFormDataContent();
            content.Add(new StreamContent(stream), "file", fileName);

            HttpResponseMessage response = await this.PostAsync(
                $"staging/storage/filenode?parentId={targerFolderNodeId}&fileType=RAW&overwrite=true&bypass=true",
                content);

            this.logger.LogInformation($"Uploading has been finished with Status code:\"{response.StatusCode}\"");
            response.EnsureSuccessStatusCode();
        }
    }
}
