﻿using System.IO;
using System.Threading.Tasks;

using Deloitte.Cortex.SftpService.Clients.Models.StagingServiceClient;

namespace Deloitte.Cortex.SftpService.Clients
{
    /// <summary> Staging service client. </summary>
    public interface IStagingServiceClient
    {
        /// <summary> Authenticates client by password. </summary>
        /// <param name="clientId"> Client id. </param>
        /// <param name="password"> The password. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<SftpOutputPayload> Authenticate(string clientId, SftpPassword password);

        /// <summary> Gets Path model by bundle id. </summary>
        /// <param name="bundleId"> Bundle id. </param>
        /// <returns> Path and node id. </returns>
        Task<SftpOutputPayload> GetPathByBundleId(string bundleId);

        /// <summary> Gets Path model by datarequest id. </summary>
        /// <param name="dataRequestId"> DataRequest id. </param>
        /// <returns> Path and node id. </returns>
        Task<SftpOutputPayload> GetPathByDataRequestId(string dataRequestId);

        /// <summary> Uploads file to ADLS through staging service. </summary>
        /// <param name="targerFolderNodeId"> Node Id to define which part to upload file in. </param>
        /// <param name="fileName"> Name of being created file. </param>
        /// <param name="stream"> Stream of file. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task Upload(string targerFolderNodeId, string fileName, Stream stream);
    }
}