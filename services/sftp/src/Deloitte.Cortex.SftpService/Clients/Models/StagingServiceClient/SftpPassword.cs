﻿// ReSharper disable StyleCop.SA1600

namespace Deloitte.Cortex.SftpService.Clients.Models.StagingServiceClient
{
    public class SftpPassword
    {
        public string PasswordHash { get; set; }
    }
}