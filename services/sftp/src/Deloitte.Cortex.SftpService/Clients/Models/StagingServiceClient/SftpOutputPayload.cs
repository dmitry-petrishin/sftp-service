﻿using Newtonsoft.Json;

// ReSharper disable StyleCop.SA1600

namespace Deloitte.Cortex.SftpService.Clients.Models.StagingServiceClient
{
    public class SftpOutputPayload
    {
        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("RawFilesFolderNodeId")]
        public string RawFilesFolderNodeId { get; set; }
    }
}
