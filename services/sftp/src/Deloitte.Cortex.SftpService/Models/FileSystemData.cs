﻿using System.Collections.Generic;
using System.IO;

using Rebex.IO.FileSystem;

namespace Deloitte.Cortex.SftpService.Models
{
    public class FileSystemData
    {
        public FileSystemData()
        {
            this.Attributes = new NodeAttributes(FileAttributes.Temporary);
            this.Children = new List<NodeBase>();
            this.Content = new MemoryStream();
            this.TimeInfo = new NodeTimeInfo();
        }

        public NodeAttributes Attributes { get; set; }

        public List<NodeBase> Children { get; set; }

        public MemoryStream Content { get; set; }

        public long Length => this.Content.Length;

        public NodeTimeInfo TimeInfo { get; set; }
    }
}