﻿namespace Deloitte.Cortex.SftpService.Models.Configuration
{
    public class SshKeysPath
    {
        public string Private { get; set; }

        public string Public { get; set; }
    }
}
