﻿using Deloitte.Cortex.Shared.Serialization.Configuration;
// ReSharper disable StyleCop.SA1600

namespace Deloitte.Cortex.SftpService.Models.Configuration
{
    /// <summary> appsettings.json model. </summary>
    public class AppSettings
    {
        public Authentication Authentication { get; set; }

        /// <summary> The licence key for using Rebex library. </summary>
        // ReSharper disable once StyleCop.SA1623
        public string LicenseKey { get; set; }

        public Services Services { get; set; }

        public short SftpPort { get; set; }

        public SshKeysPath SshKeysPath { get; set; }

        public string TenantName { get; set; }
    }
}
