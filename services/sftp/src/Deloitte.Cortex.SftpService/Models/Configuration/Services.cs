﻿namespace Deloitte.Cortex.SftpService.Models.Configuration
{
    /// <summary>
    /// Service references configuration model.
    /// </summary>
    public class Services
    {
        public string DataRequest { get; set; }

        public string Staging { get; set; }

        public string Security { get; set; }
    }
}
