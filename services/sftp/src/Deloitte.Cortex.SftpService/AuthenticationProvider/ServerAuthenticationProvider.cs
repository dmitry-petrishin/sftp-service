﻿using System;
using System.Net;

using Deloitte.Cortex.SftpService.Clients;
using Deloitte.Cortex.SftpService.FileSystem;
using Deloitte.Cortex.SftpService.Clients.Models.StagingServiceClient;
using Deloitte.Cortex.Shared.Clients;
using Deloitte.Cortex.Shared.Crypto;

using Microsoft.Extensions.Logging;

using Rebex.Net.Servers;

namespace Deloitte.Cortex.SftpService.AuthenticationProvider
{
    /// <inheritdoc />
    public class ServerAuthenticationProvider : IServerAuthenticationProvider
    {
        /// <summary> Instance of the <see cref="CustomFileSystemProvider"/>. </summary>
        private readonly CustomFileSystemProvider fileSystemProvider;

        /// <summary> Instance of the <see cref="ILogger"/>. </summary>
        private readonly ILogger<ServerAuthenticationProvider> logger;

        /// <summary> Instance of the <see cref="IStagingServiceClient"/>. </summary>
        private readonly IStagingServiceClient stagingService;

        /// <summary> Initializes a new instance of the <see cref="ServerAuthenticationProvider"/> class. </summary>
        /// <param name="fileSystemProvider"> <see cref="fileSystemProvider"/>. </param>
        /// <param name="stagingService"> <see cref="stagingService"/>. </param>
        /// <param name="logger"> <see cref="logger"/>. </param>
        public ServerAuthenticationProvider(
            CustomFileSystemProvider fileSystemProvider,
            IStagingServiceClient stagingService,
            ILogger<ServerAuthenticationProvider> logger)
        {
            this.fileSystemProvider = fileSystemProvider;
            this.stagingService = stagingService;
            this.logger = logger;
        }

        /// <inheritdoc />
        public bool Authenticate(string userName, string password)
        {
            this.logger.LogInformation(
                $"Authentication process for userName: \"{userName}\" is about to start.");

            string hash = SHA512Helper.ComputeStringHash(password);

            if (string.IsNullOrEmpty(hash))
            {
                this.logger.LogInformation("Hash hasn't been calculated successfully.");
                return false;
            }

            this.logger.LogInformation("Hash has been calculated successfully.");
            var sftpPassword = new SftpPassword { PasswordHash = hash };

            try
            {
                this.stagingService.Authenticate(userName, sftpPassword).GetAwaiter().GetResult();
                return true;
            }
            catch (InvalidResponseStatusCodeException ex) when (ex.ResponseCode == HttpStatusCode.Forbidden)
            {
                this.logger.LogInformation($"User: \"{userName}\" failed to authenticate in staging service.");
                return false;
            }
            catch (Exception ex)
            {
                this.logger.LogError(
                    ex,
                    "Unhandled exception is happened in process of authentication in staging service.");
                return false;
            }
        }

        /// <inheritdoc />
        public FileServerUser PrepareUser(string userName)
        {
            this.logger.LogInformation($"Preparation of User: \"{userName}\" for authentication is about to start.");

            var user = new FileServerUser(userName, userName);

            user.SetFileSystem(this.fileSystemProvider);

            this.logger.LogInformation($"User: \"{userName}\" with file system has been created successfully.");

            return user;
        }
    }
}