﻿using Rebex.Net.Servers;

namespace Deloitte.Cortex.SftpService.AuthenticationProvider
{
    /// <summary> Provides custom authentication using staging service client. </summary>
    public interface IServerAuthenticationProvider
    {
        /// <summary> Authenticates user by password through staging service. </summary>
        /// <param name="userName"> User name aka Client id. </param>
        /// <param name="password"> The password. </param>
        /// <returns> Result of operation. </returns>
        bool Authenticate(string userName, string password);

        /// <summary> Prepare user for further authentication process. </summary>
        /// <param name="userName"> User name aka Client id. </param>
        /// <returns> New <see cref="FileServerUser"/> instance. </returns>
        FileServerUser PrepareUser(string userName);
    }
}