﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Deloitte.Cortex.SftpService.Clients;
using Deloitte.Cortex.SftpService.Enums;
using Deloitte.Cortex.SftpService.FilePath;
using Deloitte.Cortex.SftpService.Models;
using Deloitte.Cortex.SftpService.Clients.Models.StagingServiceClient;
using Deloitte.Cortex.Shared.Clients;

using Microsoft.Extensions.Logging;

using Rebex.IO.FileSystem;

namespace Deloitte.Cortex.SftpService.FileSystem
{
    /// <summary> Custom file system provider. </summary>
    public class CustomFileSystemProvider : ReadWriteFileSystemProvider
    {
        /// <summary> Instance of the <see cref="ILogger"/>. </summary>
        private readonly ILogger<CustomFileSystemProvider> logger;

        /// <summary> <see cref="NodeBase"/> repository accessible by <see cref="NodePath"/>. </summary>
        private readonly Dictionary<NodePath, NodeBase> pathNodeStorage;

        /// <summary> Instance of the <see cref="IStagingServiceClient"/> child. </summary>
        private readonly IStagingServiceClient stagingService;

        /// <summary> Main data repository accessible by <see cref="NodeBase"/>. </summary>
        private readonly Dictionary<NodeBase, FileSystemData> storage;

        /// <summary>
        /// Dictionary which is stored functions of getting the path by id. Being used instead of switch statement.
        /// </summary>
        private readonly IDictionary<ServiceEnum, Func<string, Task<SftpOutputPayload>>> getPathByIdDictionary;

        /// <summary> Initializes a new instance of the <see cref="CustomFileSystemProvider"/> class. </summary>
        /// <param name="stagingService"> Instance of the <see cref="IStagingServiceClient"/>. </param>
        /// <param name="logger"> Instance of the <see cref="ILogger"/>. </param>
        public CustomFileSystemProvider(IStagingServiceClient stagingService, ILogger<CustomFileSystemProvider> logger)
            : this(stagingService, logger, null)
        {
        }

        /// <summary> Initializes a new instance of the <see cref="CustomFileSystemProvider"/> class. </summary>
        /// <param name="stagingService"> Instance of the <see cref="IStagingServiceClient"/>. </param>
        /// <param name="logger"> Instance of the <see cref="ILogger"/>. </param>
        /// <param name="settings"> Instance of the <see cref="FileSystemProviderSettings"/>. </param>
        public CustomFileSystemProvider(
            IStagingServiceClient stagingService,
            ILogger<CustomFileSystemProvider> logger,
            FileSystemProviderSettings settings)
            : base(settings)
        {
            this.stagingService = stagingService;
            this.logger = logger;

            this.pathNodeStorage = new Dictionary<NodePath, NodeBase>();
            this.storage = new Dictionary<NodeBase, FileSystemData>();

            this.getPathByIdDictionary = 
                new Dictionary<ServiceEnum, Func<string, Task<SftpOutputPayload>>>
                {
                    { ServiceEnum.Bundle, this.stagingService.GetPathByBundleId },
                    { ServiceEnum.DataRequest, this.stagingService.GetPathByDataRequestId },
                };

            this.InitializeRoot();
        }

        /// <inheritdoc />
        protected override DirectoryNode CreateDirectory(DirectoryNode parent, DirectoryNode child)
        {
            DirectoryNode node = this.CreateElement(parent, child) as DirectoryNode;
            this.logger.LogInformation($"Directory node with path: \"{node?.Path.StringPath}\" has been created.");
            return node;
        }

        /// <inheritdoc />
        protected override FileNode CreateFile(DirectoryNode parent, FileNode child)
        {
            FileNode node = this.CreateElement(parent, child) as FileNode;
            this.logger.LogInformation($"File node with path: \"{node?.Path.StringPath}\" has been created.");
            return node;
        }

        /// <inheritdoc />
        protected override NodeBase Delete(NodeBase node)
        {
            this.logger.LogInformation($"Delete operation for Node: \"{node.Path.StringPath}\" is about to start...");
            if (!node.Exists())
            {
                return node;
            }

            this.storage.Remove(node);
            this.pathNodeStorage.Remove(node.Path);
            this.logger.LogInformation($"Node: \"{node.Path.StringPath}\" has been deleted.");

            if (node.Parent != null)
            {
                this.storage[node.Parent].Children.Remove(node);
                this.logger.LogInformation($"Node: \"{node.Path.StringPath}\" has been deleted from parent: \"{node.Parent.Path.StringPath}\".");
            }

            return node;
        }

        /// <inheritdoc />
        protected override bool Exists(NodePath path, NodeType nodeType)
        {
            this.pathNodeStorage.TryGetValue(path, out var node);

            bool result = node != null && node.NodeType == nodeType;

            this.logger.LogInformation(
                result
                    ? $"Node: \"{path.StringPath}\" has been found."
                    : $"Node: \"{path.StringPath}\" doesn't exist.");

            return result;
        }

        /// <inheritdoc />
        protected override NodeBase GetChild(string name, DirectoryNode parent)
        {
            var child = this.storage[parent].Children.FirstOrDefault(x => x.Name == name);

            if (child == null)
            {
                if (Path.HasExtension(name))
                {
                    FileNode node = this.CreateFile(parent, new FileNode(name, parent));
                    this.logger.LogInformation($"File node: \"{node.Path.StringPath}\" has been created.");
                    return node;
                }
                else
                {
                    DirectoryNode node = this.CreateDirectory(parent, new DirectoryNode(name, parent));
                    this.logger.LogInformation($"Directory node: \"{node.Path.StringPath}\" has been created.");
                    return node;
                }
            }

            this.logger.LogInformation($"Child node: \"{name}\" has been found.");

            return child;
        }

        /// <inheritdoc />
        protected override IEnumerable<NodeBase> GetChildren(DirectoryNode parent, NodeType nodeType)
        {
            if (!parent.Exists())
            {
                return Enumerable.Empty<NodeBase>();
            }

            return this.storage[parent].Children.Where(x => x.NodeType == nodeType);
        }

        /// <inheritdoc />
        protected override NodeContent GetContent(NodeBase node, NodeContentParameters contentParameters)
        {
            if (node.Exists())
            {
                var tempStream = new MemoryStream();
                this.storage[node].Content.CopyTo(tempStream);

                tempStream.Position = 0;
                this.storage[node].Content.Position = 0;
                
                return contentParameters.AccessType == NodeContentAccess.Read
                           ? NodeContent.CreateReadOnlyContent(tempStream)
                           : NodeContent.CreateDelayedWriteContent(tempStream);
            }

            return NodeContent.CreateImmediateWriteContent(new MemoryStream());
        }

        /// <inheritdoc />
        protected override long GetLength(NodeBase node)
        {
            return node.Exists() ? this.storage[node].Length : 0L;
        }

        /// <inheritdoc />
        protected override NodeTimeInfo GetTimeInfo(NodeBase node)
        {
            return this.storage[node].TimeInfo;
        }

        /// <inheritdoc />
        /// <summary> Unsupported action. </summary>
        protected override NodeBase Rename(NodeBase node, string newName)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        protected override NodeBase SaveContent(NodeBase node, NodeContent content)
        {
            if (node.Exists())
            {
                Stream stream = content.GetStream();
                this.logger.LogInformation($"Save content from File: \"{node.Path.StringPath}\" is about to start.");

                IFilePathParser parser = new FilePathParser(node.Path.StringPath);

                parser.Parse();

                string nodeId = this.GetPath(parser).GetAwaiter().GetResult();

                this.UploadFileToStagingService(nodeId, parser.FileName, stream);

                this.Delete(node);
            }

            return node;
        }

        /// <inheritdoc />
        protected override NodeBase SetTimeInfo(NodeBase node, NodeTimeInfo newTimeInfo)
        {
            this.storage[node].TimeInfo = newTimeInfo;
            return node;
        }

        /// <summary> Create File or Directory in current storage. </summary>
        /// <param name="parent"> Parent to be destination of child element. </param>
        /// <param name="child"> Element to be created in parent. </param>
        /// <returns> The <see cref="NodeBase"/>. </returns>
        private NodeBase CreateElement(NodeBase parent, NodeBase child)
        {
            this.storage.Add(child, new FileSystemData());
            this.storage[parent].Children.Add(child);
            this.pathNodeStorage.Add(child.Path, child);
            
            return child;
        }

        /// <summary> Uploads file to staging service. </summary>
        /// <param name="targetNodeId"> Target Node id. </param>
        /// <param name="fileName"> File name being created. </param>
        /// <param name="stream"> Stream of file being created. </param>
        private void UploadFileToStagingService(string targetNodeId, string fileName, Stream stream)
        {
            try
            {
                this.stagingService.Upload(targetNodeId, fileName, stream).GetAwaiter().GetResult();
            }
            catch (InvalidResponseStatusCodeException ex) when (ex.ResponseCode == HttpStatusCode.Conflict)
            {
                this.logger.LogInformation($"File with Name:\"{fileName}\" already exists in ADLS.");
                throw;
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, "Exception thrown during the uploading process.");
                throw;
            }
        }

        /// <summary>
        /// Initialize Root in current storage and path-node storage.
        /// </summary>
        private void InitializeRoot()
        {
            if (!this.storage.Any())
            {
                this.storage.Add(this.Root, new FileSystemData());
                this.pathNodeStorage.Add(this.Root.Path, this.Root);

                this.logger.LogInformation("Root has been initialized successfully");
            }
        }

        /// <summary> Gets path by data request id or bundle id. </summary>
        /// <param name="parser"> Instance of <see cref="IFilePathParser"/>. </param>
        /// <returns> The <see cref="Task"/> of Node Id. </returns>
        /// <exception cref="FormatException"> throws when Id hasn't been parsed. </exception>
        private async Task<string> GetPath(IFilePathParser parser)
        {
            // TODO: maybe return string path only further.
            SftpOutputPayload outputPayload;

            if (parser.IsIdParsed)
            {
                outputPayload = await this.getPathByIdDictionary[parser.Service](parser.Id);
                this.logger.LogInformation(
                    $"Path for NodeID: \"{outputPayload.RawFilesFolderNodeId}\" and Path: \"{outputPayload.Path}\" has been parsed successfully ");
            }
            else
            {
                string message = $"Id of Service: \"{parser.Service.ToString()}\" haven't been parsed successfully.";
                Exception ex = new FormatException(message);
                this.logger.LogError(ex, message);
                throw ex;
            }

            return outputPayload.RawFilesFolderNodeId;
        }
    }
}