﻿namespace Deloitte.Cortex.SftpService.Server
{
    /// <summary> Sftp server host interface. </summary>
    public interface IServerHost
    {
        /// <summary> Runs sftp server. </summary>
        void Run();

        /// <summary> Stops sftp server. </summary>
        void Stop();
    }
}