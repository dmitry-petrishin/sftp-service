﻿using System;

using Deloitte.Cortex.SftpService.AuthenticationProvider;
using Deloitte.Cortex.SftpService.Clients;
using Deloitte.Cortex.SftpService.FileSystem;
using Deloitte.Cortex.SftpService.Models.Configuration;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Configuration;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Rebex;

namespace Deloitte.Cortex.SftpService.Server
{
    /// <summary> Startup class. Being used for sftp server initialization. </summary>
    public class StartupServer : IStartupServer
    {
        /// <summary> Instance of <see cref="StartupServer"/>. </summary>
        private static IStartupServer instance;

        /// <summary> Prevents a default instance of the <see cref="StartupServer"/> class from being created. </summary>
        private StartupServer()
        {
            var configWrapper = new ConfigurationWrapper<AppSettings>(); 

            this.AppSettings = configWrapper.Settings;

            Licensing.Key = this.AppSettings.LicenseKey;

            IServiceCollection collection = new ServiceCollection();

            this.ConfigureServices(collection);

            this.ServiceProvider = collection.BuildServiceProvider();
        }

        /// <inheritdoc />
        public IServerHost SftpServerHost => this.ServiceProvider.GetService<IServerHost>();

        // ReSharper disable once StyleCop.SA1600
        private IServiceProvider ServiceProvider { get; }

        /// <summary> Gets parsed appsettings.json </summary>
        private AppSettings AppSettings { get; }

        /// <summary> Gets instance of Startup class. </summary>
        /// <returns> The <see cref="StartupServer"/> instance. </returns>
        public static IStartupServer GetInstance()
        {
            if (instance == null)
            {
                instance = new StartupServer();
                return instance;
            }

            return instance;
        }

        /// <summary> Method is needed for configuring and registering of dependencies. </summary>
        /// <param name="collection"> The collection. </param>
        private void ConfigureServices(IServiceCollection collection)
        {
            // Register full config.
            collection.AddSingleton(this.AppSettings);

            // Register services endpoints.
            collection.AddSingleton(this.AppSettings.Services);

            // Register authentication settings.
            collection.AddAuth(this.AppSettings.Authentication);

            collection.AddScoped<CustomFileSystemProvider>();

            // Register service clients.
            collection.AddScoped<IStagingServiceClient, StagingServiceClient>();

            collection.AddSingleton(new LoggerFactory()
                .AddConsole()
                .AddDebug());

            collection.AddLogging();

            // Add sftp server
            collection.AddSingleton<IServerHost, ServerHost>();

            // Add custom auth provider
            collection.AddScoped<IServerAuthenticationProvider, ServerAuthenticationProvider>();
        }
    }
}