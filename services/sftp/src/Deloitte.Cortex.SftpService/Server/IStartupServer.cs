﻿namespace Deloitte.Cortex.SftpService.Server
{
    /// <summary> Startup server interface. </summary>
    public interface IStartupServer
    {
        /// <summary> Gets the sftp server host. </summary>
        IServerHost SftpServerHost { get; }
    }
}