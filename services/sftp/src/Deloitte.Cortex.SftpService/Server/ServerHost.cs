﻿using System;

using Deloitte.Cortex.SftpService.AuthenticationProvider;
using Deloitte.Cortex.SftpService.Models.Configuration;

using Microsoft.Extensions.Logging;

using Rebex;
using Rebex.Net;
using Rebex.Net.Servers;

using LogLevel = Rebex.LogLevel;

namespace Deloitte.Cortex.SftpService.Server
{
    /// <summary> Sftp server host. </summary>
    public class ServerHost : IServerHost
    {
        /// <summary> Parsed appsettings.json model. </summary>
        private readonly AppSettings appSettings;

        /// <summary> Instance of the <see cref="IServerAuthenticationProvider"/>. </summary>
        private readonly IServerAuthenticationProvider authenticationProvider;

        /// <summary> Instance of the <see cref="ILogger"/>. </summary>
        private readonly ILogger<ServerHost> logger;

        /// <summary> Instance of the <see cref="FileServer"/>. </summary>
        private FileServer server;

        /// <summary> Initializes a new instance of the <see cref="ServerHost"/> class. </summary>
        /// <param name="appSettings"> <see cref="appSettings"/>. </param>
        /// <param name="authenticationProvider"> <see cref="authenticationProvider"/>. </param>
        /// <param name="logger"> <see cref="logger"/>. </param>
        public ServerHost(
            AppSettings appSettings,
            IServerAuthenticationProvider authenticationProvider,
            ILogger<ServerHost> logger)
        {
            this.appSettings = appSettings;
            this.authenticationProvider = authenticationProvider;
            this.logger = logger;
        }

        /// <inheritdoc />
        public void Run()
        {
            this.server = new FileServer();
            this.InitializeConfiguration();
            this.logger.LogInformation("Server configuration has been initialized.");

            this.server.Start();
            this.logger.LogInformation("Server has been started.");
        }

        /// <inheritdoc />
        public void Stop()
        {
            if (this.server != null)
            {
                this.server.Stop();
                this.server = null;
            }
        }

        /// <summary> Server's authentication event handler. </summary>
        /// <param name="sender"> Sender object. </param>
        /// <param name="e"> <see cref="AuthenticationEventArgs"/>. </param>
        private void AuthenticationEventHandler(object sender, AuthenticationEventArgs e)
        {
            bool authResult = this.authenticationProvider.Authenticate(e.UserName, e.Password);
            if (authResult)
            {
                e.Accept(this.server.Users[e.UserName]);
                this.logger.LogInformation($"Authentication for User: \"{e.UserName}\" has been accepted.");
            }
            else
            {
                e.Reject();
                this.logger.LogInformation($"Authentication for User: \"{e.UserName}\" has been rejected.");
            }
        }

        /// <summary> Server's disconnected event hanlder. </summary>
        /// <param name="sender"> Sender object. </param>
        /// <param name="e"> <see cref="DisconnectedEventArgs"/>. </param>
        private void DisconnectedEventHandler(object sender, DisconnectedEventArgs e)
        {
            ServerUser serverUser = e.User;
            if (serverUser != null)
            {
                FileServerUser fileServerUser = this.server.Users[serverUser.Name];
                if (fileServerUser != null)
                {
                    this.server.Users.Remove(this.server.Users[fileServerUser.Name]);
                    this.logger.LogInformation($"User: \"{serverUser.Name}\" has been disconnected.");
                }
            }
        }

        /// <summary> Initializes configuration of the server. </summary>
        private void InitializeConfiguration()
        {
            string sshKeyPath = this.appSettings.SshKeysPath.Private;

            SshPrivateKey sshKey = new SshPrivateKey(sshKeyPath);

            short port = this.appSettings.SftpPort;

            this.server.Bind(port, FileServerProtocol.Sftp);

            this.server.Keys.Add(sshKey);

            this.server.PreAuthentication += this.PreAuthenticationEventHandler;
            this.server.Authentication += this.AuthenticationEventHandler;
            this.server.Disconnected += this.DisconnectedEventHandler;

            // use console log writer
            this.server.LogWriter = new ConsoleLogWriter(LogLevel.Error);
        }

        /// <summary> Server's pre-authentication event handler. </summary>
        /// <param name="sender"> Sender object. </param>
        /// <param name="e"> <see cref="PreAuthenticationEventArgs"/>. </param>
        private void PreAuthenticationEventHandler(object sender, PreAuthenticationEventArgs e)
        {
            if (!Guid.TryParse(e.UserName, out _))
            {
                e.Reject();
                return;
            }

            this.logger.LogInformation($@"
-------------------------------------------------------
PreAuthentication is about to start...
Client identificator: ""{e.ClientSoftwareIdentifier}""
Client end point: ""{e.ClientEndPoint}""
-------------------------------------------------------");

            FileServerUser user = this.authenticationProvider.PrepareUser(e.UserName);

            if (user == null)
            {
                this.logger.LogInformation($"Authentication for User: \"{e.UserName}\" has been rejected.");
                e.Reject();
                return;
            }

            FileServerUser serverUser = this.server.Users[user.Name];
            if (serverUser == null)
            {
                this.server.Users.Add(user);
            }

            e.Accept(AuthenticationMethods.Password);
            this.logger.LogInformation($"PreAuthentication for User: \"{e.UserName}\" has been accepted.");
        }
    }
} 