﻿namespace Deloitte.Cortex.SftpService.Enums
{
    /// <summary> Name of service which be defined from file name. </summary>
    public enum ServiceEnum
    {
        /// <summary> Bundle service. </summary>
        Bundle,

        /// <summary> DataRequest service. </summary>
        DataRequest
    }
}