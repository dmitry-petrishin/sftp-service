﻿using System;
using System.Collections.Generic;
using System.IO;

using Deloitte.Cortex.SftpService.Tests.Models.Configuration;
using Deloitte.Cortex.SftpService.Tests.Models.StagingServiceClient;

using NUnit.Framework;

using Renci.SshNet;
using Renci.SshNet.Common;

namespace Deloitte.Cortex.SftpService.Tests
{
    /// <summary> Class for testing of uploading of files. </summary>
    public class SftpServerTests : StartupTests
    {
        /// <summary> Authentication method instance for authenticating to sftp server. </summary>
        private readonly AuthenticationMethod authMethod;

        /// <summary> Connection info instance for connecting to sftp server. </summary>
        private readonly ConnectionInfo connectionInfo;

        /// <summary> Temporary directories (based on nodes in testsettings) which is being created before tests start. </summary>
        private readonly List<string> createdDirectories;

        /// <summary> Path to directory which is being used as endpoint to store files in fake adls client implementation. </summary>
        private readonly string tempDirectoryFullPath;

        /// <summary> Sftp client instance. </summary>
        private SftpClient sftpClient;

        /// <summary> Initializes a new instance of the <see cref="SftpServerTests"/> class. </summary>
        public SftpServerTests()
        {
            this.tempDirectoryFullPath = Path.Combine(AppContext.BaseDirectory, this.TestSettings.TempDirectory);

            this.createdDirectories = new List<string>();

            this.authMethod = new PasswordAuthenticationMethod(this.User1.ClientId, this.User1.Password);
            this.connectionInfo = new ConnectionInfo(
                this.Endpoint.Host,
                this.Endpoint.Port,
                this.User1.ClientId,
                this.authMethod);

            this.sftpClient = new SftpClient(this.connectionInfo);
        }

        /// <summary> Finalizes an instance of the <see cref="SftpServerTests"/> class.  </summary>
        ~SftpServerTests()
        {
            if (this.sftpClient != null)
            {
                if (this.sftpClient.IsConnected)
                {
                    this.sftpClient.Disconnect();
                }

                this.sftpClient.Dispose();
                this.sftpClient = null;
            }
        }

        /// <summary> Sftp server endpoint for sftp client in test. </summary>
        private Endpoint Endpoint => this.TestSettings.SftpServer.Endpoint;

        /// <summary> Template file location. </summary>
        private FileBasement FileBasement => this.TestSettings.Files[0];

        /// <summary> File path. </summary>
        private string FilePath => Path.Combine(this.FileBasement.Folder, this.FileBasement.Name);

        /// <summary> User for authentication. </summary>
        private Client User1 => this.TestSettings.SftpServer.Clients[0];

        /// <summary> User for authentication. </summary>
        private Client User2 => this.TestSettings.SftpServer.Clients[1];

        /// <summary> Deletes test directories recursively. </summary>
        [TearDown]
        public void DeleteTestDirectories()
        {
            this.CheckAndDeleteTempDirectory();
        }

        /// <summary> Prepares(creates) test directories for mocking of uploading the files. </summary>
        [SetUp]
        public void PrepareTestDirectories()
        {
            this.CheckAndDeleteTempDirectory();

            // create root directory.
            DirectoryInfo tempRoot = Directory.CreateDirectory(this.tempDirectoryFullPath);

            // create node directories and save paths to list.
            foreach (Node node in this.TestSettings.SftpServer.Nodes)
            {
                string path = Path.Combine(tempRoot.FullName, node.Name);
                this.createdDirectories.Add(path);
                Directory.CreateDirectory(path);
            }
        }

        /// <summary> Uploading of files tests. </summary>
        /// <param name="rawFileName"> File path being received from informatica. </param>
        /// <param name="expectedCreatedFileName"> Created (parsed) file name. </param>
        [Test]
        [TestCase("/FD71B0BB-405D-4199-9461-A764EBA13D1F_F0116_ASAP.csv", "F0116_ASAP.csv")]
        [TestCase("/D07BB7C9-7537-43D2-8A31-8F0C460ED747_F0117_ASAP.csv", "F0117_ASAP.csv")]
        [TestCase("/C361BA8F-6BBC-4AE2-AB5F-4EA8C0EC11B8_F0118_ASAP.csv", "F0118_ASAP.csv")]
        [TestCase("1_datarequest/910588A7-4A1F-4356-8BBB-8DBE409FBEC8/DataRequest1_asap.csv", "DataRequest1_asap.csv")]
        [TestCase("1_datarequest/4D14B338-671E-4CBB-A016-FA472F474881/DataRequest2_asap.csv", "DataRequest2_asap.csv")]
        [TestCase("1_datarequest/73BB8CEC-F883-4591-A9B4-801543E6046B/DataRequest3_asap.csv", "DataRequest3_asap.csv")]
        public void SimpleUploadFileTest(string rawFileName, string expectedCreatedFileName)
        {
            this.UploadAndAssertFile(rawFileName, expectedCreatedFileName);
        }

        /// <summary> Upload files to the same node (the same NodeId) tests. </summary>
        /// <param name="paths"> Target paths. </param>
        /// <param name="expectedNames"> The expected names of created files. </param>
        [Test]
        [TestCase(
            new[]
                {
                    "1_datarequest/910588A7-4A1F-4356-8BBB-8DBE409FBEC8/DataRequest1_asap.csv",
                    "1_datarequest/4D14B338-671E-4CBB-A016-FA472F474881/DataRequest2_asap.csv"
                },
            new[] { "DataRequest1_asap.csv", "DataRequest2_asap.csv" })]
        [TestCase(
            new[]
                {
                    "/FD71B0BB-405D-4199-9461-A764EBA13D1F_F0116_ASAP.csv",
                    "/D07BB7C9-7537-43D2-8A31-8F0C460ED747_F0117_ASAP.csv"
                },
            new[] { "F0116_ASAP.csv", "F0117_ASAP.csv" })]
        public void UploadFilesToTheSameNodeTest(string[] paths, string[] expectedNames)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                string path = paths[i];
                string expectedName = expectedNames[i];

                Assert.That(() => this.UploadAndAssertFile(path, expectedName), Throws.Nothing);
            }
        }

        /// <summary> Upload the same files tests. </summary>
        /// <param name="path"> The path. </param>
        /// <param name="expectedName"> The expected name. </param>
        [Test]
        [TestCase("1_datarequest/73BB8CEC-F883-4591-A9B4-801543E6046B/DataRequest4_asap.csv", "DataRequest4_asap.csv")]
        [TestCase("1_datarequest/4D14B338-671E-4CBB-A016-FA472F474881/DataRequest1_asap.csv", "DataRequest1_asap.csv")]
        [TestCase("/FD71B0BB-405D-4199-9461-A764EBA13D1F_F0116_ASAP.csv", "F0116_ASAP.csv")]
        [TestCase("/C361BA8F-6BBC-4AE2-AB5F-4EA8C0EC11B8_F0118_ASAP.csv", "F0118_ASAP.csv")]
        public void UploadTheSameFilesTest(string path, string expectedName)
        {
            Assert.That(() => this.UploadAndAssertFile(path, expectedName), Throws.Nothing);

            Assert.That(() => this.UploadAndAssertFile(path, expectedName), Throws.Nothing);
        }

        /// <summary> Test on unsuccessful authentication behaviour. </summary>
        [Test]
        public void UnsuccessfulAuthenticationTest()
        {
            var authMethod = new PasswordAuthenticationMethod(this.User2.ClientId, "incorrect pass");
            var connectionInfo = new ConnectionInfo(
                this.Endpoint.Host,
                this.Endpoint.Port,
                this.User2.ClientId,
                authMethod);

            SftpClient client = new SftpClient(connectionInfo);

            Assert.That(() => client.Connect(), Throws.InstanceOf<SshAuthenticationException>());

            client.Disconnect();
            client.Dispose();
        }

        /// <summary> Test on successful authentication behaviour. </summary>
        [Test]
        public void SuccessfulAuthenticationTest()
        {
            var authMethod = new PasswordAuthenticationMethod(this.User2.ClientId, this.User2.Password);
            var connectionInfo = new ConnectionInfo(
                this.Endpoint.Host,
                this.Endpoint.Port,
                this.User2.ClientId,
                authMethod);

            SftpClient client = new SftpClient(connectionInfo);

            Assert.That(() => client.Connect(), Throws.Nothing);

            client.Disconnect();
            client.Dispose();
        }

        /// <summary> Asserts file to ability of being opened. </summary>
        /// <param name="fullPath"> Full path to the file. </param>
        private void AssertFileCanBeOpened(string fullPath)
        {
            using (FileStream fileStream = File.OpenRead(fullPath))
            {
                Assert.That(fileStream.Length, Is.Not.EqualTo(0));
                Assert.That(fileStream.CanRead, Is.True);
                Assert.That(fileStream.CanSeek, Is.True);
            }
        }

        /// <summary> Checks and deletes temporary directory. </summary>
        private void CheckAndDeleteTempDirectory()
        {
            if (Directory.Exists(this.tempDirectoryFullPath))
            {
                Directory.Delete(this.tempDirectoryFullPath, recursive: true);
            }
        }

        /// <summary> Finds created file in node folders. </summary>
        /// <param name="createdFileName"> Created file name. </param>
        /// <returns> Found file path. </returns>
        private string FindCreatedFile(string createdFileName)
        {
            foreach (string createdDirectory in this.createdDirectories)
            {
                string path = Path.Combine(createdDirectory, createdFileName);
                if (File.Exists(path))
                {
                    return path;
                }
            }

            return null;
        }

        /// <summary> Uploads file through SFTP and asserts the results. </summary>
        /// <param name="path"> File path. </param>
        /// <param name="expectedCreatedFileName"> Expected created file name. </param>
        private void UploadAndAssertFile(string path, string expectedCreatedFileName)
        {
            this.UploadFile(path);

            string createdFilePath = this.FindCreatedFile(expectedCreatedFileName);

            Assert.That(createdFilePath, Is.Not.Null);

            this.AssertFileCanBeOpened(createdFilePath);
        }

        /// <summary> Uploads file to the SFTP. </summary>
        /// <param name="path"> File path. </param>
        private void UploadFile(string path)
        {
            using (FileStream stream = File.Open(this.FilePath, FileMode.Open, FileAccess.Read))
            {
                this.sftpClient.Connect();

                this.sftpClient.UploadFile(stream, path, true);

                this.sftpClient.Disconnect();
            }
        }
    }
}