﻿using System;

using Deloitte.Cortex.SftpService.AuthenticationProvider;
using Deloitte.Cortex.SftpService.Clients;
using Deloitte.Cortex.SftpService.FileSystem;
using Deloitte.Cortex.SftpService.Models.Configuration;
using Deloitte.Cortex.SftpService.Server;
using Deloitte.Cortex.SftpService.Tests.FakeClients;
using Deloitte.Cortex.SftpService.Tests.Models.Configuration;
using Deloitte.Cortex.Shared.Configuration;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using NUnit.Framework;

using Rebex;

namespace Deloitte.Cortex.SftpService.Tests
{
    [TestFixture]
    public class StartupTests
    {
        /// <summary> Initializes a new instance of the <see cref="StartupTests"/> class. </summary>
        public StartupTests()
        {
            var appConfigWrapper = new ConfigurationWrapper<AppSettings>();
            var testConfigWrapper = new ConfigurationWrapper<TestSettings>();
            this.TestSettings = testConfigWrapper.Settings;
            this.AppSettings = appConfigWrapper.Settings;

            Licensing.Key = this.AppSettings.LicenseKey;

            IServiceCollection collection = new ServiceCollection();

            this.ConfigureServices(collection);

            this.ServiceProvider = collection.BuildServiceProvider();

            this.RunSftpServerHost();
        }

        /// <summary> Gets parsed appsettings.json </summary>
        protected AppSettings AppSettings { get; }

        /// <summary> Gets parsed testsettings.json </summary>
        protected TestSettings TestSettings { get; }

        // ReSharper disable once StyleCop.SA1600
        private IServiceProvider ServiceProvider { get; }

        /// <summary> The sftp server host. </summary>
        private IServerHost SftpServerHost => this.ServiceProvider.GetService<IServerHost>();

        /// <summary> Method is needed for configuring and registering of dependencies. </summary>
        /// <param name="collection"> The collection. </param>
        private void ConfigureServices(IServiceCollection collection)
        {
            collection.AddSingleton(new LoggerFactory()
                .AddConsole()
                .AddDebug());

            collection.AddLogging();

            // Register appsettings.
            collection.AddSingleton(this.AppSettings);

            // Register testsettings.
            collection.AddSingleton(this.TestSettings);

            // Register services endpoints.
            collection.AddSingleton(this.AppSettings.Services);

            collection.AddScoped<IStagingServiceClient, StagingServiceClientFake>();

            collection.AddScoped<CustomFileSystemProvider>();

            collection.AddScoped<IServerAuthenticationProvider, ServerAuthenticationProvider>();

            collection.AddSingleton<IServerHost, ServerHost>();
        }

        /// <summary> Run SFTP server. </summary>
        private void RunSftpServerHost()
        {
            this.SftpServerHost.Run();
        }
    }
}