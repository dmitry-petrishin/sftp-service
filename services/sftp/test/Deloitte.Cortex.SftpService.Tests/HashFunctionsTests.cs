﻿using System;
using System.Security.Cryptography;
using System.Text;

using Deloitte.Cortex.Shared.Crypto;

using NUnit.Framework;

namespace Deloitte.Cortex.SftpService.Tests
{
    /// <summary> Hash functions tests. </summary>
    [TestFixture]
    public class HashFunctionsTests
    {
        /// <summary> Calc hash function from staging service. </summary>
        /// <param name="unencrypted"> String to hash. </param>
        /// <returns> Hashed string. </returns>
        public string CalcHash(string unencrypted)
        {
            using (var sha512 = new SHA512Managed())
            {
                byte[] bytes = Encoding.UTF8.GetBytes(unencrypted);
                byte[] hashedBytes = sha512.ComputeHash(bytes);
                string hash = BitConverter.ToString(hashedBytes).Replace("-", string.Empty).ToLower();
                return hash;
            }
        }

        /// <summary> Compare of hash convertion results test. </summary>
        /// <param name="password"> The password. </param>
        [Test]
        [TestCase("1235678352")]
        [TestCase("adfsgsgd")]
        [TestCase("aADGDdlrldmD")]
        [TestCase("aADGD1d2l3r4ldmD")]
        [TestCase("fF2-!fd1_fdsD1dF2")]
        public void CompareHashResultsTest(string password)
        {
            var hash1 = this.CalcHash(password);

            var hash2 = this.ComputeHashToString(password);

            Assert.That(hash1, Is.EqualTo(hash2));
        }

        [Test]
        [TestCase("1235678352")]
        [TestCase("adfsgsgd")]
        [TestCase("aADGDdlrldmD")]
        [TestCase("aADGD1d2l3r4ldmD")]
        [TestCase("fF2-!fd1_fdsD1dF2")]
        public void SecretMaskerTest(string password)
        {
            var hash = this.ComputeHashToString(password);
            string mask = SecretMaskHelper.MaskHash512(hash);

            Assert.That(mask.Length, Is.EqualTo(128));
        }

        /// <summary> Hash function of mine :). </summary>
        /// <param name="input"> String to hash. </param>
        /// <returns> Hashed string. </returns>
        public string ComputeHashToString(string input)
        {
            var bytes = Encoding.UTF8.GetBytes(input);
            using (var hash = SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                var hashedInputStringBuilder = new StringBuilder(128);
                foreach (var b in hashedInputBytes)
                {
                    hashedInputStringBuilder.Append(b.ToString("X2").ToLowerInvariant());
                }

                return hashedInputStringBuilder.ToString();
            }
        }
    }
}