﻿using Deloitte.Cortex.SftpService.Enums;
using Deloitte.Cortex.SftpService.FilePath;

using NUnit.Framework;

namespace Deloitte.Cortex.SftpService.Tests
{
    /// <summary> File path parser tests. </summary>
    [TestFixture]
    public class FilePathParserTests
    {
        /// <summary> Test of bundle file paths parsing. </summary>
        /// <param name="filePath"> File path. </param>
        /// <param name="expectedBundleId"> Expected bundle id. </param>
        /// <param name="expectedFileName"> Expected file name. </param>
        [Test]
        [TestCase(
            "/f59ff201-86e4-4810-b576-858bb9ed5005_F0116_ASAP.csv",
            "f59ff201-86e4-4810-b576-858bb9ed5005",
            "F0116_ASAP.csv")]
        [TestCase(
            "//F4E9A0AB-AA6C-4E48-8812-7F961A78A62C_TestName_ASAP_1.csv",
            "F4E9A0AB-AA6C-4E48-8812-7F961A78A62C",
            "TestName_ASAP_1.csv")]
        [TestCase(
            "C1554B48-6208-4659-8CC6-D620AC75DDD9_ASAP NoAsap.csv",
            "C1554B48-6208-4659-8CC6-D620AC75DDD9",
            "ASAP NoAsap.csv")]
        [TestCase(
            "/8D00BEB0-7575-4A60-B681-DEAF65017CF3_111 111 22 33 4 5.csv",
            "8D00BEB0-7575-4A60-B681-DEAF65017CF3",
            "111 111 22 33 4 5.csv")]
        [TestCase("/FEF2BDA5-0365---B960A442AEA8_$%   .,.csv", "FEF2BDA5-0365---B960A442AEA8", "$%   .,.csv")]
        public void TestBundleFilePathsParsing(string filePath, string expectedBundleId, string expectedFileName)
        {
            FilePathParser parser = new FilePathParser(filePath);

            parser.Parse();

            Assert.AreEqual(true, parser.IsIdParsed);
            Assert.AreEqual(ServiceEnum.Bundle, parser.Service);
            Assert.AreEqual(expectedBundleId, parser.Id);
            Assert.AreEqual(expectedFileName, parser.FileName);
        }

        /// <summary> Test of data request paths parsing. </summary>
        /// <param name="filePath"> File path. </param>
        /// <param name="expectedDataRequestId"> Expected data request id. </param>
        /// <param name="expectedFileName"> Expected file name. </param>
        [Test]
        [TestCase(
            "1_datarequest/D46272CF-DFBB-4AA0-8B45-B71340862049/Name1_asap.csv",
            "D46272CF-DFBB-4AA0-8B45-B71340862049",
            "Name1_asap.csv")]
        [TestCase(
            "one/_one_datarequest/323C4E31-3266-4BD9-86BC-3A003F23252C/1131.csv",
            "323C4E31-3266-4BD9-86BC-3A003F23252C",
            "1131.csv")]
        [TestCase(
            "one_datarequest/B06A4803-4670-4496-BD02-6182D46EB0F2/dfs_Name1_asap.csv",
            "B06A4803-4670-4496-BD02-6182D46EB0F2",
            "dfs_Name1_asap.csv")]
        [TestCase(
            "o  ne_DataRequest/365BAC3A---76A541441B4C/dfs Name asap.csv",
            "365BAC3A---76A541441B4C",
            "dfs Name asap.csv")]
        public void TestDataRequestPathsParsing(string filePath, string expectedDataRequestId, string expectedFileName)
        {
            FilePathParser parser = new FilePathParser(filePath);

            parser.Parse();

            Assert.AreEqual(true, parser.IsIdParsed);
            Assert.AreEqual(ServiceEnum.DataRequest, parser.Service);
            Assert.AreEqual(expectedDataRequestId, parser.Id);
            Assert.AreEqual(expectedFileName, parser.FileName);
        }

        /// <summary> Tests for unhappened bundle names parsing.  </summary>
        /// <param name="filePath"> File path.  </param>
        /// <param name="isIdParsedExpected"> Expected result of parsing operation. </param>
        /// <param name="expectedBundleId"> Expected bundle id. </param>
        /// <param name="expectedFileName"> Expected file name. </param>
        [Test]
        [TestCase("guid name.csv", false, null, null)]
        [TestCase("_guid name.csv", true, "", "guid name.csv")]
        [TestCase("guid_", true, "guid", "")]
        public void TestUnhappenedBundlePathsParsing(string filePath, bool isIdParsedExpected, string expectedBundleId, string expectedFileName)
        {
            IFilePathParser parser = new FilePathParser(filePath);

            parser.Parse();

            Assert.AreEqual(isIdParsedExpected, parser.IsIdParsed);
            Assert.AreEqual(expectedBundleId, parser.Id);
            Assert.AreEqual(expectedFileName, parser.FileName);
        }

        [Test]
        [TestCase("/guid/name.csv", false, null, null)]
        [TestCase("one/guid/name.csv", false, null, null)]
        [TestCase("1_datarequest/name.csv", false, null, null)]
        public void TestUnhappenedDataRequestPathsParsing(string filePath, bool isIdParsedExpected, string expectedDataRequestId, string expectedFileName)
        {
            IFilePathParser parser = new FilePathParser(filePath);

            parser.Parse();

            Assert.AreEqual(isIdParsedExpected, parser.IsIdParsed);
            Assert.AreEqual(expectedDataRequestId, parser.Id);
            Assert.AreEqual(expectedFileName, parser.FileName);
        }
    }
}