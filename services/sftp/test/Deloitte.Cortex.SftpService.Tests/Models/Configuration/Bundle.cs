﻿// ReSharper disable StyleCop.SA1600
namespace Deloitte.Cortex.SftpService.Tests.Models.Configuration
{
    public class Bundle
    {
        public string Id { get; set; }

        public string NodeId { get; set; }
    }
}