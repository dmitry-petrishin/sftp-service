﻿// ReSharper disable StyleCop.SA1600
namespace Deloitte.Cortex.SftpService.Tests.Models.Configuration
{
    /// <summary> Endpoint configuration model. </summary>
    public class Endpoint
    {
        public string Host { get; set; }

        public short Port { get; set; }
    }
}