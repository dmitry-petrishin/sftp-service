﻿// ReSharper disable StyleCop.SA1600

namespace Deloitte.Cortex.SftpService.Tests.Models.Configuration
{
    public class Node
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}