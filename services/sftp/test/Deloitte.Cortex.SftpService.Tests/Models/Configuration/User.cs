﻿// ReSharper disable StyleCop.SA1600
namespace Deloitte.Cortex.SftpService.Tests.Models.Configuration
{
    /// <summary> User configuration model. </summary>
    public class User
    {
        public string Name { get; set; }

        public string Password { get; set; }
    }
}