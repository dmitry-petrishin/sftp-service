﻿namespace Deloitte.Cortex.SftpService.Tests.Models.Configuration
{
    public class FileBasement
    {
        public string Folder { get; set; }

        public string Name { get; set; }
    }
}
