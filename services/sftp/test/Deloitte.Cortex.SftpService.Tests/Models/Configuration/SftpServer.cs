﻿// ReSharper disable StyleCop.SA1600

using System.Collections.Generic;

using Deloitte.Cortex.SftpService.Tests.Models.StagingServiceClient;

namespace Deloitte.Cortex.SftpService.Tests.Models.Configuration
{
    /// <summary> The sftp server configuration model. </summary>
    public class SftpServer
    {
        public List<Bundle> Bundles { get; set; }

        public List<Client> Clients { get; set; }

        public List<DataRequest> DataRequests { get; set; }

        public Endpoint Endpoint { get; set; }

        public List<Node> Nodes { get; set; }
    }
}