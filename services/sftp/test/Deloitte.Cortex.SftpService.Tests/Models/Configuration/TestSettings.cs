﻿// ReSharper disable StyleCop.SA1600
namespace Deloitte.Cortex.SftpService.Tests.Models.Configuration
{
    public class TestSettings
    {
        public FileBasement[] Files { get; set; }

        public SftpServer SftpServer { get; set; }

        public string TempDirectory { get; set; }
    }
}
