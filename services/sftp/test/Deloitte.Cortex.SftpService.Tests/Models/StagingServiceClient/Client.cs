﻿

// ReSharper disable StyleCop.SA1600
namespace Deloitte.Cortex.SftpService.Tests.Models.StagingServiceClient
{
    public class Client
    {
        public string ClientId { get; set; }
        public string Password { get; set; }
    }
}