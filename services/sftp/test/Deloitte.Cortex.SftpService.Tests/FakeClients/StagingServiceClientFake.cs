﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Deloitte.Cortex.SftpService.Clients;
using Deloitte.Cortex.SftpService.Clients.Models.StagingServiceClient;
using Deloitte.Cortex.SftpService.Tests.Models.Configuration;
using Deloitte.Cortex.SftpService.Tests.Models.StagingServiceClient;
using Deloitte.Cortex.Shared.Clients;
using Deloitte.Cortex.Shared.Crypto;

namespace Deloitte.Cortex.SftpService.Tests.FakeClients
{
    /// <summary> Fake staging service client implementation for testing. </summary>
    public class StagingServiceClientFake : IStagingServiceClient
    {
        private readonly List<Bundle> bundles;

        private readonly List<Client> clients;

        private readonly List<DataRequest> dataRequests;

        private readonly List<Node> nodes;

        private readonly string tempDirectoryName;

        /// <summary> Initializes a new instance of the <see cref="StagingServiceClientFake"/> class. </summary>
        public StagingServiceClientFake(TestSettings settings)
        {
            this.bundles = settings.SftpServer.Bundles;
            this.clients = settings.SftpServer.Clients;
            this.dataRequests = settings.SftpServer.DataRequests;
            this.nodes = settings.SftpServer.Nodes;
            this.tempDirectoryName = settings.TempDirectory;
        }

        /// <inheritdoc />
        public Task<SftpOutputPayload> Authenticate(string clientId, SftpPassword password)
        {
            
            Client client = this.clients.Find(
                x => x.ClientId == clientId 
                     && password.PasswordHash == SHA512Helper.ComputeStringHash(x.Password));
            if (client != null)
            {
                return Task.FromResult(new SftpOutputPayload { Path = "/path" });
            }

            throw new InvalidResponseStatusCodeException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
        }

        /// <inheritdoc />
        public Task<SftpOutputPayload> GetPathByBundleId(string bundleId)
        {
            IEnumerable<SftpOutputPayload> result = 
                from bundle in this.bundles
                join node in this.nodes on bundle.NodeId equals node.Id
                where bundle.Id == bundleId
                select new SftpOutputPayload
                            {
                                Path = Path.Combine(this.tempDirectoryName, node.Name),
                                RawFilesFolderNodeId = node.Id
                            };

            return Task.FromResult(result.FirstOrDefault());
        }

        /// <inheritdoc />
        public Task<SftpOutputPayload> GetPathByDataRequestId(string dataRequestId)
        {
            IEnumerable<SftpOutputPayload> result = 
                from dataRequest in this.dataRequests
                join node in this.nodes on dataRequest.NodeId equals node.Id
                where dataRequest.Id == dataRequestId
                select new SftpOutputPayload
                           {
                               Path = Path.Combine(this.tempDirectoryName, node.Name),
                               RawFilesFolderNodeId = node.Id
                           };

            return Task.FromResult(result.FirstOrDefault());
        }

        private string GetPathByNodeId(string nodeId)
        {
            return this.nodes.FirstOrDefault(x => x.Id == nodeId)?.Name;
        }

        /// <inheritdoc />
        public Task Upload(string targerFolderNodeId, string fileName, Stream stream)
        {
            var directory = AppContext.BaseDirectory;

            FileStream fileStream = 
                File.Create(
                    Path.Combine(directory, this.tempDirectoryName, this.GetPathByNodeId(targerFolderNodeId), fileName));

            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);

            fileStream.Close();
            return Task.CompletedTask;
        }
    }
}