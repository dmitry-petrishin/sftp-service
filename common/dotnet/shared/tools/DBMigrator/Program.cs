﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DBMigrator.Config;
using DBMigrator.Converters;
using DBMigrator.DBs;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.DataAccess;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace DBMigrator
{
    public static class Program
    {
        private static IDictionary<string, DBEntry> ReadConfig()
        {
            var json = JObject.Parse(File.ReadAllText("appsettings.json"));
            //json.Merge(JObject.Parse(File.ReadAllText("appsettings.Local.json")));
            var data = json.ToObject<Dictionary<string, JObject>>();

            return data.ToDictionary(p => p.Key, p => new DBEntry(p));
        }

        private static readonly ILoggerFactory LoggerFactory = new LoggerFactory();

        private static IDictionary<string, Func<JObject, IDB>> _supportedDBs =
            new Dictionary<string, Func<JObject, IDB>>(StringComparer.OrdinalIgnoreCase)
            {
                { "DocumentDB", config => new DocumentDB(config.ToObject<DocumentDBSettings>(), LoggerFactory) },
                { "MongoDB", config => new DBs.MongoDB(config.ToObject<MongoDBSettings>(), LoggerFactory) }
            };

        private static IDB GetDB(DBEntry entry)
        {
            Func<JObject, IDB> dbFactory;
            if (_supportedDBs.TryGetValue(entry.Type, out dbFactory))
            {
                return dbFactory(entry.Config);
            }
            throw new Exception($"Database \"{entry.Type}\" is not supported.");
        }

        private static IDictionary<string, IConverter> _supportedConverters = new Dictionary<string, IConverter>(StringComparer.OrdinalIgnoreCase)
        {
            { "DocumentDB -> MongoDB", new DocumentDBToMongoDBConverter() }
        };

        private static IConverter GetConverter(DBEntry sourceEntry, DBEntry destinationEntry)
        {
            IConverter converter;
            var converterName = $"{sourceEntry.Type} -> {destinationEntry.Type}";
            if (_supportedConverters.TryGetValue(converterName, out converter))
            {
                return converter;
            }
            throw new Exception($"Converions \"{converterName}\" is not supported.");
        }

        private static MigratePipeline CreatePipeline(DBEntry source, DBEntry destination)
        {
            var sourceDb = GetDB(source);
            var destDb = GetDB(destination);

            if (string.Equals(source.Type, destination.Type, StringComparison.OrdinalIgnoreCase))
            {
                return new MigratePipeline(sourceDb, destDb, LoggerFactory.CreateLogger<MigratePipeline>());
            }

            var converter = GetConverter(source, destination);
            return new MigratePipeline(sourceDb, converter, destDb, LoggerFactory.CreateLogger<MigratePipeline>());
        }

        public static void Main(string[] args)
        {
            var arguments = new Arguments(args);
            LoggerFactory.AddConsole(arguments.LogLevel);
            var entries = ReadConfig();

            var source = entries[arguments.From];
            var destination = entries[arguments.To];

            var pipeline = CreatePipeline(source, destination);
            pipeline.RunAsync().Await();
        }
    }
}