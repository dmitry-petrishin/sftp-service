﻿using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.ExceptionHandling;

namespace DBMigrator.Repositories
{
    public static class RepositoryExtensions
    {
        public static IRepository Untype<T>(this IRepository<T> repository) where T: class
        {
            return new RepositoryWrapper<T>(repository);
        }

        public static async Task<bool> TryCreateItemAsync(this IRepository repository, object item)
        {
            try
            {
                await repository.CreateItemAsync(item);
                return true;
            }
            catch (ConflictException)
            {
                return false;
            }
        }
    }
}