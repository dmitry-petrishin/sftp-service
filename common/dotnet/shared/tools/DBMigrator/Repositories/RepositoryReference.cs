﻿namespace DBMigrator.Repositories
{
    public class RepositoryReference
    {
        public string DatabaseName { get; }

        public string CollectionName { get; }

        public RepositoryReference(string databaseName, string collectionName)
        {
            DatabaseName = databaseName;
            CollectionName = collectionName;
        }

        public override string ToString()
        {
            return $"{DatabaseName}:{CollectionName}";
        }
    }
}