﻿using Deloitte.Cortex.Shared.DataAccess;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;

namespace DBMigrator.Repositories
{
    public class MongoDBRepository: MongoDBRepository<BsonDocument>
    {
        public MongoDBRepository(MongoDBSettings config, ILoggerFactory loggerFactory):
            base(config, loggerFactory.CreateLogger<MongoDBRepository>()) { }
    }
}