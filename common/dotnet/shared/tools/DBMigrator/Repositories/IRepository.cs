﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DBMigrator.Repositories
{
    /// <summary>
    /// Untyped <see cref="Deloitte.Cortex.Shared.DataAccess.IRepository{T}"/> wrapper.
    /// </summary>
    public interface IRepository
    {
        Task CreateItemAsync(object item);

        Task CreateItemsAsync(IEnumerable<object> items);

        Task<IList<object>> GetAllItemsAsync();
    }
}