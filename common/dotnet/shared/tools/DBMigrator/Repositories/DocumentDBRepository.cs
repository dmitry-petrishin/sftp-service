﻿using Deloitte.Cortex.Shared.DataAccess;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace DBMigrator.Repositories
{
    public class DocumentDBRepository: DocumentDBRepository<JObject>
    {
        public DocumentDBRepository(DocumentDBSettings config, ILoggerFactory loggerFactory):
            base(config, loggerFactory.CreateLogger<DocumentDBRepository>()) { }
    }
}