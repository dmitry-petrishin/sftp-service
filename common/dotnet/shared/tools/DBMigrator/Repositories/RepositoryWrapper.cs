﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;

namespace DBMigrator.Repositories
{
    public class RepositoryWrapper<T>: IRepository
        where T: class
    {
        private readonly IRepository<T> _repository;

        public RepositoryWrapper(IRepository<T> repository)
        {
            _repository = repository;
        }

        public Task CreateItemAsync(object item)
        {
            return _repository.CreateItemAsync((T) item);
        }

        public Task CreateItemsAsync(IEnumerable<object> items)
        {
            return _repository.CreateItemsAsync(items.Cast<T>().ToArray());
        }

        public async Task<IList<object>> GetAllItemsAsync()
        {
            return (await _repository.GetAllItemsAsync()).Cast<object>().ToList();
        }

        public override string ToString()
        {
            return _repository.ToString();
        }
    }
}