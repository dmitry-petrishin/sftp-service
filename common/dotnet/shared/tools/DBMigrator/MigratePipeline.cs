﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DBMigrator.Converters;
using DBMigrator.DBs;
using DBMigrator.Repositories;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Microsoft.Extensions.Logging;

namespace DBMigrator
{
    public class MigratePipeline
    {
        public const int BatchSize = 10;

        private readonly IDB _source;
        private readonly IConverter _converter;
        private readonly IDB _destination;

        private readonly ILogger<MigratePipeline> _logger;

        public MigratePipeline(IDB source, IConverter converter, IDB destination, ILogger<MigratePipeline> logger)
        {
            _source = source;
            _converter = converter ?? new NoConverter();
            _destination = destination;
            _logger = logger;
        }

        public MigratePipeline(IDB source, IDB destination, ILogger<MigratePipeline> logger): this(source, null, destination, logger) { }

        public async Task RunAsync()
        {
            _logger.LogInformation($"Fetching repositories for {_source}");

            var sourceRepositories = await _source.GetRepositoriesAsync();

            foreach (var pair in sourceRepositories)
            {
                var sourceReference = pair.Key;
                var sourceRepository = pair.Value;

                var targetReference = _destination.AdjustSource(sourceReference);
                var destinationRepository = _destination.CreateRepository(targetReference);

                _logger.LogInformation($"Starting {_source}:{sourceReference} -> {_destination}:{targetReference} migration.");

                var items = await sourceRepository.GetAllItemsAsync();

                _logger.LogInformation($"Fetched {items.Count} items.");

                foreach (var item in items.Select(i => _converter.Convert(i)).ToArray())
                {
                    try
                    {
                        if (await destinationRepository.TryCreateItemAsync(item))
                            _logger.LogDebug($"Added item {item}");
                        else
                            _logger.LogDebug($"Item {item} already exists.");
                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"Failed to add item {item} due to {e.GetType().FullName}:\"{e.Message}\"");
                    }
                }

                _logger.LogInformation($"Finished {_source}:{sourceReference} -> {_destination}:{targetReference} migration.");
            }
        }
    }
}