﻿using Deloitte.Cortex.Shared.AspNetCore;
using Microsoft.Extensions.Logging;

namespace DBMigrator.Config
{
    public class Arguments
    {
        public string From { get; } = "DocumentDB-Local";

        public string To { get; } = "MongoDB-Local";

        public LogLevel LogLevel { get; } = LogLevel.Information;

        public Arguments(string[] args)
        {
            if (args.Length > 0) From = args[0];
            if (args.Length > 1) To = args[1];
            if (args.Length > 2) LogLevel = Enum<LogLevel>.Parse(args[2]);
        }
    }
}