using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace DBMigrator.Config
{
    public class DBEntry
    {
        public string Type { get; }

        public JObject Config { get; }

        public DBEntry(KeyValuePair<string, JObject> jsonProperty)
        {
            var dashIndex = jsonProperty.Key.IndexOf("-", StringComparison.Ordinal);
            Type = dashIndex >= 0 ? jsonProperty.Key.Substring(0, dashIndex).Trim() : jsonProperty.Key.Trim();
            Config = jsonProperty.Value;
        }
    }
}