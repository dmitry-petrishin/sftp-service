﻿namespace DBMigrator.Converters
{
    public class NoConverter<TItem>: IConverter<TItem, TItem>
    {
        public TItem Convert(TItem @in)
        {
            return @in;
        }
    }

    public class NoConverter: IConverter
    {
        public object Convert(object @in)
        {
            return @in;
        }
    }
}