﻿using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DBMigrator.Converters
{
    public class DocumentDBToMongoDBConverter: IConverter<JObject, BsonDocument>, IConverter
    {
        private static BsonDocument AsBson(JToken json)
        {
            return BsonDocument.Parse(JsonConvert.SerializeObject(json));
        }

        public BsonDocument Convert(JObject @in)
        {
            var id = @in.Value<string>("id");

            @in.Remove("id");
            @in.Remove("_rid");
            @in.Remove("_self");
            @in.Remove("_etag");
            @in.Remove("_attachments");
            @in.Remove("_ts");

            var bson = AsBson(@in);
            bson["_id"] = id != null ? new BsonString(id) : (BsonValue) BsonNull.Value;

            return bson;
        }

        public object Convert(object @in)
        {
            return Convert((JObject)@in);
        }
    }
}