﻿namespace DBMigrator.Converters
{
    public interface IConverter<in TIn, out TOut>
    {
        TOut Convert(TIn @in);
    }

    public interface IConverter: IConverter<object, object> { }
}