﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DBMigrator.Repositories;

namespace DBMigrator.DBs
{
    public interface IDB
    {
        Task<Dictionary<RepositoryReference, IRepository>> GetRepositoriesAsync();
        IRepository CreateRepository(RepositoryReference reference);
        RepositoryReference AdjustSource(RepositoryReference source);
    }
}