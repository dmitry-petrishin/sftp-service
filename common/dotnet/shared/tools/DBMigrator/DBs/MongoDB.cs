using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBMigrator.Repositories;
using Deloitte.Cortex.Shared.DataAccess;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace DBMigrator.DBs
{
    public class MongoDB: IDB
    {
        private readonly MongoDBSettings _config;
        private readonly ILoggerFactory _loggerFactory;

        private readonly IMongoClient _client;

        public MongoDB(MongoDBSettings config, ILoggerFactory loggerFactory)
        {
            _config = config;
            _loggerFactory = loggerFactory;

            _client = new MongoClient(_config.Endpoint);
        }

        public Task<Dictionary<RepositoryReference, IRepository>> GetRepositoriesAsync()
        {
            var mongoUrl = new MongoUrl(_config.Endpoint);
            var databaseName = _config.DatabaseName ?? mongoUrl.DatabaseName;

            Dictionary<RepositoryReference, IRepository> result;

            if (databaseName != null && _config.CollectionName != null)
            {
                var reference = new RepositoryReference(databaseName, _config.CollectionName);
                result = new Dictionary<RepositoryReference, IRepository> { { reference, CreateRepository(reference) } };
            }
            else if (databaseName != null)
            {
                var database = _client.GetDatabase(databaseName);
                var collectionNames = database.ListCollections().ToEnumerable()
                    .Select(bson => bson["name"].AsString).ToArray();

                result = collectionNames.Select(c => new RepositoryReference(databaseName, c))
                    .ToDictionary(r => r, CreateRepository);
            }
            else
            {
                result = _client.GetDatabases().SelectMany(d => d.GetCollections(), (d, c) =>
                        new RepositoryReference(d.DatabaseNamespace.DatabaseName, c.CollectionNamespace.CollectionName))
                    .ToDictionary(r => r, CreateRepository);
            }

            return Task.FromResult(result);
        }

        public IRepository CreateRepository(RepositoryReference reference)
        {
            return new MongoDBRepository(new MongoDBSettings
                {
                    Endpoint = _config.Endpoint,
                    DatabaseName = reference.DatabaseName.Replace(".", "-"),
                    CollectionName = reference.CollectionName.Replace(".", "-")
                }, _loggerFactory)
                .Untype();
        }

        public RepositoryReference AdjustSource(RepositoryReference source)
        {
            var mongoUrl = new MongoUrl(_config.Endpoint);
            var databaseName = _config.DatabaseName ?? mongoUrl.DatabaseName;
            var targetDbName = databaseName ?? source.DatabaseName;
            var targetCollectionName = _config.CollectionName ?? source.CollectionName;
            
            var reference = new RepositoryReference(targetDbName, targetCollectionName);
            return reference;
        }

        public override string ToString() => $"MongoDB ({_config.Endpoint})";
    }
}