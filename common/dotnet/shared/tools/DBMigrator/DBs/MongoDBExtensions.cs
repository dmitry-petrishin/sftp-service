﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DBMigrator.DBs
{
    public static class MongoDBExtensions
    {
        public static IEnumerable<IMongoDatabase> GetDatabases(this IMongoClient client)
        {
            return client.ListDatabases().ToEnumerable()
                .Select(b => b["name"].AsString)
                .Select(n => client.GetDatabase(n));
        }

        public static IEnumerable<IMongoCollection<BsonDocument>> GetCollections(this IMongoDatabase database)
        {
            return database.ListCollections().ToEnumerable()
                .Select(b => b["name"].AsString)
                .Select(n => database.GetCollection<BsonDocument>(n));
        }
    }
}
