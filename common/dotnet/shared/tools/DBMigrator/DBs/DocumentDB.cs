using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBMigrator.Repositories;
using Deloitte.Cortex.Shared.DataAccess;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;

namespace DBMigrator.DBs
{
    public class DocumentDB: IDB
    {
        private readonly DocumentDBSettings _config;
        private readonly ILoggerFactory _loggerFactory;

        private IDocumentClient _client;

        public DocumentDB(DocumentDBSettings config, ILoggerFactory loggerFactory)
        {
            _config = config;
            _loggerFactory = loggerFactory;

            _client = new DocumentClient(new Uri(_config.Endpoint), _config.Key, new ConnectionPolicy { EnableEndpointDiscovery = false });
        }

        // Todo: simplify
        public async Task<Dictionary<RepositoryReference, IRepository>> GetRepositoriesAsync()
        {
            if (_config.DatabaseId != null && _config.CollectionId != null)
            {
                var reference = new RepositoryReference(_config.DatabaseId, _config.CollectionId);
                return new Dictionary<RepositoryReference, IRepository> { { reference, CreateRepository(reference) } };
            }

            else if (_config.DatabaseId != null)
            {
                var uri = UriFactory.CreateDatabaseUri(_config.DatabaseId);
                return (await _client.ReadDocumentCollectionFeedAsync(uri))
                    .Select(c => new RepositoryReference(_config.DatabaseId, c.Id))
                    .ToDictionary(r => r, CreateRepository);
            }

            else
            {
                var databases = (await _client.ReadDatabaseFeedAsync())
                    .Where(d => !d.Id.EndsWith("==")); // Attempt to filter out internal databases
                var databasesCollections = await Task.WhenAll(databases.Select(
                    d => _client.ReadDocumentCollectionFeedAsync(UriFactory.CreateDatabaseUri(d.Id))));

                return databases.SelectMany((database, i) => databasesCollections[i],
                        (database, collection) => new RepositoryReference(database.Id, collection.Id))
                    .ToDictionary(r => r, CreateRepository);
            }
        }

        public IRepository CreateRepository(RepositoryReference reference)
        {
            return new DocumentDBRepository(new DocumentDBSettings
                {
                    Endpoint = _config.Endpoint,
                    Key = _config.Key,
                    IndexingPolicy = _config.IndexingPolicy,
                    OfferThroughput = _config.OfferThroughput,

                    DatabaseId = reference.DatabaseName,
                    CollectionId = reference.CollectionName,
                }, _loggerFactory)
                .Untype();
        }

        public RepositoryReference AdjustSource(RepositoryReference source)
        {
            throw new NotImplementedException();
        }

        public override string ToString() => $"DocumentDB ({_config.Endpoint})";
    }
}