﻿namespace Deloitte.Cortex.Messaging.Exchanges
{
    public class Jobs
    {
        public const string Name = "Jobs";

        public static string GetStatusChangedRoutingKey(string jobId)
        {
            return $"Job.{jobId}.StatusChanged";
        }
    }
}
