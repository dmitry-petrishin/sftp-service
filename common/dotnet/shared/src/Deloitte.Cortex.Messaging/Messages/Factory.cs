﻿using Newtonsoft.Json.Linq;
using System;

namespace Deloitte.Cortex.Messaging.Messages
{
    public class Factory
    {
        public static BaseMessage Parse(JObject raw)
        {
            var type = raw.Value<string>("Type");
            switch (type)
            {
                case nameof(JobStatusChanged):
                    return raw.ToObject<JobStatusChanged>();
                default:
                    throw new Exception($"Unknown message type: {type}");
            }
        }
    }
}
