﻿namespace Deloitte.Cortex.Messaging.Messages
{
    public enum JobStatusEnum
    {
        Created,
        Running,
        Finished,
    }
}
