﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Messaging.Messages
{
    public class JobStatusChanged : BaseMessage
    {
        /// <summary>
        /// Prep-Orchestrator native id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Status of a job
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public JobStatusEnum Status { get; set; }

        /// <summary>
        /// Null if job is successful or not finished yet
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Error { get; set; }
    }
}
