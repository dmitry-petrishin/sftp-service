﻿namespace Deloitte.Cortex.Messaging.Messages
{
    public class BaseMessage
    {
        public string Type
        {
            get
            {
                return GetType().Name;
            }
        }
    }
}
