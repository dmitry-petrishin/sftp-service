﻿namespace Deloitte.Cortex.StagingService.Contracts.Models
{
    public enum ContainerTypeEnum
    {
        FILE,
        DIRECTORY
    }
}