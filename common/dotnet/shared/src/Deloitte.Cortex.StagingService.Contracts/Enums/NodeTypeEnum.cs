﻿namespace Deloitte.Cortex.StagingService.Contracts.Enums
{
    public enum NodeTypeEnum
    {
        FILE, FOLDER
    }
}