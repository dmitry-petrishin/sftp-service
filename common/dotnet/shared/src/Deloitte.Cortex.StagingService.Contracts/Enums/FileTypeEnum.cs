﻿namespace Deloitte.Cortex.StagingService.Contracts.Enums
{
    public enum FileTypeEnum
    {
        RAW,
        TABLE,
        VIEW,
        SPARK
    }
}