﻿using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.StagingService.Contracts.Models
{
    public class ParquetData
    {
        public int TotalParts { get; set; }
        public JObject[] Columns { get; set; }
        public JObject[] Rows { get; set; }
    }
}