﻿namespace Deloitte.Cortex.StagingService.Contracts.Conversions
{
    public class DownloadPathResponse
    {
        public string Path { get; set; }
        public string NodeId { get; set; }
    }
}