﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.StagingService.Contracts.Conversions
{
    public class FileConcatRequest
    {
        [JsonProperty("Header_Path")]
        public string HeaderPath { get; set; }

        [JsonProperty("Data_Path")]
        public string DataPath { get; set; }

        public string ParentNodeId { get; set; }
    }
}