﻿namespace Deloitte.Cortex.StagingService.DataLake.Models.Excel
{
    public class ConvertToCsvResponse
    {
        public string TransactionId { get; set; }
    }
}