﻿using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.StagingService.Contracts.Models
{
    public class ParquetSchema
    {
        public JObject[] Columns { get; set; }

        public int TotalParts { get; set; }
    }
}