﻿namespace Deloitte.Cortex.StagingService.Contracts.SpecialFolders
{
    public class EngagementFolders
    {
        public const string DataSets = "DataSets";
        public const string DataRequests = "Data Request";
        public const string Workitems = "Workitems";
        public const string UserImportedData = "User Imported Data";
    }
}
