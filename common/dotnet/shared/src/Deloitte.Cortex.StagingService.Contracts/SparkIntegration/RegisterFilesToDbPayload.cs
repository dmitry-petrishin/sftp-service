﻿using System.Collections.Generic;

namespace Deloitte.Cortex.StagingService.Contracts.SparkIntegration
{
    public class RegisterFilesToDbPayload
    {
        public List<FileWithSourceNodeIds> FileListWithDependentNodeIds { get; set; }
        public string ParentNodeId { get; set; }
    }
}