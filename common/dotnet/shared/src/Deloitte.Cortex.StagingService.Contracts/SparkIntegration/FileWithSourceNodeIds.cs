﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.StagingService.Contracts.SparkIntegration
{
    public class FileWithSourceNodeIds
    {
        [JsonProperty("fileName")]
        public string FileName { get; set; }

        public string[] SourceNodeIds { get; set; }
    }
}