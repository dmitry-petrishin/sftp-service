﻿namespace Deloitte.Cortex.StagingService.Contracts.StringPayload
{
    public class StringPayloadRequest
    {
        public string Item { get; set; }
    }
}