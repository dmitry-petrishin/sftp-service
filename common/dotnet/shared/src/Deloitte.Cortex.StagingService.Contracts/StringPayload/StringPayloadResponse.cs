﻿namespace Deloitte.Cortex.StagingService.Contracts.StringPayload
{
    public class StringPayloadResponse
    {
        public string Item { get; set; }
    }
}