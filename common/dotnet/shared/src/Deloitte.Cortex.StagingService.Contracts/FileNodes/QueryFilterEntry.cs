﻿using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class QueryFilterEntry
    {
        public string Type { get; set; }

        public JToken Data { get; set; }
    }
}