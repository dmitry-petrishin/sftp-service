﻿using System;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class NodeDeletionView
    {
        public string DeletionId { get; set; }

        public DateTime Date { get; set; }

        public bool IsRoot { get; set; }

        public bool ContainsNotDeleted { get; set; }
    }
}
