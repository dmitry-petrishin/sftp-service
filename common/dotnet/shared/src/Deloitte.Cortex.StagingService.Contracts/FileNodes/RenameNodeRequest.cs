﻿namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class RenameNodeRequest
    {
        public string Id { get; set; }
        public string ModifiedParentId { get; set; }
        public string ModifiedName { get; set; }
    }
}