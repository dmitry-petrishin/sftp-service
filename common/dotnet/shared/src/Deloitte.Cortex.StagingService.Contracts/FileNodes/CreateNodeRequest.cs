﻿using System;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class CreateNodeRequest
    {
        public string Name { get; set; }
        public string ParentId { get; set; }
        public bool IsRoot { get; set; }

        [Obsolete]
        public object Properties { get; set; }
    }
}