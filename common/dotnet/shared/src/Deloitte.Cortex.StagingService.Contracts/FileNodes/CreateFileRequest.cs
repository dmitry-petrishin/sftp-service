﻿using Deloitte.Cortex.StagingService.Contracts.Enums;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class CreateFileRequest
    {
        public string ParentId { get; set; }

        public FileTypeEnum? FileType { get; set; }

        public string FileFormat { get; set; }

        public string FileName { get; set; }

        public bool Overwrite { get; set; }

        public bool SkipExcelModification { get; set; }

        public bool SkipVirusCheck { get; set; }

        /// <summary>
        /// For versioned creation
        /// </summary>
        public string ChainId { get; set; }
    }
}