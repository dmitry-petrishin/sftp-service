﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class CreateSystemFolderRequest
    {
        public string Name { get; set; }
        public string SystemRootFolderName { get; set; }
        public IDictionary<string, JToken> Meta { get; set; }
    }
}