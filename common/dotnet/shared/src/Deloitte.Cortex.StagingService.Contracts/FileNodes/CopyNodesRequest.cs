﻿namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class CopyNodesRequest
    {
        public string[] Ids { get; set; }
        public string TargetNodeId { get; set; }
    }
}