﻿using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class UpdateNodeRequest
    {
        public string[] TagIds { get; set; }

        public JObject Meta { get; set; }
    }
}