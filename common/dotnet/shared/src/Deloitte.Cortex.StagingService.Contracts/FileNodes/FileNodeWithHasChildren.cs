﻿using Deloitte.Cortex.StagingService.Contracts.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class FileNodeWithHasChildren
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        public string ChainId { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string StorageId { get; set; }
        public HashSet<string> GroupIds { get; set; }
        public bool HasChildren { get; set; }
        public bool IsMarkedForDeletion { get; set; }
        public bool IsRoot { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> NodeProp { get; set; }
        public string OriginalName { get; set; }
        public string ParentId { get; set; }
        public NodeTypeEnum TypeOfNode { get; set; }
        public int Version { get; set; }

        public string NodeTypeName
        {
            get { return TypeOfNode.ToString(); }
        }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string[] TagIds { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public JObject Meta { get; set; }

        public bool? IsCurrentVersion { get; set; }
        public bool IsDeletionRoot { get; set; }

        public DateTime? UpdateTime { get; set; }
        public string UpdatedBy { get; set; }
    }
}