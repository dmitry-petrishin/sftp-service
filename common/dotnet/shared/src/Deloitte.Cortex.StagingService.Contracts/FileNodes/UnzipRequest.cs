﻿namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class UnzipRequest
    {
        public string RelativePath { get; set; }
    }
}