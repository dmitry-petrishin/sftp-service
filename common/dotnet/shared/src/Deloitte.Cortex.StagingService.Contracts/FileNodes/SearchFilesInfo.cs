﻿namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class SearchFilesInfo
    {
        public QueryFilterEntry[] Filters { get; set; }
    }
}