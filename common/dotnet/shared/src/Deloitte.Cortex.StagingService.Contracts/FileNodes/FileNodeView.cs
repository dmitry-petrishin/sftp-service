﻿using System;
using System.Collections.Generic;
using Deloitte.Cortex.StagingService.Contracts.Enums;
using Newtonsoft.Json;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class FileNodeView 
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        public DateTime? UpdateTime { get; set; }
        public string UpdatedBy { get; set; }

        public string ChainId { get; set; }

        public string StorageId { get; set; }

        public string OriginalDisplayName { get; set; }

        public string DisplayName { get; set; }

        //[JsonConverter(typeof(StringEnumConverter))]
        public NodeTypeEnum TypeOfNode { get; set; }
        
        public string NodeTypeName
        {
            get { return TypeOfNode.ToString(); }
        }

        public string ParentId { get; set; }

        public bool IsRoot { get; set; }

        public Dictionary<string, string> NodeProp { get; set; }

        public HashSet<string> GroupIds { get; set; }

        public string[] TagIds { get; set; }

        public NodeDeletionView Deletion { get; set; }

        public bool? IsCurrentVersion { get; set; }

        public int Version { get; set; }

        public override string ToString()
        {
            return $"#{Id} {DisplayName}";
        }
        
        public string Name
        {
            get { return DisplayName; }
        }
    }
}

