﻿using Newtonsoft.Json;
using System;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class FileNodeWithProperties : FileNodeWithHasChildren
    {
        public long ActualSize { get; set; }
        public bool CanDelete { get; set; }
        public bool CanMoveToTrash { get; set; }
        public DateTime? LastModified { get; set; }
        public string Type { get; set; }

        public bool HasVersions { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string FullPath { get; set; }
    }
}