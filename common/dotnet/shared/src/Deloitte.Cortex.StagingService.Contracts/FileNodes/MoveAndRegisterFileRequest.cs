﻿using Deloitte.Cortex.StagingService.Contracts.Enums;

namespace Deloitte.Cortex.StagingService.Contracts.FileNodes
{
    public class MoveAndRegisterFileRequest
    {
        public string ChainId { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public bool Versioned { get; set; }
        public FileTypeEnum? Type { get; set; }
    }
}