﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.StagingService.Contracts.Sftp
{
    public class SftpOutputPayload
    {
        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("RawFilesFolderNodeId")]
        public string RawFilesFolderNodeId { get; set; }
    }
}