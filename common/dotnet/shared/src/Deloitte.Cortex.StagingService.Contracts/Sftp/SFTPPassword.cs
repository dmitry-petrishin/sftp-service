﻿namespace Deloitte.Cortex.StagingService.Contracts.Sftp
{
    public class SftpPassword
    {
        public string PasswordSalt { get; set; }

        public string PasswordEncrypted { get; set; }

        public string PasswordHash { get; set; }
    }
}