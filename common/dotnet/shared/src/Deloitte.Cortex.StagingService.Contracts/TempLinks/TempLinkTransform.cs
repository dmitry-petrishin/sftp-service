﻿namespace Deloitte.Cortex.StagingService.Contracts.TempLinks
{
    public enum TempLinkTransform
    {
        None,
        Zip,
        CsvAsExcel,
        ParqueteAsExcel
    }
}