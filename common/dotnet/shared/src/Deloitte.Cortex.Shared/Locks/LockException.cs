﻿using System;

namespace Deloitte.Cortex.Shared.Locks
{
    public class LockException : Exception
    {
        public LockException(string objectId) : base("Failed to aquire lock for object " + objectId)
        {
        }
    }
}
