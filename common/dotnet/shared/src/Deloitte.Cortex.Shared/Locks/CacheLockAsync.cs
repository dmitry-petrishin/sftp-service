﻿using Deloitte.Cortex.Shared.Clients.Caching;
using Deloitte.Cortex.Shared.Clients.Caching.Models;
using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Locks
{
    /// <inheritdoc />
    public partial class CacheLock
    {
        /// <summary>
        /// Tries to aquire lock on specified object id
        /// </summary>
        /// <param name="cache">Caching client</param>
        /// <param name="objectId">Object identifier</param>
        /// <param name="maxLockAge">Usually 5 minutes. This time should be max time of object processing. If something is wrong, lock could be not released. However, we can't hold the object forever</param>
        /// <param name="timeout">Try to aquire lock during that period</param>
        /// <returns>Lock object. Should be disposed right after finish work with the object</returns>
        public static async Task<CacheLock> AcquireLockAsync(ICachingClient cache, string objectId, TimeSpan timeout,
            TimeSpan maxLockAge)
        {
            var startTime = DateTime.UtcNow;
            var key = LockObjectsPrefix + objectId;
            Lockable<object> res = null;

            do
            {
                //something wrong with redis async, so no GetAsync
                res = Lockable<object>.Get(cache, key, TimeSpan.Zero, null, maxLockAge);
                if (res != null)
                {
                    if (res.Status == LockStatus.Locked)
                        return new CacheLock(res);
                }
                else
                {
                    // Lock is not found for that object
                    res = Lockable<object>.SetAndLock(cache, key, new object());
                    if (res.Status == LockStatus.Locked)
                        return new CacheLock(res);
                }

                await Task.Delay(1000); // Wait for 1 second and try again. Lock will either be released or expired
            } while (DateTime.UtcNow < startTime + timeout);

            throw new LockException(objectId);
        }

        /// <summary>
        /// Tries to aquire lock on specified object id
        /// </summary>
        /// <param name="cache">Caching client</param>
        /// <param name="objectId">Object identifier</param>
        /// <returns>Lock object. Should be disposed right after finish work with the object</returns>
        public static Task<CacheLock> AcquireLockAsync(ICachingClient cache, string objectId)
        {
            return AcquireLockAsync(cache, objectId, TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5));
        }
    }
}