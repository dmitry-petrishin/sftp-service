﻿using Deloitte.Cortex.Shared.Clients.Caching;
using Deloitte.Cortex.Shared.Clients.Caching.Models;
using System;
using Deloitte.Cortex.Shared.AspNetCore;

namespace Deloitte.Cortex.Shared.Locks
{
    /// <summary>
    /// Simple lock using cache like Redis, Memcached. Lock automatically released on disposal or GC
    /// </summary>
    public partial class CacheLock : IDisposable
    {
        private const string LockObjectsPrefix = "Lock.";
        private Lockable<object> lockObj;

        private CacheLock(Lockable<object> lockObj)
        {
            this.lockObj = lockObj;
        }

        public Guid? LockId
        {
            get
            {
                return lockObj.LockId;
            }
        }

        /// <inheritdoc cref="AcquireLockAsync(Deloitte.Cortex.Shared.Clients.Caching.ICachingClient,string,System.TimeSpan,System.TimeSpan)"/>
        public static CacheLock AcquireLock(ICachingClient cache, string objectId, TimeSpan timeout, TimeSpan maxLockAge)
        {
            return AcquireLockAsync(cache, objectId, timeout, maxLockAge).Await();
        }
        
        /// <inheritdoc cref="AcquireLockAsync(Deloitte.Cortex.Shared.Clients.Caching.ICachingClient,string)"/>
        public static CacheLock AcquireLock(ICachingClient cache, string objectId)
        {
            return AcquireLockAsync(cache, objectId).Await();
        }

        /// <summary>
        /// Get lock for object. If failed, return null. No retries
        /// </summary>
        /// <param name="cache">Caching client</param>
        /// <param name="objectId">Object identifier</param>
        /// <param name="maxLockAge">Usually 5 minutes. This time should be max time of object processing. If something is wrong, lock could be not released. However, we can't hold the object forever</param>
        /// <returns></returns>
        public static CacheLock GetLock(ICachingClient cache, string objectId, TimeSpan maxLockAge)
        {
            var key = LockObjectsPrefix + objectId;

            var res = Lockable<object>.Get(cache, key, TimeSpan.Zero, null, maxLockAge);
            if (res == null)
            {
                res = Lockable<object>.SetAndLock(cache, key, new object());
            }
            if (res.Status == LockStatus.Locked)
                return new CacheLock(res);

            return null;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    if (lockObj != null)
                    {
                        lockObj.Dispose();
                        lockObj = null;
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }
        
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
