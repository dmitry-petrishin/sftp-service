﻿using System;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Collections
{
    public class FuncComparer<T> : IComparer<T>
    {
        private readonly Func<T, T, int> _compare;

        public FuncComparer(Func<T, T, int> compare)
        {
            _compare = compare ?? throw new ArgumentNullException();
        }

        public FuncComparer(Func<T, IComparable> getter)
            : this(getter, false)
        {
        }
        
        public FuncComparer(Func<T, IComparable> getter, bool desc)
        {
            _compare = (x, y) =>
            {
                var xValue = getter(x);
                var yValue = getter(y);
                return desc
                    ? -xValue.CompareTo(yValue)
                    : xValue.CompareTo(yValue);
            };
        }

        public int Compare(T x, T y)
        {
            return _compare(x, y);
        }
    }
}
