﻿using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Collections
{
    public static class CollectionExtensions
    {
        public static V TryGet<T, V>(this IDictionary<T, V> self, T key)
        {
            V result;
            if (self.TryGetValue(key, out result))
                return result;
            else
                return default(V);
        }
    }
}
