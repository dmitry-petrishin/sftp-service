﻿using System;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Collections
{
    /// <summary>
    /// Dictionary that returns and saves default or computed value for missing key.
    /// </summary>
    public class DefaultDictionary<TKey, TValue>: OverridableDictionary<TKey, TValue>
    {
        protected Func<TKey, TValue> ValueFactory { get; }

        public DefaultDictionary(IDictionary<TKey, TValue> dictionary, Func<TKey, TValue> valueFactory):
            base(dictionary)
        {
            if (valueFactory == null) throw new ArgumentNullException(nameof(valueFactory));

            ValueFactory = valueFactory;
        }

        public DefaultDictionary(IDictionary<TKey, TValue> dictionary, TValue defaultValue = default):
            this(dictionary, key => defaultValue) { }

        public DefaultDictionary(Func<TKey, TValue> valueFactory):
            this(new Dictionary<TKey, TValue>(), valueFactory) { }

        public DefaultDictionary(TValue defaultValue = default):
            this(new Dictionary<TKey, TValue>(), defaultValue) { }

        public override bool TryGetValue(TKey key, out TValue value)
        {
            if (base.TryGetValue(key, out value)) return true;

            this[key] = value = ValueFactory(key);
            return false;
        }

        public override TValue this[TKey key]
        {
            get
            {
                TValue value;
                TryGetValue(key, out value);
                return value;
            }
            set { base[key] = value; }
        }
    }

    public static class DefaultDictionary
    {
        public static DefaultDictionary<TKey, TValue> Create<TKey, TValue>(IDictionary<TKey, TValue> dictionary,
            Func<TKey, TValue> valueFactory)
        {
            return new DefaultDictionary<TKey, TValue>(dictionary, valueFactory);
        }

        public static DefaultDictionary<TKey, TValue> Create<TKey, TValue>(IDictionary<TKey, TValue> dictionary,
            TValue defaultValue = default)
        {
            return new DefaultDictionary<TKey, TValue>(dictionary, defaultValue);
        }
    }
}