﻿using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Collections
{
    /// <summary>
    /// Dictionary that returns and saves value obtained via default constructor for missing key.
    /// </summary>
    public class DefaultNewDictionary<TKey, TValue>: DefaultDictionary<TKey, TValue>
        where TValue: new()
    {
        public DefaultNewDictionary(IDictionary<TKey, TValue> dictionary): base(dictionary, key => new TValue()) { }

        public DefaultNewDictionary(): base(key => new TValue()) { }
    }
}