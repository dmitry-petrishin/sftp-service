﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Deloitte.Cortex.Shared.Collections
{
    public class OrderedQueue<T>
    {
        private readonly IComparer<T> _comparer;

        public LinkedList<T> Values { get; } = new LinkedList<T>();

        public OrderedQueue(IComparer<T> comparer)
        {
            _comparer = comparer;
        }

        public OrderedQueue(Func<T, T, int> compare)
        {
            _comparer = new FuncComparer<T>(compare);
        }

        public OrderedQueue(Func<T, IComparable> getter, bool desc = false)
        {
            _comparer = new FuncComparer<T>(getter, desc);
        }

        public OrderedQueue(bool desc = false)
            : this(t => (IComparable)t, desc)
        {
        }

        public int Add(T value)
        {
            var index = -1;
            for (var recentNode = Values.First;
                 recentNode != null;
                 recentNode = recentNode.Next)
            {
                index++;
                if (_comparer.Compare(value, recentNode.Value) < 0)
                {
                    Values.AddBefore(recentNode, value);
                    return index;
                }
            }

            Values.AddLast(value);
            return 0;
        }

        public T Peek()
        {
            if (Values.First != null)
                return Values.First.Value;
            else
                return default(T);
        }

        public IEnumerable<T> PopWhile(Func<T, bool> predicate)
        {
            if (!Values.Any())
            {
                yield break;
            }

            var nextNode = Values.First;
            for (var recentNode = nextNode;
                recentNode != null; 
                recentNode = nextNode)
            {
                nextNode = recentNode.Next;
                var res = recentNode.Value;
                if (predicate(res))
                {
                    Values.RemoveFirst();
                    yield return res;
                }
                else
                {
                    break;
                }
            }
        }

        public bool TryPop(out T res)
        {
            res = default(T);
            if (!Values.Any())
            {
                return false;
            }

            res = Values.First.Value;
            Values.RemoveFirst();
            return true;
        }
    }
}
