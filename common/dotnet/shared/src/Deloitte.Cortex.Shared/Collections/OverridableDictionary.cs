﻿using System.Collections;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Collections
{
    /// <summary>
    /// <see cref="Dictionary{TKey,TValue}"/> adapter with most members marked as virtual.
    /// </summary>
    public class OverridableDictionary<TKey, TValue>: IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _dictionary;

        private readonly IDictionary<TKey, TValue> _iDictionary;

        public OverridableDictionary()
        {
            _iDictionary = _dictionary = new Dictionary<TKey, TValue>();
        }

        public OverridableDictionary(int capacity)
        {
            _iDictionary = _dictionary = new Dictionary<TKey, TValue>(capacity);
        }

        public OverridableDictionary(IEqualityComparer<TKey> comparer)
        {
            _iDictionary = _dictionary = new Dictionary<TKey, TValue>(comparer);
        }

        public OverridableDictionary(int capacity, IEqualityComparer<TKey> comparer)
        {
            _iDictionary = _dictionary = new Dictionary<TKey, TValue>(capacity, comparer);
        }

        public OverridableDictionary(IDictionary<TKey, TValue> dictionary)
        {
            _iDictionary = _dictionary = new Dictionary<TKey, TValue>(dictionary);
        }

        public OverridableDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
        {
            _iDictionary = _dictionary = new Dictionary<TKey, TValue>(dictionary, comparer);
        }

        #region Implementation of IEnumerable

        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() => GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        #endregion

        #region Implementation of ICollection<KeyValuePair<TKey,TValue>>

        public virtual void Add(KeyValuePair<TKey, TValue> item)
        {
            _iDictionary.Add(item);
        }

        public virtual void Clear()
        {
            _iDictionary.Clear();
        }

        public virtual bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _iDictionary.Contains(item);
        }

        public virtual void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _iDictionary.CopyTo(array, arrayIndex);
        }

        public virtual bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return _iDictionary.Remove(item);
        }

        public virtual int Count => _iDictionary.Count;

        public virtual bool IsReadOnly => _iDictionary.IsReadOnly;

        #endregion

        #region Implementation of IDictionary<TKey,TValue>

        public virtual void Add(TKey key, TValue value)
        {
            _iDictionary.Add(key, value);
        }

        public virtual bool ContainsKey(TKey key)
        {
            return _iDictionary.ContainsKey(key);
        }

        public virtual bool Remove(TKey key)
        {
            return _iDictionary.Remove(key);
        }

        public virtual bool TryGetValue(TKey key, out TValue value)
        {
            return _iDictionary.TryGetValue(key, out value);
        }

        public virtual TValue this[TKey key]
        {
            get { return _iDictionary[key]; }
            set { _iDictionary[key] = value; }
        }

        ICollection<TKey> IDictionary<TKey, TValue>.Keys => Keys;

        ICollection<TValue> IDictionary<TKey, TValue>.Values => Values;

        #endregion

        #region Implementation of IDictionary<TKey,TValue>

        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => Keys;

        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => Values;

        #endregion

        #region Implementation of Dictionary<TKey,TValue>

        public virtual IEqualityComparer<TKey> Comparer => _dictionary.Comparer;

        public virtual Dictionary<TKey, TValue>.KeyCollection Keys => _dictionary.Keys;

        public virtual Dictionary<TKey, TValue>.ValueCollection Values => _dictionary.Values;

        public virtual bool ContainsValue(TValue value) => _dictionary.ContainsValue(value);

        public virtual Dictionary<TKey, TValue>.Enumerator GetEnumerator() => _dictionary.GetEnumerator();

        #endregion

    }
}