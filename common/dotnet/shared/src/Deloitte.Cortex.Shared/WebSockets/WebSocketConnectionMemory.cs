﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.WebSockets
{
    public class WebSocketConnectionMemory
    {
        private readonly ConcurrentDictionary<string, WebSocket> sockets = new ConcurrentDictionary<string, WebSocket>();

        public void AddSocket(WebSocket socket)
        {
            this.sockets.TryAdd(this.CreateSocketId(), socket);
        }

        public ConcurrentDictionary<string, WebSocket> GetAll()
        {
            return this.sockets;
        }

        public string GetId(WebSocket socket)
        {
            string result = this.sockets.FirstOrDefault(x => x.Value == socket).Key;
            return result;
        }

        public WebSocket GetSocket(string id)
        {
            WebSocket result = this.sockets.FirstOrDefault(x => x.Key == id).Value;
            return result;
        }

        public async Task RemoveSocket(string id)
        {
            bool isRemoved = this.sockets.TryRemove(id, out WebSocket socket);

            if (isRemoved)
            {
                await socket.CloseAsync(
                    closeStatus: WebSocketCloseStatus.NormalClosure,
                    statusDescription: "Closed by WebSocketConnectionMemory.",
                    cancellationToken: CancellationToken.None);
            }
        }

        private string CreateSocketId()
        {
            return Guid.NewGuid().ToString();
        }
    }
}