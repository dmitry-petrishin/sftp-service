﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

namespace Deloitte.Cortex.Shared.WebSockets
{
    public class WebSocketManagerMiddleware<T> where T: class 
    {
        private readonly RequestDelegate next;
        private WebSocketManager<T> WebSocketManager { get; set; }

        public WebSocketManagerMiddleware(RequestDelegate next, WebSocketManager<T> webSocketManager)
        {
            this.next = next;
            this.WebSocketManager = webSocketManager;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                return;
            }

            WebSocket socket = await context.WebSockets.AcceptWebSocketAsync();
            this.WebSocketManager.OnConnected(socket);

            await this.Receive(socket);

            // TODO: test dat shit
            await this.next(context);
        }

        private async Task Receive(WebSocket socket)
        {
            byte[] buffer = new byte[1024 * 4];

            while (socket.State == WebSocketState.Open)
            {
                WebSocketReceiveResult result = await socket.ReceiveAsync(
                                 buffer: new ArraySegment<byte>(buffer),
                                 cancellationToken: CancellationToken.None);

                await this.HandleMessage(socket, result, buffer);
            }
        }

        private async Task HandleMessage(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            switch (result.MessageType)
            {
                case WebSocketMessageType.Text:
                    await this.WebSocketManager.ReceiveAsync(socket, result, buffer);
                    break;
                case WebSocketMessageType.Close:
                    await this.WebSocketManager.OnDisconnected(socket);
                    break;
            }
        }
    }
}
