﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Deloitte.Cortex.Shared.WebSockets
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder MapWebSocketPath<T>(
            this IApplicationBuilder applicationBuilder,
            PathString path,
            WebSocketManager<T> manager) where T: class
        {
            return applicationBuilder.Map(
                path, 
                app => app.UseMiddleware<WebSocketManagerMiddleware<T>>(manager));
        }
    }
}
