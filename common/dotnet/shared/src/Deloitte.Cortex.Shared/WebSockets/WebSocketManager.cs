﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.WebSockets
{
    public class WebSocketManager<T> where T : class 
    {
        public WebSocketManager(WebSocketConnectionMemory memory)
        {
            this.Memory = memory;
        }

        public WebSocketConnectionMemory Memory { get; set; }

        public virtual void OnConnected(WebSocket socket)
        {
            this.Memory.AddSocket(socket);
        }

        public virtual async Task OnDisconnected(WebSocket socket)
        {
            // TODO: remove socket by socket object
            await this.Memory.RemoveSocket(this.Memory.GetId(socket));
        }

        public async Task<T> ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            string message = Encoding.UTF8.GetString(buffer);

            T res = JsonConvert.DeserializeObject<T>(message);

            return res;
        }

        public async Task SendMessageAsync(WebSocket socket, string message)
        {
            if (socket.State != WebSocketState.Open)
            {
                // TODO: maybe throw an exeption.
                return;
            }

            ArraySegment<byte> array = new ArraySegment<byte>(
                array: Encoding.ASCII.GetBytes(message),
                offset: 0,
                count: message.Length);

            await socket.SendAsync(
                buffer: array,
                messageType: WebSocketMessageType.Text,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }

        public async Task SendMessageAsync(string socketId, string message)
        {
            WebSocket socket = this.Memory.GetSocket(socketId);

            await this.SendMessageAsync(socket, message);
        }

        public async Task SendMessageToAllAsync(string message)
        {
            ConcurrentDictionary<string, WebSocket> sockets = this.Memory.GetAll();

            foreach (KeyValuePair<string, WebSocket> socket in sockets)
            {
                if (socket.Value.State == WebSocketState.Open)
                {
                    await this.SendMessageAsync(socket.Value, message);
                }
            }
        }
    }
}