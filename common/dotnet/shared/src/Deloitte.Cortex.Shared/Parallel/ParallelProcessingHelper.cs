﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Parallel
{
    public static class ParallelProcessingHelper
    {
        public static async Task<Task[]> DoInParallel(
            this IEnumerable<Func<Task>> asyncDelegateSource,
            int maxParallel,
            CancellationToken token = default)
        {
            var tasks = new List<Task>();

            using (var semaphore = new SemaphoreSlim(maxParallel))
            using (var enumerator = asyncDelegateSource.GetEnumerator())
            while (!token.IsCancellationRequested)
            {
                if (!enumerator.MoveNext())
                {
                    return tasks.ToArray();
                }
                var action = enumerator.Current;
                await semaphore.WaitAsync(token);
                var task = DoAction(action, semaphore, tasks);
            }

            return tasks.ToArray();
        }

        public static async Task<T[]> DoInParallel<T>(
            this IEnumerable<Func<Task<T>>> asyncDelegateSource,
            int maxParallel,
            CancellationToken token = default)
        {
            var tasks = await asyncDelegateSource
                .Select(f => (Func<Task>)f)
                .DoInParallel(maxParallel, token);

            var results = tasks
                .Select(t => (Task<T>)t)
                .Select(t => t.Result)
                .ToArray();

            return results;
        }

        private static async Task DoAction(Func<Task> action, SemaphoreSlim semaphore, List<Task> tasks)
        {
            try
            {
                var task = action();
                tasks.Add(task);
                await task;
                tasks.Remove(task);
            }
            finally
            {
                semaphore.Release();
            }
        }

        public static IEnumerable<T> Repeat<T>(
            this T item, 
            CancellationToken token = default)
        {
            while (!token.IsCancellationRequested)
            {
                yield return item;
            }
        }

        public static IEnumerable<T> Repeat<T>(
            this T item,
            int n,
            CancellationToken token = default)
        {
            var i = 0;
            while (!token.IsCancellationRequested && i < n)
            {
                i++;
                yield return item;
            }
        }
    }
}
