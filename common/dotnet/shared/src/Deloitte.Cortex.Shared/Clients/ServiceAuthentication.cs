﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Authorization;

namespace Deloitte.Cortex.Shared.Clients
{
    /// <summary>
    /// Adds authorization header with bearer token obtained from <see cref="IAuthenticationService"/>.
    /// </summary>
    public class ServiceAuthentication: RequestAuthentication
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly string _resource;
        private readonly AuthenticationLevelEnum _level;

        public ServiceAuthentication(IAuthenticationService authenticationService,
            string resource = null, AuthenticationLevelEnum level = AuthenticationLevelEnum.Main)
        {
            _resource = resource;
            _level = level;
            _authenticationService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
        }

        public ServiceAuthentication(ServiceAuthentication authentication)
        {
            _authenticationService = authentication._authenticationService;
            _resource = authentication._resource;
            _level = authentication._level;
        }

        public override async Task AddAuthenticationAsync(HttpRequestMessage request)
        {
            // Todo: provide resource and level
            var authenticationResult = await _authenticationService.GetServiceTokenAsync(_resource, _level);
            var token = authenticationResult.AccessToken;

            if (!string.IsNullOrEmpty(token))
                request.Headers.AddOrReplace("Authorization", $"Bearer {token}");
        }
    }
}