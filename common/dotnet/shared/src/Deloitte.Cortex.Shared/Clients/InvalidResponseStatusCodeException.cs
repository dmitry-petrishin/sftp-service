using System.Net;
using System.Net.Http;

namespace Deloitte.Cortex.Shared.Clients
{
    public class InvalidResponseStatusCodeException: RequestFailedException
    {
        public HttpStatusCode ResponseCode { get; }

        public string Content { get; }

        public InvalidResponseStatusCodeException(HttpResponseMessage response, string content = null):
            base(response.RequestMessage, reason: $"unexpected response status code: {(int) response.StatusCode} {response.StatusCode}")
        {
            ResponseCode = response.StatusCode;
            Content = content;
        }

        public override string ToString()
        {
            return base.ToString() + "\nResponse:\n" + Content;
        }
    }
}