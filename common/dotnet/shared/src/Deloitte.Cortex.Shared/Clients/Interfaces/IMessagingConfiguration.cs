﻿using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Clients.Interfaces
{
    public interface IMessagingConfiguration
    {
        string Type { get; set; }
        string QueueName { get; set; }
        string Endpoint { get; set; }
        IDictionary<string, string> QueueNames { get; set; }
        int ConsumersCount { get; set; }
        ushort PrefetchCount { get; set; }
        bool PrefetchGlobal { get; set; }
        bool DirtyAck { get; set; }

        /// <summary>
        /// Path to CA to validate server. If not specified, built-in system CA store will be used
        /// </summary>
        string CA { get; set; }
        /// <summary>
        /// Path to client PKCS12 .p12 certificate. When specified, SSL + x509 is used for rabbit connection
        /// </summary>
        string Crt { get; set; }
        /// <summary>
        /// Password to client PKCS12 .p12 certificate
        /// </summary>
        string CrtPass { get; set; }

        bool MirrorQueues { get; set; }
    }
}