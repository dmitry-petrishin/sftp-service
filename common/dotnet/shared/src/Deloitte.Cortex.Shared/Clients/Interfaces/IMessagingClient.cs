﻿using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Interfaces
{
    public interface IMessagingClient
    {
        Task<bool> SendToQueueAsync<T>(T obj) where T: class;
        Task<T> RecieveFromQueueAsync<T>() where T : class;
        Task OnMessageAsync<T>(Func<T, Task> callback) where T : class;
        string GetQueueName<T>();
    }
}
