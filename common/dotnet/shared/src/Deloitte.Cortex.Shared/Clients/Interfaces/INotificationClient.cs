﻿using System;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Audit.Models;

namespace Deloitte.Cortex.Shared.Clients.Interfaces
{
    public interface INotificationClient
    {
        Task<bool> ConnectionPasswordUpcomingExpiration(string userEmail, string dataSourceName, string connectionName,
            string connectionId, DateTime expirationDate);
        
        Task<bool> SendAuditLog(AuditLogDto dto);

        Task<bool> ConnectionParametersNeedToBeUpdated(string connectionName, string clientId, bool isClientChosen);
        Task<bool> SymphonyRequestsForData(string appName, string engagementId, string engagementName, string workItemId, string workItemName,
            string dataStartDate, string dataEndDate);
        Task<bool> CortexCompletesSymphonyRequest(string appName, string engagementId, string engagementName, string workItemId, string workItemName,
            string dataStartDate, string dataEndDate);

        Task<bool> DataRequestApprovalReminderBeforeExtraction(string userEmail, string dataRequestId, string dataRequestName, string engagementName, string inactivityAmount, string timeIntervalType);
        Task<bool> DataRequestApprovalReminderOnExtraction(string userEmail, string dataRequestId, string dataRequestName, string engagementName);
        Task<bool> DataRequestApprovalReminderAfterExtraction(string userEmail, string dataRequestId, string dataRequestName, string engagementName, string inactivityAmount);

        Task<bool> EngagementReportIssuanceDateReminder(string engagementId, string engagementName, string reportIssuanceDate);
        Task<bool> EngagementReportIssuanceDateReminderForAppAdmin(string engagementId, string engagementName, string reportIssuanceDate);
        Task<bool> EngagementCloseoutProcessInit(string userEmail, string engagementName);

        Task<bool> DataExtractionsScheduledAfterEngagementCloseout(string engagementId, string engagementName);
    }
}