﻿using System;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Audit.Models;

namespace Deloitte.Cortex.Shared.Clients.Interfaces
{
    public static class AuditLogGuidExtensions
    {
        public static Task<bool> ObjectCreated(this IAuditLogClient client, Guid objectId, ObjectType objectType, object @object) =>
            client.ObjectCreated(objectId.ToString(), objectType, @object);

        public static Task<bool> ObjectChanged(this IAuditLogClient client, Guid objectId, ObjectType objectType, object oldObject, object newObject) =>
            client.ObjectChanged(objectId.ToString(), objectType, oldObject, newObject);

        public static Task<bool> ObjectDeleted(this IAuditLogClient client, Guid objectId, ObjectType objectType) =>
            client.ObjectDeleted(objectId.ToString(), objectType);

        public static Task<bool> ObjectDeleted(this IAuditLogClient client, Guid objectId, ObjectType objectType, object @object) =>
            client.ObjectDeleted(objectId.ToString(), objectType, @object);
    }
}