﻿namespace Deloitte.Cortex.Shared.Clients.Interfaces
{
    public static class IMessagingConfigurationeExtensions
    {
        public static void CopyTo(this IMessagingConfiguration source, IMessagingConfiguration target)
        {
            target.CA = source.CA;
            target.ConsumersCount = source.ConsumersCount;
            target.Crt = source.Crt;
            target.CrtPass = source.CrtPass;
            target.DirtyAck = source.DirtyAck;
            target.Endpoint = source.Endpoint;
            target.MirrorQueues = source.MirrorQueues;
            target.PrefetchCount = source.PrefetchCount;
            target.PrefetchGlobal = source.PrefetchGlobal;
            target.QueueName = source.QueueName;
            target.QueueNames = source.QueueNames;
            target.Type = source.Type;
        }
    }
}
