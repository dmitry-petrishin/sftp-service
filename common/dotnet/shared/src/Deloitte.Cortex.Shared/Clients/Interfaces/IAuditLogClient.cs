﻿using Deloitte.Cortex.Shared.Clients.Audit.Models;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Interfaces
{
    public interface IAuditLogClient
    {
        #region Staging

        Task<bool> FileCreated(string nodeId, string nodeName);
        Task<bool> FileDeleted(string nodeId, string nodeName);
        Task<bool> FolderCreated(string nodeId, string nodeName);
        Task<bool> FolderDeleted(string nodeId, string nodeName);

        #endregion

        #region Common

        Task<bool> ObjectCreated(string objectId, ObjectType objectType, object @object, bool isHidden = false);
        Task<bool> ObjectCreated(string objectId, ObjectType objectType, object @object, string byUser, bool isHidden = false);
        Task<bool> ObjectChanged(string objectId, ObjectType objectType, object oldObject, object newObject, bool isHidden = false);
        Task<bool> ObjectChanged(string objectId, ObjectType objectType, object oldObject, object newObject, string byUser, bool isHidden = false);
        Task<bool> ObjectDeleted(string objectId, ObjectType objectType, bool isHidden = false);
        Task<bool> ObjectDeleted(string objectId, ObjectType objectType, object @object, bool isHidden = false);

        #endregion

        #region Users
        Task<bool> UserCreated(string userEmail, string userType, string byEmail);
        Task<bool> UserAddedToTier(string userEmail, string byEmail, string tier, string tierId = null);
        Task<bool> UserTierRolesChanged(string userEmail, string byEmail, string[] oldRoles, string[] newRoles, string tier, string tierId = null);
        Task<bool> UserRemovedFromTier(string userEmail, string byEmail, string tier, string tierId = null);
        #endregion
    }
}
