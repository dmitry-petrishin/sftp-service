﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    public class UserPermissions
    {
        [JsonProperty]
        public Guid? ClientId { get; private set; }

        [JsonProperty]
        public Guid? EngagementId { get; private set; }

        [JsonProperty]
        public IDictionary<Entity, Actions> Permissions { get; private set; } = new Dictionary<Entity, Actions>();
    }
}