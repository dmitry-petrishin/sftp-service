﻿namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    /// <summary>
    /// Specifies that <see cref="Entity"/> permissions should be promoted to app-tier from client and engagement levels.
    /// </summary>
    public class AppTierAttribute: TierAttribute { }
}