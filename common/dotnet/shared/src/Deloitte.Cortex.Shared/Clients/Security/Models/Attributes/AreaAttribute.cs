﻿using System;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class AreaAttribute: Attribute
    {
        public Area Area { get; }

        public AreaAttribute(Area area)
        {
            Area = area;
        }
    }
}