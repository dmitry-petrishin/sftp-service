﻿using System;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    /// <summary>
    /// Attribute to specify that <see cref="Entity"/> permissions should be promoted to upper tier.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public abstract class TierAttribute: Attribute
    {
        /// <summary>
        /// Justification for permissions to be promoted to upper tier.
        /// </summary>
        public string Justification { get; set; }
    }
}