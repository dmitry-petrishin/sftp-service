﻿using System;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    public class AppRoleSource: RoleSource
    {
        public AppRoleSource(Guid roleId): base(roleId) { }
    }
}