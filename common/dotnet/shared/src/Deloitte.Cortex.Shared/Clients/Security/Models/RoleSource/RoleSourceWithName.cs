﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    public class RoleSourceWithName : RoleSource
    {
        /// <summary>
        /// Role id by which to filter users.
        /// Tier is determined by presence of ClientId/EngagementId.
        /// If both are not present or null - app tier is used.
        /// </summary>
        [JsonProperty]
        public string RoleName { get; set; }
    }
}
