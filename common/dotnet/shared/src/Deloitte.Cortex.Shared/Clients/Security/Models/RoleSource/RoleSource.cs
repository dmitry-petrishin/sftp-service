﻿using System;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    public class RoleSource
    {
        /// <summary>
        /// Role id by which to filter users.
        /// Tier is determined by presence of ClientId/EngagementId.
        /// If both are not present or null - app tier is used.
        /// </summary>
        [JsonProperty]
        public Guid RoleId { get; set; }

        /// <summary>
        /// If present will lookup for users with given client-tier role in this client.
        /// </summary>
        [JsonProperty]
        public Guid? ClientId { get; set; }

        /// <summary>
        /// If present will lookup for users with given engagement-tier role in this engagement.
        /// </summary>
        [JsonProperty]
        public Guid? EngagementId { get; set; }

        [JsonConstructor]
        protected RoleSource() { }

        public RoleSource(Guid roleId)
        {
            RoleId = roleId;
        }
    }
}