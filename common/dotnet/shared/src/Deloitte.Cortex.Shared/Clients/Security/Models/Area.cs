﻿namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    /// <summary>
    /// An area is a grouping that of various <see cref="Entity"/>'s that share common traits.
    /// </summary>
    public enum Area
    {
        /// <summary>
        /// Objects in this area are concentrated around the security infrastructure related to setting up user permissions within Cortex.
        /// </summary>
        Admin,

        /// <summary>
        /// Objects in this area are concentrated around the creation of common items or templates that client and engagement users can use.
        /// </summary>
        ContentLibrary,

        /// <summary>
        /// Objects in this area are concentrated around the setting up of client details and source systems in Cortex.
        /// </summary>
        ClientSetup,

        /// <summary>
        /// Objects in this area are concentrated around performing the audit work required in an engagement.
        /// </summary>
        Engagement
    }
}