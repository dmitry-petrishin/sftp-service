﻿using System;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    public class EngagementRoleSource: RoleSource
    {
        public EngagementRoleSource(Guid roleId, Guid engagementId): base(roleId)
        {
            EngagementId = engagementId;
        }
    }
}