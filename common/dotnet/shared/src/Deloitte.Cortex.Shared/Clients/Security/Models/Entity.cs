﻿using System.ComponentModel;
using Deloitte.Cortex.Shared.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    /// <summary>
    /// Item on a sitemap page for which a user has certain permissions to perform actions.
    /// </summary>
    /// <remarks>Originally named "Object" in Security Matrix it's renamed to avoid frequent collisions with .NET <see cref="System.Object"/>.</remarks>
    [JsonConverter(typeof(StringEnumConverter))]
    [TypeConverter(typeof(EnumHyphenSeparatedConverter<Entity>))]
    public enum Entity: byte
    {

        #region Security and Administration

        /// <summary>
        /// Application Roles. <para/>
        /// <see cref="Actions.View"/> allows to view all application roles. <para/>
        /// <see cref="Actions.Add"/> allows to create new roles. <para/>
        /// <see cref="Actions.Update"/> allows to update any existing role. <para/>
        /// <see cref="Actions.Delete"/> allows to delete any application role.
        /// </summary>
        /// <remarks>
        /// Permissions for this object on any tier will take effect for every tier.
        /// </remarks>
        [Area(Area.Admin)]
        [AppTier(Justification = "Roles access is needed for adding/updating engagement/client users.")]
        Roles,

        /// <summary>
        /// Application users in app-tier.
        /// <see cref="Actions.View"/> allows to view all app-tier users. <para/>
        /// <see cref="Actions.Add"/> allows to add users to app-tier. <para/>
        /// <see cref="Actions.Update"/> allows to edit roles of any user in app-tier. <para/>
        /// <see cref="Actions.Delete"/> allows to remove user from app-tier.
        /// </summary>
        /// <remarks>
        /// Permissions for this object on any tier will take effect for every tier.
        /// </remarks>
        [Area(Area.Admin)]
        AppSetupRoleUsers,

        [Area(Area.Admin)]
        RetentionPeriod,

        [Area(Area.Admin)]
        Rationale,

        /// <summary>
        /// Recertification permission for app-tier users. <para/>
        /// <see cref="Actions.View"/> allows to view recertification status. <para/>
        /// <see cref="Actions.Add"/> allows to recertify.
        /// </summary>
        /// <remarks>
        /// App-tier object.
        /// </remarks>
        [Area(Area.Admin)]
        App_Recertify,

        #endregion

        #region Content Library

        /// <summary>
        /// Source Systems (versions, tables, fields)
        /// </summary>
        [Area(Area.ContentLibrary)]
        [AppTier]
        SourceSystems,

        /// <summary>
        /// Extraction Bundles
        /// </summary>
        [Area(Area.ContentLibrary)]
        [AppTier]
        ExtractionBundles,

        /// <summary>
        /// Releases
        /// </summary>
        [Area(Area.ContentLibrary)]
        Releases,

        /// <summary>
        /// Tags
        /// </summary>
        [Area(Area.ContentLibrary)]
        Tags,

        /// <summary>
        /// Connector Templates
        /// </summary>
        [Area(Area.ContentLibrary)]
        [AppTier]
        ConnectorTemplates,

        /// <summary>
        /// Data Models (Testing)
        /// </summary>
        [Area(Area.ContentLibrary)]
        DataModels,

        [Area(Area.ContentLibrary)]
        PreparationFunctions,

        [Area(Area.ContentLibrary)]
        PreparationTools,

        [Area(Area.ContentLibrary)]
        PracticeManagementItems,

        [Area(Area.ContentLibrary)]
        Datasets,

        [Area(Area.ContentLibrary)]
        ObjectFolders,
        #endregion

        #region Client Setup

        /// <summary>
        /// Application clients. <para/>
        /// <see cref="Actions.View"/> allows to view client details. <para/>
        /// <see cref="Actions.Add"/> on app-tier allows to add client. <para/>
        /// <see cref="Actions.Update"/> allows to edit client. <para/>
        /// <see cref="Actions.Delete"/> allows to remove client.
        /// </summary>
        /// <remarks>
        /// App-tier and client-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        [ClientTier(Justification = "Client read access is needed when editing/creating data models/request.")] // Todo: remove
        Client,

        /// <summary>
        /// Access to client staging storage (including folder structure and access permissions). <para/>
        /// <see cref="Actions.Add"/> allows to create staging storage for client(s). <para/>
        /// <see cref="Actions.View"/> allows to view client staging data.
        /// </summary>
        /// <remarks>
        /// App-tier and client-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        ClientStagingStorage,

        /// <summary>
        /// Client entities. <para/>
        /// <see cref="Actions.View"/> allows to view client entities. <para/>
        /// <see cref="Actions.Add"/> allows to add entity to the client. <para/>
        /// <see cref="Actions.Update"/> allows to edit client entity. <para/>
        /// <see cref="Actions.Delete"/> allows to remove entity from the client.
        /// </summary>
        /// <remarks>
        /// App-tier and client-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        [ClientTier(Justification = "Client entities read access is needed when editing/creating data models/request.")]
        Entities,

        /// <summary>
        /// Engagements
        /// </summary>
        [Area(Area.ClientSetup)]
        Engagements,

        /// <summary>
        /// Client organizations. <para/>
        /// <see cref="Actions.Add"/> allows to create new organization. <para/>
        /// <see cref="Actions.View"/> <see cref="Client"/> allows to view all client details including organization. <para/>
        /// <see cref="Actions.Update"/> <see cref="Client"/> is required to link/unlink new or existing organization to/from the client.
        /// </summary>
        /// <remarks>
        /// App-tier and client-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        Orgs,

        /// <summary>
        /// Client sub-organizations. <para/>
        /// <see cref="Actions.View"/> allows to view client's sub-organizations. <para/>
        /// <see cref="Actions.Add"/> allows to create sub-organization and add it to client. <para/>
        /// <see cref="Actions.Update"/> allows to rename client sub-organization or link it to the entity. <para/>
        /// <see cref="Actions.Delete"/> allows to remove sub-organization from the client.
        /// </summary>
        /// <remarks>
        /// App-tier and client-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        Suborgs,

        /// <summary>
        /// Secure Agent (Download)
        /// </summary>
        [Area(Area.ClientSetup)]
        SecureAgent,

        /// <summary>
        /// Data Sources
        /// </summary>
        [Area(Area.ClientSetup)]
        DataSources,

        /// <summary>
        /// Connections
        /// </summary>
        [Area(Area.ClientSetup)]
        Connections,

        /// <summary>
        /// Blackout Schedule
        /// </summary>
        [Area(Area.ClientSetup)]
        [ClientTier(Justification = "Stored on client-level.")] // see #137957
        BlackoutSchedule,

        /// <summary>
        /// Allows to manage Deloitte client users. <para/>
        /// <see cref="Actions.View"/> allows to view Deloitte client users. <para/>
        /// <see cref="Actions.Add"/> allows to add Deloitte users to the client. <para/>
        /// <see cref="Actions.Update"/> allows to edit roles for any Deloitte client user. <para/>
        /// <see cref="Actions.Delete"/> allows to remove Deloitte user from the client.
        /// </summary>
        /// <remarks>
        /// App-tier, client-tier and engagement-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        [ClientTier(Justification = "Read access to client users is needed for many engagement roles.")] // Todo: clarify if needed
        DeloitteClientRoleUsers,

        /// <summary>
        /// Allows to manage external client users. <para/>
        /// <see cref="Actions.View"/> allows to view external client users. <para/>
        /// <see cref="Actions.Add"/> allows to add external users to the client. <para/>
        /// <see cref="Actions.Update"/> allows to edit roles for any external client user. <para/>
        /// <see cref="Actions.Delete"/> allows to remove external user from the client.
        /// </summary>
        /// <remarks>
        /// App-tier, client-tier and engagement-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        [ClientTier(Justification = "Read access to client users is needed for many engagement roles.")] // Todo: clarify if needed
        ExternalClientRoleUsers,

        /// <summary>
        /// Management of email whitelist for client external users. <para/>
        /// <see cref="Actions.Add"/> allows to add new domains to whitelist. <para/>
        /// <see cref="Actions.Delete"/> allows to remove domains from whitelist.
        /// </summary>
        /// <remarks>
        /// Client-tier object.
        /// </remarks>
        [Area(Area.ClientSetup)]
        ExternalUserDomainWhitelist,

        /// <summary>
        /// Recertification permission for Deloitte and external client users. <para/>
        /// <see cref="Actions.View"/> allows to view recertification status. <para/>
        /// <see cref="Actions.Add"/> allows to recertify (after acknowledgment).
        /// </summary>
        /// <remarks>
        /// Client-tier permission.
        /// </remarks>
        [Area(Area.ClientSetup)]
        Client_Recertify,

        /// <summary>
        /// Acknowledgment permission for external client users. <para/>
        /// <see cref="Actions.View"/> allows to view acknowledgment status (acknowledged / declined). <para/>
        /// <see cref="Actions.Add"/> allows to acknowledge or decline. <para/>
        /// </summary>
        /// <remarks>
        /// Client-tier permission.
        /// </remarks>
        [Area(Area.ClientSetup)]
        Client_External_Acknowledge,

        #endregion

        #region Engagement

        /// <summary>
        /// Data Source Subscriptions
        /// </summary>
        [Area(Area.Engagement)]
        [Description("")]
        DataSourceSubscription,

        /// <summary>
        /// Data Source Workflow (Approval)
        /// </summary>
        [Area(Area.Engagement)]
        DataSourceWorkflow,

        /// <summary>
        /// Data Requests
        /// </summary>
        [Area(Area.Engagement)]
        DataRequests,

        [Area(Area.Engagement)]
        [Description("")]
        DataRequests_Internal,

        [Area(Area.Engagement)]
        [Description("")]
        DataRequests_External,

        [Area(Area.Engagement)]
        [Description("")]
        DataRequests_Transfer,

        /// <summary>
        /// Staging Folder Permissions
        /// </summary>
        [Area(Area.Engagement)]
        StagingFolderStructure,

        /// <summary>
        /// Workpapers
        /// </summary>
        [Area(Area.Engagement)]
        [AppTier]
        WorkItems,

        /// <summary>
        /// Review Notes
        /// </summary>
        [Area(Area.Engagement)]
        ReviewNotes,

        /// <summary>
        /// Tickmarks
        /// </summary>
        [Area(Area.Engagement)]
        Tickmarks,

        /// <summary>
        /// Engagement Closeout
        /// </summary>
        [Area(Area.Engagement)]
        EngagementCloseout,

        /// <summary>
        /// Legal Hold
        /// </summary>
        [Area(Area.Engagement)]
        LegalHold,

        /// <summary>
        /// Allows to manage Deloitte engagement users. <para/>
        /// <see cref="Actions.View"/> allows to view Deloitte engagement users. <para/>
        /// <see cref="Actions.Add"/> allows to add Deloitte users to the engagement. <para/>
        /// <see cref="Actions.Update"/> allows to edit roles for any Deloitte engagement user. <para/>
        /// <see cref="Actions.Delete"/> allows to remove Deloitte user from the engagement.
        /// </summary>
        /// <remarks>
        /// Client-tier and engagement-tier object.
        /// </remarks>
        [Area(Area.Engagement)]
        DeloitteEngagementRoleUsers,

        /// <summary>
        /// Allows to manage external engagement users. <para/>
        /// <see cref="Actions.View"/> allows to view external engagement users. <para/>
        /// <see cref="Actions.Add"/> allows to add external users to the engagement. <para/>
        /// <see cref="Actions.Update"/> allows to edit roles for any external engagement user. <para/>
        /// <see cref="Actions.Delete"/> allows to remove external user from the engagement.
        /// </summary>
        /// <remarks>
        /// Client-tier and engagement-tier object.
        /// </remarks>
        [Area(Area.Engagement)]
        ExternalEngagementRoleUsers,

        /// <summary>
        /// Recertification permission for Deloitte and external engagement users. <para/>
        /// <see cref="Actions.View"/> allows to view recertification status. <para/>
        /// <see cref="Actions.Add"/> allows to recertify (after acknowledgment).
        /// </summary>
        /// <remarks>
        /// Engagement-tier object.
        /// </remarks>
        [Area(Area.Engagement)]
        Engagement_Recertify,

        /// <summary>
        /// Acknowledgment permission for external engagement users. <para/>
        /// <see cref="Actions.View"/> allows to view acknowledgment status (acknowledged / declined). <para/>
        /// <see cref="Actions.Add"/> allows to acknowledge or decline.
        /// </summary>
        /// <remarks>
        /// Engagement-tier object.
        /// </remarks>
        [Area(Area.Engagement)]
        Engagement_External_Acknowledge,

        [Area(Area.Engagement)]
        EngagementWorkpapers,

        #endregion

        #region for tax

        [Area(Area.Engagement)]
        Spreadsheet,

        [Area(Area.Engagement)]
        XmlApp,

        [Area(Area.Engagement)]
        EngagementChecklist,

        [Area(Area.Engagement)]
        ObligationsForEntities,

        [Area(Area.Engagement)]
        ProjectTrackingTasks_Details,

        [Area(Area.Engagement)]
        ProjectTrackingTasks_Comments,
        
        [Area(Area.Engagement)]
        ProjectTrackingTasks_LinkFiles,

        [Area(Area.Engagement)]
        EngagementMappings,

        [Area(Area.Engagement)]
        EngagementAdjustments,

        [Area(Area.ContentLibrary)]
        ContentLibrary_InfoRequests,

        [Area(Area.ContentLibrary)]
        ContentLibrary_InfoRequests_Tags,

        [Area(Area.ContentLibrary)]
        ContentLibrary_Obligations,

        [Area(Area.ContentLibrary)]
        ContentLibrary_Templates,

        [Area(Area.ContentLibrary)]
        ContentLibrary_ProjectTracking,

        [Area(Area.ContentLibrary)]
        ContentLibrary_Datasets

        #endregion
    }
}