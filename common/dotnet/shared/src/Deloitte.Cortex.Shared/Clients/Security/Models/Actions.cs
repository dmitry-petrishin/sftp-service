﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    [Flags]
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Actions
    {
        View = 1,
        Update = 1 << 2,
        Delete = 1 << 3,
        Add = 1 << 4,
        Approve = 1 << 5,
        Submit = 1 << 7
    }
}
