﻿namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    public class UserDetails
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
