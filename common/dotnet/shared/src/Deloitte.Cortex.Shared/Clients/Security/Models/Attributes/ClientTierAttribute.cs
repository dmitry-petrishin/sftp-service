﻿namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    /// <summary>
    /// Specifies that <see cref="Entity"/> permissions should be promoted to client-tier from engagement level.
    /// </summary>
    public class ClientTierAttribute: TierAttribute { }
}