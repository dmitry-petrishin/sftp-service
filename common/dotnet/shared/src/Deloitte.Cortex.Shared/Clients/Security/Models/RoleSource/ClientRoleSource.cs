﻿using System;

namespace Deloitte.Cortex.Shared.Clients.Security.Models
{
    public class ClientRoleSource: RoleSource
    {
        public ClientRoleSource(Guid roleId, Guid clientId): base(roleId)
        {
            ClientId = clientId;
        }
    }
}