﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Security.Models;

namespace Deloitte.Cortex.Shared.Clients.Security
{
    public interface ISecurityClient
    {
        Task<bool> HasSetupPermissionsAsync(IReadOnlyDictionary<string, Actions> permissions);

        Task<bool> HasClientPermissionsAsync(Guid clientId, IReadOnlyDictionary<string, Actions> permissions);

        Task<bool> HasEngagementPermissionsAsync(Guid engagementId, IReadOnlyDictionary<string, Actions> permissions);
    }
}
