﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Security.Models;

namespace Deloitte.Cortex.Shared.Clients.Security
{
    public interface IUserClient
    {
        Task<UserDetails[]> GetUsersByPermissionsAsync(UserPermissions[] permissions);
        Task<UserDetails[]> GetUsersByRoleNameAsync(string roleName, Guid? clientId = null, Guid? engagementId = null);
        Task<Dictionary<Guid, UserDetails[]>> GetUsersGroupedByRoleIdsAsync(RoleSource[] roleSources);
    }
}