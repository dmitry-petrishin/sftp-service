﻿using System;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Security.Models;

namespace Deloitte.Cortex.Shared.Clients.Security
{
    public static class UserClientExtensions
    {
        public static Task<UserDetails[]> GetUsersByAppRoleNameAsync(this IUserClient userClient, string roleName)
        {
            return userClient.GetUsersByRoleNameAsync(roleName);
        }

        public static Task<UserDetails[]> GetUsersByClientRoleNameAsync(this IUserClient userClient, Guid clientId, string roleName)
        {
            return userClient.GetUsersByRoleNameAsync(roleName, clientId: clientId);
        }

        public static Task<UserDetails[]> GetUsersByEngagementRoleNameAsync(this IUserClient userClient, Guid engagementId, string roleName)
        {
            return userClient.GetUsersByRoleNameAsync(roleName, engagementId: engagementId);
        }

        public static async Task<UserDetails[]> GetUsersByRoleIdAsync(this IUserClient userClient, RoleSource roleSource)
        {
            var usersByRole = await userClient.GetUsersGroupedByRoleIdsAsync(new[] { roleSource });
            return usersByRole[roleSource.RoleId];
        }

        public static Task<UserDetails[]> GetUsersByRoleIdAsync(this IUserClient userClient,
            Guid roleId, Guid? clientId = null, Guid? engagementId = null)
        {
            return userClient.GetUsersByRoleIdAsync(new RoleSource(roleId)
            {
                ClientId = clientId,
                EngagementId = engagementId
            });
        }
    }
}