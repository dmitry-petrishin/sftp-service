﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Clients.Security.Models;
using Deloitte.Cortex.Shared.ExceptionHandling;

namespace Deloitte.Cortex.Shared.Clients.Security
{
    public static class SecurityClientExtensions
    {
        public static Task<bool> HasSetupPermissionAsync(this ISecurityClient client, string entity, Actions actions)
        {
            return client.HasSetupPermissionsAsync(new Dictionary<string, Actions>
            {
                { entity, actions }
            });
        }

        public static Task<bool> HasClientPermissionAsync(this ISecurityClient client, Guid clientId, string entity, Actions actions)
        {
            return client.HasClientPermissionsAsync(clientId, new Dictionary<string, Actions>
            {
                { entity, actions }
            });
        }

        public static Task<bool> HasEngagementPermissionAsync(this ISecurityClient client, Guid engagementId, string entity, Actions actions)
        {
            return client.HasEngagementPermissionsAsync(engagementId, new Dictionary<string, Actions>
            {
                { entity, actions }
            });
        }

        public static async Task VerifySetupPermissionsAsync(this ISecurityClient client,
            IReadOnlyDictionary<string, Actions> permissions)
        {
            if (!await client.HasSetupPermissionsAsync(permissions))
                throw new ForbiddenException($"User doesn't have required app permissions: {permissions.ToDictionaryString()}.");
        }

        public static Task VerifySetupPermissionAsync(this ISecurityClient client,
            string entity, Actions actions)
        {
            return client.VerifySetupPermissionsAsync(new Dictionary<string, Actions> { { entity, actions } });
        }

        public static async Task VerifyClientPermissionsAsync(this ISecurityClient client, Guid clientId,
            IReadOnlyDictionary<string, Actions> permissions)
        {
            if (!await client.HasClientPermissionsAsync(clientId, permissions))
                throw new ForbiddenException($"User doesn't have required permissions for this client: {permissions.ToDictionaryString()}.");
        }

        public static Task VerifyClientPermissionAsync(this ISecurityClient client, Guid clientId,
            string entity, Actions actions)
        {
            return client.VerifyClientPermissionsAsync(clientId, new Dictionary<string, Actions> { { entity, actions } });
        }

        public static async Task VerifyEngagementPermissionsAsync(this ISecurityClient client, Guid engagementId,
            IReadOnlyDictionary<string, Actions> permissions)
        {
            if (!await client.HasEngagementPermissionsAsync(engagementId, permissions))
                throw new ForbiddenException($"User doesn't have required permissions for this engagement: {permissions.ToDictionaryString()}.");
        }

        public static Task VerifyEngagementPermissionAsync(this ISecurityClient client, Guid engagementId,
            string entity, Actions actions)
        {
            return client.VerifyEngagementPermissionsAsync(engagementId, new Dictionary<string, Actions> { { entity, actions } });
        }
    }
}