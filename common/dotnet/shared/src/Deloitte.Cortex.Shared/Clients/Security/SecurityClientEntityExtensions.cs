﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Security.Models;

namespace Deloitte.Cortex.Shared.Clients.Security
{
    /// <summary>
    /// Extension methods for providing <see cref="Entity"/> enum members to <see cref="ISecurityClient"/> instead of plain string.
    /// </summary>
    public static class SecurityClientEntityExtensions
    {
        private static IReadOnlyDictionary<string, Actions> ToDictionary(this IReadOnlyDictionary<Entity, Actions> permissions) =>
            permissions.ToDictionary(p => p.Key.ToString(), p => p.Value);

        public static Task<bool> HasSetupPermissionsAsync(this ISecurityClient client, IReadOnlyDictionary<Entity, Actions> permissions) =>
            client.HasSetupPermissionsAsync(permissions.ToDictionary());

        public static Task<bool> HasClientPermissionsAsync(this ISecurityClient client, Guid clientId,
            IReadOnlyDictionary<Entity, Actions> permissions) =>
            client.HasClientPermissionsAsync(clientId, permissions.ToDictionary());

        public static Task<bool> HasEngagementPermissionsAsync(this ISecurityClient client, Guid engagementId,
            IReadOnlyDictionary<Entity, Actions> permissions) =>
            client.HasEngagementPermissionsAsync(engagementId, permissions.ToDictionary());

        public static Task<bool> HasSetupPermissionAsync(this ISecurityClient client, Entity entity, Actions actions) =>
            client.HasSetupPermissionAsync(entity.ToString(), actions);

        public static Task<bool> HasClientPermissionAsync(this ISecurityClient client, Guid clientId, Entity entity, Actions actions) =>
            client.HasClientPermissionAsync(clientId, entity.ToString(), actions);

        public static Task<bool> HasEngagementPermissionAsync(this ISecurityClient client, Guid engagementId, Entity entity, Actions actions) =>
            client.HasEngagementPermissionAsync(engagementId, entity.ToString(), actions);

        public static Task VerifySetupPermissionsAsync(this ISecurityClient client,
            Dictionary<Entity, Actions> permissions) =>
            client.VerifySetupPermissionsAsync(permissions.ToDictionary(p => p.Key.ToString(), p => p.Value));

        public static Task VerifySetupPermissionAsync(this ISecurityClient client,
            Entity entity, Actions actions) =>
            client.VerifySetupPermissionAsync(entity.ToString(), actions);

        public static Task VerifyClientPermissionsAsync(this ISecurityClient client, Guid clientId,
            Dictionary<Entity, Actions> permissions) =>
            client.VerifyClientPermissionsAsync(clientId, permissions.ToDictionary());

        public static Task VerifyClientPermissionAsync(this ISecurityClient client, Guid clientId,
            Entity entity, Actions actions) =>
            client.VerifyClientPermissionAsync(clientId, entity.ToString(), actions);

        public static Task VerifyEngagementPermissionsAsync(this ISecurityClient client, Guid engagementId,
            IReadOnlyDictionary<Entity, Actions> permissions) =>
            client.VerifyEngagementPermissionsAsync(engagementId, permissions.ToDictionary());

        public static Task VerifyEngagementPermissionAsync(this ISecurityClient client, Guid engagementId,
            Entity entity, Actions actions) =>
            client.VerifyEngagementPermissionAsync(engagementId, entity.ToString(), actions);
    }
}