﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Authorization;
using Deloitte.Cortex.Shared.Clients.Security.Models;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.Clients.Security
{
    public class UserClient: IUserClient
    {
        protected readonly ServiceClient Client;

        public UserClient(string endpoint, ILogger<UserClient> logger, IAuthenticationService authenticationService)
        {
            Client = new ServiceClient(endpoint.UriCombine("users/"), logger, new RequestSettings
            {
                ForwardHttpException = true,
                Authentication = new ServiceAuthentication(authenticationService)
            });
        }

        public Task<UserDetails[]> GetUsersByPermissionsAsync(UserPermissions[] permissions) =>
            Client.PostAsync<UserDetails[]>("by-permissions", permissions);

        public Task<UserDetails[]> GetUsersByRoleNameAsync(string roleName, Guid? clientId = null, Guid? engagementId = null) =>
            Client.PostAsync<UserDetails[]>("by-role-name", new
            {
                roleName = roleName,
                clientId = clientId,
                engagementId = engagementId
            });

        public Task<Dictionary<Guid, UserDetails[]>> GetUsersGroupedByRoleIdsAsync(RoleSource[] roleSources) =>
            Client.PostAsync<Dictionary<Guid, UserDetails[]>>("grouped-by-role-id", roleSources);
    }
}