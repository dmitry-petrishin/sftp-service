﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Authorization;
using Deloitte.Cortex.Shared.Clients.Security.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.Clients.Security
{
    /// <summary>
    /// Security service client for checking permissions.
    /// Works with user authentication by default.
    /// </summary>
    public class SecurityClient: ISecurityClient
    {
        protected readonly IHttpContextAccessor HttpContextAccessor;
        protected readonly ServiceClient Client;

        public SecurityClient(string endpoint, ILogger<SecurityClient> logger, IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;

            Client = new ServiceClient(endpoint, logger, new RequestSettings
            {
                ForwardHttpException = true,
                Authentication = new UserAuthentication(httpContextAccessor),
                IsValidResponse = response => response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.Forbidden
            });
        }

        private bool IsServiceCall => HttpContextAccessor.HttpContext.User.IsInRole(CallerTypeRole.Service);

        public async Task<bool> HasSetupPermissionsAsync(IReadOnlyDictionary<string, Actions> permissions)
        {
            if (IsServiceCall) return true;

            var response =  await Client.PostAsync("permission/setup", permissions);
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> HasEngagementPermissionsAsync(Guid engagementId, IReadOnlyDictionary<string, Actions> permissions)
        {
            if (IsServiceCall) return true;

            var response = await Client.PostAsync($"permission/engagement/{engagementId}", permissions);
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> HasClientPermissionsAsync(Guid clientId, IReadOnlyDictionary<string, Actions> permissions)
        {
            if (IsServiceCall) return true;

            var response = await Client.PostAsync($"permission/client/{clientId}", permissions);
            return response.IsSuccessStatusCode;
        }
    }
}