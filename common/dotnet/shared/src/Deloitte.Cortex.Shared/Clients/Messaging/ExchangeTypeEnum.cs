﻿namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    public enum ExchangeTypeEnum
    {
        Direct,
        Topic,
    }
}
