﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using Timer = Deloitte.Cortex.Shared.AspNetCore.Timer;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    public class RabbitClient : IMessagingClient
    {
        private readonly IMessagingConfiguration _configuration;
        private readonly ILogger<RabbitClient> _logger;
        private readonly ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private readonly List<Uri> _endpoints;
        private Uri _selectedEndpoint;

        private IConnection Connection
        {
            get
            {
                if (_connection == null || !_connection.IsOpen)
                {
                    try
                    {
                        _connection = CreateConnection();
                        _connection.ConnectionShutdown += (s, a) =>
                        {
                            _logger.LogDebug("Connection was forced to close");
                            SetNextEndpoint();
                            _connection = _connectionFactory.CreateConnection();
                        };
                    }
                    catch (BrokerUnreachableException exception)
                    {
                        _logger.LogError($"Handling internal exception ({exception.GetType().Name}).\n" + $"Exception: {exception}");
                        SetNextEndpoint();
                    }
                }
                return _connection;
            }
        }

        private IConnection CreateConnection(int attempt = 0)
        {
            try
            {
                return _connectionFactory.CreateConnection();
            }
            catch (BrokerUnreachableException exception)
            {
                _logger.LogError($"Handling internal exception ({exception.GetType().Name}).\n" + $"Exception: {exception}");

                SetNextEndpoint();
                if (attempt > 50)
                {
                    throw;
                }
                return CreateConnection(attempt + 1);
            }
        }

        public RabbitClient(IMessagingConfiguration configuration, ILogger<RabbitClient> logger)
        {
            _configuration = configuration;
            _logger = logger;

            _endpoints = GetEndpoints();
            _selectedEndpoint = _endpoints.First();

            _connectionFactory = new ConnectionFactory
            {
                Uri = _selectedEndpoint,
                RequestedHeartbeat = MessagingConstants.DefaultRequestHeartbeat,
                AutomaticRecoveryEnabled = true,
                NetworkRecoveryInterval = TimeSpan.FromSeconds(MessagingConstants.NetworkRecoveryIntervalSec),
                AuthMechanisms = new AuthMechanismFactory[] { new ExternalMechanismFactory(), new PlainMechanismFactory() },
            };
            var sslOptions = GetSslOptions();
            if (sslOptions != null)
            {
                _connectionFactory.Ssl = sslOptions;
            }
        }

        public async Task<bool> SendToQueueAsync<T>(T obj) where T : class
        {
            var queueName = GetQueueName<T>();

            var json = JsonConvert.SerializeObject(obj);
            var bytes = Encoding.UTF8.GetBytes(json);

            var timedResult = await Timer.TimeAsync(async () => await SendToQueue(queueName, bytes));
            if (timedResult.Result)
            {
                _logger.LogDebug($"Sent {typeof(T).Name} in {timedResult.ExecutionTime}.");
            }

            return timedResult.Result;
        }

        public string GetQueueName<T>() 
        {
            var queueName = QueueHelper.GetQueueByType(typeof(T))
                            ?? QueueHelper.GetQueueByType<T>(_configuration.QueueNames)
                            ?? _configuration.QueueName;
            if (string.IsNullOrEmpty(queueName))
            {
                throw new NotSupportedException($"Unknown queue for argument type: {typeof(T).Name}");
            }
            return queueName;
        }

        public Task<T> RecieveFromQueueAsync<T>() where T : class
        {
            T result = null;
            var queueName = GetQueueName<T>();

            using (var channel = Connection.CreateModel())
            {
                var arguments = _configuration.MirrorQueues
                    ? new Dictionary<string, object> { {"ha-mode", "all"}}
                    : null;

                channel.QueueDeclare(
                    queue: queueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: arguments);

                var data = channel.BasicGet(queueName, false);
                if (data != null)
                {
                    _logger.LogInformation($"{DateTime.UtcNow}>Received a message");
                    var body = data.Body;
                    var json = Encoding.UTF8.GetString(body);
                    _logger.LogDebug($"{DateTime.UtcNow}>Content: {json}");
                    result = JsonConvert.DeserializeObject<T>(json);

                    channel.BasicAck(data.DeliveryTag, false);
                }
            }

            return Task.FromResult(result);
        }

        public Task OnMessageAsync<T>(Func<T, Task> callback) where T : class
        {
            T result;
            var queueName = GetQueueName<T>();
            var channel = Connection.CreateModel();

            channel.QueueDeclare(
                queue: queueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );

            channel.ModelShutdown += async (sender, args) =>
            {
                _logger.LogDebug("Channel was forced to close");
                await OnMessageAsync(callback);
            };

            for (int i = 0; i < _configuration.ConsumersCount; i++)
            {
                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += async (model, args) =>
                {
                    var body = args.Body;
                    try
                    {
                        _logger.LogInformation($"{DateTime.UtcNow}>Message received");

                        var json = Encoding.UTF8.GetString(body);
                        _logger.LogDebug($"{DateTime.UtcNow}>Content: {json}");
                        result = JsonConvert.DeserializeObject<T>(json);

                        if (_configuration.DirtyAck)
                        {
                            channel.BasicAck(args.DeliveryTag, false);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                            callback(result);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        }
                        else
                        {
                            await callback(result);
                            channel.BasicAck(args.DeliveryTag, false);
                        }
                    }
                    catch (Exception exc)
                    {
                        await SendToQueue($"error-{queueName}", body);
                        channel.BasicNack(args.DeliveryTag, false, false);
                        _logger.LogError(exc, $"Error while trying to read from queue");
                    }
                };

                channel.BasicQos(0, _configuration.PrefetchCount, _configuration.PrefetchGlobal);
                channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);
            }

            return Task.CompletedTask;
        }

        #region Private methods

        private List<Uri> GetEndpoints()
        {
            var result = new List<Uri>();
            var split = _configuration.Endpoint.Split(',');
            var endpoint = new Uri(split[0]);
            result.Add(endpoint);
            result.AddRange(split.Skip(1).Select(x => new Uri($"{endpoint.Scheme}://{x}")));

            return result;
        }

        private SslOption GetSslOptions()
        {
            SslOption sslOptions = null;

            if (!string.IsNullOrWhiteSpace(_configuration.Crt))
            {
                var certs = new X509Certificate2Collection();

                _logger.LogInformation($"Importing client certificate {_configuration.Crt} {(string.IsNullOrWhiteSpace(_configuration.CrtPass) ? "without" : "with")} passsword");
                certs.Add(string.IsNullOrWhiteSpace(_configuration.CrtPass)
                    ? new X509Certificate2(_configuration.Crt)
                    : new X509Certificate2(_configuration.Crt, _configuration.CrtPass));

                sslOptions = new SslOption { Certs = certs, Enabled = true };

                if (!string.IsNullOrWhiteSpace(_configuration.CA))
                {
                    var serverCert = new X509Certificate2(_configuration.CA);
                    var serverCertTumbprint = serverCert.GetCertHashString();

                    sslOptions.CertificateValidationCallback = (sender, certificate, chain, errors) =>
                    {
                        var root = chain.ChainElements[chain.ChainElements.Count - 1].Certificate;
                        if (root.Thumbprint == serverCertTumbprint)
                        {
                            return true;
                        }
                        else
                        {
                            _logger.LogCritical("Failed to validate CA");
                            return false;
                        }
                    };
                }
            }

            return sslOptions;
        }

        private void SetNextEndpoint()
        {
            var index = _endpoints.IndexOf(_selectedEndpoint);
            _selectedEndpoint = index < _endpoints.Count - 1 ? _endpoints[index + 1] : _endpoints[0];
            _logger.LogInformation($"RabbitMQ endpoint switched to {_selectedEndpoint}");
            _connectionFactory.Uri = _selectedEndpoint;
        }

        private Task<bool> SendToQueue(string queueName, byte[] bytes)
        {
            try
            {
                using (var channel = Connection.CreateModel())
                {
                    channel.QueueDeclare(
                        queue: queueName,
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    channel.BasicPublish(
                        exchange: "",
                        routingKey: queueName,
                        basicProperties: null,
                        body: bytes);
                }
            }
            catch (Exception exc)
            {
                _logger.LogError(exc, $"{DateTime.UtcNow}>Error while sending to queue {queueName}");
                return Task.FromResult(false);
            }

            return Task.FromResult(true);
        }
        

        #endregion
    }
}
