﻿namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    internal class MessagingConstants
    {
        public const int DefaultRequestHeartbeat = 20;
        public const int DefaultConsumersCount = 1;
        public const int DefaultPrefetchCount = 10;
        public const int NetworkRecoveryIntervalSec = 10;
    }
}
