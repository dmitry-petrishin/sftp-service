﻿using System;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Messaging.DbMessaging
{
    public class QueueItemModel : IQueueItem
    {
        public string Id { get; set; }
        
        public DateTime? CreationDate { get; set; }
        
        public DateTime? StartProcessingDate { get; set; }
        
        public string ProcessorToken { get; set; }

        public DateTime? EndProcessingDate { get; set; }

        public JObject Payload {get;set;}
    }
}
