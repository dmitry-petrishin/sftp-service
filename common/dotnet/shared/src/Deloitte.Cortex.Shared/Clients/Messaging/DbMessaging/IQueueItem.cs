﻿using System;
using Deloitte.Cortex.Shared.Serialization;

namespace Deloitte.Cortex.Shared.Clients.Messaging.DbMessaging
{
    public interface IQueueItem : IIdentifier
    {
        DateTime? CreationDate { get; set; }
        DateTime? StartProcessingDate { get; set; }
        string ProcessorToken { get; set; }
        DateTime? EndProcessingDate { get; set; }
    }
}
