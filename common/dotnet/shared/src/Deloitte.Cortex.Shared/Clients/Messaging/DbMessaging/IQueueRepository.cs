﻿using Deloitte.Cortex.Shared.DataAccess;

namespace Deloitte.Cortex.Shared.Clients.Messaging.DbMessaging
{
    public interface IQueueRepository : IQueueRepository<QueueItemModel>
    {
    }

    public interface IQueueRepository<T> : IRepository<T>
        where T: class, IQueueItem
    {
    }
}
