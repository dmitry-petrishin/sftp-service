﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Deloitte.Cortex.Shared.DataAccess;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Messaging.DbMessaging
{
    public class RepositoryMessagingClient : IMessagingClient
    {
        private readonly IQueueRepository _repository;

        private readonly TimeSpan PollTimeout = TimeSpan.FromSeconds(1);

        private readonly string _processorToken = Guid.NewGuid().ToString();

        private readonly ILogger _logger;

        protected bool DirtyAck { get; set; } = false;

        protected bool DeleteFailed { get; set; } = false;

        public RepositoryMessagingClient(
            IQueueRepository repository,
            ILogger logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<bool> SendToQueueAsync<T>(T obj) where T : class
        {
            var repo = GetRepository<T>();

            var entry = new QueueItemModel
            {
                CreationDate = DateTime.UtcNow,
                Payload = JObject.FromObject(obj)
            };

            await repo.CreateItemAsync(entry);

            return true;
        }

        // This was for one-client, many-queues mapping. Probably don't need
        private IQueueRepository GetRepository<T>() where T : class
        {
            return _repository;
        }

        public async Task<T> RecieveFromQueueAsync<T>() where T : class
        {
            var item = await RecieveFromQueueInnerAsync<T>();
            if (item == null)
            {
                return null;
            }

            _logger.LogDebug("Recieved message:\n" + JObject.FromObject(item));

            //item.ProcessorToken = _processorToken;
            //item.StartProcessingDate = DateTime.UtcNow;
            //item.EndProcessingDate = DateTime.UtcNow;

            var payload = item.Payload.ToObject<T>();

            var repo = GetRepository<T>();
            await repo.DeleteItemAsync(item.Id);

            return payload;
        }

        public async Task OnMessageAsync<T>(Func<T, Task> callback) where T : class
        {
            await OnMessageInnerAsync(callback);
        }

        public string GetQueueName<T>()
        {
            return typeof(T).Name;
        }

        private async Task OnMessageInnerAsync<T>(Func<T, Task> callback) where T : class
        {
            var repo = GetRepository<T>();

            await Task.Yield();
            while (true)
            {
                try
                {
                    await ProcessNextMessage(callback, repo);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception in reader loop");
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }
            }
        }

        private async Task ProcessNextMessage<T>(Func<T, Task> callback, IRepository<QueueItemModel> repo)
            where T : class
        {
            _logger.LogDebug("Started to listen for messages");
            var item = await RecieveFromQueueInnerAsync<T>();
            if (item == null)
            {
                //TODO: cleanup old messages if none in queue?
                await Task.Delay(PollTimeout);
                return;
            }
            _logger.LogDebug("Recieved message:\n" + JObject.FromObject(item));

            item.ProcessorToken = _processorToken;
            item.StartProcessingDate = DateTime.UtcNow;
            var itemId = item.Id;
            var updated = await repo.UpdateItemAsync(
                e => e.Id == itemId && e.ProcessorToken == null, item);

            if (updated == 0)
            {
                _logger.LogDebug($"Message {item.Id} was taken to execution with another token");
                return;
            }

            _logger.LogDebug($"Updated message {item.Id} with token {_processorToken}. Modified: {updated}");

            var value = item.Payload.ToObject<T>();

            try
            {
                var cbTask = callback(value);
                if (DirtyAck)
                {
                    _logger.LogDebug($"Fired callback on message {item.Id}");
                }
                else
                {
                    await cbTask;
                    _logger.LogDebug($"Done executing callback on message {item.Id}");
                }
                await repo.DeleteItemAsync(item.Id);
            }
            catch (Exception)
            {
                _logger.LogError($"Error when executing callback on message {item.Id}");
                await HandleProcessingFailure<T>(repo, item);
                throw;
            }
            finally
            {
                _logger.LogDebug($"Done with callback on message {item.Id}");
            }
        }

        private async Task HandleProcessingFailure<T>(IRepository<QueueItemModel> repo, QueueItemModel item) where T : class
        {
            try
            {
                if (DeleteFailed)
                {
                    await repo.DeleteItemAsync(item.Id);
                }
                else
                {
                    item.ProcessorToken = null;
                    item.StartProcessingDate = null;
                    item.EndProcessingDate = null;
                    await repo.UpdateItemAsync(item);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Also failed on failure processing of item {item.Id}: " + ex);
            }
        }

        private async Task<QueueItemModel> RecieveFromQueueInnerAsync<T>() where T : class
        {
            var repo = GetRepository<T>();

            var entries = await repo.GetItemsAsync(
                e => e.StartProcessingDate == null,
                e => e.CreationDate,
                0,
                1,
                sortAsc: true);

            if (entries.TotalCount == 0)
            {
                return null;
            }

            return entries.Items.First();
        }
    }
}
