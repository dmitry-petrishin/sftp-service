﻿using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    /// <summary>
    /// Completely new messaging interface, with extended capabilities and proper queues handling
    /// </summary>
    public interface IMessagingClient2
    {
        event MessageReceivedHandler MessageReceived;
        bool SkipProcessingErrors { get; set; }

        void ExchangeDeclare(string name, ExchangeTypeEnum type);

        /// <summary>
        /// Declare anonymous temporary queue
        /// </summary>
        string QueueDeclare();

        /// <summary>
        /// Declare named durable queue
        /// </summary>
        string QueueDeclare(string queue);

        void QueueBind(string queue, string exchange, string routingKey);

        void QueueBind(string queue, string exchange, IEnumerable<string> routingKeys);

        void Consume(string queue, bool autoAck);

        void Publish(string exchangeName, string routingKey, object obj);
    }
}
