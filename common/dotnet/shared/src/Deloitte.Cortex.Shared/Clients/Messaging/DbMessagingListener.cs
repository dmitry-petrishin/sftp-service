﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Messaging.DbMessaging;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
   public abstract class DbMessagingListener<T> 
        where T: class, IQueueItem
    {
        protected bool DirtyAck { get; set; }

        protected bool DeleteFailed { get; set; }

        protected TimeSpan PollInterval { get; set; } = TimeSpan.FromSeconds(1);

        protected string Token { get; set; } = Guid.NewGuid().ToString();

        private readonly IQueueRepository<T> _repository;

        private readonly ILogger _logger;

        protected DbMessagingListener(
            IQueueRepository<T> repository, 
            ILogger logger = null)
        {
            _repository = repository;
            _logger = logger;
        }

        public Task Start(CancellationToken token = default(CancellationToken))
        {
            return DoLoop(token);
        }

        public abstract Task Callback(T arg);

        public async Task<T> Peek()
        {
            var entries = await _repository.GetItemsAsync(
                e => e.StartProcessingDate == null,
                e => e.CreationDate,
                0,
                1,
                sortAsc: true);
            
            return entries.Items.FirstOrDefault();
        }

        private async Task DoLoop(CancellationToken token) 
        {
            _logger.LogDebug($"Started to listen for messages: {typeof(T).Name}");
            await Task.Yield();
            while (!token.IsCancellationRequested)
            {
                try
                {
                    await ProcessNextMessage();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Exception in reader loop: " + ex);
                    await Task.Delay(TimeSpan.FromSeconds(1), token);
                }
            }
        }

        private async Task ProcessNextMessage()
        {
            var item = await Peek();
            if (item == null)
            {
                //TODO: cleanup old messages if none in queue?
                await Task.Delay(PollInterval);
                return;
            }
            _logger.LogDebug("Recieved message:\n" + JObject.FromObject(item));

            item.ProcessorToken = Token;
            item.StartProcessingDate = DateTime.UtcNow;
            var itemId = item.Id;
            var updated = await _repository.UpdateItemAsync(
                e => e.Id == itemId && e.ProcessorToken == null, item);

            if (updated == 0)
            {
                _logger.LogDebug($"Message {item.Id} was taken to execution with another token");
                return;
            }

            _logger.LogDebug($"Updated message {item.Id} with token {Token}. Modified: {updated}");

            try
            {
                var cbTask = this.Callback(item);
                if (DirtyAck)
                {
                    _logger.LogDebug($"Fired callback on message {item.Id}");
                }
                else
                {
                    await cbTask;
                    _logger.LogDebug($"Done executing callback on message {item.Id}");
                }
                await _repository.DeleteItemAsync(item.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when executing callback on message {item.Id}");
                await HandleProcessingFailure(item, ex);
                throw;
            }
            finally
            {
                _logger.LogDebug($"Done with callback on message {item.Id}");
            }
        }

        protected virtual Task HandleProcessingFailure(T item)
        {
            return this.HandleProcessingFailure(item, null);
        }

        protected virtual async Task HandleProcessingFailure(T item, Exception failException)
        {
            try
            {
                if (DeleteFailed)
                {
                    await _repository.DeleteItemAsync(item.Id);
                }
                else
                {
                    item.ProcessorToken = null;
                    item.StartProcessingDate = null;
                    item.EndProcessingDate = null;
                    await _repository.UpdateItemAsync(item);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Also failed on failure processing of item {item.Id}: " + ex);
            }
        }

    }
}