﻿using System;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Deloitte.Cortex.Shared.Clients.Messaging.Faux;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    public static class MessagingFactory
    {
        public static IMessagingClient GetClient(IMessagingConfiguration config, ILoggerFactory loggerFactory)
        {
            switch (config.Type)
            {
                case "Rabbit":
                    return new RabbitClient(config, loggerFactory.CreateLogger<RabbitClient>());
                case "List":
                    return new ListMessagingClient();
                default:
                    throw new Exception("Unknown type of messaging client: " + config.Type);
            }
        }
    }
}