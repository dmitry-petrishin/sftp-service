﻿using System.Threading;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Interfaces;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    public abstract class BaseMessagingListener<T> where T: class
    {
        private readonly IMessagingClient _client;

        protected BaseMessagingListener(IMessagingClient client)
        {
            _client = client;
        }

        public async Task StartAsync(CancellationTokenSource cts = null)
        {
            // set handling for cancellation token
            await _client.OnMessageAsync<T>(ProcessReceivedMessage);
        }

        public abstract Task ProcessReceivedMessage(T arg);
    }
}