﻿using System;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Clients.Messaging.Faux
{
    public class FauxMessagingClient : IMessagingClient2
    {
        private readonly List<FauxQueue> _queues = new List<FauxQueue>();

        public event MessageReceivedHandler MessageReceived;
        public bool SkipProcessingErrors { get; set; }

        public void ExchangeDeclare(string name, ExchangeTypeEnum type)
        {
            //throw new NotImplementedException();
        }

        public string QueueDeclare()
        {
            //throw new NotImplementedException();
            return "";
        }

        public string QueueDeclare(string queue)
        {
            //throw new NotImplementedException();
            return queue;
        }

        public void QueueBind(string queue, string exchange, string routingKey)
        {
            //throw new NotImplementedException();
        }

        public void QueueBind(string queue, string exchange, IEnumerable<string> routingKeys)
        {
            //throw new NotImplementedException();
        }

        public void Consume(string queue, bool autoAck)
        {
            //throw new NotImplementedException();
        }

        public void Publish(string exchangeName, string routingKey, object obj)
        {
            //throw new NotImplementedException();
        }
    }
}