﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Messaging.Faux
{
        public class ListMessagingClient : IMessagingClient
    {
        private readonly IList<string> _objects = new List<string>();

        public Task<bool> SendToQueueAsync<T>(T obj) where T : class
        {
            var json = JsonConvert.SerializeObject(obj);
            _objects.Add(json);
            return Task.FromResult(true);
        }

        public Task<bool> SendToQueueAsync<T>(IList<T> obj) where T : class
        {
            var json = JsonConvert.SerializeObject(obj);
            _objects.Add(json);
            return Task.FromResult(true);
        }

        public Task<T> RecieveFromQueueAsync<T>() where T : class
        {
            T result = null;
            var json = _objects.FirstOrDefault();

            if (!string.IsNullOrEmpty(json))
            {
                _objects.Remove(json);
                result = JsonConvert.DeserializeObject<T>(json);
            }

            return Task.FromResult(result);
        }

        public async Task OnMessageAsync<T>(Func<T, Task> callback) where T : class
        {
            while (_objects.Any())
            {
                var @object = await RecieveFromQueueAsync<T>();
                await callback(@object);
            }
        }

        public string GetQueueName<T>()
        {
            return "Fake";
        }
    }
}