﻿using System;
using Deloitte.Cortex.Shared.Clients.Messaging.Faux;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    /// <summary>
    /// New factory for proper clients
    /// </summary>
    public static class Factory
    {
        public static IMessagingClient2 GetClient(Configuration config, ILoggerFactory loggerFactory)
        {
            switch (config.Type)
            {
                case "Rabbit":
                    return new RabbitMQ.RabbitMQClient(config, loggerFactory.CreateLogger<RabbitMQ.RabbitMQClient>());
                case "List":
                    return new FauxMessagingClient();
                default:
                    throw new Exception("Unknown type of messaging client: " + config.Type);
            }
        }
    }
}