﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    /// <summary>
    /// Proper messaging configuration. Without interface,
    /// without queue names, consumer and ack settings, since it is specific to use case and ONE connection could be used for various consumer models
    /// </summary>
    public class Configuration
    {
        public Configuration()
        {
        }

        public string Type { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Endpoint { get; set; }

        public string CA { get; set; } = string.Empty;
        public string Crt { get; set; } = string.Empty;
        public string CrtPass { get; set; } = string.Empty;

        public bool MirrorQueues {get;set; } = false;
    }
}
