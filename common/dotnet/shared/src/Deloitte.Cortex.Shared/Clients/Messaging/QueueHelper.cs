﻿using System;
using System.Collections.Generic;
using Deloitte.Cortex.Shared.Clients.Audit.Models;
using Deloitte.Cortex.Shared.Clients.Notification.Models;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    public static class QueueHelper
    {
        private const string NotificationQueueName = "notification";
        private const string AuditLogQueueName = "auditlog";
        
        public static string GetQueueByType(Type type)
        {
            if (type == typeof(NotificationDto))
            {
                return NotificationQueueName;
            }
            if (type == typeof(AuditLogDto))
            {
                return AuditLogQueueName;
            }

            return null;
        }

        public static string GetQueueByType<T>(IDictionary<string, string> lookup)
        {
            if (lookup == null)
            {
                return null;
            }

            var name = typeof(T).Name;
            string result;
            lookup.TryGetValue(name, out result);
            return result;
        }
    }
}