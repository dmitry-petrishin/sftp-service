﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Deloitte.Cortex.Shared.Clients.Messaging.RabbitMQ
{
    /// <summary>
    /// New implementation of RabbitMQ
    /// </summary>
    public class RabbitMQClient : IMessagingClient2
    {
        private readonly Configuration configuration;
        private readonly ILogger logger;
        private readonly ConnectionFactory connectionFactory;
        private IConnection connection;
        private readonly List<Uri> endpoints;
        private Uri selectedEndpoint;

        public event MessageReceivedHandler MessageReceived;
        public bool SkipProcessingErrors { get; set; } = false;

        private IModel channel = null;
        private EventingBasicConsumer consumer;
        private string consumerTag;

        private IConnection Connection
        {
            get
            {
                if (connection == null || !connection.IsOpen)
                {
                    if (channel != null)
                    {
                        channel.Dispose();
                        channel = null;
                    }

                    try
                    {
                        connection = CreateConnection();
                        connection.ConnectionShutdown += (s, a) =>
                        {
                            logger.LogDebug("Connection was forced to close");
                            SetNextEndpoint();
                            connection = connectionFactory.CreateConnection();
                        };
                    }
                    catch (BrokerUnreachableException exc)
                    {
                        logger.LogError(exc, $"Handling internal exception");
                        SetNextEndpoint();
                    }
                }
                return connection;
            }
        }

        private IModel Channel
        {
            get
            {
                // this forces channel to dispose, if connection is broken
                var conn = Connection;
                if (channel == null)
                {
                    channel = conn.CreateModel();
                }
                return channel;
            }
        }

        public RabbitMQClient(Configuration configuration, ILogger<RabbitMQClient> logger)
        {
            this.configuration = configuration;
            this.logger = logger;

            endpoints = GetEndpoints();
            selectedEndpoint = endpoints.First();

            connectionFactory = new ConnectionFactory
            {
                Uri = selectedEndpoint,
                RequestedHeartbeat = MessagingConstants.DefaultRequestHeartbeat,
                AutomaticRecoveryEnabled = true,
                NetworkRecoveryInterval = TimeSpan.FromSeconds(MessagingConstants.NetworkRecoveryIntervalSec),
                AuthMechanisms = new AuthMechanismFactory[] { new ExternalMechanismFactory(), new PlainMechanismFactory() },
            };

            var sslOptions = GetSslOptions();
            if (sslOptions != null)
            {
                connectionFactory.Ssl = sslOptions;
            }
        }

        private IConnection CreateConnection(int attempt = 0)
        {
            try
            {
                return connectionFactory.CreateConnection();
            }
            catch (BrokerUnreachableException exc)
            {
                logger.LogError(exc, $"Handling internal exception");

                SetNextEndpoint();
                if (attempt > 50)
                {
                    throw;
                }
                return CreateConnection(attempt + 1);
            }
        }

        private List<Uri> GetEndpoints()
        {
            var result = new List<Uri>();
            var split = configuration.Endpoint.Split(',');
            var endpoint = new Uri(split[0]);
            result.Add(endpoint);
            result.AddRange(split.Skip(1).Select(x => new Uri($"{endpoint.Scheme}://{x}")));

            return result;
        }

        private SslOption GetSslOptions()
        {
            SslOption sslOptions = null;

            if (!string.IsNullOrWhiteSpace(configuration.Crt))
            {
                var certs = new X509Certificate2Collection();

                logger.LogInformation($"Importing client certificate {configuration.Crt} {(string.IsNullOrWhiteSpace(configuration.CrtPass) ? "without" : "with")} passsword");
                certs.Add(string.IsNullOrWhiteSpace(configuration.CrtPass)
                    ? new X509Certificate2(configuration.Crt)
                    : new X509Certificate2(configuration.Crt, configuration.CrtPass));

                sslOptions = new SslOption { Certs = certs, Enabled = true };

                if (!string.IsNullOrWhiteSpace(configuration.CA))
                {
                    var serverCert = new X509Certificate2(configuration.CA);
                    var serverCertTumbprint = serverCert.GetCertHashString();

                    sslOptions.CertificateValidationCallback = (sender, certificate, chain, errors) =>
                    {
                        var root = chain.ChainElements[chain.ChainElements.Count - 1].Certificate;
                        if (root.Thumbprint == serverCertTumbprint)
                        {
                            return true;
                        }
                        else
                        {
                            logger.LogCritical("Failed to validate CA");
                            return false;
                        }
                    };
                }
            }

            return sslOptions;
        }

        private void SetNextEndpoint()
        {
            var index = endpoints.IndexOf(selectedEndpoint);
            selectedEndpoint = index < endpoints.Count - 1 ? endpoints[index + 1] : endpoints[0];
            logger.LogInformation($"RabbitMQ endpoint switched to {selectedEndpoint}");
            connectionFactory.Uri = selectedEndpoint;
        }

        private string GetExchangeType(ExchangeTypeEnum type)
        {
            switch (type)
            {
                case ExchangeTypeEnum.Direct:
                    return "direct";
                case ExchangeTypeEnum.Topic:
                    return "topic";
                default:
                    throw new Exception($"Unknown exchange type: {type}");
            }
        }

        public void ExchangeDeclare(string name, ExchangeTypeEnum type)
        {
            lock (Channel)
            {
                Channel.ExchangeDeclare(name, GetExchangeType(type));
            }
        }

        public string QueueDeclare()
        {
            lock (Channel)
            {
                var res = Channel.QueueDeclare();
                return res.QueueName;
            }
        }

        public string QueueDeclare(string queue)
        {
            lock (Channel)
            {
                var res = Channel.QueueDeclare(queue, true);
                return res.QueueName;
            }
        }

        public void QueueBind(string queue, string exchange, string routingKey)
        {
            lock (Channel)
            {
                Channel.QueueBind(queue, exchange, routingKey);
            }
        }

        public void QueueBind(string queue, string exchange, IEnumerable<string> routingKeys)
        {
            foreach (var routingKey in routingKeys)
            {
                Channel.QueueBind(queue, exchange, routingKey);
            }
        }

        public void Consume(string queue, bool autoAck)
        {
            if (consumer != null)
                throw new Exception("Consumer already created");

            consumer = new EventingBasicConsumer(Channel);
            consumer.Received += async (ch, evt) =>
            {
                //var channel = ch as IModel;
                if (MessageReceived != null)
                {
                    try
                    {
                        var raw = Encoding.UTF8.GetString(evt.Body, 0, evt.Body.Length);
                        var obj = JObject.Parse(raw);
                        await MessageReceived(obj, () =>
                        {
                            lock (Channel)
                            {
                                Channel.BasicAck(evt.DeliveryTag, false);
                            }
                        });
                    }
                    catch (Exception exc)
                    {
                        logger.LogError(exc, "Error while processing message");
                        if (!SkipProcessingErrors)
                            throw;
                    }
                }
            };
            lock (Channel)
            {
                consumerTag = Channel.BasicConsume(queue, autoAck, consumer);
            }
        }

        public void Publish(string exchangeName, string routingKey, object obj)
        {
            var objToPublish = JObject.FromObject(obj);
            var raw = Encoding.UTF8.GetBytes(objToPublish.ToString(Formatting.None));
            Channel.BasicPublish(exchangeName, routingKey, false, null, raw);
        }
    }
}
