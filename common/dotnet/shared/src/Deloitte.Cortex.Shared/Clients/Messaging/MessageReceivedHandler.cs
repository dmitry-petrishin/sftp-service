﻿using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    public delegate Task MessageReceivedHandler(JObject message, Action ack);
}
