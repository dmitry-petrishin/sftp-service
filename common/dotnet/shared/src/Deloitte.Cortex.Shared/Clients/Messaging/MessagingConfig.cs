﻿using System.Collections.Generic;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Messaging
{
    public class MessagingConfig : IMessagingConfiguration
    {
        public MessagingConfig()
        {
        }

        public MessagingConfig(IMessagingConfiguration source)
        {
            source.CopyTo(this);
        }

        public string Type { get; set; }

        public string QueueName { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Endpoint { get; set; }

        public IDictionary<string, string> QueueNames { get; set; }

        public int ConsumersCount { get; set; } = MessagingConstants.DefaultConsumersCount;
        public ushort PrefetchCount { get; set; } = MessagingConstants.DefaultPrefetchCount;
        public bool PrefetchGlobal { get; set; } = false;
        public bool DirtyAck { get; set; } = false;
        public string CA { get; set; } = string.Empty;
        public string Crt { get; set; } = string.Empty;
        public string CrtPass { get; set; } = string.Empty;

        public bool MirrorQueues {get;set; } = false;
    }
}
