﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients
{
    public static class ServiceClientExtensions
    {
        public static Task<HttpResponseMessage> GetAsync(this ServiceClient client, string path,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync(HttpMethod.Get, path, null, settingsSetup);

        public static Task<T> GetAsync<T>(this ServiceClient client, string path,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync<T>(HttpMethod.Get, path, null, settingsSetup);

        public static Task<HttpResponseMessage> PostAsync(this ServiceClient client, string path, object payload,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync(HttpMethod.Post, path, payload, settingsSetup);

        public static Task<T> PostAsync<T>(this ServiceClient client, string path, object payload,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync<T>(HttpMethod.Post, path, payload, settingsSetup);

        public static Task<HttpResponseMessage> PutAsync(this ServiceClient client, string path, object payload,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync(HttpMethod.Put, path, payload, settingsSetup);

        public static Task<T> PutAsync<T>(this ServiceClient client, string path, object payload,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync<T>(HttpMethod.Put, path, payload, settingsSetup);

        public static Task<HttpResponseMessage> PatchAsync(this ServiceClient client, string path, object payload,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync(new HttpMethod("PATCH"), path, payload, settingsSetup);

        public static Task<T> PatchAsync<T>(this ServiceClient client, string path, object payload,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync<T>(new HttpMethod("PATCH"), path, payload, settingsSetup);

        public static Task<HttpResponseMessage> DeleteAsync(this ServiceClient client, string path,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync(HttpMethod.Delete, path, null, settingsSetup);

        public static Task<T> DeleteAsync<T>(this ServiceClient client, string path,
            Action<RequestSettings> settingsSetup = null) =>
            client.SendAsync<T>(HttpMethod.Delete, path, null, settingsSetup);
    }
}