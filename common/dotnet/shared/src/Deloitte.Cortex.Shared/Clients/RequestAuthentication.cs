﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients
{
    public abstract class RequestAuthentication
    {
        public abstract Task AddAuthenticationAsync(HttpRequestMessage request);
    }
}