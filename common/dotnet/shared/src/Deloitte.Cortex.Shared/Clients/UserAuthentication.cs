﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Deloitte.Cortex.Shared.Clients
{
    /// <summary>
    /// Adds authorization header with current user token.
    /// </summary>
    public class UserAuthentication: RequestAuthentication
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserAuthentication(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
        }

        public override async Task AddAuthenticationAsync(HttpRequestMessage request)
        {
            var token = await _httpContextAccessor.HttpContext.GetTokenAsync(JwtBearerDefaults.AuthenticationScheme, "access_token");

            if (!string.IsNullOrEmpty(token))
                request.Headers.AddOrReplace("Authorization", $"Bearer {token}");
        }
    }
}