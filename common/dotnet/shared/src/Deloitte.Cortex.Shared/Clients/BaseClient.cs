﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Serialization;
using System.Collections.Generic;
using System.Linq;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Deloitte.Cortex.Shared.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Deloitte.Cortex.Shared.Clients
{
    public class BaseClient: IDisposable
    {
        protected readonly CookieContainer cookies;
        protected readonly HttpClientHandler clientHandler;
        protected readonly HttpClient client;
        protected readonly ILogger<BaseClient> logger;

        /// <summary>
        /// Might be extremely useful for clients which invokes another Cortex microservices
        /// </summary>
        public bool PassErrorsThrough { get; set; }

        /// <summary>
        /// Gets or sets the timespan to wait before the request times out. <para />
        /// Default is 30 seconds.
        /// </summary>
        public TimeSpan Timeout
        {
            get { return client.Timeout; }
            set { client.Timeout = value; }
        }

        public BaseClient(string endpoint = null, ILogger<BaseClient> logger = null)
        {
            cookies = new CookieContainer();
            clientHandler = new HttpClientHandler { CookieContainer = cookies };

            client = new HttpClient(clientHandler);
            if (endpoint != null)
                client.BaseAddress = new Uri(endpoint);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            this.logger = logger;

            PassErrorsThrough = false;
            Timeout = TimeSpan.FromSeconds(30);
        }

        protected void HandlePassErrorsThrough(HttpResponseMessage response)
        {
            if (PassErrorsThrough &&
                response.Content?.Headers.ContentType?.MediaType == "application/json" &&
                response.StatusCode.In(HttpStatusCode.BadRequest, HttpStatusCode.NotFound, HttpStatusCode.InternalServerError))
            {
                try
                {
                    var errorResp = response.Content.ReadAsAsync<ErrorsResponse>().Await();
                    if (!errorResp.Errors.Any()) return;

                    var responseError = new ErrorsResponse.Error(
                        new InvalidResponseStatusCodeException(response).Message,
                        (int) ErrorCodes.ClientCallFailed);

                    var errors = new[] { responseError }.Union(errorResp.Errors);

                    throw response.StatusCode == HttpStatusCode.BadRequest
                        ? new BadRequestException(errors)
                        : new HttpException(response.StatusCode, errors);
                }
                catch (JsonReaderException)
                {
                    // It seems we do not have any errors to chain
                }
            }
        }

        protected void ValidateStatusCode<T>(HttpResponseMessage response)
        {
            if (!HasValidStatusCode<T>(response))
            {
                HandlePassErrorsThrough(response);
                string content = null;
                try
                {
                    content = response.Content?.ReadAsStringAsync().Await();
                }
                catch
                {
                }
                throw new InvalidResponseStatusCodeException(response, content);
            }
        }

        protected void ValidateStatusCode(HttpResponseMessage response)
        {
            if (!HasValidStatusCode(response))
            {
                HandlePassErrorsThrough(response);
                string content = null;
                try
                {
                    content = response.Content?.ReadAsStringAsync().Await();
                }
                catch
                {
                }
                throw new InvalidResponseStatusCodeException(response, content);
            }
        }

        /// <summary>
        /// Used for authenticated client-2-service calls on behalf of current user.
        /// Current user's token if present will be added to Authorization header for all futher calls made by this client.
        /// Token will be obtained through provided <paramref name="httpContextAccessor"/>.
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        protected void UseAuthTokenFromContext(IHttpContextAccessor httpContextAccessor)
        {
            var accessToken = httpContextAccessor?.HttpContext?.GetTokenAsync(JwtBearerDefaults.AuthenticationScheme, "access_token").Await();
            if (!string.IsNullOrEmpty(accessToken))
                client.DefaultRequestHeaders.AddOrReplace("Authorization", $"Bearer {accessToken}");
        }

        /// <summary>
        /// Used for authenticated service-2-service calls on behalf of this service.
        /// This service's token will be added to Authorization header for all futher calls made by this client.
        /// Token will be obtained via call to <paramref name="authenticationService"/>.
        /// </summary>
        public async Task UseServiceAuthTokenAsync(Authorization.IAuthenticationService authenticationService, string resource = null, AuthenticationLevelEnum level = AuthenticationLevelEnum.Main)
        {
            var authenticationResult = await authenticationService.GetServiceTokenAsync(resource, level);
            var accessToken = authenticationResult.AccessToken;
            if (!string.IsNullOrEmpty(accessToken))
                client.DefaultRequestHeaders.AddOrReplace("Authorization", $"Bearer {accessToken}");
        }

        protected virtual bool HasValidStatusCode(HttpResponseMessage response)
        {
            return response.StatusCode == HttpStatusCode.OK;
        }

        protected virtual bool HasValidStatusCode<T>(HttpResponseMessage response)
        {
            return response.StatusCode == HttpStatusCode.OK
                   || response.StatusCode == HttpStatusCode.Created
                   || default(T) == null && response.StatusCode == HttpStatusCode.NoContent;
        }

        /// <summary>
        /// Builds url for complex get requests requiring escaping of query parameters
        /// </summary>
        /// <param name="url">Request URL without endpoint part. Can contain some other query parameters</param>
        /// <param name="parametersToAdd">New parameters to be escaped and appended to the URL</param>
        /// <returns>Final URL which can be used in Post/Get/etc methods</returns>
        public string BuildUrl(string url, IReadOnlyDictionary<string, Microsoft.Extensions.Primitives.StringValues> parametersToAdd)
        {
            foreach (var kv in parametersToAdd)
            {
                foreach (var v in kv.Value)
                {
                    url = Microsoft.AspNetCore.WebUtilities.QueryHelpers.AddQueryString(url, kv.Key, v);
                }
            }
            return url;
        }

        /// <summary>
        /// Logs request HTTP method, url and execution time if <see cref="logger"/> is not <c>null</c>.
        /// <see cref="LogLevel"/> is <see cref="LogLevel.Warning"/> if request takes more than 1 second and <see cref="LogLevel.Debug"/> otherwise.
        /// </summary>
        /// <remarks>
        /// You can override this to customize logging logic or message.
        /// Note, that <see cref="logger"/> will be <c>null</c> if not passed to the <see cref="BaseClient"/> constructor!
        /// </remarks>
        protected virtual void TryLog(HttpResponseMessage response, TimeSpan executionTime)
        {
            var request = response.RequestMessage;
            logger?.Log(executionTime.TotalSeconds < 1 ? LogLevel.Debug : LogLevel.Warning,
                $"Executed \"{request.Method} {request.RequestUri}\" in {executionTime}.");
        }

        protected async Task<HttpResponseMessage> SendAsync
            (HttpMethod method, 
            string path, 
            object payload = null,
            HttpCompletionOption httpCompletionOption = HttpCompletionOption.ResponseContentRead)
        {
            var content = payload as HttpContent ?? new JsonContent(payload);
            var request = new HttpRequestMessage(method, path) { Content = content };

            try
            {
                var timedRes = await Timer.TimeAsync(async () => await client.SendAsync(request, httpCompletionOption));
                var response = timedRes.Result;
                TryLog(response, timedRes.ExecutionTime);

                ValidateStatusCode(response);
                return response;
            }
            catch (TaskCanceledException)
            {
                throw new RequestTimeoutException(request, Timeout);
            }
            catch (Exception exception) when (!(exception is RequestFailedException) && !(exception is HttpException))
            {
                logger.LogTrace(exception.StackTrace);
                throw new RequestFailedException(request, exception);
            }
        }

        protected async Task<T> SendAsync<T>(HttpMethod method, string path, bool validateResponse = true, object payload = null)
        {
            var content = payload as HttpContent ?? new JsonContent(payload);
            var request = new HttpRequestMessage(method, path) { Content = content };

            try
            {
                var timedRes = await Timer.TimeAsync(async () => await client.SendAsync(request));
                var response = timedRes.Result;
                TryLog(response, timedRes.ExecutionTime);

                if (validateResponse)
                    ValidateStatusCode<T>(response);
                return await response.Content.ReadAsAsync<T>();
            }
            catch (TaskCanceledException)
            {
                throw new RequestTimeoutException(request, Timeout);
            }
            catch (Exception exception) when (!(exception is RequestFailedException) && !(exception is HttpException))
            {
                throw new RequestFailedException(request, exception);
            }
        }

        protected Task<T> SendAsync<T>(HttpMethod method, string path, object payload = null)
        {
            return SendAsync<T>(method, path, true, payload);
        }

        protected bool TryAddRequestHeaderWithoutValidation(string name, string value)
        {
            return client.DefaultRequestHeaders.TryAddWithoutValidation(name, value);
        }
        
        protected void AddAuthenticationHeaderValue(string scheme, string parameter)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(scheme, parameter);
        }

        public Task<HttpResponseMessage> GetAsync(string path) => SendAsync(HttpMethod.Get, path);

        public Task<T> GetAsync<T>(string path, bool validateStatusCode = true) => SendAsync<T>(HttpMethod.Get, path, validateStatusCode);

        public Task<HttpResponseMessage> PostAsync(string path, object payload) => SendAsync(HttpMethod.Post, path, payload);
        
        public Task<HttpResponseMessage> PostAsync(string path, object payload, HttpCompletionOption httpCompletionOption) 
            => SendAsync(HttpMethod.Post, path, payload, httpCompletionOption);

        public Task<T> PostAsync<T>(string path, object payload, bool validateStatusCode = true) => SendAsync<T>(HttpMethod.Post, path,validateStatusCode, payload);

        public Task<HttpResponseMessage> PutAsync(string path, object payload) => SendAsync(HttpMethod.Put, path, payload);

        public Task<T> PutAsync<T>(string path, object payload, bool validateStatusCode = true) => SendAsync<T>(HttpMethod.Put, path, validateStatusCode, payload);

        public Task<HttpResponseMessage> PatchAsync(string path, object payload) => SendAsync(new HttpMethod("PATCH"), path, payload);

        public Task<T> PatchAsync<T>(string path, object payload) => SendAsync<T>(new HttpMethod("PATCH"), path, true, payload);

        public Task<HttpResponseMessage> DeleteAsync(string path) => SendAsync(HttpMethod.Delete, path);

        public Task<T> DeleteAsync<T>(string path, bool validateStatusCode = true) => SendAsync<T>(HttpMethod.Delete, path, validateStatusCode);

        public void Dispose()
        {
            client?.Dispose();
            clientHandler?.Dispose();
        }
    }
}