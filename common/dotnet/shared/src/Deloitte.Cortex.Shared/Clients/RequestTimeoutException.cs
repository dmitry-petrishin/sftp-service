using System;
using System.Net.Http;

namespace Deloitte.Cortex.Shared.Clients
{
    public class RequestTimeoutException: RequestFailedException
    {
        public TimeSpan? Timeout { get; }

        public RequestTimeoutException(HttpRequestMessage request, TimeSpan? timeout = null):
            base(request, timeout != null
                ? $"request timed out after {timeout}"
                : "request timed out")
        {
            Timeout = timeout;
        }
    }
}