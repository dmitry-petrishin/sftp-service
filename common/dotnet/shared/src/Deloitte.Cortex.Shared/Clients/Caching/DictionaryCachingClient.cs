﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Caching
{
    public class DictionaryCachingClient : ICachingClient
    {
        private readonly string _prefix;
        internal Dictionary<string, string> Objects = new Dictionary<string, string>();

        public DictionaryCachingClient(string prefix)
        {
            _prefix = prefix;
        }

        public void Delete(string key)
        {
            key = _prefix + key;
            if (Objects.ContainsKey(key))
                Objects.Remove(key);
        }

        public Task DeleteAsync(string key)
        {
            key = _prefix + key;
            Delete(key);
            return Task.FromResult(0);
        }

        public T Get<T>(string key) where T : class
        {
            key = _prefix + key;
            if (Objects.ContainsKey(key))
            {
                return JsonConvert.DeserializeObject<T>(Objects[key]);
            }
            return null;
        }

        public Task<T> GetAsync<T>(string key) where T : class
        {
            key = _prefix + key;
            return Task.FromResult(Get<T>(key));
        }

        public void Set<T>(string key, T value, TimeSpan? expiry = null) where T : class
        {
            key = _prefix + key;
            Objects[key] = JsonConvert.SerializeObject(value);
        }

        public Task SetAsync<T>(string key, T value, TimeSpan? expiry = null) where T : class
        {
            key = _prefix + key;
            Set<T>(key, value);
            return Task.FromResult(0);
        }

        public string LockQuery(string key)
        {
            throw new NotImplementedException();
        }

        public Task<string> LockQueryAsync(string key)
        {
            throw new NotImplementedException();
        }

        public bool LockRelease(string key, string token)
        {
            throw new NotImplementedException();
        }

        public Task<bool> LockReleaseAsync(string key, string token)
        {
            throw new NotImplementedException();
        }

        public bool LockTake(string key, string token, TimeSpan expiry)
        {
            throw new NotImplementedException();
        }

        public Task<bool> LockTakeAsync(string key, string token, TimeSpan expiry)
        {
            throw new NotImplementedException();
        }

        public Task<long> SetAddAsync<T>(string key, params T[] members)
        {
            throw new NotImplementedException();
        }

        public Task<T[]> SetMembersAsync<T>(string key)
        {
            throw new NotImplementedException();
        }

        public Task<long> SetRemoveAsync<T>(string key, params T[] members)
        {
            throw new NotImplementedException();
        }
    }
}
