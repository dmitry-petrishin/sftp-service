﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Caching
{
    /// <summary>
    /// It is thread-safe! You can use just one instance of it
    /// </summary>
    public class RedisClient: ICachingClient
    {
        private readonly ConnectionMultiplexer redis;
        private readonly string prefix;
        private readonly ILogger<RedisClient> logger;

        /// <summary>
        /// Regular constructor
        /// </summary>
        /// <param name="configuration">Configuration in redis format, like "localhost" or "server1:6379,server2:6379"</param>
        /// <param name="prefix">Prefix for all keys, since redis instance is shared among services, better to use service name as a prefix</param>
        public RedisClient(string configuration, string prefix, ILogger<RedisClient> logger, string caPath, string clientCertificatePath)
        {
            this.logger = logger;

            var config = ConfigurationOptions.Parse(configuration);
            AddSsl(config, caPath, clientCertificatePath);
            config.AbortOnConnectFail = false;
            redis = ConnectionMultiplexer.Connect(config);
            this.prefix = prefix;
        }

        private void AddSsl(ConfigurationOptions config, string caPath, string clientCertificatePath)
        {
            if (!string.IsNullOrWhiteSpace(clientCertificatePath))
            {
                logger.LogInformation($"Importing client certificate {clientCertificatePath}");
                var clientCert = new X509Certificate2(clientCertificatePath);
                config.CertificateSelection += (object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers) =>
                {
                    return clientCert;
                };
            }

            if (!string.IsNullOrWhiteSpace(caPath))
            {
                var serverCert = new X509Certificate2(caPath);
                var serverCertTumbprint = serverCert.GetCertHashString();
                config.CertificateValidation += (object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) =>
                {
                    var root = chain.ChainElements[chain.ChainElements.Count - 1].Certificate;
                    if (root.Thumbprint == serverCertTumbprint)
                    {
                        return true;
                    }
                    else
                    {
                        logger.LogCritical("Failed to validate CA");
                        return false;
                    }
                };
            }
        }

        private string GetFinalKey(string key) => $"{prefix}.{key}";

        private IDatabase DB => redis.GetDatabase();
        private RedisValue Serialize<T>(T value) => value == null ? RedisValue.Null : (RedisValue) JsonConvert.SerializeObject(value);
        private T Deserialize<T>(RedisValue raw) => raw.IsNull ? default : JsonConvert.DeserializeObject<T>(raw);

        public async Task<T> GetAsync<T>(string key) where T: class
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Fetching {finalKey}");

            var raw = await DB.StringGetAsync(finalKey);
            return Deserialize<T>(raw);
        }

        public T Get<T>(string key) where T: class
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Fetching {finalKey}");

            var raw = DB.StringGet(finalKey);
            return Deserialize<T>(raw);
        }

        public async Task SetAsync<T>(string key, T value, TimeSpan? expiry = null) where T: class
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Storing {value} to {finalKey}");

            await DB.StringSetAsync(finalKey, Serialize(value), expiry);
        }

        public void Set<T>(string key, T value, TimeSpan? expiry = null) where T: class
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Storing {value} to {finalKey}");

            DB.StringSet(finalKey, Serialize(value), expiry);
        }

        public async Task DeleteAsync(string key)
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Removing {finalKey}");

            await DB.KeyDeleteAsync(finalKey);
        }

        public void Delete(string key)
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Removing {finalKey}");

            DB.KeyDelete(finalKey);
        }

        public Task<long> SetAddAsync<T>(string key, params T[] members)
        {
            var finalKey = GetFinalKey(key);
            logger.LogDebug(members.Length == 1
                ? $"Adding {members[0]} to set at {key}"
                : $"Adding {members.Length} items to set at {key}");

            return DB.SetAddAsync(finalKey, members.Select(Serialize).ToArray());
        }

        public async Task<T[]> SetMembersAsync<T>(string key)
        {
            var finalKey = GetFinalKey(key);
            logger.LogDebug($"Fetching set at {finalKey}");

            var raw = await DB.SetMembersAsync(finalKey);
            return raw.Select(Deserialize<T>).ToArray();
        }

        public Task<long> SetRemoveAsync<T>(string key, params T[] members)
        {
            var finalKey = GetFinalKey(key);
            logger.LogDebug(members.Length == 1
                ? $"Removing {members[0]} from set at {key}"
                : $"Removing {members.Length} items from set at {key}");

            return DB.SetRemoveAsync(finalKey, members.Select(Serialize).ToArray());
        }

        public Task<bool> LockTakeAsync(string key, string token, TimeSpan expiry)
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Locking {token} to {finalKey}");

            return DB.LockTakeAsync(finalKey, token, expiry);
        }

        public bool LockTake(string key, string token, TimeSpan expiry)
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Locking {token} from {finalKey}");

            return DB.LockTake(finalKey, token, expiry);
        }

        public Task<bool> LockReleaseAsync(string key, string token)
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Releasing lock {token} from {finalKey}");

            return DB.LockReleaseAsync(finalKey, token);
        }

        public bool LockRelease(string key, string token)
        {
            string finalKey = GetFinalKey(key);
            logger.LogDebug($"Releasing lock {token} from {finalKey}");

            return DB.LockRelease(finalKey, token);
        }

        public async Task<string> LockQueryAsync(string key)
        {
            string finalKey = GetFinalKey(key);
            return await DB.LockQueryAsync(finalKey);
        }

        public string LockQuery(string key)
        {
            string finalKey = GetFinalKey(key);
            return DB.LockQuery(finalKey);
        }
    }
}