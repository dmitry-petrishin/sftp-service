﻿using Newtonsoft.Json;
using System;

namespace Deloitte.Cortex.Shared.Clients.Caching.Models
{
    public partial class Lockable<T> : IDisposable where T : class
    {
        /// <summary>
        /// Check that first before using object
        /// </summary>
        [JsonIgnore]
        public LockStatus Status { get; private set; } = LockStatus.LockFailed;
        /// <summary>
        /// Object itself
        /// </summary>
        public T Object { get; set; }
        /// <summary>
        /// Time when object was added to cache
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// Lock identifier
        /// </summary>
        public Guid? LockId { get; set; }
        /// <summary>
        /// Lock time
        /// </summary>
        public DateTime? LockTime { get; set; }
        private ICachingClient cache;
        private string key;

        public Lockable()
        {
        }

        private Guid SetLock()
        {
            LockId = Guid.NewGuid();
            LockTime = DateTime.UtcNow;
            return LockId.Value;
        }

        private void ClearLock()
        {
            LockId = null;
            LockTime = null;
        }

        /// <summary>
        /// Gets object from cache. If it is expired or not found: null is returned
        /// There is something wrong with async Get for Redis, so no async version
        /// </summary>
        /// <param name="cache">Some caching client</param>
        /// <param name="key">Key for object</param>
        /// <param name="maxAge">Max age of object</param>
        /// <param name="expirationCallback">Action in case object is expired</param>
        /// <returns>Not expired entity, either succesfully locked or not. Otherwise null</returns>
        public static Lockable<T> Get(ICachingClient cache, string key, TimeSpan maxAge, Action<T> expirationCallback, TimeSpan maxLockAge)
        {
            if (cache == null)
                throw new ArgumentNullException(nameof(cache), "Caching client must be specified");

            var obj = cache.Get<Lockable<T>>(key);
            if (obj == null)
                return null;

            if (obj.LockId.HasValue)
            {
                // Lock is valid only when age is not exceeded. Otherwise it is considered that something went wrong
                if (DateTime.UtcNow <= obj.LockTime.Value + maxLockAge)
                {
                    obj.Object = null;
                    return obj;
                }
            }

            // Set lock
            var lockId = obj.SetLock();
            cache.Set(key, obj);

            // Check lock
            obj = cache.Get<Lockable<T>>(key);
            if (obj == null)
                return null; // Maybe someone deleted due to expiration it or Redis reset

            if (obj.LockId != lockId)
            {
                // Someone has done that faster
                obj.Object = null;
                return obj;
            }

            // Check whether object is expired
            if (maxAge != TimeSpan.Zero)
            {
                if (DateTime.UtcNow > obj.CreateTime + maxAge)
                {
                    cache.Delete(key);
                    expirationCallback(obj.Object);
                    return null;
                }
            }

            obj.cache = cache;
            obj.key = key;
            obj.Status = LockStatus.Locked;
            return obj;
        }

        public static Lockable<T> SetAndLock(ICachingClient cache, string key, T val)
        {
            var obj = new Lockable<T>();
            obj.Object = val;
            obj.CreateTime = DateTime.UtcNow;
            var lockId = obj.SetLock();
            cache.Set(key, obj);

            // Check lock
            obj = cache.Get<Lockable<T>>(key);
            if (obj.LockId != lockId)
            {
                // Someone has done that faster
                obj.Object = null;
                return obj;
            }

            obj.cache = cache;
            obj.key = key;
            obj.Status = LockStatus.Locked;
            return obj;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                if (Status == LockStatus.Locked)
                {
                    ClearLock();
                    cache.Set(key, this);
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~Lockable()
        {
           // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
           Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
