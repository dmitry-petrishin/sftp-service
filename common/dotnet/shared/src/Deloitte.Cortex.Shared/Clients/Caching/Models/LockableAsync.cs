﻿using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Caching.Models
{
    /// <inheritdoc />
    public partial class Lockable<T> 
    {
        /// <summary>
        /// something wrong with redis async so don't use until cleared
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="maxAge"></param>
        /// <param name="expirationCallback"></param>
        /// <param name="maxLockAge"></param>
        /// <returns></returns>
        public static async Task<Lockable<T>> GetAsync(ICachingClient cache, string key, TimeSpan maxAge, Action<T> expirationCallback, TimeSpan maxLockAge)
        {
            var obj = await cache.GetAsync<Lockable<T>>(key);
            if (obj == null)
                return null;

            if (obj.LockId.HasValue)
            {
                // Lock is valid only when age is not exceeded. Otherwise it is considered that something went wrong
                if (DateTime.UtcNow <= obj.LockTime.Value + maxLockAge)
                {
                    obj.Object = null;
                    return obj;
                }
            }

            // Set lock
            var lockId = obj.SetLock();
            await cache.SetAsync(key, obj);

            // Check lock
            obj = await cache.GetAsync<Lockable<T>>(key);
            if (obj.LockId != lockId)
            {
                // Someone has done that faster
                obj.Object = null;
                return obj;
            }

            // Check whether object is expired
            if (maxAge != TimeSpan.Zero)
            {
                if (DateTime.UtcNow > obj.CreateTime + maxAge)
                {
                    await cache.DeleteAsync(key);
                    expirationCallback(obj.Object);
                    return null;
                }
            }

            obj.cache = cache;
            obj.key = key;
            obj.Status = LockStatus.Locked;
            return obj;
        }
    }
}
