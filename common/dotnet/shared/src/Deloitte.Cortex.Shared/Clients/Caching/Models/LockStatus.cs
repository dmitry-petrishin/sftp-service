﻿namespace Deloitte.Cortex.Shared.Clients.Caching.Models
{
    public enum LockStatus
    {
        Locked,
        LockFailed
    }
}
