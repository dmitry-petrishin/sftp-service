﻿using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Caching
{
    /// <summary>
    /// Can be Redis, Memcached, etc. Depends on configuration and available implementations
    /// </summary>
    public interface ICachingClient
    {
        Task<T> GetAsync<T>(string key) where T : class;
        T Get<T>(string key) where T : class;
        Task SetAsync<T>(string key, T value, TimeSpan? expiry = null) where T : class;
        void Set<T>(string key, T value, TimeSpan? expiry = null) where T : class;
        Task DeleteAsync(string key);
        void Delete(string key);

        Task<long> SetAddAsync<T>(string key, params T[] members);
        Task<T[]> SetMembersAsync<T>(string key);
        Task<long> SetRemoveAsync<T>(string key, params T[] members);

        Task<bool> LockTakeAsync(string key, string token, TimeSpan expiry);
        bool LockTake(string key, string token, TimeSpan expiry);
        Task<bool> LockReleaseAsync(string key, string token);
        bool LockRelease(string key, string token);
        Task<string> LockQueryAsync(string key);
        string LockQuery(string key);
    }
}
