﻿using Deloitte.Cortex.Shared.Serialization.Configuration;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Caching
{
    public class CachingConfiguration
    {
        [JsonProperty(Required = Required.Always)]
        public string Type { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Secret Endpoint { get; set; }

        /// <summary>
        /// Path to CA to validate server. If not specified, built-in system CA store will be used
        /// </summary>
        public string CA { get; set; }

        /// <summary>
        /// Path to client PKCS12 .p12 certificate. When specified, SSL + x509 is used for mongo connection
        /// </summary>
        public string Crt { get; set; }

        [JsonIgnore] // configuration key not supported by deployment
        public string ServiceName { get; set; }

        [JsonConstructor]
        public CachingConfiguration() { }

        public CachingConfiguration(CachingConfiguration configuration)
        {
            Type = configuration.Type;
            Endpoint = configuration.Endpoint?.Clone();
            CA = configuration.CA;
            Crt = configuration.Crt;
            ServiceName = configuration.ServiceName;
        }
    }
}