﻿using Microsoft.Extensions.Logging;
using System;

namespace Deloitte.Cortex.Shared.Clients.Caching
{
    public static class Factory
    {
        public static ICachingClient GetClient(CachingConfiguration config, string prefix, ILoggerFactory loggerFactory)
        {
            prefix = prefix ?? config.ServiceName ?? throw new ArgumentNullException(nameof(prefix));
            
            switch (config.Type)
            {
                case "Redis":
                    return new RedisClient(config.Endpoint.Value, prefix, loggerFactory.CreateLogger<RedisClient>(), config.CA, config.Crt);
                case "Dictionary":
                    return new DictionaryCachingClient(prefix);
                default:
                    throw new Exception("Unknown type of caching client: " + config.Type);
            }
        }
    }
}
