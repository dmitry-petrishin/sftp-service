﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Shared.Clients.Audit.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ObjectType
    {
        Bundle,
        DataRequest,
        Schedule,
        SourceSystem,
        SourceVersion,
        DataSource,
        Connection,
        Engagement,
        DataSourceSubscription,
        User,
        Role,
        Client,
        Entity,
        ConnectionTemplate,
        DataModel,
        BundleBase,
        Tag,
        TagGroup,
        GlossaryTerm,
        GlossaryGroup,
        Workpaper,
        CloseoutRationale,
        RetentionPeriod,
        BlackoutWindow,
        Recertification,
        DataRequestApprover,
        BlackoutSchedule,
        SymponyRequest,
        SymponyRequestData,
        InfoRequest,
        ProjectTracking,
        PreparationFunction,
        PreparationTool,
        PracticeManagementItem,
        Dataset,
        FileNode,
        ObjectFolder,
        TaxEntity,
        Obligation,
        Mapping,
        EntityGroup,
        EntityObligationMapping,
        ReviewNote
    }
}