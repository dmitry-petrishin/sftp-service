﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Shared.Clients.Audit.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AuditLogEventType
    {
        None,
        General,
        
        ObjectChanged,
        ObjectCreated,
        ObjectDeleted,
        // reverted back because of compatibility issues
        ObjectStateChanged,

        FileCreated,
        FileDeleted,
        FolderCreated,
        FolderDeleted,

        UserCreated,
        UserAddedToTier,
        UserTierRolesChanged,
        UserRemovedFromTier,

        DataModelCreated,
        DataModelCompleted,

        // analytics
        StartTransformation,
        StartTransformationStep,
        PreparationRuleFailed
    }
}