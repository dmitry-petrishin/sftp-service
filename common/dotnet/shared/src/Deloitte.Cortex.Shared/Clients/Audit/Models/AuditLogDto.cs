﻿using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Audit.Models
{
    public class AuditLogDto
    {
        public AuditLogEventType Type { get; set; }
        public JObject Parameters { get; set; }
    }
}
