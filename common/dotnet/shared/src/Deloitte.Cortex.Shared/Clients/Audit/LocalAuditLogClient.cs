﻿using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Audit.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.Audit
{
    /// <summary>
    /// <see cref="Interfaces.IAuditLogClient"/> implementation that only logs data but doesn't send it to audit queue.
    /// </summary>
    /// <remarks>Should only be used for local debugging. For example, when queue is not available locally.</remarks>
    public class LocalAuditLogClient: AuditLogClient
    {
        private readonly ILogger<LocalAuditLogClient> _logger;

        public LocalAuditLogClient(ILogger<LocalAuditLogClient> logger, IHttpContextAccessor httpContextAccessor):
            base(null, httpContextAccessor)
        {
            _logger = logger;
        }

        protected override Task<bool> Send(AuditLogDto dto)
        {
            _logger.LogDebug(JsonConvert.SerializeObject(dto, Formatting.Indented));
            return Task.FromResult(true);
        }
    }
}