﻿using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Audit.Helpers
{
    public interface IStateTracker<TObject>
    {
        Task Save(TObject objAfter);
    }
}
