﻿using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Clients.Audit.Models;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Deloitte.Cortex.Shared.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Audit.Helpers
{
    public static class AuditlogTrackHelper
    {
        public static IStateTracker<T> GetChangeTracker<T>(
            this IAuditLogClient alc,
            string oldId,
            T oldValue, 
            ObjectType type) where T : class
        {
            var tracker = new StateTracker<T, JObject>(
                oldId,
                oldValue,
                ToJObject,
                async (id, o, n) => await alc.ObjectChanged(id, type, o, n));

            return tracker;
        }

        public static IStateTracker<T> GetChangeTracker<T>(
            this IAuditLogClient alc,
            T oldValue,
            ObjectType type,
            string user)
            where T : class, IIdentifier
        {
            var tracker = user == null 
                ? alc.GetChangeTracker(oldValue, type)
                : new StateTrackerWithUser<T, JObject>(
                    oldValue.Id,
                    oldValue,
                    ToJObject,
                    async (id, o, n, u) => await alc.ObjectChanged(id, type, o, n, u),
                    user);

            return tracker;
        }

        public static IStateTracker<T> GetChangeTracker<T>(
            this IAuditLogClient alc,
            T oldValue,
            ObjectType type)
            where T : class, IIdentifier
        {
            var tracker = new StateTracker<T, JObject>(
                oldValue.Id,
                oldValue,
                ToJObject,
                async (id, o, n) => await alc.ObjectChanged(id, type, o, n));

            return tracker;
        }

        private static string Serialize<T>(T t)
        {
            if (t == null)
            {
                return null;
            }

            var result = JsonConvert.SerializeObject(t);
            if (typeof(T).IsEnum())
            {
                result = $"{{\"{typeof(T).Name}\" : {result}}}";
            }
            return result;
        }

        private static JObject ToJObject<T>(T t)
        {
            if (t == null)
            {
                return null;
            }

            var json = Serialize(t);
            return JObject.Parse(json);
        }
        
    }
}
