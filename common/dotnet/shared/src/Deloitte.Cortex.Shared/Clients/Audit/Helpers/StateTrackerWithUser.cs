﻿using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Audit.Helpers
{
    internal class StateTrackerWithUser<TObject, TState> : IStateTracker<TObject>
    {
        private readonly Func<string, TState, TState, string, Task> _saveAction;
        private readonly Func<TObject, TState> _convertAction;

        private TState _before;
        private TState _after;
        private string _id;

        private string _userId;

        public StateTrackerWithUser(
            string id,
            TObject obj,
            Func<TObject, TState> convertAction,
            Func<string, TState, TState, string, Task> saveAction,
            string userId)
        {
            _convertAction = convertAction;
            _saveAction = saveAction;
            _userId = userId;

            _before = _convertAction(obj);
            _id = id;
        }

        public async Task Save(TObject after)
        {
            _after = _convertAction(after);
            await _saveAction(_id, _before, _after, _userId);
        }
    }

}
