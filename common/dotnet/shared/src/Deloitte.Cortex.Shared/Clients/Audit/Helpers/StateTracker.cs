﻿using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Clients.Audit.Helpers
{
    internal class StateTracker<TObject, TState> : IStateTracker<TObject>
    {
        private readonly Func<string, TState, TState, Task> _saveAction;

        private readonly Func<TObject, TState> _convertAction;

        private TState _before;

        private TState _after;

        private string _id;

        public StateTracker(
            string id,
            TObject obj,
            Func<TObject, TState> convertAction,
            Func<string, TState, TState, Task> saveAction)
        {
            this._convertAction = convertAction;
            this._saveAction = saveAction;

            _before = _convertAction(obj);
            _id = id;
        }

        public async Task Save(TObject after)
        {
            _after = _convertAction(after);
            await _saveAction(_id, _before, _after);
        }
    }

}
