﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Audit.Models;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Audit
{
    public class AuditLogClient : IAuditLogClient
    {
        private readonly IMessagingClient _messagingClient;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuditLogClient(IMessagingClient messagingClient, IHttpContextAccessor httpContextAccessor)
        {
            _messagingClient = messagingClient;
            _httpContextAccessor = httpContextAccessor;
        }

        #region Staging

        public async Task<bool> FileCreated(string nodeId, string nodeName)
        {
            var args = JObject.FromObject(new { NodeId = nodeId, NodeName = nodeName });

            return await CreateDtoAndSend(args, AuditLogEventType.FileCreated);
        }

        public async Task<bool> FileDeleted(string nodeId, string nodeName)
        {
            var args = JObject.FromObject(new { NodeId = nodeId, NodeName = nodeName });

            return await CreateDtoAndSend(args, AuditLogEventType.FileDeleted);
        }

        public async Task<bool> FolderCreated(string nodeId, string nodeName)
        {
            var args = JObject.FromObject(new { NodeId = nodeId, NodeName = nodeName });

            return await CreateDtoAndSend(args, AuditLogEventType.FolderCreated);
        }

        public async Task<bool> FolderDeleted(string nodeId, string nodeName)
        {
            var args = JObject.FromObject(new { NodeId = nodeId, NodeName = nodeName });

            return await CreateDtoAndSend(args, AuditLogEventType.FolderDeleted);
        }

        #endregion

        #region Common

        public virtual async Task<bool> ObjectCreated(string objectId, ObjectType objectType, object @object, bool isHidden = false)
        {
            var args = JObject.FromObject(
                new
                {
                    ObjectId = objectId,
                    ObjectType = objectType,
                    Object = @object,
                    IsHidden = isHidden
                });

            return await CreateDtoAndSend(args, AuditLogEventType.ObjectCreated);
        }

        public virtual async Task<bool> ObjectCreated(string objectId, ObjectType objectType, object @object, string byUser, bool isHidden = false)
        {
            var args = JObject.FromObject(
                new
                {
                    UserId = byUser,
                    ObjectId = objectId,
                    ObjectType = objectType,
                    Object = @object,
                    IsHidden = isHidden
                });

            return await CreateDtoAndSend(args, AuditLogEventType.ObjectCreated);
        }

        public virtual async Task<bool> ObjectChanged(string objectId, ObjectType objectType, object oldObject, object newObject, bool isHidden = false)
        {
            var args = JObject.FromObject(
                new
                {
                    ObjectId = objectId,
                    ObjectType = objectType,
                    OldObject = oldObject,
                    NewObject = newObject,
                    IsHidden = isHidden
                });

            return await CreateDtoAndSend(args, AuditLogEventType.ObjectChanged);
        }

        public virtual async Task<bool> ObjectChanged(string objectId, ObjectType objectType, object oldObject, object newObject, string byUser, bool isHidden = false)
        {
            var args = JObject.FromObject(
                new
                {
                    UserId = byUser,
                    ObjectId = objectId,
                    ObjectType = objectType,
                    OldObject = oldObject,
                    NewObject = newObject,
                    IsHidden = isHidden
                });

            return await CreateDtoAndSend(args, AuditLogEventType.ObjectChanged);
        }

        public virtual async Task<bool> ObjectDeleted(string objectId, ObjectType objectType, bool isHidden = false)
        {
            return await ObjectDeleted(objectId, objectType, null, isHidden);
        }

        public virtual async Task<bool> ObjectDeleted(string objectId, ObjectType objectType, object @object, bool isHidden = false)
        {
            var args = JObject.FromObject(
                new
                {
                    ObjectId = objectId,
                    ObjectType = objectType,
                    Object = @object,
                    IsHidden = isHidden
                });

            return await CreateDtoAndSend(args, AuditLogEventType.ObjectDeleted);
        }

        #endregion

        #region Users

        public Task<bool> UserCreated(string userEmail, string userType, string byEmail)
        {
            return CreateDtoAndSend(JObject.FromObject(new
            {
                User = userEmail,
                Type = userType,
                By = byEmail
            }), AuditLogEventType.UserCreated);
        }

        public Task<bool> UserAddedToTier(string userEmail, string byEmail,
            string tier, string tierId = null)
        {
            var args = JObject.FromObject(new
            {
                User = userEmail,
                Tier = tier,
                TierId = tierId,
                By = byEmail
            }, JsonSerializer.Create(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            return CreateDtoAndSend(args, AuditLogEventType.UserAddedToTier);
        }

        public Task<bool> UserTierRolesChanged(string userEmail, string byEmail, string[] oldRoles, string[] newRoles,
            string tier, string tierId = null)
        {
            var args = JObject.FromObject(new
            {
                User = userEmail,
                Tier = tier,
                TierId = tierId,
                OldRoles = oldRoles,
                NewRoles = newRoles,
                By = byEmail
            }, JsonSerializer.Create(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            return CreateDtoAndSend(args, AuditLogEventType.UserTierRolesChanged);
        }

        public Task<bool> UserRemovedFromTier(string userEmail, string byEmail,
            string tier, string tierId = null)
        {
            var args = JObject.FromObject(new
            {
                User = userEmail,
                Tier = tier,
                TierId = tierId,
                By = byEmail
            }, JsonSerializer.Create(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            return CreateDtoAndSend(args, AuditLogEventType.UserRemovedFromTier);
        }

        #endregion

        #region Internal methods

        protected virtual AuditLogDto CreateDto(JObject args, AuditLogEventType eventType)
        {
            args.Add("CreationDate", DateTime.UtcNow);
            if (args["UserId"] == null)
                args.Add("UserId", GetUserEmail());
            args.Add("TechInfo",
                JObject.FromObject(
                    new
                    {
                        StackTrace = Environment.StackTrace.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries),
                        Assembly = Assembly.GetEntryAssembly().FullName
                    }));

            return new AuditLogDto
            {
                Type = eventType,
                Parameters = args
            };
        }

        protected virtual async Task<bool> Send(AuditLogDto dto) => await _messagingClient.SendToQueueAsync(dto);

        protected virtual Task<bool> CreateDtoAndSend(JObject args, AuditLogEventType eventType) => Send(CreateDto(args, eventType));

        private string GetUserEmail()
        {
            return _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(claim => claim.Type == "email")?.Value ?? "system";
        }

        #endregion
    }
}
