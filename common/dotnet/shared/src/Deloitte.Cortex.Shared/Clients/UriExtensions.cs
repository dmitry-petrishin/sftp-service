﻿using System.Collections.Generic;
using Microsoft.Extensions.Primitives;

namespace Deloitte.Cortex.Shared.Clients
{
    public static class UriExtensions
    {
        public static string UriCombine(this string baseUri, string relativeUri)
        {
            if (baseUri.EndsWith("/") && relativeUri.StartsWith("/"))
                return baseUri.Substring(0, baseUri.Length - 1) + relativeUri;

            if (!baseUri.EndsWith("/") && !relativeUri.StartsWith("/"))
                return baseUri + "/" + relativeUri;

            return baseUri + relativeUri;
        }

        public static string AddUriQueryParameters(string uri, IEnumerable<KeyValuePair<string, StringValues>> parameters)
        {
            foreach (var kv in parameters)
            foreach (var v in kv.Value)
                uri = Microsoft.AspNetCore.WebUtilities.QueryHelpers.AddQueryString(uri, kv.Key, v);
            return uri;
        }
    }
}
