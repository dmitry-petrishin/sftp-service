﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.KeyVault.Models
{
    class GenericResponse
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
