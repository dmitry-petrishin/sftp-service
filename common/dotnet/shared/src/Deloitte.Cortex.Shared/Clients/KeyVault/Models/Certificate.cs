﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace Deloitte.Cortex.Shared.Clients.KeyVault.Models
{
    public class Certificate
    {
        [JsonProperty("cer")]
        public string Base64Value { get; set; }

        [JsonIgnore]
        public byte[] Value
        {
            get
            {
                return Convert.FromBase64String(Base64Value);
            }
        }

        [JsonIgnore]
        public string ValueAsString
        {
            get
            {
                return Encoding.UTF8.GetString(Value);
            }
        }
    }
}
