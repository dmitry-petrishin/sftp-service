﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients.KeyVault.Models
{
    class KeyOperationResult
    {
        [JsonConverter(typeof(Microsoft.Rest.Serialization.Base64UrlJsonConverter))]
        [JsonProperty("value")]
        public byte[] Result { get; set; }
    }
}
