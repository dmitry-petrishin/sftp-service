﻿using Microsoft.Extensions.Logging;
using Deloitte.Cortex.Shared.Authorization;
using System.Threading.Tasks;
using System.Web;
using Deloitte.Cortex.Shared.Clients.KeyVault.Models;
using System;
using Deloitte.Cortex.Shared.ExceptionHandling;

namespace Deloitte.Cortex.Shared.Clients.KeyVault
{
    class KeyVaultClient : BaseClient
    {
        readonly IAuthenticationService authService;
        private const string authResource = "https://vault.azure.net";
        private const string apiVersion = "2016-10-01";

        public KeyVaultClient(string keyVaultName, ILogger<KeyVaultClient> logger, IAuthenticationService authService) :
            base(string.Format("https://{0}.vault.azure.net", keyVaultName), logger)
        {
            this.authService = authService;
        }

        public async Task<string> GetSecretAsync(string name, string version, AuthenticationLevelEnum level = AuthenticationLevelEnum.Main)
        {
            logger.LogInformation($"Fetching secret {name}, auth level is {level}");

            logger.LogInformation($"Setting up service token");
            await UseServiceAuthTokenAsync(authService, authResource, level);

            var url = $"secrets/{HttpUtility.UrlEncode(name)}";
            if (!string.IsNullOrWhiteSpace(version))
            {
                url += $"/{HttpUtility.UrlEncode(version)}";
            }
            url += $"?api-version={apiVersion}";

            logger.LogInformation($"Making request {url} to keyvault");
            var resp = await GetAsync<GenericResponse>(url);
            return resp.Value;
        }

        public async Task<Certificate> GetCertificateAsync(string name, string version, AuthenticationLevelEnum level = AuthenticationLevelEnum.Main)
        {
            await UseServiceAuthTokenAsync(authService, authResource, level);

            var url = $"certificates/{HttpUtility.UrlEncode(name)}/{HttpUtility.UrlEncode(version)}?api-version={apiVersion}";
            return await GetAsync<Certificate>(url);
        }

        /// <summary>
        /// Encrypts data using KEK
        /// </summary>
        /// <param name="name">Name of a key to use</param>
        /// <param name="version">Version of a key to use</param>
        /// <param name="algorithm">Encryption algorithm</param>
        /// <param name="value">Value to encrypt</param>
        /// <returns>base64-encoded encrypted value</returns>
        public async Task<string> EncryptAsync(string name, string version, string algorithm, byte[] value)
        {
            await UseServiceAuthTokenAsync(authService, authResource, AuthenticationLevelEnum.KEK);

            var url = $"keys/{HttpUtility.UrlEncode(name)}";
            if (!string.IsNullOrWhiteSpace(version))
            {
                url += $"/{HttpUtility.UrlEncode(version)}";
            }
            url += $"/encrypt?api-version={apiVersion}";

            try
            {
                var resp = await PostAsync<GenericResponse>(url, new { alg = algorithm, value = Convert.ToBase64String(value) });
                return resp.Value;
            }
            catch (InvalidResponseStatusCodeException exc)
            {
                logger.LogError(exc, "Failed to decrypt using KEK:\n" + exc.Content);
                throw new HttpException(exc.Message);
            }
        }

        /// <summary>
        /// Decrypts data using KEK
        /// </summary>
        /// <param name="name">Name of a key to use</param>
        /// <param name="version">Version of a key to use</param>
        /// <param name="algorithm">Encryption algorithm</param>
        /// <param name="base64value">Base64-decoded encrypted value</param>
        /// <returns>Decrypted data</returns>
        public async Task<byte[]> DecryptAsync(string name, string version, string algorithm, string base64value)
        {
            await UseServiceAuthTokenAsync(authService, authResource, AuthenticationLevelEnum.KEK);

            var url = $"keys/{HttpUtility.UrlEncode(name)}";
            if (!string.IsNullOrWhiteSpace(version))
            {
                url += $"/{HttpUtility.UrlEncode(version)}";
            }
            url += $"/decrypt?api-version={apiVersion}";
            
            try
            {
                var resp = await PostAsync<KeyOperationResult>(url, new { alg = algorithm, value = base64value });
                return resp.Result;
            }
            catch (InvalidResponseStatusCodeException exc)
            {
                logger.LogError(exc, "Failed to decrypt using KEK:\n" + exc.Content);
                throw new HttpException(exc.Message);
            }
        }
    }
}
