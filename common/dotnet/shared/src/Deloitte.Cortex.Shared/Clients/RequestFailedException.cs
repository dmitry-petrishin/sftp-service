using System;
using System.Net.Http;

namespace Deloitte.Cortex.Shared.Clients
{
    public class RequestFailedException: Exception
    {
        public HttpMethod RequestMethod { get; }

        public Uri RequestUri { get; }

        public RequestFailedException(HttpRequestMessage request, Exception exception):
            base($"Failed to execute {request.Method} {request.RequestUri}: {exception.Message}", exception)
        {
            RequestMethod = request.Method;
            RequestUri = request.RequestUri;
        }

        public RequestFailedException(HttpRequestMessage request, string reason):
            base($"Failed to execute {request.Method} {request.RequestUri}: {reason}")
        {
            RequestMethod = request.Method;
            RequestUri = request.RequestUri;
        }
    }
}