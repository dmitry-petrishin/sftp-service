﻿using Microsoft.EntityFrameworkCore;

using Deloitte.Cortex.Shared.Clients.Mat.Models.Db;

namespace Deloitte.Cortex.Shared.Clients.Mat
{
    internal sealed class MatContext : DbContext
    {
        private string connectionString;

        public MatContext(string connectionString) : base()
        {
            this.connectionString = connectionString;
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        internal DbSet<Client> ClientSet { get; set; }
        internal DbSet<Engagement> EngagementSet { get; set; }
        internal DbSet<Entity> EntitySet { get; set; }
        internal DbSet<Employee> EmployeeSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Entity>().HasKey(e => new { e.EntityId, e.EngagementId });
        }
    }
}
