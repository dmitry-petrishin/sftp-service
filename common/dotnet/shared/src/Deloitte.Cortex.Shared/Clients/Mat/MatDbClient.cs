﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Clients.Mat.Models;
using Deloitte.Cortex.Shared.Clients.Mat.Models.Db;
using Microsoft.EntityFrameworkCore;

namespace Deloitte.Cortex.Shared.Clients.Mat
{
    public class MatDbClient: IMatDbClient
    {
        private MatContext dbContext;

        public MatDbClient(string connectionString)
        {
            dbContext = new MatContext(connectionString);
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }

        public async Task<MatClient> GetClientAsync(int clientId)
        {
            var query =
                from x in dbContext.ClientSet
                where x.ClientId == clientId.ToString()
                select x;

            Models.Db.Client record = await query.FirstOrDefaultAsync();
            MatClient result = null;
            if (record != null)
            {
                result = new MatClient();
                record.Populate(result);
            }
            return result;
        }

        public async Task<MatClientWithEntities> GetClientWithEntitiesAsync(int clientId)
        {
            var query =
                from client in dbContext.ClientSet
                join entity in dbContext.EntitySet
                    on client.ClientId equals entity.ClientId into entities
                where client.ClientId == clientId.ToString()
                select new { client, entities = entities.GroupBy(e => e.EntityId).Select(g => g.First()) };

            var record = (await query.ToListAsync()).FirstOrDefault();
            MatClientWithEntities result = null;
            if (record != null)
            {
                result = new MatClientWithEntities();
                record.client.Populate(result);
                result.Entities = record.entities.Select(r =>
                {
                    var entity = new MatEntity();
                    r.Populate(entity);
                    return entity;
                }).ToArray();
            }
            return result;
        }

        public async Task<MatEngagement> GetEngagementAsync(int engagementId)
        {
            var query =
                from x in dbContext.EngagementSet
                where x.EngagementId == engagementId.ToString()
                select x;

            Models.Db.Engagement record = await query.FirstOrDefaultAsync();
            MatEngagement result = null;
            if (record != null)
            {
                result = new MatEngagement();
                record.Populate(result);
            }
            return result;
        }

        public async Task<MatEngagementWithEntities> GetEngagementWithEntitiesAsync(int engagementId)
        {
            var query =
                from x in dbContext.EngagementSet
                where x.EngagementId == engagementId.ToString()
                select x;

            Models.Db.Engagement record = await query.FirstOrDefaultAsync();
            MatEngagementWithEntities result = null;
            if (record != null)
            {
                result = new MatEngagementWithEntities();
                record.Populate(result);
                IEnumerable<MatEntity> entities = await GetEntitiesForEngagementAsync(result.Id);
                result.Entities = entities.ToArray();
            }
            return result;
        }

        /// <summary>
        /// Only for internal use
        /// </summary>
        /// <param name="query">Query to perform against MAT database</param>
        /// <returns>List of engagements</returns>
        private async Task<IEnumerable<T>> GetEngagementsAsync<T>(IQueryable<Models.Db.Engagement> query) where T: MatEngagement, new()
        {
            List<Models.Db.Engagement> list = await query.ToListAsync();
            var result = new List<T>();
            foreach (Models.Db.Engagement record in list)
            {
                T item = new T();
                record.Populate(item);
                result.Add(item);
            }
            return result;
        }

        public async Task<IEnumerable<MatEngagement>> GetEngagementsAsync(IEnumerable<int> ids)
        {
            var idsString = ids.Select(item => item.ToString());

            var query =
                from x in dbContext.EngagementSet
                where idsString.Contains(x.EngagementId)
                select x;

            return await GetEngagementsAsync<MatEngagement>(query);
        }

        public async Task<IEnumerable<MatEngagementWithEntities>> GetEngagementsWithEntitiesAsync(IEnumerable<int> ids)
        {
            var idsString = ids.Select(item => item.ToString());

            var query =
                from x in dbContext.EngagementSet
                where idsString.Contains(x.EngagementId)
                select x;

            var res = await GetEngagementsAsync<MatEngagementWithEntities>(query);
            foreach (var item in res)
                item.Entities = (await GetEntitiesForEngagementAsync(item.Id)).ToArray();
            return res;
        }

        public async Task<IEnumerable<MatEngagement>> ListEngagementByClientIdAsync(int clientId)
        {
            var query =
                from x in dbContext.EngagementSet
                where x.ClientId == clientId.ToString()
                select x;

            return await GetEngagementsAsync<MatEngagement>(query);
        }

        public async Task<IEnumerable<MatEngagement>> SearchEngagementsAsync(int clientId, string name, int count)
        {
            var query =
                from x in dbContext.EngagementSet
                where x.ClientId == clientId.ToString() && x.EngagementName.Contains(name)
                select x;

            return await GetEngagementsAsync<MatEngagement>(query.Take(count));
        }

        public async Task<IEnumerable<MatEntity>> GetEntitiesForClientAsync(int clientId)
        {
            var query =
                from x in dbContext.EntitySet
                where x.ClientId == clientId.ToString()
                select x;

            // Remove duplicates
            List<Models.Db.Entity> list = await query.GroupBy(item => item.EntityId).Select(g => g.First()).ToListAsync();
            List<MatEntity> result = new List<MatEntity>();
            foreach (Models.Db.Entity record in list)
            {
                MatEntity item = new MatEntity();
                record.Populate(item);
                result.Add(item);
            }
            return result;
        }

        private async Task<IEnumerable<MatEntity>> GetEntitiesForEngagementAsync(int id)
        {
            var query =
                from x in dbContext.EntitySet
                where x.EngagementId == id.ToString()
                select x;

            // Remove duplicates
            List<Models.Db.Entity> list = await query.GroupBy(item => item.EntityId).Select(g => g.First()).ToListAsync();
            List<MatEntity> result = new List<MatEntity>();
            foreach (Models.Db.Entity record in list)
            {
                MatEntity item = new MatEntity();
                record.Populate(item);
                result.Add(item);
            }
            return result;
        }

        public async Task<MatEntity> GetEntityAsync(int entityId)
        {
            var query =
                from x in dbContext.EntitySet
                where x.EntityId == entityId.ToString()
                select x;

            Models.Db.Entity record = await query.FirstOrDefaultAsync();
            MatEntity result = null;
            if (record != null)
            {
                result = new MatEntity();
                record.Populate(result);
            }
            return result;
        }

        public async Task<IEnumerable<MatEntity>> ListEntityByClientIdAsync(int clientId)
        {
            var query =
                from x in dbContext.EntitySet
                where x.ClientId == clientId.ToString()
                select x;

            List<Models.Db.Entity> list = await query.ToListAsync();
            List<MatEntity> result = new List<MatEntity>();
            foreach (Models.Db.Entity record in list)
            {
                MatEntity item = new MatEntity();
                record.Populate(item);
                result.Add(item);
            }
            return result;
        }

        public async Task<MatEmployee> GetEmployeeAsync(int employeeId)
        {
            var query =
                from x in dbContext.EmployeeSet
                where x.PersonId == employeeId.ToString()
                select x;

            Models.Db.Employee record = await query.FirstOrDefaultAsync();
            MatEmployee result = null;
            if (record != null)
            {
                result = new MatEmployee();
                record.Populate(result);
            }
            return result;
        }

        public async Task<MatEmployee> GetEmployeeAsync(string email)
        {
            var query =
                from x in dbContext.EmployeeSet
                where x.Email_Address == email
                select x;

            Models.Db.Employee record = await query.FirstOrDefaultAsync();
            MatEmployee result = null;
            if (record != null)
            {
                result = new MatEmployee();
                record.Populate(result);
            }
            return result;
        }

        public async Task<IEnumerable<MatEmployee>> SearchEmployeesAsync(string term, int limit)
        {
            var nameParts = term.ToUpper().Split(' ', StringSplitOptions.RemoveEmptyEntries);

            IQueryable<Employee> query = dbContext.EmployeeSet;
            if (nameParts.Any())
            {
                var filter = nameParts
                    .Select<string, Expression<Func<Employee, bool>>>(p => u =>
                        u.Email_Address.ToUpper().Contains(p) || u.Name_First.ToUpper().Contains(p) || u.Name_Last.ToUpper().Contains(p))
                    .Aggregate((f1, f2) => f1.AndAlso(f2));
                query = query.Where(filter);
            }

            List<Models.Db.Employee> list = await query.Take(limit).ToListAsync();
            List<MatEmployee> result = new List<MatEmployee>();
            foreach (Models.Db.Employee record in list)
            {
                MatEmployee item = new MatEmployee();
                record.Populate(item);
                result.Add(item);
            }
            return result;
        }

        public async Task<IEnumerable<MatEmployee>> SearchEmployeesByEmailAsync(string partialEmail, int limit)
        {
            partialEmail = partialEmail.ToUpper();
            var query =
                from x in dbContext.EmployeeSet
                where x.Email_Address.ToUpper().Contains(partialEmail)
                select x;

            List<Models.Db.Employee> list = await query.Take(limit).ToListAsync();
            List<MatEmployee> result = new List<MatEmployee>();
            foreach (Models.Db.Employee record in list)
            {
                MatEmployee item = new MatEmployee();
                record.Populate(item);
                result.Add(item);
            }
            return result;
        }

        public async Task<IEnumerable<MatEmployee>> SearchEmployeesByNameAsync(string partialName, int limit)
        {
            var nameParts = partialName.ToUpper().Split(' ', StringSplitOptions.RemoveEmptyEntries);

            IQueryable<Employee> query = dbContext.EmployeeSet;
            if (nameParts.Any())
            {
                var filter = nameParts
                    .Select<string, Expression<Func<Employee, bool>>>(p =>
                        u => u.Name_First.ToUpper().Contains(p) || u.Name_Last.ToUpper().Contains(p))
                    .Aggregate((f1, f2) => f1.AndAlso(f2));
                query = query.Where(filter);
            }

            List<Models.Db.Employee> list = await query.Take(limit).ToListAsync();
            List<MatEmployee> result = new List<MatEmployee>();
            foreach (Models.Db.Employee record in list)
            {
                MatEmployee item = new MatEmployee();
                record.Populate(item);
                result.Add(item);
            }
            return result;
        }

        public async Task<IEnumerable<MatClient>> SearchClientByNameAsync(string partialName, int count)
        {
            var query =
                from c in dbContext.ClientSet
                where c.ClientName.Contains(partialName)
                select c;

            return (await query.Take(count).ToListAsync())
                .Select(record =>
                {
                    var client = new MatClient();
                    record.Populate(client);
                    return client;
                });
        }
    }
}