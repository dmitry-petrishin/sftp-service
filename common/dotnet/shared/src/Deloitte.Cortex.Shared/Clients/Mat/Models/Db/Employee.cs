﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Deloitte.Cortex.Shared.Clients.Mat.Models.Db
{
    [Table("MDR_EMPLOYEES")]
    internal class Employee
    {
        [Key]
        public string PersonId { get; set; }
        public string PersonnelNbr { get; set; }
        public string Name_First { get; set; }
        public string Name_PreferredFirst { get; set; }
        public string Name_Middle { get; set; }
        public string Name_Last { get; set; }
        public string PreferredFullName { get; set; }
        public string Name_OutlookDisplayName { get; set; }
        public string EmployeeAlias { get; set; }
        public string Email_Address { get; set; }

        [Column("JobLevelText")]
        public string Title { get; set; }

        public void Populate(Models.MatEmployee item)
        {
            item.PersonId = int.Parse(PersonId);
            item.PersonnelNbr = int.Parse(PersonnelNbr);
            item.Name_First = Name_First?.Trim();
            item.Name_PreferredFirst = Name_PreferredFirst?.Trim();
            item.Name_Middle = Name_Middle?.Trim();
            item.Name_Last = Name_Last?.Trim();
            item.PreferredFullName = PreferredFullName?.Trim();
            item.Name_OutlookDisplayName = Name_OutlookDisplayName?.Trim();
            item.EmployeeAlias = EmployeeAlias?.Trim();
            item.Email_Address = Email_Address?.Trim();
            item.Title = Title?.Trim();
        }
    }
}
