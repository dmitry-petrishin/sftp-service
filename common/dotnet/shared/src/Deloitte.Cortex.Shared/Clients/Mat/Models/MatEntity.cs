﻿namespace Deloitte.Cortex.Shared.Clients.Mat.Models
{
    public class MatEntity
    {
        public int ClientId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
