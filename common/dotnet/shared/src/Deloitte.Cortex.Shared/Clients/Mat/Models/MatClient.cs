﻿using Deloitte.Cortex.Shared.Serialization;

namespace Deloitte.Cortex.Shared.Clients.Mat.Models
{
    public class MatClient
    {
        public int Id { get; set; }
        public string CustomerNumber { get; set; }
        public string Name { get; set; }
        public DayMonth FiscalYearEnd { get; set; }
        public string ProgramClientNumber { get; set; }
        public string ProgramClientName { get; set; }
        public int? IndustryKey { get; set; }
        public string IndustryName { get; set; }
        public string SectorCode { get; set; }
        public string SectorName { get; set; }
        public string SubSectorName { get; set; }
        public string Country { get; set; }
        public string LCSP_PersonnelNumber { get; set; }
        public string LCSP_Alias { get; set; }
    }
}
