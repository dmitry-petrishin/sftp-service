﻿namespace Deloitte.Cortex.Shared.Clients.Mat.Models
{
    public class MatClientWithEntities: MatClient
    {
        public MatEntity[] Entities { get; set; }
    }
}