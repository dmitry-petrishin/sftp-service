﻿using Deloitte.Cortex.Shared.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Deloitte.Cortex.Shared.Clients.Mat.Models.Db
{
    [Table("MAT_CLIENT")]
    internal class Client
    {
        [Key]
        public string ClientId { get; set; }
        public string CustomerNumber { get; set; }
        public string ClientName { get; set; }
        public string FiscalYearEnd { get; set; }
        public string ProgramClientNumber { get; set; }
        public string ProgramClientName { get; set; }
        public string IndustryKey { get; set; }
        public string IndustryName { get; set; }
        public string SectorCode { get; set; }
        public string SectorName { get; set; }
        public string SubSectorName { get; set; }
        public string Country { get; set; }
        public string LCSP_PersonnelNumber { get; set; }
        public string LCSP_Alias { get; set; }

        public void Populate(Models.MatClient item)
        {
            item.Id = int.Parse(ClientId);
            item.CustomerNumber = CustomerNumber;
            item.Name = ClientName;
            if (FiscalYearEnd != null)
            {
                string[] parts = FiscalYearEnd.Split('/');
                if (parts.Length > 1)
                {
                    int month = int.Parse(parts[0]);
                    int day = int.Parse(parts[1]);
                    item.FiscalYearEnd = new DayMonth(day, month);
                }
            }
            item.ProgramClientNumber = ProgramClientNumber;
            item.ProgramClientName = ProgramClientName;
            item.IndustryKey = string.IsNullOrEmpty(IndustryKey) ? (int?)null : int.Parse(IndustryKey);
            item.IndustryName = IndustryName;
            item.SectorCode = SectorCode;
            item.SectorName = SectorName;
            item.SubSectorName = SubSectorName;
            item.Country = Country;
            item.LCSP_PersonnelNumber = LCSP_PersonnelNumber;
            item.LCSP_Alias = LCSP_Alias;
        }
    }
}
