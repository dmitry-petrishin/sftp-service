﻿using System;

namespace Deloitte.Cortex.Shared.Clients.Mat.Models
{
    public class MatEngagement
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? FiscalYearEnd { get; set; }
        public string OfficeKey { get; set; }
        public string OfficeName { get; set; }
        public string RegionKey { get; set; }
        public string RegionName { get; set; }
    }
}
