﻿namespace Deloitte.Cortex.Shared.Clients.Mat.Models
{
    public class MatEmployee
    {
        public int PersonId { get; set; }
        public int PersonnelNbr { get; set; }
        public string Name_First { get; set; }
        public string Name_PreferredFirst { get; set; }
        public string Name_Middle { get; set; }
        public string Name_Last { get; set; }
        public string PreferredFullName { get; set; }
        public string Name_OutlookDisplayName { get; set; }
        public string EmployeeAlias { get; set; }
        public string Email_Address { get; set; }
        public string Title { get; set; }
    }
}
