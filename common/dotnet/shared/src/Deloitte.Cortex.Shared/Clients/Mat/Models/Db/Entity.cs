﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Deloitte.Cortex.Shared.Clients.Mat.Models.Db
{
    [Table("MAT_ENTITY")]
    internal class Entity
    {
        public string ClientId { get; set; }
        public string EngagementId { get; set; }
        public string EntityId { get; set; }
        public string EntityName { get; set; }

        public void Populate(Models.MatEntity item)
        {
            item.ClientId = int.Parse(ClientId);
            item.Id = int.Parse(EntityId);
            item.Name = EntityName;
        }
    }
}
