﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Deloitte.Cortex.Shared.Clients.Mat.Models.Db
{
    [Table("MAT_ENGAGEMENT")]
    internal class Engagement
    {
        [Key]
        public string EngagementId { get; set; }
        public string ClientId { get; set; }
        public string EngagementNumber { get; set; }
        public string EngagementName { get; set; }
        public string EngagementDescription { get; set; }
        public string FiscalYearEnd { get; set; }
        public string OfficeKey { get; set; }
        public string OfficeName { get; set; }
        public string RegionKey { get; set; }
        public string RegionName { get; set; }

        public void Populate(Models.MatEngagement item)
        {
            item.Id = int.Parse(EngagementId);
            item.ClientId = int.Parse(ClientId);
            item.Number = EngagementNumber;
            item.Name = EngagementName;
            item.Description = EngagementDescription;
            item.FiscalYearEnd = string.IsNullOrEmpty(FiscalYearEnd) ? (DateTime?)null : DateTime.ParseExact(FiscalYearEnd, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            item.OfficeKey = OfficeKey;
            item.OfficeName = OfficeName;
            item.RegionKey = RegionKey;
            item.RegionName = RegionName;
        }
    }
}
