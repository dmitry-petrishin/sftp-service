﻿namespace Deloitte.Cortex.Shared.Clients.Mat.Models
{
    public class MatEngagementWithEntities: MatEngagement
    {
        public MatEntity[] Entities { get; set; }
    }
}
