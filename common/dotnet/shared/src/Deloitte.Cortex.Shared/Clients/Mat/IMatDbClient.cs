using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Mat.Models;

namespace Deloitte.Cortex.Shared.Clients.Mat
{
    public interface IMatDbClient : IDisposable
    {
        Task<MatClient> GetClientAsync(int clientId);
        Task<MatClientWithEntities> GetClientWithEntitiesAsync(int clientId);
        Task<IEnumerable<MatEntity>> GetEntitiesForClientAsync(int clientId);
        Task<IEnumerable<MatClient>> SearchClientByNameAsync(string partialName, int count);
        Task<MatEngagement> GetEngagementAsync(int engagementId);
        Task<MatEngagementWithEntities> GetEngagementWithEntitiesAsync(int engagementId);
        Task<IEnumerable<MatEngagement>> GetEngagementsAsync(IEnumerable<int> ids);
        Task<IEnumerable<MatEngagementWithEntities>> GetEngagementsWithEntitiesAsync(IEnumerable<int> ids);
        Task<IEnumerable<MatEngagement>> ListEngagementByClientIdAsync(int clientId);
        Task<IEnumerable<MatEngagement>> SearchEngagementsAsync(int clientId, string name, int count);
        Task<MatEntity> GetEntityAsync(int entityId);
        Task<IEnumerable<MatEntity>> ListEntityByClientIdAsync(int clientId);
        Task<MatEmployee> GetEmployeeAsync(int employeeId);
        Task<MatEmployee> GetEmployeeAsync(string email);
        Task<IEnumerable<MatEmployee>> SearchEmployeesAsync(string term, int limit);
        Task<IEnumerable<MatEmployee>> SearchEmployeesByEmailAsync(string partialEmail, int limit);
        Task<IEnumerable<MatEmployee>> SearchEmployeesByNameAsync(string partialName, int limit);
    }
}
