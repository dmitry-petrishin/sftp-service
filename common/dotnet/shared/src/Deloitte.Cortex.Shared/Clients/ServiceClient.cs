﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Clients
{
    public class ServiceClient: IDisposable
    {
        public readonly ILogger Logger;
        public readonly HttpClient HttpClient;
        public readonly HttpClientHandler HttpClientHandler;

        /// <summary>
        /// Default cookies for each client request.
        /// </summary>
        public CookieContainer Cookies => HttpClientHandler.CookieContainer;

        /// <summary>
        /// Gets or sets the timespan to wait before the request times out.
        /// Default is 30 seconds.
        /// </summary>
        public TimeSpan Timeout
        {
            get => HttpClient.Timeout;
            set => HttpClient.Timeout = value;
        }

        /// <summary>
        /// <see cref="RequestSettings"/> used for all requests unless provided explicitly.
        /// </summary>
        public readonly RequestSettings DefaultRequestSettings;

        public ServiceClient(string endpoint = null, ILogger logger = null, RequestSettings settings = null,
            HttpClientHandler handler = null)
        {
            Logger = logger;

            HttpClientHandler = handler ?? new HttpClientHandler();
            HttpClient = new HttpClient(HttpClientHandler, disposeHandler: true);

            HttpClient.DefaultRequestHeaders.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (endpoint != null) HttpClient.BaseAddress = new Uri(endpoint);

            DefaultRequestSettings = settings ?? new RequestSettings();

            Timeout = TimeSpan.FromSeconds(30);
        }

        /// <summary>
        /// Logs request HTTP method, url and execution time if <see cref="Logger"/> is not <c>null</c>.
        /// <see cref="LogLevel"/> is <see cref="LogLevel.Warning"/> if request takes more than 1 second and <see cref="LogLevel.Debug"/> otherwise.
        /// </summary>
        protected virtual void TryLog(HttpResponseMessage response, TimeSpan executionTime)
        {
            var request = response.RequestMessage;
            Logger?.Log(executionTime.TotalSeconds < 1 ? LogLevel.Debug : LogLevel.Warning,
                $"Executed \"{request.Method} {request.RequestUri}\" in {executionTime}.");
        }

        protected HttpException TryDeserializeHttpException(HttpResponseMessage response, string content)
        {
            if (response.Content?.Headers.ContentType?.MediaType != "application/json") return null;

            try
            {
                var responseErrors = JsonConvert.DeserializeObject<ErrorsResponse>(content);
                if (responseErrors.Errors != null && responseErrors.Errors.Any())
                {
                    var requestFailedError = new ErrorsResponse.Error(
                        new InvalidResponseStatusCodeException(response).Message,
                        (int)ErrorCodes.ClientCallFailed);

                    return new HttpException(response.StatusCode, errors: new[] { requestFailedError }.Union(responseErrors.Errors));
                }
            }
            catch (JsonReaderException)
            {
                // It seems we do not have any errors to chain
            }

            return null;
        }

        protected async Task ValidateResponseAsync(HttpResponseMessage response, RequestSettings settings)
        {
            if (settings.IsValidResponse?.Invoke(response) != true)
            {
                string content = null;
                try
                {
                    content = await response.Content.ReadAsStringAsync();
                }
                catch
                {
                    // pass null content if failed to deserialize
                }

                if (settings.ForwardHttpException)
                {
                    var responseException = TryDeserializeHttpException(response, content);
                    if (responseException != null) throw responseException;
                }

                throw new InvalidResponseStatusCodeException(response, content);
            }
        }

        public virtual async Task<HttpResponseMessage> SendAsync(HttpMethod method, string path, object payload = null,
            Action<RequestSettings> settingsSetup = null)
        {
            var settings = new RequestSettings(DefaultRequestSettings);
            settingsSetup?.Invoke(settings);
            var content = payload as HttpContent ?? new JsonContent(payload);

            var request = new HttpRequestMessage(method, path) { Content = content };
            if (settings.Authentication != null)
                await settings.Authentication.AddAuthenticationAsync(request);

            HttpResponseMessage response;
            TimeSpan executionTime;

            try
            {
                var timedResponse = await Timer.TimeAsync(() => HttpClient.SendAsync(request));
                response = timedResponse.Result;
                executionTime = timedResponse.ExecutionTime;
            }
            catch (TaskCanceledException)
            {
                throw new RequestTimeoutException(request, Timeout);
            }
            catch (Exception exception)
            {
                throw new RequestFailedException(request, exception);
            }

            TryLog(response, executionTime);

            await ValidateResponseAsync(response, settings);
            return response;
        }

        public virtual async Task<TResponse> SendAsync<TResponse>(HttpMethod method, string path, object payload = null,
            Action<RequestSettings> settingsSetup = null)
        {
            var responseMessage = await SendAsync(method, path, payload, settingsSetup);
            return await responseMessage.Content.ReadAsAsync<TResponse>();
        }

        public void Dispose() => HttpClient?.Dispose();
    }
}