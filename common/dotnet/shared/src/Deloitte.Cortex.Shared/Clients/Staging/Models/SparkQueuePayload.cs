﻿using Newtonsoft.Json;
using System;

namespace Deloitte.Cortex.Shared.Clients.Staging.Models
{
    [Obsolete]
    public class SparkQueuePayload
    {
        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("operation")]
        public ACTIONS operation { get; set; }

        [JsonProperty("from")]
        public string from { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }

    public enum ACTIONS
    {
        TRANSFER_FILES_TO_RAWTABLES,
        MOVE_FILES_FROM_RAWFILES_TO_RAWTABLE,
        MOVE_FILES_FROM_RAWFILES_TO_RAWTABLE_SUCCESS,
        MOVE_FILES_FROM_RAWFILES_TO_RAWTABLE_FAILURE,
        MOVE_FILES_FROM_RAWTABLE_TO_RAWCONSOLIDATED,
        MOVE_FILES_FROM_RAWTABLE_TO_RAWCONSOLIDATED_SUCCESS,
        MOVE_FILES_FROM_RAWTABLE_TO_RAWCONSOLIDATED_FAILURE,
        ADDFILESTODB
    }
}
