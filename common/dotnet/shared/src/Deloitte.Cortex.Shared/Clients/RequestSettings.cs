﻿using System;
using System.Net.Http;

namespace Deloitte.Cortex.Shared.Clients
{
    /// <summary>
    /// Options that can be specified per each request separately but not present in <see cref="HttpRequestMessage"/>.
    /// </summary>
    public class RequestSettings
    {
        /// <summary>
        /// Authentication used for request (see <see cref="UserAuthentication"/>, <see cref="ServiceAuthentication"/>).
        /// Specify <c>null</c> to not provide any authentication information.
        /// </summary>
        public RequestAuthentication Authentication { get; set; } = null;

        /// <summary>
        /// Callback to check whether response is valid.
        /// Most use cases should just check HTTP response code.
        /// Specify <c>null</c> to skip request validation.
        /// </summary>
        /// <remarks>Do not read response content here as it can be read only once.</remarks>
        public Func<HttpResponseMessage, bool> IsValidResponse = response => response.IsSuccessStatusCode;

        /// <summary>
        /// If <c>true</c>, client will attempt to deserialize and rethrow invalid response as <see cref="ExceptionHandling.HttpException"/>.
        /// </summary>
        public bool ForwardHttpException { get; set; } = false;

        public RequestSettings() { }

        public RequestSettings(RequestSettings settings)
        {
            Authentication = settings.Authentication;
            IsValidResponse = settings.IsValidResponse;
            ForwardHttpException = settings.ForwardHttpException;
        }
    }
}