﻿using Deloitte.Cortex.Shared.Clients.Audit.Models;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Notification.Models
{
    public class NotificationDto
    {
        public NotificationEventType? Type { get; set; }
        public AuditLogEventType? AuditLogType { get; set; }
        public JObject Parameters { get; set; }
    }
}
