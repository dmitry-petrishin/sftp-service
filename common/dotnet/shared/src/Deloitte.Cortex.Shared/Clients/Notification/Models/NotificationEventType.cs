﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Shared.Clients.Notification.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NotificationEventType
    {
        UserResetPasswordReminder,
        ConnectionPasswordUpcomingExpiration,
        DataRequestApprovalReminderBeforeExtraction,
        DataRequestApprovalReminderOnExtraction,
        DataRequestApprovalReminderAfterExtraction,

        Corptax,
        TradeChain,


        /// <summary>
        /// Engagement closeout notification group:
        /// Automated message for engagement closeout after report issuance date.
        /// The reminder emails will occur on day 15 and 25,
        /// and daily from day 30 onwards until the engagement is closed out.
        /// </summary>
        EngagementReportIssuanceDateReminder,
        /// <summary>
        /// Engagement closeout notification group:
        /// After 45 days, an additional email is sent to the Cortex App Security Admin 
        /// to alert them to contact the engagement team 
        /// about why the closeout has not yet been completed.
        /// </summary>
        EngagementReportIssuanceDateReminderForAppAdmin,

        /// <summary>
        /// During engagement closeout process, 
        /// data request author and data request approver 
        /// should receive an email as well as an in-app notification stating 
        /// "Engagement closeout process initiated for [engagement name].  
        /// Future extractions scheduled will not roll forward, please reschedule in new engagement."
        /// 
        /// Prior to approving engagement closeout, 
        /// Engagement PPMD should get a pop-up warning prior to approving engagement closeout 
        /// that states "Future extractions scheduled will not roll forward, please reschedule in new engagement."
        /// </summary>
        DataExtractionsScheduledAfterEngagementCloseout,

        // Event types to delete
        DataRequestApprovalReminder,

        EngagementCloseoutProcessInit
    }
}