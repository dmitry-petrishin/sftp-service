﻿using System;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Audit.Models;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Deloitte.Cortex.Shared.Clients.Notification.Models;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Clients.Notification
{
    public class NotificationClient : INotificationClient
    {
        private readonly IMessagingClient _messagingClient;

        public NotificationClient(IMessagingClient messagingClient)
        {
            _messagingClient = messagingClient;
        }

        public async Task<bool> ConnectionPasswordUpcomingExpiration(string userEmail, string dataSourceName,
            string connectionName, string connectionId, DateTime expirationDate)
        {
            var args = JObject.FromObject(
                new
                {
                    DataSourceName = dataSourceName,
                    ConnectionName = connectionName,
                    ConnectionId = connectionId,
                    ExpirationDate = expirationDate
                });

            return await CreateDtoAndSend(args, userEmail, NotificationEventType.ConnectionPasswordUpcomingExpiration);
        }

        public async Task<bool> SendAuditLog(AuditLogDto dto)
        {
            var args = JObject.FromObject(new { AuditLogDto = dto });
            return await CreateDtoAndSend(args, dto.Type);
        }

        public async Task<bool> ConnectionParametersNeedToBeUpdated(string connectionName, string clientId, bool isClientChosen)
        {
            var args = JObject.FromObject(new
            {
                ConnectionName = connectionName,
                IsClientChosen = isClientChosen,
                ClientId = clientId
            });
            
            return await SendAuditLog(args, ObjectType.Connection, AuditLogEventType.None);
        }

        public async Task<bool> CortexCompletesSymphonyRequest(string appName, string engagementId, string engagementName, string workItemId,
            string workItemName, string dataStartDate, string dataEndDate)
        {
            var args = JObject.FromObject(
                new
                {
                    AppName = appName,
                    EngagementId = engagementId,
                    EngagementName = engagementName,
                    WorkItemId = workItemId,
                    WorkItemName = workItemName,
                    DataStartDate = dataStartDate,
                    DataEndDate = dataEndDate
                });
            
            return await SendAuditLog(args, ObjectType.SymponyRequest, AuditLogEventType.None);
        }

        public async Task<bool> DataRequestApprovalReminderBeforeExtraction(string userEmail, string dataRequestId, string dataRequestName, string engagementName, string inactivityAmount, string timeIntervalType)
        {
            var args = JObject.FromObject(
                new
                {
                    DataRequestId = dataRequestId,
                    DataRequestName = dataRequestName,
                    EngagementName = engagementName,
                    InactivityAmount = inactivityAmount,
                    TimeIntervalType = timeIntervalType
                });

            return await CreateDtoAndSend(args, userEmail, NotificationEventType.DataRequestApprovalReminderBeforeExtraction);
        }

        public async Task<bool> DataRequestApprovalReminderOnExtraction(string userEmail, string dataRequestId, string dataRequestName, string engagementName)
        {
            var args = JObject.FromObject(
                new
                {
                    DataRequestId = dataRequestId,
                    DataRequestName = dataRequestName,
                    EngagementName = engagementName
                });

            return await CreateDtoAndSend(args, userEmail, NotificationEventType.DataRequestApprovalReminderOnExtraction);
        }

        public async Task<bool> DataRequestApprovalReminderAfterExtraction(string userEmail, string dataRequestId, string dataRequestName, string engagementName, string inactivityAmount)
        {
            var args = JObject.FromObject(
                new
                {
                    DataRequestId = dataRequestId,
                    DataRequestName = dataRequestName,
                    EngagementName = engagementName,
                    InactivityAmount = inactivityAmount
                });

            return await CreateDtoAndSend(args, userEmail, NotificationEventType.DataRequestApprovalReminderAfterExtraction);
        }

        public async Task<bool> EngagementReportIssuanceDateReminder(string engagementId, string engagementName, string reportIssuanceDate)
        {
            var args = JObject.FromObject(
                new
                {
                    NotificationEventType = NotificationEventType.EngagementReportIssuanceDateReminder,
                    EngagementId = engagementId,
                    Name = engagementName,
                    ReportIssuanceDate = reportIssuanceDate,
                });

            return await SendAuditLog(args, ObjectType.Engagement, AuditLogEventType.None);
        }
        public async Task<bool> EngagementReportIssuanceDateReminderForAppAdmin(string engagementId, string engagementName, string reportIssuanceDate)
        {
            var args = JObject.FromObject(
                new
                {
                    NotificationEventType = NotificationEventType.EngagementReportIssuanceDateReminderForAppAdmin,
                    EngagementId = engagementId,
                    Name = engagementName,
                    ReportIssuanceDate = reportIssuanceDate
                });

            return await SendAuditLog(args, ObjectType.Engagement, AuditLogEventType.None);
        }

        public async Task<bool> EngagementCloseoutProcessInit(string userEmail, string engagementName)
        {
            var args = JObject.FromObject(
                new
                {
                    EngagementName = engagementName
                });

            return await CreateDtoAndSend(args, userEmail, NotificationEventType.EngagementCloseoutProcessInit);
        }

        public async Task<bool> DataExtractionsScheduledAfterEngagementCloseout(string engagementId, string engagementName)
        {
            var args = JObject.FromObject(
                new
                {
                    NotificationEventType = NotificationEventType.DataExtractionsScheduledAfterEngagementCloseout,
                    EngagementId = engagementId,
                    Name = engagementName
                });

            return await SendAuditLog(args, ObjectType.Engagement, AuditLogEventType.None);
        }

        public async Task<bool> SymphonyRequestsForData(string appName, string engagementId, string engagementName, string workItemId,
            string workItemName, string dataStartDate, string dataEndDate)
        {
            var args = JObject.FromObject(
                new {
                    AppName = appName,
                    EngagementId = engagementId,
                    EngagementName = engagementName,
                    WorkItemId = workItemId,
                    WorkItemName = workItemName,
                    DataStartDate = dataStartDate,
                    DataEndDate = dataEndDate
                });

            return await SendAuditLog(args, ObjectType.SymponyRequestData, AuditLogEventType.None);
        }

        #region Private methods

        private async Task<bool> SendAuditLog(JObject args, ObjectType type, AuditLogEventType eventType)
        {
            var auditLogDto = new AuditLogDto
            {
                Type = eventType,
                Parameters = JObject.FromObject(new { Object = args, ObjectType = type })
            };

            var args1 = JObject.FromObject(new { AuditLogDto = auditLogDto });
            return await CreateDtoAndSend(args1, eventType);
        }

        private async Task<bool> CreateDtoAndSend(JObject args, string userEmail, NotificationEventType eventType)
        {
            args.Add("UserEmail", userEmail);
            args.Add("CreationDate", DateTime.UtcNow);

            var dto = new NotificationDto
            {
                Type = eventType,
                Parameters = args
            };

            return await _messagingClient.SendToQueueAsync(dto);
        }
        private async Task<bool> CreateDtoAndSend(JObject args, NotificationEventType eventType)
        {
            args.Add("CreationDate", DateTime.UtcNow);

            var dto = new NotificationDto
            {
                Type = eventType,
                Parameters = args
            };

            return await _messagingClient.SendToQueueAsync(dto);
        }

        private async Task<bool> CreateDtoAndSend(JObject args, AuditLogEventType auditLogEventType)
        {
            args.Add("CreationDate", DateTime.UtcNow);

            var dto = new NotificationDto
            {
                AuditLogType = auditLogEventType,
                Parameters = args
            };

            return await _messagingClient.SendToQueueAsync(dto);
        }

        #endregion
    }
}