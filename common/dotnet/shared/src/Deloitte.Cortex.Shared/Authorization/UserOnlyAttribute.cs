﻿using Microsoft.AspNetCore.Authorization;

namespace Deloitte.Cortex.Shared.Authorization
{
    /// <summary>
    /// Only allow calls from application user.
    /// </summary>
    public class UserOnlyAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Only allow calls from application user.
        /// </summary>
        public UserOnlyAttribute()
        {
            Roles = CallerTypeRole.User;
        }
    }
}
