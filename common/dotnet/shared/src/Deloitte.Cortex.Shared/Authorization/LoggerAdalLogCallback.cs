﻿using Deloitte.Cortex.Shared.AspNetCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;
using AdalLogLevel = Microsoft.IdentityModel.Clients.ActiveDirectory.LogLevel;

namespace Deloitte.Cortex.Shared.Authorization
{
    // Todo: should be tied to MS logging level
    public class LoggerAdalLogCallback: IAdalLogCallback
    {
        private readonly ILogger _logger;

        public LoggerAdalLogCallback(ILogger logger) => _logger = logger;

        private LogLevel MapLogLevel(AdalLogLevel adalLogLevel)
        {
            switch (adalLogLevel)
            {
                case AdalLogLevel.Verbose:
                    return LogLevel.Trace;
                case AdalLogLevel.Information:
                    return LogLevel.Debug;

                case AdalLogLevel.Error:
                case AdalLogLevel.Warning:
                    return LogLevel.Warning;

                default: return LogLevel.None;
            }
        }

        public void Log(AdalLogLevel level, string message) => _logger.Log(MapLogLevel(level), message);
    }
}