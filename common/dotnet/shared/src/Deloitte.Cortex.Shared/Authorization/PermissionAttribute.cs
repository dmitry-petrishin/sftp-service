﻿using System;
using Deloitte.Cortex.Shared.Clients.Security.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Deloitte.Cortex.Shared.Authorization
{
    public abstract class PermissionAttribute: Attribute, IFilterMetadata
    {
        public string Entity { get; }

        public Actions Actions { get; }

        public abstract Tier Tier { get; }

        protected PermissionAttribute(string entity, Actions actions)
        {
            Entity = entity;
            Actions = actions;
        }

        protected PermissionAttribute(Entity entity, Actions actions): this(entity.ToString(), actions) { }

        /// <summary>
        /// Entity from permission required by this attribute with regards to current <see cref="AuthorizationFilterContext"/>.
        /// </summary>
        public virtual string GetEntity(AuthorizationFilterContext context) => Entity;

        /// <summary>
        /// <see cref="Actions"/> from permission required by this attribute with regards to current <see cref="AuthorizationFilterContext"/>.
        /// </summary>
        public virtual Actions GetActions(AuthorizationFilterContext context) => Actions;

        /// <summary>
        /// Client or engagement id. <c>null</c> for <see cref="Authorization.Tier.App"/>.
        /// </summary>
        public abstract Guid? GetTierId(AuthorizationFilterContext context);
    }
}