﻿namespace Deloitte.Cortex.Shared.Authorization
{
    public static class CallerTypeRole
    {
        public const string User = "Caller.User";
        public const string Service = "Caller.Service";
        public const string SupplementaryAudiencePrefix = "Caller.Audience.";
    }
}
