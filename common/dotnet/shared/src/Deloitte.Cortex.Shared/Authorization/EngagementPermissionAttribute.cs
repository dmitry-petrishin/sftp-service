﻿using System;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Clients.Security.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Deloitte.Cortex.Shared.Authorization
{
    /// <summary>
    /// Attribute to specify engagement-tier permissions for action or controller.
    /// Multiple attributes are grouped with 'and' operator; action inherit attributes from its controller.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class EngagementPermissionAttribute: PermissionAttribute
    {
        public override Tier Tier => Tier.Engagement;

        public EngagementPermissionAttribute(Entity entity, Actions actions): base(entity, actions) { }

        public EngagementPermissionAttribute(string entity, Actions actions): base(entity, actions) { }

        /// <summary>
        /// Name of the 'engagement id' parameter in route template.
        /// </summary>
        public string IdParameter { get; set; } = "engagementId";

        public override Guid? GetTierId(AuthorizationFilterContext context)
        {
            var clientId = (string) context.RouteData.Values.GetOrDefault(IdParameter);
            if (clientId == null) throw new InvalidOperationException($"\"{IdParameter}\" not found in route parameters.");

            return Guid.Parse(clientId);
        }
    }
}