﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Deloitte.Cortex.Shared.Serialization.Configuration;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Authorization
{
    /// <inheritdoc cref="IAuthenticationService"/>
    public class AuthenticationService: IAuthenticationService
    {
        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger logger;
        private readonly Authentication config;
        private Dictionary<AuthenticationLevelEnum, X509Certificate2> certificates = new Dictionary<AuthenticationLevelEnum, X509Certificate2>();
        private bool initialized = false;

        public AuthenticationService(ILoggerFactory loggerFactory, Authentication config)
        {
            this.loggerFactory = loggerFactory;
            this.logger = loggerFactory.CreateLogger<AuthenticationService>();
            this.config = config;
        }

        /// <summary>
        /// Initialized all necessary certificates for each level of authentication and validates configuration
        /// </summary>
        public async Task Initialize()
        {
            if (initialized)
                return;

            logger.LogInformation("Initializing authentication service");

            // Fix ADAL logging writing everything to console by default
            LoggerCallbackHandler.UseDefaultLogging = false;
            LoggerCallbackHandler.Callback = new LoggerAdalLogCallback(logger);

            if (config.Main.SPNSource.Type == SPNSource.TypeEnum.LocalCertificate)
                throw new Exception("Main level authentication does not support local certificate");
            if (config.KEK?.SPNSource.Type == SPNSource.TypeEnum.LocalCertificate)
                throw new Exception("KEK level authentication does not support local certificate");

            if (config.Initialisation != null)
            {
                logger.LogInformation("Init level specified");

                // Initialize initialisation level certificate
                var level = config.Initialisation;
                if (string.IsNullOrWhiteSpace(level.Tenant) || string.IsNullOrWhiteSpace(level.ClientId))
                    throw new Exception($"{nameof(level.Tenant)} or {nameof(level.ClientId)} are not specified for {nameof(config.Initialisation)} level");
                if (level.SPNSource == null)
                    throw new Exception($"{nameof(level.SPNSource)} is not specified for {nameof(config.Initialisation)} level");
                if (level.SPNSource.Type != SPNSource.TypeEnum.LocalCertificate)
                    throw new Exception($"{nameof(config.Initialisation)} level SPN must be local certificate");

                {
                    var options = (SPNSource.LocalCertificateOptions)level.SPNSource.GetOptions();
                    var cert = new X509Certificate2(options.Path, options.Password);
                    ValidateAndAdd(cert, AuthenticationLevelEnum.Initialisation);
                }

                // Initialize Main level certificate
                level = config.Main;
                if (string.IsNullOrWhiteSpace(level.Tenant) || string.IsNullOrWhiteSpace(level.ClientId))
                    throw new Exception($"{nameof(level.Tenant)} or {nameof(level.ClientId)} are not specified for {nameof(config.Main)} level");
                if (level.SPNSource.Type != SPNSource.TypeEnum.KeyVaultCertificate)
                    throw new Exception($"When {nameof(config.Initialisation)} level is specified, Main level supports only: {SPNSource.TypeEnum.KeyVaultCertificate}");

                {
                    var options = (SPNSource.KeyVaultCertificateOptions)level.SPNSource.GetOptions();
                    if (options?.CertificateId.NotEmpty != true)
                        throw new Exception($"CertificateId is not set for {nameof(config.Main)} level");

                    await AddCertificate(options, AuthenticationLevelEnum.Initialisation, AuthenticationLevelEnum.Main);
                }
            }

            // Initialize SecretsStorage level certificate, if needed
            if (config.SecretsStorage != null)
            {
                var level = config.SecretsStorage;
                if (string.IsNullOrWhiteSpace(level.Tenant) || string.IsNullOrWhiteSpace(level.ClientId))
                    throw new Exception($"{nameof(level.Tenant)} or {nameof(level.ClientId)} are not specified for {nameof(config.SecretsStorage)} level");
                if (level.SPNSource == null)
                    throw new Exception($"{nameof(level.SPNSource)} is not specified for {nameof(config.SecretsStorage)} level");

                if (level.SPNSource.Type == SPNSource.TypeEnum.Secret)
                {
                    var options = (SPNSource.SecretOptions)level.SPNSource.GetOptions();
                    if (string.IsNullOrWhiteSpace(options.Value))
                        throw new Exception($"{nameof(config.SecretsStorage)} level SPN has no secret value specified");
                }
                else if (level.SPNSource.Type == SPNSource.TypeEnum.LocalCertificate)
                {
                    var options = (SPNSource.LocalCertificateOptions)level.SPNSource.GetOptions();
                    var cert = new X509Certificate2(options.Path, options.Password);
                    ValidateAndAdd(cert, AuthenticationLevelEnum.SecretsStorage);
                }
                else if (level.SPNSource.Type == SPNSource.TypeEnum.KeyVaultCertificate)
                {
                    var options = (SPNSource.KeyVaultCertificateOptions)level.SPNSource.GetOptions();
                    if (options?.CertificateId.NotEmpty != true)
                        throw new Exception($"CertificateId is not set for {nameof(config.SecretsStorage)} level");

                    await AddCertificate(options, AuthenticationLevelEnum.Initialisation, AuthenticationLevelEnum.SecretsStorage);
                }
            }

            // Load KEK certificate if needed
            if (config.KEK != null)
            {
                // Initialize KEK level certificate
                var level = config.KEK;
                if (string.IsNullOrWhiteSpace(level.Tenant) || string.IsNullOrWhiteSpace(level.ClientId))
                    throw new Exception($"{nameof(level.Tenant)} or {nameof(level.ClientId)} are not specified for {nameof(config.KEK)} level");
                if (level.SPNSource.Type != SPNSource.TypeEnum.KeyVaultCertificate)
                    throw new Exception($"When {nameof(config.KEK)} level is specified, it supports only: {SPNSource.TypeEnum.KeyVaultCertificate}");

                {
                    var options = (SPNSource.KeyVaultCertificateOptions)level.SPNSource.GetOptions();
                    if (options?.CertificateId.NotEmpty != true)
                        throw new Exception($"CertificateId is not set for {nameof(config.KEK)} level");

                    await AddCertificate(options, AuthenticationLevelEnum.Main, AuthenticationLevelEnum.KEK);
                }
            }

            initialized = true;
            logger.LogInformation($"{GetType().Name} initialized.");
        }

        private async Task AddCertificate(SPNSource.KeyVaultCertificateOptions options, AuthenticationLevelEnum keyVaultLevel, AuthenticationLevelEnum certificateLevel)
        {
            initialized = true;
            try
            {
                var client = new Clients.KeyVault.KeyVaultClient(options.KeyVaultName, loggerFactory.CreateLogger<Clients.KeyVault.KeyVaultClient>(), this);
                var response = await client.GetSecretAsync(options.CertificateId.Name, options.CertificateId.Version, keyVaultLevel);
                var raw = Convert.FromBase64String(response);
                var cert = new X509Certificate2(raw, options.Password);
                ValidateAndAdd(cert, certificateLevel);
            }
            finally
            {
                initialized = false;
            }
        }

        private void ValidateAndAdd(X509Certificate2 cert, AuthenticationLevelEnum level)
        {
            if (!cert.HasPrivateKey)
                throw new Exception($"Certificate for level {level} #{cert.Thumbprint} does not have private key inside");

            if (certificates.ContainsKey(level))
                throw new Exception($"Certificate for level {level} has been already added");

            certificates.Add(level, cert);

            logger.LogInformation($"Initialized certificate for {level} level, thumbprint is {cert.Thumbprint}, serial is {cert.SerialNumber}");
        }

        private AuthenticationLevel GetLevel(AuthenticationLevelEnum level)
        {
            switch (level)
            {
                case AuthenticationLevelEnum.Initialisation:
                    return config.Initialisation;
                case AuthenticationLevelEnum.Main:
                    return config.Main;
                case AuthenticationLevelEnum.SecretsStorage:
                    return config.SecretsStorage ?? config.Main;
                case AuthenticationLevelEnum.KEK:
                    return config.KEK ?? config.Main;
                default:
                    throw new ArgumentException($"Unknown level: {level.ToString()}");
            }
        }

        private X509Certificate2 GetCertificate(AuthenticationLevelEnum level)
        {
            var actualLevel = level;

            if (level == AuthenticationLevelEnum.KEK && config.KEK == null)
                actualLevel = AuthenticationLevelEnum.Main;
            else if (level == AuthenticationLevelEnum.SecretsStorage && config.SecretsStorage == null)
                actualLevel = AuthenticationLevelEnum.Main;

            if (!certificates.ContainsKey(actualLevel))
                throw new Exception($"Certificate was not found for {actualLevel.ToString()} level authentication");

            return certificates[actualLevel];
        }

        // AuthenticationContext will automatically cache and renew access tokens as needed for each client
        // By default cache is stored in memory and is shared across different instances
        // http://www.cloudidentity.com/blog/2015/08/13/adal-3-didnt-return-refresh-tokens-for-5-months-and-nobody-noticed/
        public async Task<TokenResult> GetServiceTokenAsync(string resource = null, AuthenticationLevelEnum level = AuthenticationLevelEnum.Main)
        {
            await Initialize();
            var config = GetLevel(level);

            AuthenticationResult result;
            logger.LogDebug($"Retrieving token for level {level}, resource {resource}, authority {config.Authority}");
            var context = new AuthenticationContext(config.Authority, false);

            if (config.SPNSource.Type != SPNSource.TypeEnum.Secret)
            {
                var clientCertificate = GetCertificate(level);
                var clientAssertion = new ClientAssertionCertificate(config.ClientId, clientCertificate);
                if (resource != null)
                    result = await context.AcquireTokenAsync(resource, clientAssertion);
                else
                    result = await context.AcquireTokenAsync(config.ClientId, clientAssertion);
            }
            else
            {
                var clientCredential = new ClientCredential(config.ClientId, ((SPNSource.SecretOptions)config.SPNSource.GetOptions()).Value);
                if (resource != null)
                    result = await context.AcquireTokenAsync(resource, clientCredential);
                else
                    result = await context.AcquireTokenAsync(config.ClientId, clientCredential);
            }

            return new TokenResult(result);
        }
    }
}