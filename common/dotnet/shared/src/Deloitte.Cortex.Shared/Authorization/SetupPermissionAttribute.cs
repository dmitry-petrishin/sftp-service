﻿using System;
using Deloitte.Cortex.Shared.Clients.Security.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Deloitte.Cortex.Shared.Authorization
{
    /// <summary>
    /// Attribute to specify setup-level permissions for action or controller.
    /// Multiple attributes are grouped with 'and' operator; action inherit attributes from its controller.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class SetupPermissionAttribute: PermissionAttribute
    {
        public override Tier Tier => Tier.App;

        public SetupPermissionAttribute(string entity, Actions actions): base(entity, actions) { }

        public SetupPermissionAttribute(Entity entity, Actions actions): base(entity, actions) { }

        public override Guid? GetTierId(AuthorizationFilterContext context) => null;
    }
}