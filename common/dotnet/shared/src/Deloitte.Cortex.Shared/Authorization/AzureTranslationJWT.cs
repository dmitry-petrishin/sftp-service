using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Authorization
{
    public class AzureTranslationJWT
    {
        [JsonProperty("iss")] 
        public string Iss { get; set; }

        [JsonProperty("exp")] 
        public double ExpirationUnixTime { get; set; }
        
        [JsonProperty("region")] 
        public string Region { get; set; }
        
        [JsonProperty("subscription-id")] 
        public string SubscriptionId { get; set; }
        
        [JsonProperty("product-id")] 
        public string ProductId { get; set; }
        
        [JsonProperty("cognitive-services-endpoint")] 
        public string CognitiveServicesEndpoint { get; set; }
        
        [JsonProperty("azure-resource-id")] 
        public string AzureResourceId { get; set; }
        
        [JsonProperty("scope")] 
        public string Scope { get; set; }
        
        [JsonProperty("aud")] 
        public string Aud { get; set; }
    }
}