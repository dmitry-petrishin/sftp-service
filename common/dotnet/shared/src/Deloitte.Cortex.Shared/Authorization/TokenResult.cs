﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Deloitte.Cortex.Shared.Authorization
{
    /// <summary>
    /// Wrapper for enabling mocking sealed class
    /// </summary>
    public class TokenResult
    {
        private readonly AuthenticationResult result;

        public virtual string AccessTokenType
        {
            get
            {
                return result.AccessTokenType;
            }
        }

        public virtual string AccessToken
        {
            get
            {
                return result.AccessToken;
            }
        }

        public TokenResult()
        {
        }

        public TokenResult(AuthenticationResult result)
        {
            this.result = result;
        }
    }
}
