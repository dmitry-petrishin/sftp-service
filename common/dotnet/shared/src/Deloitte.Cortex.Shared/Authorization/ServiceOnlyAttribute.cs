﻿using Microsoft.AspNetCore.Authorization;

namespace Deloitte.Cortex.Shared.Authorization
{
    /// <summary>
    /// Only allow calls from another service.
    /// </summary>
    public class ServiceOnlyAttribute: AuthorizeAttribute
    {
        /// <summary>
        /// Not yet used.
        /// </summary>
        /// <remarks>But you my provide it to describe what services depends on your endpoint</remarks>
        public string ServiceName { get; }

        /// <summary>
        /// Only allow calls from another service.
        /// </summary>
        public ServiceOnlyAttribute(string serviceName = null)
        {
            ServiceName = serviceName;
            Roles = CallerTypeRole.Service;
        }
    }
}