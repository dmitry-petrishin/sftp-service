﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Deloitte.Cortex.Shared.Authorization
{
    public static class PermissionsAuthorizationExtensions
    {
        /// <summary>
        /// Adds authorization filter required for <see cref="PermissionAttribute"/>'s to work.
        /// </summary>
        public static IServiceCollection AddPermissionsAuthorization(this IServiceCollection services)
        {
            services.AddScoped<PermissionsAuthorizationFilter>(); // Scoped because implicitly depends on the current user
            services.Configure<MvcOptions>(mvc =>
                mvc.Filters.Add(new ServiceFilterAttribute(typeof(PermissionsAuthorizationFilter))));

            return services;
        }
    }
}