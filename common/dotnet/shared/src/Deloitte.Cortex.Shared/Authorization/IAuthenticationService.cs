using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Authorization
{
    /// <summary>
    /// Service for obtaining service security token for S2S authenticated calls.
    /// Thread safe and can be registered as a singleton.
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// Provides authentication token for current service for use by authenticated service-2-service calls.
        /// Token is obtained from AD and is cached until expires.
        /// </summary>
        /// <returns>Newly acquired or cached security token.</returns>
        Task<TokenResult> GetServiceTokenAsync(string resource = null, AuthenticationLevelEnum level = AuthenticationLevelEnum.Main);
        Task Initialize();
    }
}