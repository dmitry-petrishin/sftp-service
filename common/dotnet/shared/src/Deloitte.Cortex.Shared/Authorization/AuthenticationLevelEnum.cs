﻿namespace Deloitte.Cortex.Shared.Authorization
{
    public enum AuthenticationLevelEnum
    {
        Initialisation,
        Main,
        SecretsStorage,
        KEK
    }
}
