﻿namespace Deloitte.Cortex.Shared.Authorization
{
    public enum Tier
    {
        App,
        Client,
        Engagement,
    }
}
