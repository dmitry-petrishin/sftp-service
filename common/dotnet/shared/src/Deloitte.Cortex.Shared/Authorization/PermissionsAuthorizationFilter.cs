﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Clients.Security;
using Deloitte.Cortex.Shared.Clients.Security.Models;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Deloitte.Cortex.Shared.Authorization
{
    public class PermissionsAuthorizationFilter: IAsyncAuthorizationFilter
    {
        public static readonly HttpStatusCode NotAuthorizedStatusCode = HttpStatusCode.Forbidden;

        private readonly ISecurityClient _securityClient;

        public PermissionsAuthorizationFilter(ISecurityClient securityClient)
        {
            _securityClient = securityClient;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.HttpContext.User.IsInRole(CallerTypeRole.Service))
                return; // Skip permissions check for service

            foreach (var byTier in context.Filters.OfType<PermissionAttribute>()
                .GroupBy(p => new { Tier = p.Tier, TierId = p.GetTierId(context) }))
            {
                Dictionary<string, Actions> permissions = byTier
                    .GroupBy(a => a.GetEntity(context), a => a.GetActions(context))
                    .ToDictionary(g => g.Key, g => g.Aggregate((actions, action) => actions | action));
                if (!permissions.Any()) continue;

                Tier tier = byTier.Key.Tier;
                Guid? tierId = byTier.Key.TierId;

                bool hasSetupPermissions = false;
                bool hasClientPermissions = false;
                bool hasEngagementPermissions = false;

                if (permissions.Any())
                {
                    hasSetupPermissions = await _securityClient.HasSetupPermissionsAsync(permissions);                    
                }
                if (tierId != null)
                {
                    hasClientPermissions = await _securityClient.HasClientPermissionsAsync((Guid)tierId, permissions);
                    hasEngagementPermissions =
                        await _securityClient.HasEngagementPermissionsAsync((Guid) tierId, permissions);
                }

                if (tier == Tier.App && !hasSetupPermissions ||
                    tier == Tier.Client && !hasClientPermissions ||
                    tier == Tier.Engagement && !hasEngagementPermissions)
                {
                    string permissionsString = $"{string.Join(", ", permissions.Select(p => $"{p.Value} {p.Key}"))}.";
                    string message = $"User doesn't have required {tier} permissions: {permissionsString}";
                    throw new UnauthorizedException(message);
                }
            }
        }
    }
}