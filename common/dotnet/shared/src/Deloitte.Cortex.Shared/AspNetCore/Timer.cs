﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class Timer
    {
        public class TimedExecutionResult<T>
        {
            public T Result;
            public TimeSpan ExecutionTime;

            public static implicit operator T(TimedExecutionResult<T> timedResult) => timedResult.Result;
        }

        public static async Task<TimeSpan> TimeAsync(Func<Task> action)
        {
            var timer = new Stopwatch();
            timer.Start();

            await action();

            timer.Stop();
            return timer.Elapsed;
        }

        public static async Task<TimedExecutionResult<T>> TimeAsync<T>(Func<Task<T>> action)
        {
            var timer = new Stopwatch();
            timer.Start();

            var result = await action();

            timer.Stop();
            return new TimedExecutionResult<T>
            {
                Result = result,
                ExecutionTime = timer.Elapsed
            };
        }

        public static Task StartPeriodicTask(Func<CancellationToken, Task> callback, TimeSpan interval, Action<Exception> exceptionHandler = null,
            CancellationToken cancellationToken = default)
        {
            return Task.Run(async () =>
            {
                while (true)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    try
                    {
                        await callback(cancellationToken);
                    }
                    catch (OperationCanceledException)
                    {
                        throw;
                    }
                    catch (Exception exception) when (exceptionHandler != null)
                    {
                        exceptionHandler(exception);
                    }

                    await Task.Delay(interval, cancellationToken);
                }
            }, cancellationToken);
        }

        public static Task StartPeriodicTask(Func<CancellationToken, Task<bool>> callback, TimeSpan interval,
            Action<Exception> exceptionHandler = null, CancellationToken cancellationToken = default)
        {
            return Task.Run(async () =>
            {
                bool @continue = false;
                do
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    try
                    {
                        @continue = await callback(cancellationToken);
                    }
                    catch (OperationCanceledException)
                    {
                        throw;
                    }
                    catch (Exception exception) when (exceptionHandler != null)
                    {
                        exceptionHandler(exception);
                    }

                    await Task.Delay(interval, cancellationToken);
                } while (@continue);
            }, cancellationToken);
        }
    }
}