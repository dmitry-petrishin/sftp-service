﻿using System;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.AspNetCore.Logging
{
    public class TimedLogger<T>: ILogger<T>
    {
        private readonly ILogger _logger;

        public TimedLogger(ILogger logger) => _logger = logger;

        public TimedLogger(ILoggerFactory loggerFactory): this(new Logger<T>(loggerFactory)) { }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
            Func<TState, Exception, string> formatter)
        {
            _logger.Log(logLevel, eventId, state, exception,
                (s, ex) => $"[{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss.fff}]: {formatter(s, ex)}");
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return _logger.IsEnabled(logLevel);
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            var dtStart = DateTime.UtcNow;
            var scope = new WrapperClass( _logger.BeginScope(state), () =>
            {
                var time = DateTime.UtcNow - dtStart;
                this.LogInformation($"End of logging scope {state}. Start time: {dtStart:u} Duration: {time}");
            });

            return scope;
        }

        private class WrapperClass:IDisposable
        {
            private readonly IDisposable _wrapped;
            private readonly Action _doWhat;

            public WrapperClass(IDisposable wrapped, Action doWhat)
            {
                _wrapped = wrapped;
                _doWhat = doWhat;
            }

            public void Dispose()
            {
                using (_wrapped)
                {
                }

                _doWhat();
            }
        }
    }
}