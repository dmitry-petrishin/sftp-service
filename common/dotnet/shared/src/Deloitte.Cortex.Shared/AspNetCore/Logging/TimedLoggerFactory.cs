﻿using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.AspNetCore.Logging
{
    public class TimedLoggerFactory: ILoggerFactory
    {
        private readonly ILoggerFactory _loggerFactory;

        public TimedLoggerFactory(ILoggerFactory loggerFactory) => _loggerFactory = loggerFactory;

        public void AddProvider(ILoggerProvider provider) => _loggerFactory.AddProvider(provider);

        public ILogger CreateLogger(string categoryName) => new TimedLogger<object>(_loggerFactory.CreateLogger(categoryName));

        public void Dispose() => _loggerFactory.Dispose();
    }
}