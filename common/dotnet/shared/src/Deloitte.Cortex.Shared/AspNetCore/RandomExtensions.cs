﻿using System;
using System.Linq;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class RandomExtensions
    {
        const string AlphanumericChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        /// <summary>
        /// Generates random alphanumeric string of given <paramref name="length"/> using <paramref name="random"/> seed.
        /// </summary>
        public static string NextString(this Random random, int length)
        {
            return new string(Enumerable.Repeat(AlphanumericChars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}