﻿using System.Collections.Generic;
using System.Linq;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class SetExtensions
    {
        /// <summary>
        /// Use this instead of <see cref="Enumerable.Contains{TSource}(IEnumerable{TSource},TSource)"/>
        /// as the last one is not guaranteed to be optimized for <see cref="ISet{T}"/> to have O(1) complexity.
        /// </summary>
        public static bool SetContains<T>(this ISet<T> set, T item)
        {
            return (set as HashSet<T>)?.Contains(item) ?? set.Overlaps(new[] { item });
        }

        /// <summary>
        /// Intersects <paramref name="set"/> and <paramref name="items"/> in O(<paramref name="items"/>) operations.
        /// </summary>
        public static ISet<T> SetIntersect<T>(this ISet<T> set, IEnumerable<T> items)
        {
            return new HashSet<T>(items.Where(set.SetContains));
        }

        /// <summary>
        /// Adds <paramref name="items"/> to the current set.
        /// </summary>
        public static void Add<T>(this ISet<T> set, IEnumerable<T> items)
        {
            foreach (var item in items) set.Add(item);
        }

        public static bool In<T>(this T item, ISet<T> set)
        {
            return set.SetContains(item);
        }
    }
}