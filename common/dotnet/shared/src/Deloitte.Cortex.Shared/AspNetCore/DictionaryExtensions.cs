﻿using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class DictionaryExtensions
    {
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue @default = default) =>
            dictionary.TryGetValue(key, out var value) ? value : @default;

        public static TValue? GetOrNull<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
            where TValue: struct =>
            dictionary.TryGetValue(key, out var value) ? value : (TValue?)null;
    }
}