﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Deloitte.Cortex.Shared.ExceptionHandling;
using NodaTime;

namespace Deloitte.Cortex.Shared.AspNetCore.Time
{
    public static class TimeZoneHelper
    {
        public static readonly DayOfWeek[] DaysOfWeek = new[]
            {
                DayOfWeek.Sunday,
                DayOfWeek.Monday, 
                DayOfWeek.Tuesday, 
                DayOfWeek.Wednesday,
                DayOfWeek.Thursday,
                DayOfWeek.Friday,
                DayOfWeek.Saturday
            };

        public static IReadOnlyDictionary<string, string> TimeZonesForUi = new ReadOnlyDictionary<string, string>(
            new Dictionary<string, string>
            {
                {"Etc/GMT", "GMT/Greenwich Mean Time (GMT)"},
                {"Etc/GMT-1", "ECT/European Central Time (GMT+1:00)"},
                {"Etc/GMT-2", "EET/Eastern European Time (GMT+2:00)"},
                {"Egypt", "ART/(Arabic) Egypt Standard Time (GMT+2:00)"},
                {"Etc/GMT-3", "EAT/Eastern African Time (GMT+3:00)"},
                {"MET", "MET/Middle East Time (GMT+3:30)"},
                {"Etc/GMT-4", "NET/Near East Time (GMT+4:00)"},
                {"Etc/GMT-5", "PLT/Pakistan Lahore Time (GMT+5:00)"},
                {"Indian/Maldives", "IST/India Standard Time (GMT+5:30)"},
                {"Etc/GMT-6", "BST/Bangladesh Standard Time (GMT+6:00)"},
                {"Etc/GMT-7", "VST/Vietnam Standard Time (GMT+7:00)"},
                {"Etc/GMT-8", "CTT/China Taiwan Time (GMT+8:00)"},
                {"Etc/GMT-9", "JST/Japan Standard Time (GMT+9:00)"},
                {"Australia/ACT", "ACT/Australia Central Time (GMT+9:30)"},
                {"Etc/GMT-10", "AET/Australia Eastern Time (GMT+10:00)"},
                {"Etc/GMT-11", "SST/Solomon Standard Time (GMT+11:00)"},
                {"Etc/GMT-12", "NST/New Zealand Standard Time (GMT+12:00)"},
                {"Etc/GMT+11", "MIT/Midway Islands Time (GMT-11:00)"},
                {"Etc/GMT+10", "HST/Hawaii Standard Time (GMT-10:00)"},
                {"Etc/GMT+9", "AST/Alaska Standard Time (GMT-9:00)"},
                {"US/Alaska", "ADT/Alaska Daylight Time (GMT-8:00)"},
                {"Etc/GMT+8", "PST/Pacific Standard Time (GMT-8:00)"},
                {"PST8PDT", "PDT/Pacific Daylight Time (GMT-7:00)"},
                {"Etc/GMT+7", "PNT/Phoenix Standard Time (GMT-7:00)"},
                {"MST", "MST/Mountain Standard Time (GMT-7:00)"},
                {"Etc/GMT+6", "MDT/Mountain Daylight Time (GMT-6:00)"},
                {"America/Belize", "CST/Central Standard Time (GMT-6:00)"},
                {"CST6CDT", "CDT/Central Daylight Time (GMT-5:00)"},
                {"Etc/GMT+5", "IET/Indiana Eastern Standard Time (GMT-5:00)"},
                {"EST", "EST/Eastern Standard Time (GMT-5:00)"},
                {"Etc/GMT+4", "EDT/Eastern Daylight Time (GMT-4:00)"},
                {"America/Puerto_Rico", "PRT/Puerto Rico and US Virgin Islands Time (GMT-4:00)"},
                {"Canada/Newfoundland", "CNT/Canada Newfoundland Time (GMT-3:30)"},
                {"Etc/GMT+3", "AGT/Argentina Standard Time (GMT-3:00)"},
                {"Brazil/East", "BET/Brazil Eastern Time (GMT-3:00)"},
                {"Etc/GMT+1", "CAT/Central African Time (GMT-1:00)"}
            });

        public static DateTime ApplyTimeZone(DateTime utcDt, string tzName)
        {
            var tz = GetTimeZone(tzName);
            return ApplyTimeZone(utcDt, tz);
        }
        
        public static TimeSpan GetOffset(DateTime utcReferenceTime, string rzName)
        {
            var tz = GetTimeZone(rzName);
            return GetOffset(utcReferenceTime, tz);
        }

        public static TimeSpan GetOffset(DateTime utcReferenceTime, DateTimeZone tz)
        {
           
            var instant = Instant.FromDateTimeUtc(utcReferenceTime);
            var offset = tz.GetUtcOffset(instant);
            return offset.ToTimeSpan();
        }

        public static DateTimeZone GetTimeZone(string name)
        {
            var abbrev = name.Split("/").First();
            var value = DateTimeZoneProviders.Tzdb.GetZoneOrNull(name) ?? DateTimeZoneProviders.Tzdb.GetZoneOrNull(abbrev);
            if (value == null)
            {
                throw new BadRequestException($"Time zone not parsed for: {string.Join(",", name, abbrev)}");
            }

            return value;
        }
        
        public static DateTime ApplyTimeZone(DateTime utcDt, DateTimeZone tz)
        {
            var instant = Instant.FromDateTimeUtc(utcDt);
            var result = utcDt + tz.GetUtcOffset(instant).ToTimeSpan();
            return result;
        }

        /// <summary>
        /// Time conversion from the specified time zone to UTC
        /// </summary>
        /// <param name="dt">Local time for conversion</param>
        /// <param name="tz">Time zone by GMT</param>
        /// <returns></returns>
        public static DateTime ToUtc(DateTime dt, DateTimeZone tz)
        {
            var instant = Instant.FromDateTimeUtc(dt);
            var result = dt - tz.GetUtcOffset(instant).ToTimeSpan();
            return result;
        }

    }
}
