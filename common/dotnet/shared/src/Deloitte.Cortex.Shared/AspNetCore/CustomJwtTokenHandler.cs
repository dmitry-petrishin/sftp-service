﻿using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public class CustomJwtTokenHandler : ISecurityTokenValidator
    {
        private int _maxTokenSizeInBytes = TokenValidationParameters.DefaultMaximumTokenSizeInBytes;
        private JwtSecurityTokenHandler _tokenHandler;

        private readonly string[] extraAudiences;
        private readonly string[] extraIssuers;
        private readonly SecurityKey[] extraKeys;

        public CustomJwtTokenHandler(IEnumerable<string> extraAudiences, IEnumerable<string> extraIssuers, IEnumerable<SecurityKey> extraKeys)
        {
            _tokenHandler = new JwtSecurityTokenHandler();

            this.extraAudiences = extraAudiences.ToArray();
            this.extraIssuers = extraIssuers.ToArray();
            this.extraKeys = extraKeys.ToArray();
        }

        public bool CanValidateToken
        {
            get
            {
                return true;
            }
        }

        public int MaximumTokenSizeInBytes
        {
            get
            {
                return _maxTokenSizeInBytes;
            }

            set
            {
                _maxTokenSizeInBytes = value;
            }
        }

        public bool CanReadToken(string securityToken)
        {
            var result = _tokenHandler.CanReadToken(securityToken);
            return result;
        }

        public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            validationParameters.IssuerSigningKeys = validationParameters.IssuerSigningKeys.Concat(extraKeys);
            //validationParameters.IssuerSigningKeyValidator = somedelegate
            validationParameters.ValidAudiences = new[] { validationParameters.ValidAudience }.Concat(extraAudiences);
            validationParameters.ValidAudience = null;
            validationParameters.ValidIssuers = validationParameters.ValidIssuers.Concat(extraIssuers);
#if DEBUG
            validationParameters.ValidateLifetime = false; // Debug
#endif

            var principal = _tokenHandler.ValidateToken(securityToken, validationParameters, out validatedToken);

            return principal;
        }

        private IEnumerable<SecurityKey> IssuerSigningKeyResolver(
            string token, 
            SecurityToken securitytoken, 
            string kid, 
            TokenValidationParameters validationparameters)
        {
            throw new System.NotImplementedException();
        }
    }
}
