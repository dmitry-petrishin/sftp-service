﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Comparison = System.StringComparison;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class StringExtensions
    {
        public static bool Contains(this string @string, string substring, Comparison comparison) =>
            @string.IndexOf(substring, comparison) >= 0;

        public static bool ContainsWord(this string @string, string word, bool ignoreCase = false) =>
            new Regex($@"\b{Regex.Escape(word)}\b", ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None)
                .IsMatch(@string);

        public static string[] Split(this string @string, char separator, StringSplitOptions options) =>
            @string.Split(new[] { separator }, options);

        public static string ToSentenceCase(this string s)
        {
            if (s == "") return "";
            return s.First().ToString().ToUpper()
                   + new string(s.Skip(1).ToArray()).ToLower();
        }
        
        public static string ToFirstUpper(this string s)
        {
            if (s == "") return "";
            return s.First().ToString().ToUpper()
                   + new string(s.Skip(1).ToArray());
        }

        public static string FirstToLower(this string s)
        {
            if (s == "") return "";
            return s.First().ToString().ToLower()
                   + new string(s.Skip(1).ToArray());
        }

        public static string FirstToUpper(this string s)
        {
            if (s == "") return "";
            return s.First().ToString().ToLower()
                   + new string(s.Skip(1).ToArray());
        }

        public static string GetEmailDomain(this string email)
        {
            var atIndex = email.IndexOf('@');
            if (atIndex <= 0 || atIndex != email.LastIndexOf('@') || atIndex == email.Length - 1)
                throw new FormatException($"\"{email}\" is not an email string.");

            return email.Substring(atIndex + 1);
        }

        public static bool EqualsAny(this string @string, Comparison comparison = Comparison.OrdinalIgnoreCase, params string[] strings) =>
            strings.Any(s => @string.Equals(s, comparison));
    }
}