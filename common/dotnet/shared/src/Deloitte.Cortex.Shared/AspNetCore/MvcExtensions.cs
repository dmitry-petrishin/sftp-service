﻿using Deloitte.Cortex.Shared.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class MvcExtensions
    {
        /// <summary>
        /// Sets 'global' authentication requirement for all MVC calls.
        /// Use <see cref="AllowAnonymousAttribute"/> to remove requirement for specific action or controller.
        /// </summary>
        public static void RequireAuthentication(this MvcOptions options)
        {
            options.Filters.Add(new AuthorizeFilter(new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build()));
        }

        /// <summary>
        /// Enables validation for all action parameters (not body-only) and automatically checks <see cref="ControllerBase.ModelState"/> before each action.
        /// </summary>
        /// <remarks>
        /// <see cref="ExceptionHandling.BadRequestException"/> with <see cref="Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary"/> details will throw if model is invalid.
        /// </remarks>
        public static void UseModelValidation(this MvcOptions options)
        {
            options.Filters.Add(new ModelValidationFilter());
            options.Filters.Add(new ValidateActionParametersAttribute());
        }

        /// <summary>
        /// Will invoke all validation attributes for actions parameters and add model errors to <see cref="Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary"/> if any.
        /// This is not done in MVC by default.
        /// </summary>
        public static void UseActionParametersValidation(this MvcOptions options)
        {
            options.Filters.Add(new ValidateActionParametersAttribute());
        }
    }
}
