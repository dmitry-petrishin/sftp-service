﻿using System;
using System.Linq;
using System.Security.Claims;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class ClaimsPrincipalExtensions
    {
        public static Guid GetObjectId(this ClaimsPrincipal user)
        {
            const string objectIdClaimType = "http://schemas.microsoft.com/identity/claims/objectidentifier";
            var objectIdClaim = user.Claims.First(claim => claim.Type == objectIdClaimType);
            return Guid.Parse(objectIdClaim.Value);
        }

        /// <summary>
        /// Tries to find <see cref="ClaimTypes.Email"/> <see cref="Claim"/> in provided <see cref="ClaimsPrincipal"/> and return its value.
        /// If claim is not present <c>null</c> is returned.
        /// </summary>
        public static string TryGetEmail(this ClaimsPrincipal user)
        {
            var email = user.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Email)?.Value;
            if (email == null || !email.Contains("@"))
                email = user.Claims.FirstOrDefault(claim => claim.Type.EndsWith("/upn"))?.Value;
            if (email == null || !email.Contains("@"))
                email = user.Identity?.Name;

            return email?.ToLower();
        }

        /// <summary>
        /// Tries to find <see cref="ClaimTypes.Email"/> <see cref="Claim"/> in provided <see cref="ClaimsPrincipal"/> and return its value.
        /// If claim is not present <see cref="Exception"/> is thrown.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetEmail(this ClaimsPrincipal user)
        {
            string res = TryGetEmail(user);
            if (string.IsNullOrWhiteSpace(res))
                throw new Exception("Claim Email not found");
            return res;
        }

        public static ClaimsIdentity AddRole(this ClaimsIdentity identity, string role)
        {
            identity.AddClaim(new Claim(ClaimTypes.Role, role));
            return identity;
        }
    }
}