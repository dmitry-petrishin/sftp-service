﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore.Scheduling.Models;

namespace Deloitte.Cortex.Shared.AspNetCore.HostedServices
{
    public interface ISchedulerService
    {
        Task Start(CancellationToken cancellationToken);

        void AddTask(string name, DateTime? executionTime, Func<Task> action);
        void AddTask(DateTime? executionTime, Func<Task> action);
        void AddTask(Func<Task> action);

        void RepeatTask(string name, DateTime? executionTime, TimeSpan interval, Func<Task> action);
        void RepeatTask(DateTime? executionTime, TimeSpan interval, Func<Task> action);
        void RepeatTask(TimeSpan interval, Func<Task> action);

        List<ScheduledTaskModel> GetTasks();

        bool IsStarted { get; }
    }
}
