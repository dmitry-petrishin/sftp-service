﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore.Scheduling.Models;
using Deloitte.Cortex.Shared.AspNetCore.Scheduling;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.AspNetCore.HostedServices
{
    public class SchedulerService: ISchedulerService
    {
        private readonly ScheduleProcessor _scheduleProcessor;
        private readonly ILogger _logger;

        public bool IsStarted { get; private set; }

        public SchedulerService(
            ILogger<SchedulerService> logger = null)
        {
            _scheduleProcessor = new ScheduleProcessor(logger);
            _logger = logger;
        }

        public async Task Start(CancellationToken cancellationToken)
        {
            IsStarted = true;
            _logger.LogInformation("Scheduler service started");
            
            await _scheduleProcessor.Start(cancellationToken);
        }

        public void AddTask(string name, DateTime? executionTime, Func<Task> action)
        {
            AddTaskInner(action, executionTime, name);
        }
        
        public void AddTask(DateTime? executionTime, Func<Task> action)
        {
            AddTaskInner(action, executionTime);
        }

        public void AddTask(Func<Task> action)
        {
            AddTaskInner(action, null);
        }

        public void RepeatTask(string name, DateTime? executionTime, TimeSpan interval, Func<Task> action)
        {
            RepeatTaskInner(action, interval, executionTime, name);
        }

        public void RepeatTask(DateTime? executionTime, TimeSpan interval, Func<Task> action)
        {
            RepeatTaskInner(action, interval, executionTime);
        }

        public void RepeatTask(TimeSpan interval, Func<Task> action)
        {
            RepeatTaskInner(action, interval);
        }

        public List<ScheduledTaskModel> GetTasks()
        {
            return _scheduleProcessor.GetSchedules();
        }
        
        private void AddTaskInner(Func<Task> action, DateTime? executionTime, string name = null)
        {
            name = name ?? Guid.NewGuid().ToString();
            executionTime = executionTime.GetValueOrDefault(DateTime.UtcNow);
            _scheduleProcessor.Add(new ScheduledTaskModel
            {
                Action = action,
                ExecutionTime = executionTime,
                Name = name
            });
        }

        private void RepeatTaskInner(Func<Task> action, TimeSpan interval, DateTime? executionTime = null, string name = null)
        {
            executionTime = executionTime.GetValueOrDefault(DateTime.UtcNow);
            Func<Task> actionWithReschedule = async () =>
            {
                var dtStart = DateTime.UtcNow;
                try
                {
                    await action();
                }
                finally
                {
                    var elapsed = DateTime.UtcNow - dtStart;
                    var nextRunTime = DateTime.UtcNow + interval - elapsed;
                    RepeatTaskInner(action, interval, nextRunTime, name);
                }
            };

            AddTaskInner(actionWithReschedule, executionTime, name);
        }
    }
}
