﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Deloitte.Cortex.Shared.AspNetCore.HostedServices
{
    public abstract class HostedServiceBase : IHostedService
    {
        private CancellationTokenSource _cts;
        private Task _executingTask;

        /// <summary>
        /// Use to run and wait
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Run(CancellationToken cancellationToken)
        {
            await ExecuteAsync(cancellationToken);
        }

        /// <summary>
        /// To be called by ASP.NET Core, don't use explicitly
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            // Create a linked token so we can trigger cancellation outside of this token's cancellation
            _cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            
            _executingTask = ExecuteAsync(_cts.Token);

            return Task.CompletedTask;
        }

        /// <summary>
        /// To be called by ASP.NET Core, don't use explicitly
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop called without start
            if (_executingTask == null)
            {
                return;
            }

            // Signal cancellation to the executing method
            _cts.Cancel();

            // Wait until the task completes or the stop token triggers
            await _executingTask;
        }

        // Derived classes should override this and execute a long running method until 
        // cancellation is requested
        protected abstract Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
