﻿using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests
{
    public static class RepositoryTestExtensions
    {
        public static Task CreateItemsAsync<T>(this IRepository<T> repository, params T[] items) where T: class
        {
            return repository.CreateItemsAsync(items);
        }
    }
}
