﻿using System;
using Deloitte.Cortex.Shared.AspNetCore.Tests.Authentication;
using Microsoft.AspNetCore.Authentication;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests
{
    public static class TestAuthenticationExtensions
    {
        /// <summary>
        /// Uses authentication designed for integration tests via <see cref="TestHost{TStartup}"/>.
        /// It expects a header in format "<see cref="TestAuthenticationOptions.AuthorizationHeader"/> {userEmail}" 
        /// where <c>userEmail</c> will be used as authenticated user email.
        /// </summary>
        public static AuthenticationBuilder AddTestAuthentication(this AuthenticationBuilder authentication, string scheme, Action<TestAuthenticationOptions> configureOptions = null)
        {
            return authentication.AddScheme<TestAuthenticationOptions, TestAuthenticationHandler>(scheme, scheme, configureOptions);
        }
    }
}
