﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore.Tests.Repository;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils;
using Deloitte.Cortex.Shared.Serialization;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests
{
    public class CleanupRepositoryHelper
    {
        public static T MakeCleanupRepository<T, S>(T repo)
            where T : class, IRepository<S> 
            where S : class, IIdentifier
        {
            var wrapped = repo.WrapToProxy(typeof(ICleanupRepository));

            var created = new List<S>();
            wrapped.Override<T, S, Task>(
                o => o.CreateItemAsync(null),
                async (o, arg1) =>
                {
                    await RepositoryExtensions.CreateItemsAsync(o, arg1);
                    created.Add(arg1);
                });

            ((ICleanupRepository)wrapped).Override(
                o => o.CleanupAsync(),
                async _ =>
                {
                    foreach (var i in created)
                    {
                        var item = await repo.TryGetItemByIdAsync(i.Id);
                        if (item != null)
                        {
                            await repo.DeleteItemAsync(item.Id);
                        }
                    }
                    created.Clear();
                });

            return wrapped;
        }
    }
}
