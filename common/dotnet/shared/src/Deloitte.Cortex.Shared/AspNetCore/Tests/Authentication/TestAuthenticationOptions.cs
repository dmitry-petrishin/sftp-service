﻿using Microsoft.AspNetCore.Authentication;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests.Authentication
{
    public sealed class TestAuthenticationOptions: AuthenticationSchemeOptions
    {
        /// <summary>
        /// Authorization header that signals test authentication should be used.
        /// If <c>null</c> test authentication won't be invoked.
        /// </summary>
        public string AuthorizationHeader { get; set; } = "Test";
    }
}