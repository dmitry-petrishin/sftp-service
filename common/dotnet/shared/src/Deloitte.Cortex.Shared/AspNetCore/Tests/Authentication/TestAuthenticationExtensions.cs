﻿using System.Net.Http;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests.Authentication
{
    public static class TestAuthenticationExtensions
    {
        /// <summary>
        /// Sets Authorization header of <paramref name="client"/> to authenticate as user if <see cref="TestAuthenticationHandler"/> is used.
        /// </summary>
        public static void AuthenticateAsUser(this HttpClient client, string userEmail)
        {
            client.DefaultRequestHeaders.AddOrReplace("Authorization", $"Test {userEmail}");
        }

        /// <summary>
        /// Sets Authorization header of <paramref name="client"/> to authenticate as service if <see cref="TestAuthenticationHandler"/> is used.
        /// </summary>
        /// <remarks>Do not use '@' symbol in <paramref name="serviceName"/>.</remarks>
        public static void AuthenticateAsService(this HttpClient client, string serviceName = "service")
        {
            client.DefaultRequestHeaders.AddOrReplace("Authorization", $"Test {serviceName}");
        }

        /// <summary>
        /// Clears <paramref name="client"/> Authorization handler to make <see cref="TestAuthenticationHandler"/> ignore its requests.
        /// </summary>
        public static void DontAuthenticate(this HttpClient client)
        {
            client.DefaultRequestHeaders.Remove("Authorization");
        }
    }
}
