﻿using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests.Authentication
{
    public sealed class TestAuthenticationHandler: AuthenticationHandler<TestAuthenticationOptions>
    {
        public TestAuthenticationHandler(
            IOptionsMonitor<TestAuthenticationOptions> options, ILoggerFactory logger,
            UrlEncoder encoder, ISystemClock clock):
            base(options, logger, encoder, clock) { }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            return Task.FromResult(HandleAuthenticate());
        }

        private AuthenticateResult HandleAuthenticate()
        {
            if (Options.AuthorizationHeader == null)
                return AuthenticateResult.NoResult();

            string testHeaderStart = Options.AuthorizationHeader + " ", requestHeader = Request.Headers.GetOrDefault("Authorization");
            if (requestHeader == null || !requestHeader.StartsWith(testHeaderStart))
                return AuthenticateResult.NoResult();

            var emailOrName = requestHeader.Substring(testHeaderStart.Length);
            return emailOrName.Contains("@")
                ? HandleUserAuthenticate(userEmail: emailOrName)
                : HandleServiceAuthenticate(serviceName: emailOrName);
        }

        private AuthenticateResult HandleUserAuthenticate(string userEmail)
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Email, userEmail)
            }, Scheme.Name);

            return AuthenticateResult.Success(new AuthenticationTicket(
                new ClaimsPrincipal(identity), new AuthenticationProperties(), Scheme.Name));
        }

        private AuthenticateResult HandleServiceAuthenticate(string serviceName)
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Role, CallerTypeRole.Service)
            }, Scheme.Name);

            return AuthenticateResult.Success(new AuthenticationTicket(
                new ClaimsPrincipal(identity), new AuthenticationProperties(), Scheme.Name));
        }
    }
}