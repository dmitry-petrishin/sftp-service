using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests.Repository
{
    public interface ICleanupRepository
    {
        Task CleanupAsync();
    }

    /// <summary>
    /// <see cref="IRepository{T}"/> decorator for testing.
    /// It tracks all created items and provides a <see cref="CleanupAsync()"/> method to remove them.
    /// </summary>
    public interface ICleanupRepository<T>: IRepository<T>, ICleanupRepository
        where T: class
    {
    }
}