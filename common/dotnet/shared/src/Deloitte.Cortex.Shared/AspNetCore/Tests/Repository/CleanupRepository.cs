using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.Azure.Documents.Client;

namespace Deloitte.Cortex.Shared.AspNetCore.Tests.Repository
{
    /// <inheritdoc cref="ICleanupRepository{T}"/>
    [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
    public class CleanupRepository<T>: ICleanupRepository<T> where T: class
    {
        private readonly ISet<string> _createdItemsIds = new HashSet<string>();

        public CleanupRepository(IRepository<T> repository, Func<T, string> idGetter = null)
        {
            _repository = repository;
            _idGetter = idGetter;
        }

        private string GetId(T item)
        {
            return _idGetter?.Invoke(item) ??
                   ((dynamic)item).Id.ToString();
        }

        public async Task CreateItemAsync(T item)
        {
            var id = GetId(item);
            await _repository.CreateItemAsync(item);
            _createdItemsIds.Add(id);
        }

        public async Task CreateItemsAsync(IEnumerable<T> items)
        {
            var ids = items.Select(GetId);
            await _repository.CreateItemsAsync(items);
            foreach (var id in ids) _createdItemsIds.Add(id);
        }

        public async Task<bool> UpsertItemAsync(T item)
        {
            var inserted = await _repository.UpsertItemAsync(item);
            _createdItemsIds.Add(GetId(item));
            return inserted;
        }

        public async Task DeleteItemAsync(string id)
        {
            await _repository.DeleteItemAsync(id);
            _createdItemsIds.Remove(id);
        }

        public async Task<long> DeleteItemsAsync(IEnumerable<string> ids)
        {
            var deletedCount = await _repository.DeleteItemsAsync(ids);
            foreach (var id in ids) _createdItemsIds.Remove(id);
            return deletedCount;
        }

        public Task<long> DeleteItemsAsync(Expression<Func<T, bool>> predicate = null)
        {
            return _repository.DeleteItemsAsync(predicate);
        }

        public Task CleanupAsync()
        {
            return DeleteItemsAsync(_createdItemsIds.ToArray()); // Make a copy to avoid 'collection was modified'
        }

        #region Implementation of IRepository<T>

        private readonly IRepository<T> _repository;
        private readonly Func<T, string> _idGetter;

        public Task<T> TryGetItemByIdAsync(string id)
        {
            return _repository.TryGetItemByIdAsync(id);
        }

        public Task<T> GetItemByIdAsync(string id)
        {
            return _repository.GetItemByIdAsync(id);
        }

        public Task<T> TryGetItemAsync(Expression<Func<T, bool>> predicate)
        {
            return _repository.TryGetItemAsync(predicate);
        }

        public Task<IList<T>> GetItemsAsync(Expression<Func<T, bool>> predicate, FeedOptions feedOptions = null)
        {
            return _repository.GetItemsAsync(predicate, feedOptions);
        }

        public Task<int> GetItemCountAsync(Expression<Func<T, bool>> predicate, FeedOptions feedOptions = null)
        {
            return _repository.GetItemCountAsync(predicate, feedOptions);
        }

        public Task<IList<TResult>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector,
            FeedOptions feedOptions = null)
        {
            return _repository.GetItemsAsync(predicate, selector, feedOptions);
        }

        public Task<Range<T>> GetItemsAsync(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy,
            int offset, int limit, bool sortAsc = true,
            FeedOptions feedOptions = null)
        {
            return _repository.GetItemsAsync(predicate, orderBy, offset, limit, sortAsc, feedOptions);
        }

        public async Task<Range<TResult>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, Expression<Func<T, object>> orderBy, int offset, int limit,
            bool sortAsc = true) where TResult : class
        {
            return await _repository.GetItemsAsync(predicate, selector, orderBy, offset, limit, sortAsc);
        }

        public Task<IList<T>> GetAllItemsAsync()
        {
            return _repository.GetAllItemsAsync();
        }

        public Task UpdateItemAsync(string id, T item)
        {
            return _repository.UpdateItemAsync(id, item);
        }

        public Task<long> UpdateItemAsync(Expression<Func<T, bool>> predicate, T item)
        {
            return _repository.UpdateItemAsync(predicate, item);
        }

        public Task UpdateItemsAsync(IEnumerable<T> items, Func<T, string> idSelector)
        {
            return _repository.UpdateItemsAsync(items, idSelector);
        }

        public Task UpdateItemAsync(IIdentifier item)
        {
            return _repository.UpdateItemAsync(item);
        }

        public Task<TResult> TryGetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult: class
        {
            return _repository.TryGetItemByIdAsync(id, selector);
        }

        public Task<TResult> GetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult: class
        {
            return _repository.GetItemByIdAsync(id, selector);
        }

        #endregion

    }

    public static class CleanupRepository
    {
        public static CleanupRepository<T> Create<T>(IRepository<T> repository, Func<T, string> idGetter = null) where T: class =>
            new CleanupRepository<T>(repository, idGetter);
    }
}