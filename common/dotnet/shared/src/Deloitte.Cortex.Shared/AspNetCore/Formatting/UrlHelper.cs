﻿using System;
using System.Collections.Generic;

using Microsoft.Extensions.Primitives;

namespace Deloitte.Cortex.Shared.AspNetCore.Formatting
{
    public static class UrlHelper
    {
        private static readonly UrlFormatProvider provider = new UrlFormatProvider();

        public static string GetUriFromArrays(string uri, IDictionary<string, StringValues> parameters)
        {
            string resultString = uri;
            foreach (KeyValuePair<string, StringValues> kv in parameters)
            {
                foreach (string v in kv.Value)
                {
                    resultString = Microsoft.AspNetCore.WebUtilities.QueryHelpers.AddQueryString(resultString, kv.Key, v);
                }
            }

            return resultString.ToString(provider);
        }

        public static string Format(FormattableString fs)
        {
            return fs.ToString(provider);
        }
    }
}
