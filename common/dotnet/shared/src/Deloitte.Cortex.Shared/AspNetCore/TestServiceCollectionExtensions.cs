using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    /// <summary>
    /// Extensions methods for <see cref="IServiceCollection"/> useful for testing.
    /// </summary>
    public static class TestServiceCollectionExtensions
    {
        private static bool Replace(this IServiceCollection services, ServiceDescriptor oldDescriptor, ServiceDescriptor newDescriptor)
        {
            var oldIndex = services.IndexOf(oldDescriptor);
            if (oldIndex < 0) return false;

            services[oldIndex] = newDescriptor;
            return true;
        }

        private static object Instantiate(this ServiceDescriptor service, IServiceProvider provider)
        {
            object instance = null;
            if (service.ImplementationFactory != null)
                instance = service.ImplementationFactory(provider);
            else if (service.ImplementationType != null)
                instance = provider.CreateInstance(service.ImplementationType);

            return instance;
        }

        // Todo: fix in scenario with pre-created singleton
        /// <summary>
        /// Replaces first <typeparamref name="TService"/> implementation.
        /// Old implementation is passed as parameter in <paramref name="wrapFactory"/>.
        /// </summary>
        public static bool Wrap<TService>(this IServiceCollection services, Func<TService, TService> wrapFactory)
        {
            var oldService = services.FirstOrDefault(sd => sd.ServiceType == typeof(TService));
            if (oldService == null) return false;

            ServiceDescriptor newService = null;

            if (oldService.ImplementationInstance != null)
            {
                var oldInstance = (TService)oldService.ImplementationInstance;
                var newInstance = wrapFactory(oldInstance);

                newService = new ServiceDescriptor(oldService.ServiceType, newInstance);
            }

            else if (oldService.ImplementationFactory != null)
            {
                Func<IServiceProvider, TService> oldFactory = provider => (TService)oldService.ImplementationFactory(provider);
                Func<IServiceProvider, object> newFactory = provider => wrapFactory(oldFactory(provider));

                newService = new ServiceDescriptor(oldService.ServiceType, newFactory, oldService.Lifetime);
            }

            else if (oldService.ImplementationType != null)
            {
                var oldType = oldService.ImplementationType;
                Func<IServiceProvider, object> newFactory = provider => wrapFactory((TService)provider.CreateInstance(oldType));

                newService = new ServiceDescriptor(oldService.ServiceType, newFactory, oldService.Lifetime);
            }

            if (newService != null) return services.Replace(oldService, newService);

            return false;
        }

        /// <summary>
        /// Replaces first <paramref name="serviceType"/> implementation.
        /// <see cref="ServiceProvider"/> is passed as parameter in <paramref name="newFactory"/>.
        /// </summary>
        public static bool Replace(this IServiceCollection services, Type serviceType, Func<IServiceProvider, object> newFactory)
        {
            var oldService = services.FirstOrDefault(sd => sd.ServiceType == serviceType);
            if (oldService == null) return false;

            ServiceDescriptor newService = new ServiceDescriptor(serviceType, newFactory, oldService.Lifetime);
            return services.Replace(oldService, newService);
        }

        /// <summary>
        /// Replaces first <typeparamref name="TService"/> implementation.
        /// <see cref="ServiceProvider"/> is passed as parameter in <paramref name="newFactory"/>.
        /// </summary>
        public static bool Replace<TService>(this IServiceCollection services, Func<IServiceProvider, TService> newFactory) =>
            services.Replace(typeof(TService), p => newFactory(p));

        /// <summary>
        /// Replaces first <paramref name="serviceType"/> singleton implementation.
        /// </summary>
        public static bool ReplaceSingleton(this IServiceCollection services, Type serviceType, object implementation)
        {
            var oldService = services.FirstOrDefault(sd => sd.ServiceType == serviceType && sd.Lifetime == ServiceLifetime.Singleton);
            if (oldService == null) return false;

            var newService = new ServiceDescriptor(oldService.ServiceType, implementation);
            return services.Replace(oldService, newService);
        }

        /// <summary>
        /// Replaces first <typeparamref name="TService"/> singleton implementation.
        /// </summary>
        public static bool ReplaceSingleton<TService>(this IServiceCollection services, TService implementation) =>
            services.ReplaceSingleton(typeof(TService), implementation);

        public static int InstantiateAllSingletons<TService>(this IServiceCollection services)
        {
            var count = 0;
            var provider = services.BuildServiceProvider();

            foreach (var service in services.ToArray().Where(s => s.ServiceType == typeof(TService) && s.Lifetime == ServiceLifetime.Singleton
                                                                                                    && s.ImplementationInstance == null))
            {
                services.Replace(service, new ServiceDescriptor(service.ServiceType, service.Instantiate(provider)));
                count++;
            }

            return count;
        }
    }
}