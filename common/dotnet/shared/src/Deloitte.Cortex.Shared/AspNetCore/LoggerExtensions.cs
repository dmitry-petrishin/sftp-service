﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Internal;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class LoggerExtensions
    {
        private static readonly Func<object, Exception, string> MessageFormatter = (state, exception) => state.ToString();

        public static void Log(this ILogger logger, LogLevel level, string message, params object[] args)
        {
            logger.Log(level, 0, new FormattedLogValues(message, args), null, MessageFormatter);
        }

        public static string ToDictionaryString<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return ToDictionaryString((IDictionary<TKey, TValue>)dictionary);
        }

        public static string ToDictionaryString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            var pairs = dictionary.Select(p => $"\"{p.Key}:\"{p.Value}\"\"");
            return $"[{string.Join(", ", pairs)}]";
        }

        public static string ToDictionaryString<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary)
        {
            var pairs = dictionary.Select(p => $"\"{p.Key}:\"{p.Value}\"\"");
            return $"[{string.Join(", ", pairs)}]";
        }

        public static string ToArrayString<T>(this IEnumerable<T> array, string separator = ", ")
        {
            return $"[{string.Join(separator, array)}]";
        }
    }
}