﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class LINQExtensions
    {
        private class SelectorEqualityComparer<T, TP>: IEqualityComparer<T>
        {
            private static readonly IEqualityComparer<TP> Comparer = EqualityComparer<TP>.Default;

            private readonly Func<T, TP> _selector;

            public SelectorEqualityComparer(Func<T, TP> selector)
            {
                if (selector == null) throw new ArgumentNullException(nameof(selector));
                _selector = selector;
            }

            public bool Equals(T x, T y)
            {
                return Comparer.Equals(_selector(x), _selector(y));
            }

            public int GetHashCode(T obj)
            {
                return Comparer.GetHashCode(_selector(obj));
            }
        }

        public static IEnumerable<T> Union<T>(this IEnumerable<T> source, params T[] items)
        {
            return source.Union(items.AsEnumerable());
        }

        public static IEnumerable<Tuple<T1, T2>> Cross<T1, T2>(this IEnumerable<T1> source1, IEnumerable<T2> source2)
        {
            return source1.Join(source2, _ => true, _ => true, Tuple.Create);
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(
            this IEnumerable<KeyValuePair<TKey, TValue>> pairs, IEqualityComparer<TKey> comparer = null)
        {
            return pairs.ToDictionary(p => p.Key, p => p.Value, comparer);
        }

        public static OrderedDictionary ToOrderedDictionary<TKey, TValue>(this IEnumerable source,
            Func<object, TKey> keySelector, Func<object, TValue> valueSelector, IEqualityComparer comparer = null)
        {
            var dictionary = new OrderedDictionary(comparer);
            foreach (var element in source) dictionary.Add(keySelector(element), valueSelector(element));
            return dictionary;
        }

        public static OrderedDictionary ToOrderedDictionary<T, TKey, TValue>(this IEnumerable<T> source,
            Func<T, TKey> keySelector, Func<T, TValue> valueSelector, IEqualityComparer comparer = null)
        {
            var dictionary = new OrderedDictionary(comparer);
            foreach (var element in source) dictionary.Add(keySelector(element), valueSelector(element));
            return dictionary;
        }

        public static OrderedDictionary ToOrderedDictionary<TKey, TValue>(this IEnumerable<TValue> source,
            Func<TValue, TKey> keySelector, IEqualityComparer comparer = null) =>
            source.ToOrderedDictionary(keySelector, v => v);

        public static OrderedDictionary ToOrderedDictionary<TKey, TValue>(
            this IEnumerable<KeyValuePair<TKey, TValue>> source, IEqualityComparer comparer = null) =>
            source.ToOrderedDictionary(p => p.Key, p => p.Value);

        public static ILookup<TKey, TValue> ToLookup<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> pairs)
        {
            return pairs.ToLookup(p => p.Key, p => p.Value);
        }

        public static ISet<T> AsSet<T>(this IEnumerable<T> source)
        {
            return source as ISet<T> ?? new HashSet<T>(source);
        }

        public static IList<T> AsList<T>(this IEnumerable<T> source)
        {
            return source as IList<T> ?? source.ToList();
        }

        public static bool In<T>(this T item, params T[] source)
        {
            return item.In(source.AsEnumerable());
        }

        public static bool In<T>(this T item, IEqualityComparer<T> comparer, params T[] source)
        {
            return item.In(comparer, source.AsEnumerable());
        }

        public static bool In<T>(this T item, IEqualityComparer<T> comparer, IEnumerable<T> source)
        {
            return source.Contains(item, comparer);
        }

        public static bool In<T>(this T item, IEnumerable<T> source)
        {
            return source.Contains(item);
        }

        public static T? FirstOrNull<T>(this IEnumerable<T> source)
            where T: struct
        {
            var items = source.Take(1).ToArray();
            if (items.Length > 0)
            {
                return items[0];
            }

            return null;
        }

        public static T? FirstOrNull<T>(this IEnumerable<T> source, Func<T, bool> predicate)
            where T: struct
        {
            var items = source.Where(predicate).Take(1).ToArray();
            if (items.Length > 0)
            {
                return items[0];
            }

            return null;
        }

        public static IEnumerable<T> Distinct<T, TP>(this IEnumerable<T> source, Func<T, TP> selector)
        {
            return source.Distinct(new SelectorEqualityComparer<T, TP>(selector));
        }

        public static int FirstIndex<T>(this IEnumerable<T> source, Predicate<T> predicate)
        {
            int index = 0;
            foreach (var element in source)
            {
                if (predicate(element)) return index;
                index++;
            }
            return -1;
        }

        public static T[] ToArray<T>(this IEnumerable<T> source, ref int count)
        {
            var array = new T[count];

            using (var enumerator = source.GetEnumerator())
                for (count = 0; count < array.Length && enumerator.MoveNext(); count++)
                    array[count] = enumerator.Current;

            return array;
        }

        public static T[] ToArray<T>(this IEnumerable<T> source, int count)
        {
            var array = new T[count];

            var enumerable = source.Take(5);
            foreach (var pair in enumerable.Select((el, i) => (el, i)))
                array[pair.i] = pair.el;

            return array;
        }

        /// <example>
        /// (a, b, c, d), (x, y) -> (a, x, b, y, c, d)
        /// </example>
        public static IEnumerable<T> Alternate<T>(this IEnumerable<T> source1, IEnumerable<T> source2)
        {
            using (IEnumerator<T> enumerator1 = source1.GetEnumerator(), enumerator2 = source2.GetEnumerator())
            {
                bool next1, next2;
                while ((next1 = enumerator1.MoveNext()) | (next2 = enumerator2.MoveNext()))
                {
                    if (next1) yield return enumerator1.Current;
                    if (next2) yield return enumerator2.Current;
                }
            }
        }

        public static IOrderedEnumerable<T> Order<T>(this IEnumerable<T> source,
            IComparer<T> comparer = null) =>
            source.OrderBy(_ => _, comparer);

        public static IOrderedEnumerable<T> OrderDescending<T>(this IEnumerable<T> source,
            IComparer<T> comparer = null) =>
            source.OrderByDescending(_ => _, comparer);
    }
}