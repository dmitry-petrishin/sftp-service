﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    /// <summary>
    /// Provides few helper methods for enums with concrete return types.
    /// </summary>
    /// <typeparam name="TEnum">Enum type.</typeparam>
    /// <remarks>
    /// It's not possible to fully ensure that <typeparamref name="TEnum"/> is enum via generics limitations
    /// so this is implicit requirement.
    /// </remarks>
    public static class Enum<TEnum>
        where TEnum: struct, IComparable, IFormattable, IConvertible
    {
        static Enum()
        {
            if (!typeof(TEnum).GetTypeInfo().IsEnum)
                throw new ArgumentException($"{typeof(TEnum).FullName} is not an enum.", nameof(TEnum));
        }

        public static IEnumerable<TEnum> Values { get; } = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

        public static TEnum Parse(string value) => (TEnum)Enum.Parse(typeof(TEnum), value);

        public static T Attribute<T>(TEnum value) where T: Attribute =>
            typeof(TEnum).GetField(value.ToString()).GetCustomAttribute<T>();
    }
}