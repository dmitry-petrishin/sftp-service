﻿using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class TaskExtensions
    {
        /// <summary>
        /// Synchronously awaits task throwing first exception if any instead of <see cref="System.AggregateException"/>.
        /// </summary>
        public static T Await<T>(this Task<T> task) => task.GetAwaiter().GetResult();

        /// <summary>
        /// Synchronously awaits task throwing first exception if any instead of <see cref="System.AggregateException"/>.
        /// </summary>
        public static void Await(this Task task) => task.GetAwaiter().GetResult();
    }
}