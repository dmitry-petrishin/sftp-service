﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.DynamicProxy;

namespace Deloitte.Cortex.Shared.AspNetCore.Expressions
{
    public static class DictionaryFilterBuilder
    {
        public static Expression<Func<IDictionary<string, object>, object>> CreateFilter()
        {
            return t => t;
        }

        public static Expression<Func<T, object>> AddFieldAccessor<T>(this Expression<Func<T, object>> parent, string name)
        {
            return parent.NestCall(t => (t as IDictionary<string, object>)[name]);
        }

        public static Expression<Func<T, object>> AddFieldAccessors<T>(this Expression<Func<T, object>> parent, IEnumerable<string> names)
        {
            return names.Aggregate(parent, (current, name) => current.NestCall(t => (t as IDictionary<string, object>)[name]));
        }

        public static Expression<Func<T, object>> GetFieldAccessor<T>(string name)
        {
            return t => (t as IDictionary<string, object>)[name];
        }
    }
}
