﻿using System.Linq.Expressions;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    /// <summary>
    /// Sets all parameters of expression to one specified.
    /// </summary>
    /// <remarks>Useful for changing several expressions with the same input type to use single parameter.</remarks>
    internal class ParameterRebinder: ExpressionVisitor
    {
        private readonly ParameterExpression _parameter;

        public ParameterRebinder(ParameterExpression parameter)
        {
            _parameter = parameter;
        }

        public static Expression Replace(Expression expression, ParameterExpression parameter)
        {
            return new ParameterRebinder(parameter).Visit(expression);
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            return base.VisitParameter(_parameter);
        }
    }
    
}