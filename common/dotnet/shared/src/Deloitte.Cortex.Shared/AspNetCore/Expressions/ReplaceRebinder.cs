﻿using System.Linq.Expressions;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    /// <summary>
    /// Sets all parameters of expression to one specified.
    /// </summary>
    /// <remarks>Useful for changing several expressions with the same input type to use single parameter.</remarks>
    public class ReplaceRebinder: ExpressionVisitor
    {
        private readonly Expression _from;
        private readonly Expression _to;

        public ReplaceRebinder(Expression from, Expression to)
        {
            _from = from;
            _to = to;
        }

        public static Expression Replace(Expression expression, Expression from, Expression to)
        {
            return new ReplaceRebinder(from, to).Visit(expression);
        }

        public override Expression Visit(Expression node)
        {
            return node == _from 
                ? _to 
                : base.Visit(node);
        }
    }
    
}