﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class FilterExtensions
    {

        /// <summary>
        /// Combines expression filters with "And Also" operator (<c>&amp;&amp;</c>)
        /// </summary>
        /// <param name="filter1">First expression filter</param>
        /// <param name="filter2">Second expression filter</param>
        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> filter1, Expression<Func<T, bool>> filter2)
        {
            return AndAlso(filter1, filter2, false);
        }

        /// <summary>
        /// Combines expression filters with "And Also" operator (<c>&amp;&amp;</c>)
        /// </summary>
        /// <param name="filter1">First expression filter</param>
        /// <param name="filter2">Second expression filter</param>
        /// <param name="replaceNull">Returns <paramref name="filter2"/> if <paramref name="filter2"/> is null</param>
        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> filter1, Expression<Func<T, bool>> filter2,
            bool replaceNull)
        {
            if (replaceNull && filter1 == null) return filter2;

            return Expression.Lambda<Func<T, bool>>(
                Expression.AndAlso(filter1.Body, ParameterRebinder.Replace(filter2.Body, filter1.Parameters.Single())),
                filter1.Parameters.Single()
            );
        }

        public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> filter1, Expression<Func<T, bool>> filter2)
        {
            return OrElse(filter1, filter2, false);
        }

        public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> filter1, Expression<Func<T, bool>> filter2,
            bool replaceNull)
        {
            if (replaceNull && filter1 == null) return filter2;

            return Expression.Lambda<Func<T, bool>>(
                Expression.OrElse(filter1.Body, ParameterRebinder.Replace(filter2.Body, filter1.Parameters.Single())),
                filter1.Parameters.Single()
            );
        }

        public static Expression<Func<T, V>> NestCall<S, T, V>(
                    this Expression<Func<T, S>> exprToGetArg, 
                    Expression<Func<S, V>> exprToRunOnArg)
        {
            var invokation = ReplaceRebinder.Replace(
                exprToRunOnArg.Body,
                exprToRunOnArg.Parameters.Single(),
                exprToGetArg.Body);
            return Expression.Lambda<Func<T, V>>(
                invokation, 
                exprToGetArg.Parameters.Single()
            );
        }
    }
}