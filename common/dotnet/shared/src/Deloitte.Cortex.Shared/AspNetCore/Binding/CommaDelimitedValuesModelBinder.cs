﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Deloitte.Cortex.Shared.AspNetCore.Binding
{
    /// <summary>
    /// Provides binding for comma-delimited primitive values.
    /// Supported element types are limited to <see cref="IEnumerable{T}"/>, <see cref="Array"/>, <see cref="ISet{T}"/> and <see cref="HashSet{T}"/>.
    /// </summary>
    public class CommaDelimitedValuesModelBinder: IModelBinder
    {
        internal CommaDelimitedValuesModelBinder() // Should only be instantiated by provider
        {}

        public static readonly IModelBinderProvider Provider = new CommaDelimitedValuesModelBinderProvider();

        private const char Delimiter = ',';

        Task IModelBinder.BindModelAsync(ModelBindingContext context)
        {
            var metadata = context.ModelMetadata;
            var converter = TypeDescriptor.GetConverter(metadata.ElementType);
            var values = (context.ValueProvider.GetValue(context.ModelName).ToString() ?? "")
                .Split(new[] { Delimiter }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => converter.ConvertFromString(s.Trim()))
                .ToArray();

            object model = TryCreateArray(metadata, values) ?? TryCreateSet(metadata, values);
            if (model == null) throw new NotSupportedException($"{metadata.ModelType} is not supported.");

            context.Model = model;
            context.Result = ModelBindingResult.Success(model);
            return Task.CompletedTask;
        }

        private object TryCreateArray(ModelMetadata metadata, object[] values)
        {
            if (metadata.ModelType == typeof(IEnumerable<>).MakeGenericType(metadata.ElementType)
                || metadata.ModelType.IsArray)
            {
                var typedValues = Array.CreateInstance(metadata.ElementType, values.Length);
                values.CopyTo(typedValues, 0);
                return typedValues;
            }

            return null;
        }

        private object TryCreateSet(ModelMetadata metadata, object[] values)
        {
            if (metadata.ModelType == typeof(ISet<>).MakeGenericType(metadata.ElementType)
                || metadata.ModelType == typeof(HashSet<>).MakeGenericType(metadata.ElementType))
            {
                var typedValues = Array.CreateInstance(metadata.ElementType, values.Length);
                values.CopyTo(typedValues, 0);
                return Activator.CreateInstance(typeof(HashSet<>).MakeGenericType(metadata.ElementType), typedValues);
            }

            return null;
        }
    }
}
