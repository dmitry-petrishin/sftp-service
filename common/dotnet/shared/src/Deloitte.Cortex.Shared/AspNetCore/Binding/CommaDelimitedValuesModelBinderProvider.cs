﻿using System;
using System.ComponentModel;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Deloitte.Cortex.Shared.AspNetCore.Binding
{
    public class CommaDelimitedValuesModelBinderProvider : IModelBinderProvider
    {
        private static readonly CommaDelimitedValuesModelBinder ModelBinderInstance = new CommaDelimitedValuesModelBinder();

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            return context.Metadata.IsEnumerableType
                && IsPrimitiveType(context.Metadata.ElementType)
                && context.BindingInfo.BindingSource.CanAcceptDataFrom(BindingSource.Query)
                    ? ModelBinderInstance
                    : null;
        }

        private bool IsPrimitiveType(Type type)
        {
            return TypeDescriptor.GetConverter(type).CanConvertFrom(typeof(string));
        }
    }
}
