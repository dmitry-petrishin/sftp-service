﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class JsonExtensions
    {
        public static void Populate<T>(this JToken json, T target) where T : class
        {
            using (var sr = json.CreateReader())
            {
                JsonSerializer.CreateDefault().Populate(sr, target);
            }
        }

        public static string String(this JToken jToken, string propertyName) => jToken.Value<string>(propertyName);
    }
}
