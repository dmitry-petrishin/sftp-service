﻿using System;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.AspNetCore.Scheduling.Models
{
    public class ScheduledTaskModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name {get;set;}

        /// <summary>
        /// When to run
        /// </summary>
        public DateTime? ExecutionTime { get; set; }

        /// <summary>
        /// What to do
        /// </summary>
        public Func<Task> Action { get; set; }

        public string ToShortString()
        {
            var self = this;
            return $"Task {self.Name} on {self.ExecutionTime}";
        }
    }
}
