﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore.Scheduling.Models;
using Deloitte.Cortex.Shared.Collections;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.AspNetCore.Scheduling
{
    public class ScheduleProcessor
    {
        private readonly ILogger _logger;
        
        private readonly object _lockRoot = new object();
        private readonly OrderedQueue<ScheduledTaskModel> Schedules 
            = new OrderedQueue<ScheduledTaskModel>(t => t.ExecutionTime.GetValueOrDefault(DateTime.MinValue));
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(0, 1);

        public List<ScheduledTaskModel> GetSchedules()
        {
            lock (_lockRoot)
            {
                return Schedules.Values.ToList();
            }
        }

        public ScheduleProcessor(
            ILogger logger)
        {
            _logger = logger;
        }

        public void AddRange(List<ScheduledTaskModel> schedules)
        {
            foreach (var schedule in schedules)
            {
                this.Add(schedule);
            }
        }

        public void Add(ScheduledTaskModel task)
        {
            _logger.LogInformation($" Scheduling {task.ToShortString()}");
            ScheduledTaskModel minNextTime;
            lock (_lockRoot)
            {
                minNextTime = Schedules.Peek();
                Schedules.Add(task);
            }

            if (minNextTime != null && task.ExecutionTime >= minNextTime.ExecutionTime)
            {
                // no need to wake up
                return;
            }

            // wake up if was sleeping - newer or first task added 
            if (_semaphore.CurrentCount == 0)
            {
                _semaphore.Release();
            }
        }

        public async Task Start(CancellationToken token = default(CancellationToken))
        {
            while (!token.IsCancellationRequested)
            {
                await DoStep(token);
            }
        }

        public async Task DoStep(CancellationToken token)
        {
            var items = TakeTasksToExecute();
            var tasks = items.Select(ExecuteScheduledTask).ToArray();
            await SleepUntilNextTask();
        }

        private async Task SleepUntilNextTask()
        {
            ScheduledTaskModel soonestTask;

            lock (_lockRoot)
            {
                soonestTask = Schedules.Peek();
            }
            
            TimeSpan sleep;
            if (soonestTask != null)
            {
                sleep = soonestTask.ExecutionTime.GetValueOrDefault() - DateTime.UtcNow;
                _logger.LogDebug($" Next task is {soonestTask.ToShortString()}");
                if (sleep < TimeSpan.Zero)
                {
                    _logger.LogDebug($"[no real sleep]");
                    return;
                }
                _logger.LogDebug($" Sleeping for {sleep} until {soonestTask.ToShortString()}");
            }
            else
            {
                sleep = TimeSpan.FromSeconds(10);
                _logger.LogWarning($" No tasks, waiting for {sleep}");
            }

            var notTimedOut = await _semaphore.WaitAsync(sleep);
            if (notTimedOut)
            {
                _logger.LogDebug($" Newer tasks arrived, waking up to serve them");
            }
            else
            {
                _logger.LogDebug($" No new tasks scheduled, waking up on timer");
            }
        }

        private async Task ExecuteScheduledTask(ScheduledTaskModel task)
        {
            try
            {
                _logger.LogInformation($" Executing {task.ToShortString()}");
                var execTask = task.Action();
                await execTask;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $" {task.ToShortString()} failed");
            }
        }

        private List<ScheduledTaskModel> TakeTasksToExecute()
        {
            List<ScheduledTaskModel> tasks;

            lock (_lockRoot)
            {
                var now = DateTime.UtcNow;
                tasks =
                    Schedules.PopWhile(
                            t => t.ExecutionTime == null
                                 || t.ExecutionTime <= now)
                        .ToList();
            }

            return tasks;
        }
        
    }
}
