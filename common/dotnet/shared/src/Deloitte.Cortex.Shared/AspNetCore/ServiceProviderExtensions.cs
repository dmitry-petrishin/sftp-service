﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class ServiceProviderExtensions
    {
        /// <summary>
        /// Instantiate a type with constructor arguments provided directly and/or from an <see cref="T:System.IServiceProvider" />.
        /// </summary>
        /// <param name="provider">The service provider used to resolve dependencies.</param>
        /// <param name="parameters">Constructor arguments not provided by the <paramref name="provider" />.</param>
        /// <returns>An activated object of type <typeparamref name="T"/>.</returns>
        public static T CreateInstance<T>(this IServiceProvider provider, params object[] parameters) =>
            ActivatorUtilities.CreateInstance<T>(provider, parameters);

        /// <summary>
        /// Instantiate a type with constructor arguments provided directly and/or from an <see cref="T:System.IServiceProvider" />.
        /// </summary>
        /// <param name="provider">The service provider used to resolve dependencies.</param>
        /// <param name="instanceType">The type to activate.</param>
        /// <param name="parameters">Constructor arguments not provided by the <paramref name="provider" />.</param>
        /// <returns>An activated object of type <paramref name="instanceType"/>.</returns>
        public static object CreateInstance(this IServiceProvider provider, Type instanceType, params object[] parameters) =>
            ActivatorUtilities.CreateInstance(provider, instanceType, parameters);
    }
}