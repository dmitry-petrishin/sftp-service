﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Deloitte.Cortex.Shared.AspNetCore.RouteConstraints
{
    public class EmailRouteConstraint: IRouteConstraint
    {
        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            object value;
            if (!values.TryGetValue(routeKey, out value) || value == null)
                return false;

            var valueString = value as string ?? value.ToString();
            var firstAt = valueString.IndexOf("@", StringComparison.Ordinal);
            return firstAt != -1 && firstAt == valueString.LastIndexOf("@", StringComparison.Ordinal);
        }
    }
}