﻿using System;
using System.Linq;
using System.Reflection;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class TypeExtensions
    {
        public static bool IsEnum(this Type type) => type.GetTypeInfo().IsEnum;

        public static bool IsEnumOrNullableEnum(this TypeInfo type)
        {
            Type underlyingType;
            return type.IsEnum ||
                   (underlyingType = Nullable.GetUnderlyingType(type.AsType())) != null && underlyingType.IsEnum();
        }

        public static bool IsEnumOrNullableEnum(this Type type) => IsEnumOrNullableEnum(type.GetTypeInfo());

        public static bool ImplementsInterface(this Type type, Type @interface)
        {
            return !type.IsAbstract && !type.IsInterface
                && @interface.IsAssignableFrom(type); 
        }

        public static bool ImplementesGenericInterface(this Type type, Type @interface) =>
            (type.IsInterface ? new[] { type } : Enumerable.Empty<Type>())
            .Union(type.GetInterfaces())
            .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == @interface);

        public static Type TryGetGenericTypeDefinition(this Type type) => 
            type.IsGenericType ? type.GetGenericTypeDefinition() : null;

        public static bool CanBeAssignedTo(this Type derived, Type @base) => @base.IsAssignableFrom(derived);
    }
}