﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class HttpClientExtensions
    {
        /// <summary>
        /// Adds header if not present.
        /// </summary>
        /// <returns><c>true</c> if header added and <c>false</c> if already present.</returns>
        public static bool AddIfNotPresent(this HttpRequestHeaders headers, string name, string value)
        {
            if (headers.Contains(name)) return false;

            headers.Add(name, value);
            return true;
        }

        /// <summary>
        /// Adds or replaces header with new value.
        /// </summary>
        /// <returns><c>true</c> if header added and <c>false</c> if already present.</returns>
        public static void AddOrReplace(this HttpRequestHeaders headers, string name, string value)
        {
            if (headers.Contains(name)) headers.Remove(name);

            headers.Add(name, value);
        }

        public static IEnumerable<string> GetValues(this HttpRequestHeaders headers, string name)
        {
            IEnumerable<string> values;
            headers.TryGetValues(name, out values);

            return values ?? Enumerable.Empty<string>();
        }
    }
}