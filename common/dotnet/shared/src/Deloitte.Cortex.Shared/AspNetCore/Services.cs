﻿using Deloitte.Cortex.Shared.AspNetCore.Logging;
using Deloitte.Cortex.Shared.Serialization.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class Services
    {
        public static IServiceCollection AddTimedLogging(this IServiceCollection services)
        {
              // Todo: delay service fetching as with default factory
            var loggerFactory = services.BuildServiceProvider().GetRequiredService<ILoggerFactory>();
            services.ReplaceSingleton<ILoggerFactory>(new TimedLoggerFactory(loggerFactory));
            return services;
        }

        public static IServiceCollection AddHostedService<S, T>(this IServiceCollection services)
            where S : class
            where T : class, S, IHostedService
        {
            services.AddSingleton<S, T>();
            services.AddSingleton(p => (IHostedService)p.GetService<S>());
            return services;
        }

        public static IServiceCollection AddHostedService<S>(this IServiceCollection services)
            where S : class, IHostedService
        {
            return services.AddHostedService<S, S>();
        }

        public static IServiceCollection AddSecretsFetcher(this IServiceCollection services, SecretsStorage config)
        {
            if (config != null)
            {
                services.AddSingleton(config);
            }
            services.AddSingleton<Crypto.SecretsFetcher>();
            return services;
        }
    }
}
