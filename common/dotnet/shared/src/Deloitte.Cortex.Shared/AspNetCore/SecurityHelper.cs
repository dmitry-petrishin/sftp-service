﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Deloitte.Cortex.Shared.Serialization.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography.X509Certificates;
using System.Linq;

namespace Deloitte.Cortex.Shared.AspNetCore
{
    public static class SecurityHelper
    {
        private static bool IsService(this ClaimsPrincipal caller)
        {
            // Todo: find better way to determine if caller is a service
            return caller.TryGetEmail() == null;
        }

        /// <summary>
        /// Registers services required for service-2-service authentication (if <paramref name="authentication"/> != <c>null</c>) and permissions validation via <see cref="SetupPermissionAttribute"/>.
        /// </summary>
        public static IServiceCollection AddAuth(this IServiceCollection services, Authentication authentication = null)
        {
            if (authentication != null)
            {
                var authBuilder = services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme);
                authBuilder.AddJwtBearer(options =>
                {
                    options.SecurityTokenValidators.Clear();
                    var clientIds = authentication.IncomingClients.Select(x => x.ClientId).ToArray();
                    var authorities = authentication.IncomingClients.Select(x => x.Authority).ToArray();
                    var extraKeys = TryGetIncomingClientKeys(authentication).ToArray();
                    options.SecurityTokenValidators.Add(new CustomJwtTokenHandler(clientIds,authorities,extraKeys));

                    options.Audience = authentication.Main.ClientId;
                    options.Authority = authentication.Main.Authority;
                    options.SaveToken = true;

                    options.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = context =>
                        {
                            var caller = context.Principal;
                            ((ClaimsIdentity)caller.Identity)?.AddRole(caller.IsService() ? CallerTypeRole.Service : CallerTypeRole.User);

                            return Task.CompletedTask;
                        }
                    };
                });

                services.TryAddSingleton(authentication);
                services.TryAddSingleton<IAuthenticationService, AuthenticationService>();
            }

            services.AddPermissionsAuthorization();

            return services;
        }

        private static IEnumerable<SecurityKey> TryGetIncomingClientKeys(Authentication authentication)
        {
            foreach (var x in authentication.IncomingClients)
            {
                SecurityKey key = null;
                if (!string.IsNullOrWhiteSpace(x.SigningCertificatePath))
                {
                    try
                    {
                        var cert = new X509Certificate2(x.SigningCertificatePath);
                        key = new X509SecurityKey(cert);
                    }
                    catch
                    {
                    }
                }

                if (x.SigningKey != null)
                {
                    try
                    {
                        key = new JsonWebKey(x.SigningKey.ToString());
                    }
                    catch
                    {
                    }
                }

                if (key!= null) yield return key;
            }
        }
    }
}