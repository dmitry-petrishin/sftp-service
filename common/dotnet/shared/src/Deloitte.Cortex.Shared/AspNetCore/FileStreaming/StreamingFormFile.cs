﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Deloitte.Cortex.Shared.AspNetCore.FileStreaming
{
    public class StreamingFormFile : IFormFile
    {
        public Stream Stream {get;set;}

        public Stream OpenReadStream()
        {
            return Stream;
        }

        public void CopyTo(Stream target)
        {
            Stream.CopyTo(target);
        }

        public async Task CopyToAsync(Stream target, CancellationToken cancellationToken = new CancellationToken())
        {
            await Stream.CopyToAsync(target);
        }

        public string ContentType { get; set; }
        public string ContentDisposition { get; set; }
        public IHeaderDictionary Headers { get; set; }
        public long Length { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
    }
}
