﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;


namespace Deloitte.Cortex.Shared.AspNetCore.FileStreaming
{
    public static class FileStreamingHelper
    {
        private static readonly FormOptions _defaultFormOptions = new FormOptions();

        public static async Task<IFormFile> StreamFile(this HttpRequest request, string formFileName)
        {
            if (!MultipartRequestHelper.IsMultipartContentType(request.ContentType))
            {
                throw new Exception($"Expected a multipart request, but got {request.ContentType}");
            }

            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(request.ContentType),
                _defaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary, request.Body);

            var section = await reader.ReadNextSectionAsync();
            while (section != null)
            {
                ContentDispositionHeaderValue contentDisposition;
                var hasContentDispositionHeader =
                    ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out contentDisposition);

                if (!hasContentDispositionHeader)
                    continue;

                var name = HeaderUtilities.RemoveQuotes(contentDisposition.Name);
                if (!name.Equals(formFileName))
                    continue;

                var fileName = HeaderUtilities.RemoveQuotes(contentDisposition.FileName);

                if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                {
                    return new StreamingFormFile
                    {
                        ContentDisposition = contentDisposition.ToString(),
                        ContentType = section.ContentType,
                        FileName = fileName.ToString(),
                        Headers = null, //TODO: from section.headers
                        Length = contentDisposition.Size.GetValueOrDefault(0),
                        Stream = section.Body,
                        Name = name.ToString()
                    };
                }
                
                // Drains any remaining section body that has not been consumed and
                // reads the headers for the next section.
                section = await reader.ReadNextSectionAsync();
            }
            
            return null;
        }

        private static Encoding GetEncoding(MultipartSection section)
        {
            MediaTypeHeaderValue mediaType;
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out mediaType);
            // UTF-7 is insecure and should not be honored. UTF-8 will succeed in 
            // most cases.
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }
            return mediaType.Encoding;
        }
    }
}
