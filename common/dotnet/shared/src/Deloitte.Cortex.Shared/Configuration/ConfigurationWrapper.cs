﻿using System;

using Deloitte.Cortex.Shared.AspNetCore;

using Microsoft.Extensions.Configuration;

namespace Deloitte.Cortex.Shared.Configuration
{
    /// <summary> Wrapper for configuration files. </summary>
    /// <typeparam name="T"> Model of configuration file. </typeparam>
    public class ConfigurationWrapper<T>
        where T : class
    {
        /// <summary> Environment name. </summary>
        private readonly string environmentName;

        /// <summary>  Configuration file name. </summary>
        private readonly string fileName;

        /// <summary> Initializes a new instance of the <see cref="ConfigurationWrapper{T}"/> class. </summary>
        public ConfigurationWrapper()
        {
            this.environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Local";
            this.fileName = typeof(T).Name.ToLowerInvariant();
            IConfiguration configuration = this.GetConfiguration();

            this.Settings = configuration.ToJson().ToObject<T>();
        }

        /// <summary> Gets parsed configuration settings. </summary>
        public T Settings { get; }

        /// <summary> Gets configuration from (app/test)settings.json. </summary>
        /// <returns> The <see cref="IConfiguration"/>. </returns>
        private IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile($"{this.fileName}.json", optional: false, reloadOnChange: false)
                .AddJsonFile(
                    $"{this.fileName}.{this.environmentName}.json",
                    optional: false,
                    reloadOnChange: false)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
