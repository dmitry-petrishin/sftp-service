﻿using System;
using System.Net;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.ExceptionHandling
{
    /// <summary>
    /// Middleware processes all unhandled exception in subsequent handlers and writes <see cref="ErrorsResponse"/> to response.
    /// Details provided depends on whether the exception is <see cref="HttpException"/> and on whether current environment is considered 'safe'.
    /// Serialization happens via <see cref="JsonConverter"/>, content-type is 'application/json'.
    /// </summary>
    public sealed class ExceptionHandlingMiddleware
    {
        public class Options
        {
            public bool? IsSafeEnvironment { get; set; }
        }

        private readonly RequestDelegate _next;
        private readonly IHostingEnvironment _environment;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;

        public ExceptionHandlingMiddleware(RequestDelegate next, IHostingEnvironment environment, ILogger<ExceptionHandlingMiddleware> logger,
            Options options)
        {
            _environment = environment;
            _logger = logger;
            _next = next;

            IsSafeEnvironment = options.IsSafeEnvironment ??
                                _environment.IsDevelopment() || _environment.EnvironmentName.StartsWith("Local", StringComparison.OrdinalIgnoreCase);
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (HttpException exception)
            {
                await HandleHttpExceptionAsync(context, exception);
            }
            catch (Exception exception)
            {
                await HandleInternalException(context, exception);
            }
        }

        private bool IsSafeEnvironment { get; }

        private async Task HandleHttpExceptionAsync(HttpContext context, HttpException exception)
        {
            _logger.LogWarning($"Handling {nameof(HttpException)} ({exception.GetType().Name}).\n" +
                               $"Request: {context.Request.Method} {context.Request.Path}\n" +
                               $"User: {context.User?.TryGetEmail()}\n" +
                               $"Message: {exception.Message}");

            await WriteErrorsResponse(context.Response, exception.HttpCode, exception.Response);
        }

        private async Task HandleInternalException(HttpContext context, Exception exception)
        {
            _logger.LogError($"Handling internal exception ({exception.GetType().Name}).\n" +
                             $"Request: {context.Request.Method} {context.Request.Path}\n" +
                             $"User: {context.User?.TryGetEmail()}\n" +
                             $"Exception: {exception}");

            await WriteErrorsResponse(context.Response, HttpStatusCode.InternalServerError,
                IsSafeEnvironment ? new ErrorsResponse(exception) : new ErrorsResponse("Internal Server Error"));
        }

        private async Task WriteErrorsResponse(HttpResponse response, HttpStatusCode statusCode, ErrorsResponse errorsResponse)
        {
            try
            {
                response.ContentType = "application/json";
                response.StatusCode = (int) statusCode;

                await response.WriteAsync(JsonConvert.SerializeObject(errorsResponse)).ConfigureAwait(false);
            }

            catch(Exception)
            {
                _logger.LogError($"Failed to write {errorsResponse} to response.");
            }
        }
    }
}