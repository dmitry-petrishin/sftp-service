﻿using System.Linq;
using System.Net;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.ExceptionHandling
{
    public class BadRequestException : HttpException
    {
        public BadRequestException(string message = "Bad Request", int errorCode = 0) : base(message, HttpStatusCode.BadRequest, errorCode)
        {}

        public BadRequestException(ModelStateDictionary modelState) : this("Model state is not valid.")
        {
            Response.Errors.AddRange(modelState.SelectMany(pair => pair.Value.Errors, (pair, error) => new ErrorsResponse.Error
            {
                Code = 0, // Todo: agree on common code
                Property = pair.Key,
                Message = !string.IsNullOrEmpty(error.ErrorMessage)
                    ? error.ErrorMessage
                    : "Property value is not valid."
            }));
        }

        public BadRequestException(IEnumerable<ErrorsResponse.Error> errors): this((string)null)
        {
            Response.Errors.AddRange(errors);
        }
    }
}
