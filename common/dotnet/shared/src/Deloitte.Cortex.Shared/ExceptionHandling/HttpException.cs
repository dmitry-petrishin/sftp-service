﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Deloitte.Cortex.Shared.Serialization;

namespace Deloitte.Cortex.Shared.ExceptionHandling
{
    public class HttpException: Exception
    {
        public HttpStatusCode HttpCode { get; }

        public ErrorsResponse Response { get; } = new ErrorsResponse();

        protected void TryAddErrorMessage(string message, int errorCode)
        {
            if (message != null)
            {
                Response.Errors.Add(new ErrorsResponse.Error(message, errorCode));
            }
        }

        public HttpException(string message = "Internal Server Error", HttpStatusCode httpCode = HttpStatusCode.InternalServerError, int errorCode = 0): base(message)
        {
            HttpCode = httpCode;
            TryAddErrorMessage(message, errorCode);
        }

        public HttpException(string message, Exception innerException, HttpStatusCode httpCode = HttpStatusCode.InternalServerError, int errorCode = 0): this(message, httpCode, errorCode)
        {
            TryAddErrorMessage(innerException.Message, errorCode: 0);
        }

        public HttpException(Exception exception): this(exception.Message)
        {
        }

        public HttpException(IEnumerable<ErrorsResponse.Error> errors): this((string)null)
        {
            Response.Errors.AddRange(errors);
        }

        public HttpException(HttpStatusCode httpCode, IEnumerable<ErrorsResponse.Error> errors) : this((string)null)
        {
            HttpCode = httpCode;
            Response.Errors.AddRange(errors);
        }

        public override string Message => Response.Errors.Any()
            ? "Errors are:\n" + string.Join("\n", Response.Errors)
            : base.Message;
    }
}