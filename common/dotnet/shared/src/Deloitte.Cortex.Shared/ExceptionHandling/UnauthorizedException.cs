﻿using System.Net;

namespace Deloitte.Cortex.Shared.ExceptionHandling
{
    public class UnauthorizedException: HttpException
    {
        public UnauthorizedException(string message = "Unauthorized"): base(message, HttpStatusCode.Unauthorized) { }
    }
}