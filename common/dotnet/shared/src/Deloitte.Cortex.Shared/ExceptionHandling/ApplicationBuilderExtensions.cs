﻿using Microsoft.AspNetCore.Builder;

namespace Deloitte.Cortex.Shared.ExceptionHandling
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseExceptionHandling(this IApplicationBuilder app, bool? forwardAllExceptions = null)
        {
            app.UseMiddleware<ExceptionHandlingMiddleware>(new ExceptionHandlingMiddleware.Options
            {
                IsSafeEnvironment = forwardAllExceptions
            });
        }
    }
}