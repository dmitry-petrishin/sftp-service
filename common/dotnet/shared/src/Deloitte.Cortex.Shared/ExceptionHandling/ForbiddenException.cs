﻿using System.Net;

namespace Deloitte.Cortex.Shared.ExceptionHandling
{
    public class ForbiddenException: HttpException
    {
        public ForbiddenException(string message = "Forbidden"): base(message, HttpStatusCode.Forbidden) { }
    }
}