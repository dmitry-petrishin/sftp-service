﻿using System.Net;

namespace Deloitte.Cortex.Shared.ExceptionHandling
{
    public class NotFoundException : HttpException
    {
        public NotFoundException(string message = "Not Found") : base(message, HttpStatusCode.NotFound)
        {
        }
    }
}