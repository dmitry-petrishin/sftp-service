﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.Azure.Documents.Client;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Tries to fetch item from the database.
        /// Returns <c>null</c> if item is not present.
        /// </summary>
        Task<T> TryGetItemByIdAsync(string id);

        /// <summary>
        /// Tries to fetch item from the database.
        /// Returns <c>null</c> if item is not present.
        /// </summary>
        Task<TResult> TryGetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult : class;

        /// <summary>
        /// Fetches item from the database.
        /// </summary>
        /// <exception cref="ExceptionHandling.NotFoundException">Item with such <paramref name="id"/> doesn't exists.</exception>
        Task<T> GetItemByIdAsync(string id);

        /// <summary>
        /// Fetches selected item's data from the database.
        /// </summary>
        /// <exception cref="ExceptionHandling.NotFoundException">Item with such <paramref name="id"/> doesn't exists.</exception>
        Task<TResult> GetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult : class;

        /// <summary>
        /// Tries to fetch item first item mathing <paramref name="predicate"/> from the database.
        /// Returns <c>null</c> no matching items are present.
        /// </summary>
        Task<T> TryGetItemAsync(Expression<Func<T, bool>> predicate);

        Task<IList<T>> GetItemsAsync(Expression<Func<T, bool>> predicate, FeedOptions feedOptions = null);

        /// <summary>
        /// Gets item count filtered by optional predicate
        /// </summary>
        /// <param name="predicate">Filter for items</param>
        /// <param name="feedOptions">DocumentDB feed options</param>
        /// <returns>Count of items</returns>
        Task<int> GetItemCountAsync(Expression<Func<T, bool>> predicate = null, FeedOptions feedOptions = null);

        /// <summary>
        /// Select items with filtering and specific fields
        /// </summary>
        /// <typeparam name="TResult">Type of return class</typeparam>
        /// <param name="predicate">Filter for original elements</param>
        /// <param name="selector">Selector for fields needed from original class</param>
        /// <param name="feedOptions">DocumentDB feed options</param>
        /// <returns>Transformed and filtered collection</returns>
        Task<IList<TResult>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, FeedOptions feedOptions = null);

        /// <summary>
        /// Retrieves items using pagination
        /// </summary>
        /// <param name="predicate">Filter for items</param>
        /// <param name="orderBy">Order rules</param>
        /// <param name="offset">Offset</param>
        /// <param name="limit">Max item count to return</param>
        /// <param name="sortAsc">Sorting direction: <c>true</c> - ascending order, <c>false</c> - descending.</param>
        /// <returns>Object with pagination info and retrieved items for convenient usage in UI/consumer service</returns>
        Task<Range<T>> GetItemsAsync(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy, int offset, int limit, bool sortAsc = true, FeedOptions feedOptions = null);

        /// <summary>
        /// Retrieves items using pagination
        /// </summary>
        /// <param name="predicate">Filter for items</param>
        /// <param name="selector">Selector for fields needed from original class</param>
        /// <param name="orderBy">Order rules</param>
        /// <param name="offset">Offset</param>
        /// <param name="limit">Max item count to return</param>
        /// <param name="sortAsc">Sorting direction: <c>true</c> - ascending order, <c>false</c> - descending.</param>
        /// <returns>Object with pagination info and retrieved items for convenient usage in UI/consumer service</returns>
        Task<Range<TResult>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, Expression<Func<T, object>> orderBy, int offset, int limit, bool sortAsc = true) where TResult : class;

        Task<IList<T>> GetAllItemsAsync();

        /// <summary>
        /// Updates item in database.
        /// </summary>
        /// <exception cref="ExceptionHandling.NotFoundException">Item with such <paramref name="id"/> doesn't exist.</exception>
        Task UpdateItemAsync(string id, T item);

        /// <summary>
        /// Updates item in database by predicade
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="item"></param>
        /// <returns>Number of entries affected (1 or 0)</returns>
        Task<long> UpdateItemAsync(Expression<Func<T, bool>> predicate, T item);

        /// <summary>
        /// Updates item in database.
        /// </summary>
        /// <exception cref="ExceptionHandling.NotFoundException">At least one item from the ids specified doesn't exist.</exception>
        Task UpdateItemsAsync(IEnumerable<T> items, Func<T, string> idSelector);

        /// <summary>
        /// Updates item in database.
        /// </summary>
        /// <exception cref="ExceptionHandling.NotFoundException">Item with such id doesn't exist.</exception>
        Task UpdateItemAsync(IIdentifier item);

        /// <summary>
        /// Creates new item.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <exception cref="ExceptionHandling.ConflictException">Item with the same id already exists.</exception>
        Task CreateItemAsync(T item);

        Task CreateItemsAsync(IEnumerable<T> items);

        /// <summary>
        /// Updates item if present and creates new one otherwise.
        /// </summary>
        Task<bool> UpsertItemAsync(T item);

        /// <summary>
        /// Deletes item from the database.
        /// </summary>
        /// <exception cref="ExceptionHandling.NotFoundException">Item with specified <paramref name="id"/> doesn't exist.</exception>
        Task DeleteItemAsync(string id);

        /// <summary>
        /// Bulk delete of specified identifiers. Could be useful for tests.
        /// </summary>
        /// <param name="ids">Array of identifiers of documents to be removed</param>
        /// <exception cref="ExceptionHandling.NotFoundException">At least one item from the <paramref name="ids"/> doesn't exist.</exception>
        Task<long> DeleteItemsAsync(IEnumerable<string> ids);

        Task<long> DeleteItemsAsync(Expression<Func<T, bool>> predicate = null);
    }
}