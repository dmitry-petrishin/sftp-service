﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB
{
    public static class MongoDBExtensions
    {
        internal static MongoClientSettings AddX509Auth(this MongoClientSettings mongoSettings, MongoDBSettings settings,
            ILogger logger = null)
        {
            if (!string.IsNullOrWhiteSpace(settings.Crt))
            {
                logger?.LogInformation($"Importing client certificate {settings.Crt}");
                var clientCert = new X509Certificate2(settings.Crt);

                mongoSettings.SslSettings = new SslSettings
                {
                    ClientCertificates = new[] { clientCert },
                };

                if (!string.IsNullOrWhiteSpace(settings.CA))
                {
                    var serverCert = new X509Certificate2(settings.CA);
                    var serverCertTumbprint = serverCert.GetCertHashString();
                    mongoSettings.SslSettings.ServerCertificateValidationCallback = (sender, certificate, chain, errors) =>
                    {
                        var root = chain.ChainElements[chain.ChainElements.Count - 1].Certificate;
                        if (root.Thumbprint == serverCertTumbprint)
                        {
                            return true;
                        }
                        else
                        {
                            logger?.LogCritical("Failed to validate CA");
                            return false;
                        }
                    };
                }
            }

            return mongoSettings;
        }
    }
}