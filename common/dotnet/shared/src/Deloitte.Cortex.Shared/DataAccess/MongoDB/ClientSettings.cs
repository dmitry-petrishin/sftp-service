using System;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public class ClientSettings
    {
        [JsonProperty("ConnectTimeout")]
        public TimeSpan? ConnectTimeout;
        
        [JsonProperty("HeartbeatInterval")]
        public TimeSpan? HeartbeatInterval;
        
        [JsonProperty("LocalThreshold")]
        public TimeSpan? LocalThreshold;
        
        [JsonProperty("MaxConnectionIdleTime")]
        public TimeSpan? MaxConnectionIdleTime = TimeSpan.FromMinutes(1);
        
        [JsonProperty("MaxConnectionLifeTime")]
        public TimeSpan? MaxConnectionLifeTime;
        
        [JsonProperty("MaxConnectionPoolSize")]
        public int? MaxConnectionPoolSize;
        
        [JsonProperty("MinConnectionPoolSize")]
        public int? MinConnectionPoolSize;
        
        [JsonProperty("RetryWrites")]
        public bool? RetryWrites;
        
        [JsonProperty("SocketTimeout")]
        public TimeSpan? SocketTimeout;
        
        [JsonProperty("WaitQueueSize")]
        public int? WaitQueueSize;
        
        [JsonProperty("WaitQueueTimeout")]
        public TimeSpan? WaitQueueTimeout;

        public ClientSettings()
        {
        }

        public ClientSettings(ClientSettings settings)
        {
            ConnectTimeout = settings?.ConnectTimeout;
            HeartbeatInterval = settings?.HeartbeatInterval;
            LocalThreshold = settings?.LocalThreshold;
            MaxConnectionIdleTime = settings?.MaxConnectionIdleTime;
            MaxConnectionLifeTime = settings?.MaxConnectionLifeTime;
            MaxConnectionPoolSize = settings?.MaxConnectionPoolSize;
            MinConnectionPoolSize = settings?.MinConnectionPoolSize;
            RetryWrites = settings?.RetryWrites;
            SocketTimeout = settings?.SocketTimeout;
            WaitQueueSize = settings?.WaitQueueSize;
            WaitQueueTimeout = settings?.WaitQueueTimeout;
        }
    }
}