using MongoDB.Driver;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public static class ClientSettingsExtensions
    {
        public static void Clone(this ClientSettings settings, MongoClientSettings mongoSettings)
        {
            if (settings.MaxConnectionLifeTime.HasValue)
            {
                mongoSettings.MaxConnectionLifeTime = settings.MaxConnectionLifeTime.Value;
            }

            if (settings.HeartbeatInterval.HasValue)
            {
                mongoSettings.HeartbeatInterval = settings.HeartbeatInterval.Value;
            }

            if (settings.MaxConnectionPoolSize.HasValue)
            {
                mongoSettings.MaxConnectionPoolSize = settings.MaxConnectionPoolSize.Value;
            }

            if (settings.ConnectTimeout.HasValue)
            {
                mongoSettings.ConnectTimeout = settings.ConnectTimeout.Value;
            }

            if (settings.LocalThreshold.HasValue)
            {
                mongoSettings.LocalThreshold = settings.LocalThreshold.Value;
            }

            if (settings.MaxConnectionIdleTime.HasValue)
            {
                mongoSettings.MaxConnectionIdleTime = settings.MaxConnectionIdleTime.Value;
            }

            if (settings.MinConnectionPoolSize.HasValue)
            {
                mongoSettings.MinConnectionPoolSize = settings.MinConnectionPoolSize.Value;
            }

            if (settings.RetryWrites.HasValue)
            {
                mongoSettings.RetryWrites = settings.RetryWrites.Value;
            }

            if (settings.SocketTimeout.HasValue)
            {
                mongoSettings.SocketTimeout = settings.SocketTimeout.Value;
            }

            if (settings.WaitQueueSize.HasValue)
            {
                mongoSettings.WaitQueueSize = settings.WaitQueueSize.Value;
            }

            if (settings.WaitQueueTimeout.HasValue)
            {
                mongoSettings.WaitQueueTimeout = settings.WaitQueueTimeout.Value;
            }
        }
    }
}