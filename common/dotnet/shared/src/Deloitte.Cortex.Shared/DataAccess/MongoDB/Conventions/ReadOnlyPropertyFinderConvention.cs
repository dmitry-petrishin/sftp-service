using System.Linq;
using System.Reflection;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// A convention that finds readonly properties and adds them to the class map.
    /// </summary>
    /// <remarks>Copied from <see cref="ReadWriteMemberFinderConvention"/> and updated for readonly properties.</remarks>
    internal class ReadOnlyPropertyFinderConvention: ConventionBase, IClassMapConvention
    {
        private readonly BindingFlags _bindingFlags;
        
        public ReadOnlyPropertyFinderConvention(BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public)
        {
            _bindingFlags = bindingFlags | BindingFlags.DeclaredOnly;
        }

        public void Apply(BsonClassMap classMap)
        {
            foreach (var property in classMap.ClassType.GetTypeInfo().GetProperties(_bindingFlags))
                MapProperty(classMap, property);
        }

        private void MapProperty(BsonClassMap classMap, PropertyInfo propertyInfo)
        {
            if (!propertyInfo.CanRead || propertyInfo.CanWrite)
                return;
            if (propertyInfo.GetIndexParameters().Any())
                return;
            if (propertyInfo.GetMethod.IsVirtual && propertyInfo.GetMethod.GetBaseDefinition().DeclaringType != classMap.ClassType)
                return;
            if (classMap.DeclaredMemberMaps.Any(m => m.MemberInfo.Equals(propertyInfo)))
                return; // skip already mapped properties

            classMap.MapMember(propertyInfo);
        }
    }
}