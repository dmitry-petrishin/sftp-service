using System.Reflection;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// Uses <see cref="JsonPropertyAttribute.PropertyName"/> if present to set element name.
    /// </summary>
    internal class JsonPropertyNameConvention: ConventionBase, IMemberMapConvention
    {
        public void Apply(BsonMemberMap memberMap)
        {
            var jsonPropertyName = memberMap.MemberInfo.GetCustomAttribute<JsonPropertyAttribute>()?.PropertyName;
            if (jsonPropertyName != null) memberMap.SetElementName(jsonPropertyName);
        }
    }
}