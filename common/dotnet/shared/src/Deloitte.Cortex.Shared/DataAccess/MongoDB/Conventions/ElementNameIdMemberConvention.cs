﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// If Id not yet mapped, tries to find first element with appropriate BSON name to map as id.
    /// By default MongoDB uses only member names for Id lookup (see <see cref="NamedIdMemberConvention"/>).
    /// </summary>
    public class ElementNameIdMemberConvention: ConventionBase, IPostProcessingConvention
    {
        private readonly ISet<string> _elementNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "Id", "_id" };

        public void PostProcess(BsonClassMap classMap)
        {
            if (classMap.IdMemberMap != null) return;

            var idMemberMap = classMap.DeclaredMemberMaps.FirstOrDefault(m => _elementNames.Contains(m.ElementName));
            if(idMemberMap != null) classMap.SetIdMember(idMemberMap);
        }
    }
}