﻿using System.Linq;
using System.Reflection;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// Removes members with <see cref="JsonIgnoreAttribute"/> from mapping.
    /// </summary>
    internal class JsonIgnoreConvention: ConventionBase, IClassMapConvention
    {
        public void Apply(BsonClassMap classMap)
        {
            foreach (var memberMap in classMap.DeclaredMemberMaps.ToArray()
                .Where(m => m.MemberInfo.GetCustomAttribute<JsonIgnoreAttribute>() != null))
            {
                classMap.UnmapMember(memberMap.MemberInfo);
            }
        }
    }
}