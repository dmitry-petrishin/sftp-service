﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// Automatically registers <see cref="string"/> <c>Id</c> property as document Id 
    /// and makes MongoDB driver populate it if empty after new document is inserted.
    /// </summary>
    internal class StringIdGeneratorConvention : ConventionBase, IPostProcessingConvention
    {
        public void PostProcess(BsonClassMap classMap)
        {
            var idMap = classMap.IdMemberMap;
            if (idMap?.MemberType == typeof(string) && string.Equals(idMap.ElementName, "Id", StringComparison.OrdinalIgnoreCase))
            {
                idMap.SetSerializer(new StringSerializer(BsonType.String));
                idMap.SetIdGenerator(new StringObjectIdGenerator());
            }
        }
    }
}
