﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Deloitte.Cortex.Shared.AspNetCore;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// Makes enum marked with <see cref="JsonConverterAttribute"/>(typeof(<see cref="StringEnumConverter"/>)) be serialized as string.
    /// </summary>
    internal class JsonStringEnumConverterConvention: ConventionBase, IMemberMapConvention
    {
        // MongoDB doesn't provide enums to IClassMapConvention.
        // We fetch enums from member types or generic interfaces they implement (IEnumerable<>, IDictionary<,>, etc.).
        private IEnumerable<TypeInfo> GetEnumTypes(TypeInfo memberType)
        {
            if (memberType.IsEnum)
            {
                yield return memberType;
                yield break;
            }

            var interfaces = memberType.GetInterfaces().Select(i => i.GetTypeInfo());
            if (memberType.IsInterface) interfaces = interfaces.Union(memberType);

            foreach (var @interface in interfaces.Where(i => i.IsGenericType))
            foreach (var type in @interface.GetGenericArguments().Select(t => t.GetTypeInfo()))
                if (type.IsEnumOrNullableEnum())
                    yield return type;
        }

        public void Apply(BsonMemberMap memberMap)
        {
            var memberType = memberMap.MemberType.GetTypeInfo();

            if (memberType.IsEnumOrNullableEnum()
                && memberMap.MemberInfo.GetCustomAttributes<JsonConverterAttribute>()
                .Any(a => a.ConverterType == typeof(StringEnumConverter)))
            {
                memberMap.SetSerializer(BsonSerializerExtensions.CreateStringSerializer(memberType.AsType()));
            }

            else
            {
                foreach (var enumType in GetEnumTypes(memberMap.MemberType.GetTypeInfo()))
                {
                    if (enumType.GetCustomAttributes<JsonConverterAttribute>()
                        .Any(a => a.ConverterType == typeof(StringEnumConverter)))
                    {
                        BsonSerializerExtensions.TryRegisterSerializer(enumType.AsType(),
                            BsonSerializerExtensions.CreateStringSerializer(enumType.AsType()));
                    }
                }
            }
        }
    }
}