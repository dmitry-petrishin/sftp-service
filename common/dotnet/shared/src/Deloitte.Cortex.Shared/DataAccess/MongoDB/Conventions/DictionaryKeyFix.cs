using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Deloitte.Cortex.Shared.AspNetCore;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// Forces some types used as dictionary keys to serialize as string.
    /// Otherwise MongoDB will throw if default dictionary serialization method is used.
    /// </summary>
    internal class DictionaryKeyFix: ConventionBase, IMemberMapConvention
    {
        // Todo: simplify
        public void Apply(BsonMemberMap memberMap)
        {
            var memberType = memberMap.MemberType.GetTypeInfo();
            var serializer = memberMap.GetSerializer();

            var idictionaryType = GetDictionaryType(memberType)?.AsType();
            if (idictionaryType == null) return;

            var dictionaryRepresentation = (serializer as IBsonDictionarySerializer)?.DictionaryRepresentation;
            if (dictionaryRepresentation != DictionaryRepresentation.Document) return;

            var dictionaryTypeParameters = idictionaryType.GetGenericArguments().Select(t => t.GetTypeInfo()).ToArray();
            var keyType = dictionaryTypeParameters[0].AsType();
            var valueType = dictionaryTypeParameters[1].AsType();
            var dictionaryType = typeof(Dictionary<,>).MakeGenericType(keyType, valueType);

            var keySerializer = BsonSerializerExtensions.CreateStringSerializer(keyType);
            if (keySerializer == null) return;

            var newSerializerType = typeof(DictionaryInterfaceImplementerSerializer<,,>)
                .MakeGenericType(dictionaryType, keyType, valueType);
            var newSerializer = (IBsonSerializer) Activator.CreateInstance(newSerializerType,
                DictionaryRepresentation.Document, keySerializer, BsonSerializer.SerializerRegistry.GetSerializer(valueType));

            newSerializerType = typeof(ImpliedImplementationInterfaceSerializer<,>)
                .MakeGenericType(idictionaryType, dictionaryType);
            newSerializer = (IBsonSerializer) Activator.CreateInstance(newSerializerType, newSerializer);

            memberMap.SetSerializer(newSerializer);
        }

        private TypeInfo GetDictionaryType(TypeInfo type)
        {
            var interfaces = type.GetInterfaces().Select(i => i.GetTypeInfo());
            if (type.IsInterface) interfaces = interfaces.Union(new[] { type });

            var dictionariesTypes = interfaces
                .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IDictionary<,>));

            return dictionariesTypes.FirstOrDefault();
        }
    }
}