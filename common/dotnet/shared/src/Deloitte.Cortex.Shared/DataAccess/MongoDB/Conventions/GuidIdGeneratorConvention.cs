﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions
{
    /// <summary>
    /// Automatically registers <see cref="Guid"/> or <see cref="Guid"/>? <c>Id</c> property as document Id 
    /// and makes MongoDB driver populate it if empty after new document is inserted.
    /// </summary>
    internal class GuidIdGeneratorConvention: ConventionBase, IPostProcessingConvention
    {
        public void PostProcess(BsonClassMap classMap)
        {
            var idMap = classMap.IdMemberMap;
            if (idMap != null && string.Equals(idMap.ElementName, "Id", StringComparison.OrdinalIgnoreCase))
            {
                if (idMap.MemberType == typeof(Guid))
                {
                    idMap.SetSerializer(new GuidSerializer(BsonType.String));
                    idMap.SetIdGenerator(new GuidGenerator());
                }
                else if (idMap.MemberType == typeof(Guid?))
                {
                    idMap.SetSerializer(new NullableSerializer<Guid>(new GuidSerializer(BsonType.String)));
                    idMap.SetIdGenerator(new GuidGenerator());
                }
            }
        }
    }
}