﻿using System;
using MongoDB.Bson.Serialization;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB
{
    public class BsonClassMapExtensions
    {
        public static bool TryRegisterClassMap(Type type)
        {
            var isRegistered = BsonClassMap.IsClassMapRegistered(type); 
            if (!isRegistered) BsonClassMap.RegisterClassMap(new BsonClassMap(type));
            return isRegistered;
        }
    }
}