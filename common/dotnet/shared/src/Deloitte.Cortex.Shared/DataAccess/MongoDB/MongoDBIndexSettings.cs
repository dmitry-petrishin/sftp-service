﻿namespace Deloitte.Cortex.Shared.DataAccess.MongoDB
{
    public class MongoIndexSettings
    {
        public MongoIndexSettings(string name, bool asc)
        {
            Name = name;
            Asc = asc;
        }

        public string Name { get; set; }
        public bool Asc { get; set; }
    }
}
