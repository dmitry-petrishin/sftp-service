﻿using System;
using System.Collections.Generic;
using Deloitte.Cortex.Shared.AspNetCore;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Deloitte.Cortex.Shared.DataAccess.MongoDB
{
    internal static class BsonSerializerExtensions
    {
        private static List<Type> addedTypes = new List<Type>();

        public static bool TryRegisterSerializer<T>(IBsonSerializer<T> serializer)
        {
            return TryRegisterSerializer(typeof(T), serializer);
        }

        public static bool TryRegisterSerializer(Type type, IBsonSerializer serializer)
        {
            try
            {
                if (addedTypes.Contains(type)) return true;
                BsonSerializer.RegisterSerializer(type, serializer);
                addedTypes.Add(type);
                return true;
            }
            catch (BsonSerializationException)
            {
                return false;
            }
        }

        public static IBsonSerializer CreateStringSerializer(Type type)
        {
            IBsonSerializer serializer = null;

            Type underlyingType = Nullable.GetUnderlyingType(type);
            if (underlyingType != null && (serializer = CreateStringSerializer(underlyingType)) != null)
            {
                return (IBsonSerializer)Activator.CreateInstance(
                    typeof(NullableSerializer<>).MakeGenericType(underlyingType),
                    serializer);
            }

            if (type.IsEnum())
            {
                serializer = (IBsonSerializer)Activator.CreateInstance
                (
                    typeof(EnumSerializer<>).MakeGenericType(type),
                    BsonType.String
                );
            }

            else if (type == typeof(Guid))
            {
                serializer = new GuidSerializer(BsonType.String);
            }

            return serializer;
        }
    }
}
