﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.DataAccess
{
    /// <summary>
    /// Extensions for using <see cref="Guid"/> keys with <see cref="IRepository{T}"/>.
    /// </summary>
    public static class RepositoryGuidExtensions
    {
        public static Task<T> GetItemByIdAsync<T>(this IRepository<T> repository, Guid id)
            where T : class
        {
            return repository.GetItemByIdAsync(id.ToString());
        }

        public static Task<T> TryGetItemByIdAsync<T>(this IRepository<T> repository, Guid id)
            where T : class
        {
            return repository.TryGetItemByIdAsync(id.ToString());
        }

        public static Task<TResult> GetItemByIdAsync<T, TResult>(this IRepository<T> repository, Guid id, Expression<Func<T, TResult>> selector)
            where T : class where TResult: class
        {
            return repository.GetItemByIdAsync(id.ToString(), selector);
        }

        public static Task<TResult> TryGetItemByIdAsync<T, TResult>(this IRepository<T> repository, Guid id, Expression<Func<T, TResult>> selector)
            where T : class where TResult: class
        {
            return repository.TryGetItemByIdAsync(id.ToString(), selector);
        }

        public static Task UpdateItemAsync<T>(this IRepository<T> repository, Guid id, T item)
            where T : class
        {
            return repository.UpdateItemAsync(id.ToString(), item);
        }

        public static Task UpdateItemsAsync<T>(this IRepository<T> repository, IEnumerable<T> items, Func<T, Guid> idSelector)
            where T : class
        {
            return repository.UpdateItemsAsync(items, i => idSelector(i).ToString());
        }

        public static Task DeleteItemAsync<T>(this IRepository<T> repository, Guid id)
            where T : class
        {
            return repository.DeleteItemAsync(id.ToString());
        }

        public static Task DeleteItemsAsync<T>(this IRepository<T> repository, IEnumerable<Guid> ids)
            where T : class
        {
            return repository.DeleteItemsAsync(ids.Select(id => id.ToString()));
        }

        public static Task<bool> TryDeleteItemAsync<T>(this IRepository<T> repository, Guid id)
            where T: class
        {
            return repository.TryDeleteItemAsync(id.ToString());
        }
    }
}
