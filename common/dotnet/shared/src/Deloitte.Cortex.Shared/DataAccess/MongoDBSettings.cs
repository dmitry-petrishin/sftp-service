using System;
using System.Collections.Generic;
using Deloitte.Cortex.Shared.DataAccess.MongoDB;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public class MongoDBSettings
    {
        public MongoDBSettings() { }

        public MongoDBSettings(MongoDBSettings settings)
        {
            PopulateFrom(settings);
        }

        public string Endpoint { get; set; }

        [JsonProperty("MongoClientSettings")]
        public ClientSettings MongoClientSettings { get; set; } = new ClientSettings();

        [JsonProperty("DatabaseName")] private string _databaseName;

        [JsonIgnore]
        public string DatabaseName
        {
            get => _databaseName ?? new MongoUrl(Endpoint).DatabaseName;
            set => _databaseName = value;
        }

        public string CollectionName { get; set; }

        public IList<MongoIndexSettings> Indexes { get; set; } = new List<MongoIndexSettings>();

        /// <summary>
        /// Path to CA to validate server. If not specified, built-in system CA store will be used
        /// </summary>
        public string CA { get; set; }

        /// <summary>
        /// Path to client PKCS12 .p12 certificate. When specified, SSL + x509 is used for mongo connection
        /// </summary>
        public string Crt { get; set; }

        public MongoDBSettings PopulateFrom(MongoDBSettings settings)
        {
            if (settings == null) throw new ArgumentNullException(nameof(settings));

            Endpoint = settings.Endpoint;
            MongoClientSettings = new ClientSettings(settings.MongoClientSettings);

            _databaseName = settings._databaseName;
            CA = settings.CA;
            CollectionName = settings.CollectionName;
            Crt = settings.Crt;
            Indexes = settings.Indexes;

            return this;
        }
    }
}