﻿using System;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess.MongoDB;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public class MongoDBRepositoryFactory: IRepositoryFactory
    {
        /// <summary>
        /// Verifies that provided <paramref name="settings"/> can be used for <see cref="MongoDBRepositoryFactory"/>.
        /// </summary>
        /// <returns><paramref name="settings"/> parameter.</returns>
        public static MongoDBSettings VerifySettings(MongoDBSettings settings)
        {
            if (settings == null)
                throw new ArgumentNullException(nameof(settings));

            var mongoUrl = MongoUrl.Create(settings.Endpoint);
            if (mongoUrl.Url == null)
                throw new ArgumentException($"Endpoint is missing from {nameof(settings)}.");
            if (mongoUrl.DatabaseName == null)
                throw new ArgumentException($"Database name is missing from {nameof(settings)}.");

            return settings;
        }

        protected readonly ILogger<MongoDBRepositoryFactory> Logger;
        protected readonly MongoDBSettings Settings;
        protected readonly ILoggerFactory LoggerFactory;

        public MongoDBRepositoryFactory(ILoggerFactory loggerFactory, MongoDBSettings settings)
        {
            Logger = loggerFactory.CreateLogger<MongoDBRepositoryFactory>();
            LoggerFactory = loggerFactory;
            Settings = VerifySettings(settings);
        }

        // Todo: Consider using cache with singletons
        /// <inheritdoc cref="IRepositoryFactory.GetRepository{T}"/>.
        public virtual IRepository<TModel> GetRepository<TModel>(string collectionName) where TModel: class =>
            CreateRepository<TModel>(collectionName);

        public Task<bool> CollectionExistsAsync(string collectionName)
        {
            var database = CreateDatabase();
            return database
                .ListCollections(new ListCollectionsOptions {Filter = new BsonDocument("name", collectionName)})
                .AnyAsync();
        }

        public Task DeleteCollectionAsync(string collectionName)
        {
            var database = CreateDatabase();
            return database.DropCollectionAsync(collectionName);
        }

        protected virtual IRepository<T> CreateRepository<T>(string collectionName) where T: class =>
            new MongoDBRepository<T>(
                new MongoDBSettings(Settings) {CollectionName = collectionName},
                LoggerFactory.CreateLogger<MongoDBRepository<T>>()
            );

        protected virtual IMongoDatabase CreateDatabase()
        {
            var mongoUrl = MongoUrl.Create(Settings.Endpoint);
            var mongoSettings = MongoClientSettings.FromUrl(mongoUrl).AddX509Auth(Settings, Logger);

            var client = new MongoClient(mongoSettings);
            return client.GetDatabase(Settings.DatabaseName);
        }
    }
}