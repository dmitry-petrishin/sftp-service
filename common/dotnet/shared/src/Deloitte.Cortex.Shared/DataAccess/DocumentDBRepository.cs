﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public class DocumentDBRepository<T>: IRepository<T> where T: class
    {
        protected readonly DocumentDBSettings Settings;
        protected readonly ILogger<DocumentDBRepository<T>> Logger;
        protected DocumentClient Client;

        public DocumentDBRepository(DocumentDBSettings settings, ILogger<DocumentDBRepository<T>> logger = null)
        {
            this.Settings = settings;
            this.Logger = logger;
            Initialize();
        }

        private void Initialize()
        {
            Client = new DocumentClient(new Uri(Settings.Endpoint), Settings.Key, new ConnectionPolicy { EnableEndpointDiscovery = false });
            CreateDatabaseIfNotExistsAsync().GetAwaiter().GetResult();
            CreateCollectionIfNotExistsAsync().GetAwaiter().GetResult();
        }

        private async Task CreateDatabaseIfNotExistsAsync()
        {
            try
            {
                await Client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(Settings.DatabaseId));
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                var execTime = await Timer.TimeAsync(async () =>
                    await Client.CreateDatabaseAsync(new Database { Id = Settings.DatabaseId }));

                Logger?.LogDebug($"Created database {Settings.DatabaseId} in {execTime}.");
            }
        }

        private async Task CreateCollectionIfNotExistsAsync()
        {
            try
            {
                await Client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(Settings.DatabaseId, Settings.CollectionId));
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                var execTime = await Timer.TimeAsync(async () =>
                {
                    var docCollection = new DocumentCollection { Id = Settings.CollectionId };
                    if (Settings.IndexingPolicy != null)
                        docCollection.IndexingPolicy = Settings.IndexingPolicy;

                    await Client.CreateDocumentCollectionAsync(
                        UriFactory.CreateDatabaseUri(Settings.DatabaseId),
                        docCollection,
                        new RequestOptions { OfferThroughput = Settings.OfferThroughput });
                });

                Logger?.LogDebug($"Created collection {Settings.CollectionId} in {execTime}.");
            }
        }

        public async Task<T> TryGetItemByIdAsync(string id)
        {
            try
            {
                var timedRes = await Timer.TimeAsync(async () =>
                {
                    Document document = await QueryDocument(id);
                    return ToModel(document);
                });

                TryLog(timedRes.ExecutionTime, new[] { id });
                return timedRes.Result;
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }
        }

        public async Task<T> GetItemByIdAsync(string id)
        {
            var result = await TryGetItemByIdAsync(id);
            if (result == null) throw NotFound(id);
            return result;
        }

        public async Task<TResult> TryGetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult: class
        {
            var q = Query()
                .Where(item => ((Document) (object) item).Id == id)
                .Select(selector)
                .AsDocumentQuery();

            var response = await q.ExecuteNextAsync<TResult>();
            return response.FirstOrDefault();
        }

        public async Task<TResult> GetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult: class
        {
            var result = await TryGetItemByIdAsync(id, selector);
            if (result == null) throw NotFound(id); // Todo: fix check for non-nullable
            return result;
        }

        public Task<Range<TResult>> GetItemsAsync<TResult>(
            Expression<Func<T, bool>> predicate,
            Expression<Func<T, TResult>> selector,
            Expression<Func<T, object>> orderBy,
            int offset,
            int limit,
            bool sortAsc = true
        ) where TResult : class =>
            Task.FromException<Range<TResult>>(new NotImplementedException());

        public Task<IList<T>> GetAllItemsAsync()
        {
            return GetItemsAsync(null);
        }

        public async Task<T> TryGetItemAsync(Expression<Func<T, bool>> predicate)
        {
            var timedRes = await Timer.TimeAsync(async () =>
            {
                IDocumentQuery<T> query = Query()
                    .Where(predicate)
                    .AsDocumentQuery();

                Logger?.LogDebug($"Executing {query}");

                var r = await query.ExecuteNextAsync<T>();
                return r.FirstOrDefault();
            });

            TryLog(timedRes.ExecutionTime, new[] { predicate });
            return timedRes.Result;
        }

        public async Task<IList<T>> GetItemsAsync(Expression<Func<T, bool>> predicate, FeedOptions feedOptions = null)
        {
            var timedRes = await Timer.TimeAsync(async () =>
            {
                IQueryable<T> q = Query(feedOptions);

                if (predicate != null)
                    q = q.Where(predicate);
                if (feedOptions?.MaxItemCount != null)
                    q = q.Take((int)feedOptions.MaxItemCount);

                IDocumentQuery<T> query = q.AsDocumentQuery();

                var results = new List<T>();
                while (query.HasMoreResults)
                    results.AddRange(await query.ExecuteNextAsync<T>());

                return results;
            });

            TryLog(timedRes.ExecutionTime);
            return timedRes.Result;
        }

        public async Task<int> GetItemCountAsync(Expression<Func<T, bool>> predicate = null, FeedOptions feedOptions = null)
        {
            if (feedOptions?.MaxItemCount != null)
                throw new NotSupportedException("FeedOptions.MaxItemCount is not supported by DocumentDB for Count operation.");

            var timedRes = await Timer.TimeAsync(() =>
            {
                IQueryable<T> q = Query(feedOptions);

                if (predicate != null)
                    q = q.Where(predicate);

                return q.CountAsync();
            });

            TryLog(timedRes.ExecutionTime, new[] { predicate });
            return timedRes.Result;
        }

        public async Task<IList<TResult>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector,
            FeedOptions feedOptions = null)
        {
            var timedRes = await Timer.TimeAsync(async () =>
            {
                IQueryable<T> q = Query(feedOptions);

                if (predicate != null)
                    q = q.Where(predicate);
                if (feedOptions?.MaxItemCount != null)
                    q = q.Take((int)feedOptions.MaxItemCount);

                IDocumentQuery<TResult> query = q.Select(selector).AsDocumentQuery();

                var results = new List<TResult>();
                while (query.HasMoreResults)
                    results.AddRange(await query.ExecuteNextAsync<TResult>());

                return results;
            });

            TryLog(timedRes.ExecutionTime, new object[] { predicate, selector });
            return timedRes.Result;
        }

        public async Task<Range<T>> GetItemsAsync(Expression<Func<T, bool>> predicate,
            Expression<Func<T, object>> orderBy, int offset, int limit, bool sortAsc = true, FeedOptions feedOptions = null)
        {
            // Do basic validation
            if (limit > 0 && orderBy == null)
                throw new ArgumentException("Sorting is mandatory when using pagination", nameof(orderBy));

            var timedRes = await Timer.TimeAsync(async () =>
            {
                IQueryable<T> q = Query(feedOptions);

                if (predicate != null)
                    q = q.Where(predicate);

                q = sortAsc ? q.OrderBy(orderBy) : q.OrderByDescending(orderBy);

                IDocumentQuery<T> query = q.AsDocumentQuery();

                Logger?.LogDebug($"Executing {query}.");

                // Due to this exception in "Microsoft.Azure.DocumentDB.Core": "1.0.0":
                // Microsoft.Azure.Documents.Linq.DocumentQueryException : Method 'Skip' is not supported
                // We do pagination using actual results, not inside query
                List<T> results = new List<T>();
                while (query.HasMoreResults)
                {
                    results.AddRange(await query.ExecuteNextAsync<T>());
                }

                return new Range<T>
                {
                    TotalCount = results.Count,
                    Items = limit > 0
                        ? results.Skip(offset).Take(limit).ToArray()
                        : results.ToArray()
                };
            });

            TryLog(timedRes.ExecutionTime, new object[] { predicate, orderBy, offset, limit, sortAsc });
            return timedRes.Result;
        }

        public async Task CreateItemAsync(T item)
        {
            try
            {
                var timedRes = await Timer.TimeAsync(async () =>
                {
                    var doc = await Client.CreateDocumentAsync(
                        UriFactory.CreateDocumentCollectionUri(Settings.DatabaseId, Settings.CollectionId), item);
                    SetId(item, doc);
                    return doc;
                });

                TryLog(timedRes.ExecutionTime, new[] { item });
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.Conflict)
            {
                // Todo: find generic way to get 'id' value for item and include it in exception message
                throw Conflict();
            }
        }

        public Task CreateItemsAsync(IEnumerable<T> items)
        {
            return Task.WhenAll(items.Select(CreateItemAsync));
        }

        public async Task UpdateItemAsync(string id, T item)
        {
            try
            {
                var timedRes = await Timer.TimeAsync(async () =>
                    await Client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(Settings.DatabaseId, Settings.CollectionId, id), item));

                TryLog(timedRes.ExecutionTime, new object[] { id, item });
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                throw NotFound(id);
            }
        }

        public Task<long> UpdateItemAsync(Expression<Func<T, bool>> predicate, T item)
        {
            throw new NotImplementedException();
        }

        public Task UpdateItemsAsync(IEnumerable<T> items, Func<T, string> idSelector)
        {
            // Todo: use transaction or do it in one db call once supported
            return Task.WhenAll(items.Select(i => UpdateItemAsync(idSelector(i), i)));
        }

        public async Task UpdateItemAsync(IIdentifier item)
        {
            try
            {
                var timedRes = await Timer.TimeAsync(async () =>
                    await Client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(Settings.DatabaseId, Settings.CollectionId, item.Id), item));

                TryLog(timedRes.ExecutionTime, new[] { item });
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                throw NotFound(item.Id);
            }
        }

        public async Task<bool> UpsertItemAsync(T item)
        {
            var timedRes = await Timer.TimeAsync(async () =>
            {
                var doc = await Client.UpsertDocumentAsync(
                    UriFactory.CreateDocumentCollectionUri(Settings.DatabaseId, Settings.CollectionId), item);
                SetId(item, doc);
                return doc;
            });

            TryLog(timedRes.ExecutionTime, new[] { item });
            return true; // Todo: return correct value once implemented
        }

        public async Task DeleteItemAsync(string id)
        {
            try
            {
                var timedRes = await Timer.TimeAsync(async () =>
                    await Client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(Settings.DatabaseId, Settings.CollectionId, id)));

                TryLog(timedRes.ExecutionTime, new[] { id });
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                throw NotFound(id);
            }
        }

        public async Task<long> DeleteItemsAsync(IEnumerable<string> ids)
        {
            // Todo: use transaction or do it in one db call once supported
            return (await Task.WhenAll(ids.Select(async id =>
            {
                try
                {
                    await DeleteItemAsync(id);
                    return 1;
                }
                catch (NotFoundException)
                {
                    return 0;
                }
            }))).Sum();
        }

        public Task<long> DeleteItemsAsync(Expression<Func<T, bool>> predicate = null)
        {
            throw new NotImplementedException();
        }

        protected void TryLog(TimeSpan executionTime, IEnumerable<object> parameters = null, [CallerMemberName] string method = null)
        {
            Logger?.Log(executionTime.TotalSeconds < 1 ? LogLevel.Debug : LogLevel.Warning,
                $"Executed {method}({string.Join(", ", parameters ?? new object[0])}) in {executionTime}.");
        }

        protected IOrderedQueryable<T> Query(FeedOptions feedOptions = null) => Client.CreateDocumentQuery<T>(
            UriFactory.CreateDocumentCollectionUri(Settings.DatabaseId, Settings.CollectionId), feedOptions ?? new FeedOptions { MaxItemCount = -1 });

        protected async Task<Document> QueryDocument(string id) => await Client.ReadDocumentAsync(
            UriFactory.CreateDocumentUri(Settings.DatabaseId, Settings.CollectionId, id));

        protected T ToModel(Document document) => (T) (dynamic) document;

        protected NotFoundException NotFound(string id) => new NotFoundException($"{typeof(T).Name} with id {id} not found.");
        protected ConflictException Conflict() => new ConflictException($"{typeof(T).Name} with the same id already exists.");

        private void SetId(T item, ResourceResponse<Document> doc)
        {
            var itemWithGuid = item as DocumentItemWithId;
            if (itemWithGuid != null)
                itemWithGuid.Id = ((Document) doc).Id;
            else if (item is JObject)
                (item as JObject)["id"] = ((Document) doc).Id;
            else
                new JObject(new JProperty("id", ((Document) doc).Id)).Populate(item);
        }
    }
}