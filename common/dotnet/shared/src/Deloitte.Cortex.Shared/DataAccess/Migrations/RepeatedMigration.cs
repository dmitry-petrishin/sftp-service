﻿using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.DataAccess.Migrations
{
    /// <summary>
    /// Migration that should run each deployment.
    /// </summary>
    public abstract class RepeatedMigration: Migration
    {
        protected abstract Task<bool> RunAsync(RepeatedMigration oldMigration);

        protected sealed override async Task<bool> RunAsync(Migration oldMigration = null)
        {
            return await RunAsync(oldMigration as RepeatedMigration);
        } 
    }
}