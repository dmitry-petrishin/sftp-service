﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Clients.Caching;
using Deloitte.Cortex.Shared.Locks;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.DataAccess.Migrations
{
    public class Migrator: IMigrator
    {
        protected readonly ILogger<Migrator> Logger;
        protected readonly ICachingClient CachingClient;
        protected readonly IRepository<IMigration> MigrationsRepository;
        protected readonly ISet<IMigration> Migrations;

        /// <summary>
        /// Can be used to specify migrations order via single class.
        /// </summary>
        protected readonly IComparer<IMigration> MigrationsComparer;

        public Migrator(ILogger<Migrator> logger,
            ICachingClient cachingClient,
            IRepository<IMigration> migrationsRepository, IEnumerable<IMigration> migrations,
            IComparer<IMigration> migrationsComparer = null)
        {
            Logger = logger;
            CachingClient = cachingClient;
            MigrationsRepository = migrationsRepository;
            Migrations = migrations.ToHashSet();
            MigrationsComparer = migrationsComparer ?? Comparer<IMigration>.Default;
        }

        public virtual async Task MigrateAsync()
        {
            using (CacheLock.AcquireLock(CachingClient, "Migrations"))
            {
                var appliedMigrations = (await MigrationsRepository.GetAllItemsAsync()).ToHashSet();

                foreach (var migration in Migrations.Order(MigrationsComparer))
                {
                    var timedRun = await Timer.TimeAsync(() => migration.RunAsync(appliedMigrations));

                    if (timedRun)
                    {
                        appliedMigrations.Add(migration);
                        await MigrationsRepository.UpsertItemAsync(migration);

                        Logger.LogInformation($"Applied {migration} in {timedRun.ExecutionTime}.");
                    }
                    else
                    {
                        Logger.LogDebug($"{migration} already applied.");
                    }
                }
            }
        }
    }
}