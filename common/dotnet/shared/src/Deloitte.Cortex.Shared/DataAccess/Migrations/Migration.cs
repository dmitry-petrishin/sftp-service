﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.DataAccess.Migrations
{
    public abstract class Migration: IMigration, IComparable<Migration>, IEquatable<Migration>
    {
        /// <summary>
        /// Unique migration identifier.
        /// Used to check whether this migration has already been applied.
        /// </summary>
        [JsonProperty]
        public abstract string Id { get; }

        /// <summary>
        /// Migration order.
        /// Migrations are run in nondecreasing order. 
        /// </summary>
        [JsonProperty]
        public abstract int Order { get; }
        
        [JsonProperty]
        public DateTime? AppliedAt { get; set; }

        protected abstract Task<bool> RunAsync(Migration oldMigration = null);
        
        public async Task<bool> RunAsync(ISet<IMigration> oldMigrations)
        {
            var oldMigration = oldMigrations.FirstOrDefault(Equals) as Migration;
            var run = await RunAsync(oldMigration);
            
            if(run) AppliedAt = DateTime.UtcNow;
            
            return run;
        }

        #region Equality members

        public bool Equals(Migration other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Migration) obj);
        }

        public override int GetHashCode() => Id?.GetHashCode() ?? 0;

        public static bool operator ==(Migration left, Migration right) => Equals(left, right);

        public static bool operator !=(Migration left, Migration right) => !Equals(left, right);

        public bool Equals(IMigration other) => Equals(other as object);

        public virtual int CompareTo(Migration other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Order.CompareTo(other.Order);
        }

        public int CompareTo(IMigration other)
        {
            return CompareTo(other as Migration
                             ?? throw new NotSupportedException($"{other.GetType()} is not {nameof(Migration)}."));
        }

        public override string ToString() => $"Migration \"{Id}\"";

        #endregion
    }
}