﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.DataAccess.Migrations
{
    public interface IMigration: IComparable<IMigration>, IEquatable<IMigration>
    {
        Task<bool> RunAsync(ISet<IMigration> oldMigrations);

        /// <summary>
        /// Last time migration was applied.
        /// </summary>
        DateTime? AppliedAt { get; }
    }
}