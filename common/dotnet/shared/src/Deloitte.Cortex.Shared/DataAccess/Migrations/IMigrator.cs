﻿using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.DataAccess.Migrations
{
    public interface IMigrator
    {
        Task MigrateAsync();
    }
}