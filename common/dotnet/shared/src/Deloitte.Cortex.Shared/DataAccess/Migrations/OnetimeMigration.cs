﻿using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.DataAccess.Migrations
{
    /// <summary>
    /// Migration that should run once. 
    /// </summary>
    public abstract class OnetimeMigration: Migration
    {
        protected abstract Task<bool> RunAsync();

        protected sealed override async Task<bool> RunAsync(Migration oldMigration = null)
        {
            if (Equals(oldMigration)) return false;
            return await RunAsync();
        }
    }
}