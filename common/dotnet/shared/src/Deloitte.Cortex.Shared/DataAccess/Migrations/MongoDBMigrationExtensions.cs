﻿using System.Linq;
using System.Reflection;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Clients.Caching;
using Deloitte.Cortex.Shared.DataAccess.MongoDB;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.DataAccess.Migrations
{
    public static class MongoDBMigrationExtensions
    {
        /// <summary>
        /// Registers <see cref="IRepositoryFactory"/> and <see cref="IMigrator"/> for running migrations.
        /// </summary>
        ///
        /// <param name="services">Application services collection.</param>
        ///
        /// <param name="dbSettings">
        /// <see cref="MongoDBSettings"/> to use for storing applied migrations.
        /// Required only if <see cref="IRepository{T}"/> for <see cref="IMigration"/> is not registered elsewhere.
        /// Endpoint and database name must be present. If collection name is not provided, "_Migrations" will be used.
        /// If parameter is <c>null</c> but is required it will be resolved from services.
        /// </param>
        ///
        /// <param name="cachingConfiguration">
        /// Caching configuration used for cross-instaces locking.
        /// Required only if <see cref="ICachingClient"/> is not registered elsewhere.
        /// Service name must be present.
        /// If parameter is <c>null</c> but is required it will be resolved from services.
        /// </param>
        public static IServiceCollection AddMigrationServices(this IServiceCollection services,
            MongoDBSettings dbSettings = null, CachingConfiguration cachingConfiguration = null)
        {
            if (dbSettings != null) MongoDBRepositoryFactory.VerifySettings(dbSettings);

            // Helper services
            services.TryAddSingleton<IRepository<IMigration>>(p =>
            {
                dbSettings = dbSettings ?? p.GetRequiredService<MongoDBSettings>();
                dbSettings.CollectionName = dbSettings.CollectionName ?? "_Migrations";
                return p.CreateInstance<MongoDBRepository<IMigration>>(dbSettings);
            });
            services.TryAddSingleton(p =>
            {
                cachingConfiguration = cachingConfiguration ?? p.GetRequiredService<CachingConfiguration>();
                return Factory.GetClient(cachingConfiguration, null, p.GetRequiredService<ILoggerFactory>());
            });

            // Migration services
            services.TryAddSingleton<IRepositoryFactory>(p => p.CreateInstance<MongoDBRepositoryFactory>(dbSettings));
            services.TryAddSingleton<IMigrator, Migrator>();

            return services;
        }

        /// <summary>
        /// Adds all classes in given <paramref name="assemblies"/> that implement <see cref="IMigration"/>
        /// as singleton services.
        /// By default only calling assembly is used.
        /// </summary>
        public static IServiceCollection AddMigrations(this IServiceCollection services, params Assembly[] assemblies)
        {
            if (!assemblies.Any()) assemblies = new[] { Assembly.GetCallingAssembly() };

            foreach (var migrationType in assemblies.SelectMany(a => a.GetTypes()
                .Where(t => t.ImplementsInterface(typeof(IMigration)))))
            {
                services.AddSingleton(typeof(IMigration), migrationType);

                // see https://stackoverflow.com/a/11743295/3542151
                BsonClassMapExtensions.TryRegisterClassMap(migrationType);
            }

            return services;
        }
    }
}