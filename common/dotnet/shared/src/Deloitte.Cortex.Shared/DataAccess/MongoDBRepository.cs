﻿using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.DataAccess.MongoDB;
using Deloitte.Cortex.Shared.DataAccess.MongoDB.Conventions;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Integrations.JsonDotNet;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.DataAccess
{
    // Todo: remove FeedOptions from interface
    // Todo: simplify interface
    public class MongoDBRepository<T> : IRepository<T> where T : class
    {
        protected static FilterDefinition<T> ById(string id) => Builders<T>.Filter.Eq("_id", id);

        protected static FilterDefinition<BsonDocument> ByBsonId(string id) => Builders<BsonDocument>.Filter.Eq("_id", id);

        protected static FilterDefinition<BsonDocument> ByBsonIds(IEnumerable<string> ids) => Builders<BsonDocument>.Filter.In("_id", ids);

        protected static NotFoundException NotFound(string id) => new NotFoundException($"{typeof(T).Name} with id {id} not found.");

        protected static ConflictException Conflict() => new ConflictException($"{typeof(T).Name} with the same id already exists.");

        /// <summary>
        /// Overridable property for setting collation for all 'find' queries to collection.
        /// </summary>
        // No option to change default collection collation:
        // https://stackoverflow.com/a/44682992/3542151
        protected virtual Collation QueryCollation => null;

        protected IFindFluent<T, T> Find(Expression<Func<T, bool>> predicate = null)
        {
            return Collection.Find(predicate ?? (i => true), new FindOptions {Collation = QueryCollation});
        }

        static MongoDBRepository()
        {
            BsonSerializer.RegisterSerializationProvider(
                new JsonDotNetSerializationProvider(type => typeof(JToken).IsAssignableFrom(type)));

            BsonSerializerExtensions.TryRegisterSerializer(new GuidSerializer(BsonType.String));

            ConventionRegistry.Register("PreFix", new ConventionPack
            {
                new ReadOnlyPropertyFinderConvention()
            }, _ => true);

            ConventionRegistry.Register("Id", new ConventionPack
            {
                new ElementNameIdMemberConvention(),
                new StringIdGeneratorConvention(),
                new GuidIdGeneratorConvention()
            }, _ => true);

            ConventionRegistry.Register("JSON", new ConventionPack
            {
                new JsonIgnoreConvention(),
                new JsonStringEnumConverterConvention(),
                new JsonPropertyNameConvention()
            }, _ => true);

            ConventionRegistry.Register("PostFix", new ConventionPack
            {
                new DictionaryKeyFix(),
                new IgnoreExtraElementsConvention(true)
            }, _ => true);
        }

        protected readonly ILogger<MongoDBRepository<T>> Logger;
        private IMongoCollection<T> collection;
        private readonly MongoDBSettings settings;
        private readonly MongoClientSettings clientSettings;
        private IMongoCollection<BsonDocument> bsonCollection;
        private IMongoDatabase database;

        protected IMongoDatabase Database
        {
            get
            {
                if (database == null)
                {
                    lock (this)
                    {
                        if (database == null) Initialize();
                    }
                }
                return database;
            }
        }

        protected IMongoCollection<T> Collection 
        {
            get
            {
                if (collection == null)
                {
                    lock (this)
                    {
                        if (collection == null) Initialize();
                    }
                }
                return collection;
            }
        }

        protected IMongoCollection<BsonDocument> BsonCollection
        {
            get
            {
                if (bsonCollection == null)
                {
                    lock (this)
                    {
                        if (bsonCollection == null) Initialize();
                    }
                }
                
                return bsonCollection;
            }
        }

        public MongoDBRepository(MongoDBSettings settings, ILogger<MongoDBRepository<T>> logger = null)
        {
            Logger = logger;

            var mongoUrl = MongoUrl.Create(settings.Endpoint);
            var mongoSettings = MongoClientSettings.FromUrl(mongoUrl).AddX509Auth(settings, logger);

            settings.MongoClientSettings?.Clone(mongoSettings);
            this.settings = settings;
            this.clientSettings = mongoSettings;

            #if DEBUG
            #else
            Initialize();
            #endif
        }

        private void Initialize()
        {
            var client = new MongoClient(clientSettings);
            this.database = client.GetDatabase(settings.DatabaseName);
            this.collection = database.GetCollection<T>(settings.CollectionName);
            this.bsonCollection = database.GetCollection<BsonDocument>(settings.CollectionName);

            // index creation process is idempotent
            if (settings.Indexes.Count > 0)
            {
                var createIndexes = settings.Indexes.Select(
                    x =>
                        new CreateIndexModel<T>(new JsonIndexKeysDefinition<T>($"{{{x.Name}: {(x.Asc ? 1 : -1)}}}")));

                Collection.Indexes.CreateMany(createIndexes);
            }
        }

        private void LogExecuted(TimeSpan executionTime, IEnumerable<object> parameters = null, [CallerMemberName] string method = null)
        {
            Logger?.Log(executionTime.TotalSeconds < 1 ? LogLevel.Debug : LogLevel.Warning,
                $"Executed {method}({string.Join(", ", parameters ?? new object[0])}) in {executionTime}.");
        }

        public virtual async Task<T> TryGetItemByIdAsync(string id)
        {
            var timedRes = await Timer.TimeAsync(() =>
                Collection.Find(ById(id))
                    .FirstOrDefaultAsync());

            LogExecuted(timedRes.ExecutionTime, new object[] { id });
            return timedRes.Result;
        }

        public virtual async Task<TResult> TryGetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult : class
        {
            var timedRes = await Timer.TimeAsync(() =>
                Collection.Find(ById(id))
                    .Project(selector)
                    .FirstOrDefaultAsync());

            LogExecuted(timedRes.ExecutionTime, new[] { id });
            return timedRes.Result;
        }

        public virtual async Task<T> GetItemByIdAsync(string id)
        {
            var item = await TryGetItemByIdAsync(id);
            if (item == null) throw NotFound(id);
            return item;
        }

        public virtual async Task<TResult> GetItemByIdAsync<TResult>(string id, Expression<Func<T, TResult>> selector)
            where TResult : class
        {
            var item = await TryGetItemByIdAsync(id, selector);
            if (item == null) throw NotFound(id); // Todo: fix check for non-nullable
            return item;
        }

        public virtual async Task<T> TryGetItemAsync(Expression<Func<T, bool>> predicate)
        {
            var timedRes = await Timer.TimeAsync(() =>
                Find(predicate)
                    .FirstOrDefaultAsync());

            LogExecuted(timedRes.ExecutionTime, new[] { predicate });
            return timedRes.Result;
        }

        public virtual async Task<IList<T>> GetItemsAsync(Expression<Func<T, bool>> predicate, FeedOptions feedOptions = null)
        {
            var query = Find(predicate);
            if (feedOptions?.MaxItemCount >= 0) query = query.Limit(feedOptions.MaxItemCount);

            var timedRes = await Timer.TimeAsync(() =>
                query.ToListAsync());

            LogExecuted(timedRes.ExecutionTime, new[] { predicate });
            return timedRes.Result;
        }

        public virtual async Task<int> GetItemCountAsync(Expression<Func<T, bool>> predicate = null, FeedOptions feedOptions = null)
        {
            var query = Find(predicate);
            if (feedOptions?.MaxItemCount >= 0) query = query.Limit(feedOptions.MaxItemCount);

            var timedRes = await Timer.TimeAsync(() =>
                query.CountDocumentsAsync());

            LogExecuted(timedRes.ExecutionTime, new[] { predicate });
            return checked((int)timedRes.Result);
        }

        public virtual async Task<IList<TResult>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector,
            FeedOptions feedOptions = null)
        {
            var query = Find(predicate)
                .Project(selector);
            if (feedOptions?.MaxItemCount >= 0) query = query.Limit(feedOptions.MaxItemCount);

            var timedRes = await Timer.TimeAsync(() =>
                query.ToListAsync());

            LogExecuted(timedRes.ExecutionTime, new[] { predicate });
            return timedRes.Result;
        }

        public virtual async Task<Range<T>> GetItemsAsync(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy,
            int offset, int limit, bool sortAsc = true, FeedOptions feedOptions = null)
        {
            var timedRes = await Timer.TimeAsync(async () => {
                var query = Find(predicate);
                query = sortAsc ? query.SortBy(orderBy) : query.SortByDescending(orderBy);

                var count = await query.CountDocumentsAsync();
                var items = await query
                    .Skip(offset)
                    .Limit(limit >= 0 ? limit : (int?)null)
                    .ToListAsync();

                return new Range<T> {
                    TotalCount = checked((int)count),
                    Items = items.ToArray()
                };
            });

            LogExecuted(timedRes.ExecutionTime, new[] { predicate });
            return timedRes.Result;
        }

        public async Task<Range<TResult>> GetItemsAsync<TResult>(
            Expression<Func<T, bool>> predicate,
            Expression<Func<T, TResult>> selector,
            Expression<Func<T, object>> orderBy,
            int offset,
            int limit,
            bool sortAsc = true)
            where TResult : class
        {
            var timedRes = await Timer.TimeAsync(async () => {
                var query = Find(predicate)
                    .Project(selector);
                query = sortAsc ? query.SortBy(orderBy) : query.SortByDescending(orderBy);

                var count = await query.CountDocumentsAsync();
                var items = await query
                    .Skip(offset)
                    .Limit(limit >= 0 ? limit : (int?)null)
                    .ToListAsync();

                return new Range<TResult> {
                    TotalCount = checked((int)count),
                    Items = items.ToArray()
                };
            });

            LogExecuted(timedRes.ExecutionTime, new[] { predicate });
            return timedRes.Result;
        }

        public virtual async Task<IList<T>> GetAllItemsAsync()
        {
            var timedRes = await Timer.TimeAsync(() =>
                Find().ToListAsync()
            );

            LogExecuted(timedRes.ExecutionTime);
            return timedRes.Result;
        }

        public virtual async Task UpdateItemAsync(string id, T item)
        {
            var timedRes = await Timer.TimeAsync(() =>
                Collection.ReplaceOneAsync(ById(id), item));

            if (timedRes.Result.MatchedCount == 0) throw NotFound(id);
            LogExecuted(timedRes.ExecutionTime);
        }

        public virtual async Task<long> UpdateItemAsync(Expression<Func<T, bool>> predicate, T item)
        {
            var timedRes = await Timer.TimeAsync(() =>
                Collection.ReplaceOneAsync(predicate, item));

            LogExecuted(timedRes.ExecutionTime);
            return timedRes.Result.ModifiedCount;
        }

        public virtual async Task UpdateItemsAsync(IEnumerable<T> items, Func<T, string> idSelector)
        {
            // Todo: implement via single db call
            foreach (var item in items) await UpdateItemAsync(idSelector(item), item);
        }

        public virtual async Task UpdateItemAsync(IIdentifier item)
        {
            var timedRes = await Timer.TimeAsync(() =>
                BsonCollection.ReplaceOneAsync(ByBsonId(item.Id), item.ToBsonDocument(), new UpdateOptions { IsUpsert = false }));

            LogExecuted(timedRes.ExecutionTime);
        }

        public virtual async Task CreateItemAsync(T item)
        {
            try
            {
                var execTime = await Timer.TimeAsync(() =>
                    Collection.InsertOneAsync(item));

                LogExecuted(execTime);
            }
            catch (MongoWriteException exception) when (exception.WriteError.Category == ServerErrorCategory.DuplicateKey)
            {
                throw Conflict();
            }
        }

        public virtual async Task CreateItemsAsync(IEnumerable<T> items)
        {
            try
            {
                TimeSpan execTime = TimeSpan.Zero;

                if (items.Any())
                {
                    execTime = await Timer.TimeAsync(() =>
                        Collection.InsertManyAsync(items));
                }

                LogExecuted(execTime);
            }
            catch (MongoBulkWriteException exception) when (exception.WriteErrors.First().Category == ServerErrorCategory.DuplicateKey)
            {
                throw Conflict();
            }
        }

        public async Task<bool> UpsertItemAsync(T item)
        {
            var timedRes = await Timer.TimeAsync(() => {
                var document = item.ToBsonDocument();
                return BsonCollection.ReplaceOneAsync(ByBsonId(document["_id"].ToString()), document, new UpdateOptions { IsUpsert = true });
            });

            LogExecuted(timedRes.ExecutionTime);
            return timedRes.Result.UpsertedId != null;
        }

        public virtual async Task DeleteItemAsync(string id)
        {
            var timedRes = await Timer.TimeAsync(() =>
                Collection.DeleteOneAsync(ById(id)));

            if (timedRes.Result.DeletedCount == 0) throw NotFound(id);
            LogExecuted(timedRes.ExecutionTime);
        }

        public virtual async Task<long> DeleteItemsAsync(IEnumerable<string> ids)
        {
            var timedRes = await Timer.TimeAsync(() =>
                BsonCollection.DeleteManyAsync(ByBsonIds(ids)));

            LogExecuted(timedRes.ExecutionTime);
            return timedRes.Result.DeletedCount;
        }

        public async Task<long> DeleteItemsAsync(Expression<Func<T, bool>> predicate = null)
        {
            var timedRes = await Timer.TimeAsync(() =>
                Collection.DeleteManyAsync(predicate ?? (i => true)));

            LogExecuted(timedRes.ExecutionTime);
            return timedRes.Result.DeletedCount;
        }
    }
}