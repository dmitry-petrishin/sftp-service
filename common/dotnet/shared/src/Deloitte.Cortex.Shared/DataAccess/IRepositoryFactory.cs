﻿using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public interface IRepositoryFactory
    {
        /// <summary>
        /// Creates <see cref="IRepository{T}"/> of <typeparamref name="TModel"/> for given <paramref name="collectionName"/>.  
        /// </summary>
        IRepository<TModel> GetRepository<TModel>(string collectionName) where TModel : class;

        Task<bool> CollectionExistsAsync(string collectionName);
        
        Task DeleteCollectionAsync(string collectionName);
    }
}