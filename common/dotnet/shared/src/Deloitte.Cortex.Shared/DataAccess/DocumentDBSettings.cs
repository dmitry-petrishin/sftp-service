﻿using Microsoft.Azure.Documents;

namespace Deloitte.Cortex.Shared.DataAccess
{
    /// <summary>
    /// All the settings needed for working with a specific collection
    /// </summary>
    public class DocumentDBSettings
    {
        public string Endpoint { get; set; }
        public string Key { get; set; }
        public string DatabaseId { get; set; }
        public string CollectionId { get; set; }
        public IndexingPolicy IndexingPolicy { get; set; }
        public int? OfferThroughput { get; set; }
    }
}
