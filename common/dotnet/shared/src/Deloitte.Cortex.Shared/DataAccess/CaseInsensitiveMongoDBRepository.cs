﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public class CaseInsensitiveMongoDBRepository<T>: MongoDBRepository<T>
        where T: class
    {
        public CaseInsensitiveMongoDBRepository(MongoDBSettings settings, ILogger<MongoDBRepository<T>> logger = null): base(settings, logger) { }

        protected override Collation QueryCollation => new Collation(locale: "en", strength: CollationStrength.Secondary);
    }
}