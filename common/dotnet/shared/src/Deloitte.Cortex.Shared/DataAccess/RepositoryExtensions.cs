﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Deloitte.Cortex.Shared.Serialization;

namespace Deloitte.Cortex.Shared.DataAccess
{
    public static class RepositoryExtensions
    {
        public static async Task<T> GetItemAsync<T>(this IRepository<T> repository, Expression<Func<T, bool>> predicate)
            where T : class
        {
            T item = await repository.TryGetItemAsync(predicate);

            // TODO: get rid of it
            if (item == null)
            {
                throw new NotFoundException($"{typeof(T).Name} with given predicate not found.");
            }

            return item;
        }

        public static async Task<bool> Any<T>(this IRepository<T> repository, Expression<Func<T, bool>> predicate = null)
            where T: class
        {
            return await repository.TryGetItemAsync(predicate) != null;
        }

        public static Task CreateItemsAsync<T>(this IRepository<T> repository, params T[] items)
            where T: class
        {
            return repository.CreateItemsAsync(items);
        }

        public static async Task<bool> TryCreateItemAsync<T>(this IRepository<T> repository, T item)
            where T: class
        {
            try
            {
                await repository.CreateItemAsync(item);
                return true;
            }
            catch (ConflictException)
            {
                return false;
            }
        }

        public static async Task<bool> TryDeleteItemAsync<T>(this IRepository<T> repository, string id)
            where T: class
        {
            try
            {
                await repository.DeleteItemAsync(id);
                return true;
            }
            catch (NotFoundException)
            {
                return false;
            }
        }
        
        public static Task<long> DeleteAllItemsAsync<T>(this IRepository<T> repository)
            where T: class =>
            repository.DeleteItemsAsync();

        public static async Task<Range<string>> GetIds(
            this IRepository<IDictionary<string, object>> repo, 
            Expression<Func<IDictionary<string, object>, bool>> filter,
            int skip,
            int count)
        {
            var found = await repo.GetItemsAsync(
                filter, 
                t => (string)t["_id"], 
                t => t["_id"], 
                skip, 
                count);
            return found;
        }

        /// <summary>
        /// Fill items by id from database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="repo"></param>
        /// <param name="items">Array of items to fill</param>
        /// <param name="selector">Expression with fields to select</param>
        /// <param name="fromSourceToTarget">Func to copy from fetched to stored. Signature is (source, target)</param>
        /// <returns></returns>
        public static async Task FillByIds<T>(
            this IRepository<T> repo, 
            IEnumerable<T> items, 
            Expression<Func<T, T>> selector, 
            Action<T, T> fromSourceToTarget)
            where T : class, IIdentifier
        {
            var ids = items.Select(i => i.Id).ToArray();
            var sources = await repo.GetItemsAsync(i => ids.Contains(i.Id), selector);
            foreach (var source in sources)
            {
                var target = items.First(i => i.Id == source.Id);
                fromSourceToTarget(source, target);
            }
        }
    }
}