﻿using System.Reflection;

namespace Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils
{
    public class OverrideMethodInfo
    {
        public MethodInfo MethodInfo {get; set;}
        public OverrideMethodDelegate OverrideFunc { get; set; }
        public bool Once { get; set; }
    }
}
