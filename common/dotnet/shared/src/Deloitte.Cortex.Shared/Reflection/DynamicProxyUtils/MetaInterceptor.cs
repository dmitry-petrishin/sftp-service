﻿using Castle.DynamicProxy;

namespace Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils
{
    internal class MetaInterceptor : StandardInterceptor
    {
        public OverrideMethodsInterceptor OverrideMethodsInterceptor { get; }

        public MetaInterceptor(OverrideMethodsInterceptor overrideMethodsInterceptor)
        {
            OverrideMethodsInterceptor = overrideMethodsInterceptor;
        }
        
        protected override void PerformProceed(IInvocation invocation)
        {
            if (invocation.Method.DeclaringType == typeof(IOverrideMethodsInterceptorExposer))
            {
                invocation.ReturnValue = this.OverrideMethodsInterceptor;
                return;
            }

            base.PerformProceed(invocation);
        }
    }
}
