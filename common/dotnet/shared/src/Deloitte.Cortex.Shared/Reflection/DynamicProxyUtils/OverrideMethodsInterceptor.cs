﻿using System;
using System.Collections.Generic;
using Castle.DynamicProxy;

namespace Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils
{
    public class OverrideMethodsInterceptor : StandardInterceptor
    {
        public List<OverrideMethodInfo> Overrides { get; } = new List<OverrideMethodInfo>();
        public object Wrapped { get; }

        public OverrideMethodsInterceptor(
            object wrapped)
        {
            this.Wrapped = wrapped;
        }

        protected override void PerformProceed(IInvocation invocation)
        {
            foreach (var overrideInfo in Overrides.ToArray())
            {
                if (overrideInfo.MethodInfo == invocation.Method)
                {
                    invocation.ReturnValue = overrideInfo.OverrideFunc(Wrapped, invocation.Arguments);
                    if (overrideInfo.Once)
                    {
                        Overrides.Remove(overrideInfo);
                    }
                    return;
                }
            }

            if (invocation.MethodInvocationTarget != null)
            {
                base.PerformProceed(invocation);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
