﻿namespace Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils
{
    public interface IOverrideMethodsInterceptorExposer
    {
        OverrideMethodsInterceptor GetOverrideMethodsInterceptor();
    }
}
