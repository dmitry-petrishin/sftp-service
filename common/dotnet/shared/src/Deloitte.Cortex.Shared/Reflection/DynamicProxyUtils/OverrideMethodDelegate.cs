﻿namespace Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils
{
    public delegate object OverrideMethodDelegate(object baseObject, object[] args);
}
