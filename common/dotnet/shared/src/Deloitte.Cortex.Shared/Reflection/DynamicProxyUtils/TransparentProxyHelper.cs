﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Castle.DynamicProxy;

namespace Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils
{
    public static class TransparentProxyHelper
    {
        private static readonly ProxyGenerator ProxyGenerator = new ProxyGenerator();
        
        public static T WrapToProxy<T>(this T wrapped, params Type[] additionalTypes)
            where T : class
        {
            var overridesInterceptor = new OverrideMethodsInterceptor(wrapped);

            object res;
            res = wrapped != null
                ? ProxyGenerator.CreateInterfaceProxyWithTarget(
                    typeof(T),
                    additionalTypes.Append(typeof(IOverrideMethodsInterceptorExposer)).ToArray(),
                    wrapped,
                    new IInterceptor[]
                    {
                        new MetaInterceptor(overridesInterceptor),
                        overridesInterceptor
                    })
                : ProxyGenerator.CreateInterfaceProxyWithoutTarget(
                    typeof(T),
                    additionalTypes.Append(typeof(IOverrideMethodsInterceptorExposer)).ToArray(),
                    new IInterceptor[]
                    {
                        new MetaInterceptor(overridesInterceptor),
                        overridesInterceptor,
                    });

            return (T)res;
        }

        public static T Override<T>(
            this T obj,
            Expression<Action<T>> target,
            OverrideMethodDelegate overrideMethod,
            bool once = false)
        {
            var methodCall = (MethodCallExpression)target.Body;
            return OverrideInner(obj, methodCall, overrideMethod, once);
        }

        public static T Override<T,S>(
            this T obj,
            Expression<Func<T, S>> target,
            OverrideMethodDelegate overrideMethod,
            bool once = false)
        {
            var methodCall = (MethodCallExpression)target.Body;
            return OverrideInner(obj, methodCall, overrideMethod, once);
        }

        private static T OverrideInner<T>(
            T obj, 
            MethodCallExpression methodCall, 
            OverrideMethodDelegate overrideMethod, 
            bool once)
        {
            var interceptor = GetInterceptor(obj);
            interceptor.Overrides.Add(new OverrideMethodInfo
            {
                MethodInfo = methodCall.Method,
                OverrideFunc = overrideMethod,
                Once = once
            });

            return obj;
        }

        public static T Override<T, TR>(
            this T obj,
            Expression<Action<T>> target,
            Func<T, TR> overrider)
        {
            var interceptor = GetInterceptor(obj);
            var methodCall = (MethodCallExpression)target.Body;
            interceptor.Overrides.Add(new OverrideMethodInfo
            {
                MethodInfo = methodCall.Method,
                OverrideFunc = (wrapped, args) => overrider((T) wrapped)
            });

            return obj;
        }

        public static T Override<T, TA1, TR>(
            this T obj,
            Expression<Action<T>> target,
            Func<T, TA1, TR> overrider)
        {
            var methodCall = (MethodCallExpression)target.Body;
            var interceptor = GetInterceptor(obj);
            interceptor.Overrides.Add(new OverrideMethodInfo
            {
                MethodInfo = methodCall.Method,
                OverrideFunc = (wrapped, args) => overrider((T) wrapped, (TA1) args[0])
            });

            return obj;
        }

        public static T Override<T, TA1, TA2, TR>(
            this T obj,
            Expression<Action<T>> target,
            Func<T, TA1, TA2, TR> overrider)
        {
            var interceptor = GetInterceptor(obj);
            var methodCall = (MethodCallExpression)target.Body;
            interceptor.Overrides.Add(new OverrideMethodInfo
            {
                MethodInfo = methodCall.Method,
                OverrideFunc = (wrapped, args) => overrider((T) wrapped, (TA1) args[0], (TA2) args[1])
            });

            return obj;
        }

        public static T ClearOverrides<T>(
            this T obj)
        {
            var interceptor = GetInterceptor(obj);
            interceptor.Overrides.Clear();
            return obj;
        }

        public static OverrideHelper<T, S> Override<T, S>(this T owner, Expression<Func<T, S>> target)
        {
            return new OverrideHelper<T, S>(owner, target);
        }

        private static OverrideMethodsInterceptor GetInterceptor(object obj)
        {
            var exposer = (IOverrideMethodsInterceptorExposer) obj;
            var interceptor = exposer.GetOverrideMethodsInterceptor();
            return interceptor;
        }
    }
}
