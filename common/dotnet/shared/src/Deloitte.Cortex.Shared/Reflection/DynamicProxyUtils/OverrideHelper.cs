﻿using System;
using System.Linq.Expressions;

namespace Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils
{
    public class OverrideHelper<T, S>
    {
        private readonly T owner;
        private readonly Expression<Func<T, S>> target;

        public OverrideHelper(T owner, Expression<Func<T, S>> target)
        {
            this.owner = owner;
            this.target = target;
        }

        public T With(Func<T, object[], S> replacement, bool once = false)
        {
            owner.Override(
                target,
                (o, args) =>
                {
                    return replacement(As<T>(o), args);
                },
                once);
            return owner;
        }

        public T With(S replacement, bool once = false)
        {
            owner.Override(
                target,
                (o, args) =>
                {
                    return replacement;
                },
                once);
            return owner;
        }

        public T With<A1>(Func<T, A1, S> replacement, bool once = false)
        {
            owner.Override(
                target,
                (o, args) =>
                {
                    return replacement(As<T>(o), (A1)args[0]);
                },
                once);
            return owner;
        }
        
        public T With<A1, A2>(Func<T, A1, A2, S> replacement, bool once = false)
        {
            owner.Override(
                target,
                (o, args) =>
                {
                    return replacement(As<T>(o), (A1)args[0], (A2)args[2]);
                },
                once);
            return owner;
        }

        public T With<A1, A2, A3>(Func<T, A1, A2, A3, S> replacement, bool once = false)
        {
            owner.Override(
                target,
                (o, args) =>
                {
                    return replacement(As<T>(o), (A1)args[0], (A2)args[2], (A3)args[3]);
                },
                once);
            return owner;
        }

        private T1 As<T1>(object baseObject)
        {
            if (baseObject == null)
            {
                return default(T1);
            }

            if (baseObject is T1)
            {
                return (T1) baseObject;
            }

            return default(T1);
        }
    }
}
