﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System;
using Deloitte.Cortex.Shared.Authorization;
using System.Text;

namespace Deloitte.Cortex.Shared.Crypto.SecretEncryptor
{
    class TwoStageKeyVaultEncryptor : StaticKeyEncryptor
    {
        private readonly Serialization.Configuration.SecretEncryptor.TwoStageKeyVault options;
        private readonly IAuthenticationService authenticationService;
        private readonly ILoggerFactory loggerFactory;

        public TwoStageKeyVaultEncryptor(
            Serialization.Configuration.SecretEncryptor.TwoStageKeyVault options,
            IAuthenticationService authenticationService,
            ILoggerFactory loggerFactory) :
            base(options, loggerFactory.CreateLogger<TwoStageKeyVaultEncryptor>())
        {
            if (string.IsNullOrWhiteSpace(options.KEKKeyVaultName))
                throw new Exception($"{nameof(options.KEKKeyVaultName)} is not specified");
            if (options.KeyId?.NotEmpty != true)
                throw new Exception($"{nameof(options.KeyId)} is not specified");
            if (string.IsNullOrWhiteSpace(options.Algorithm))
                throw new Exception($"{nameof(options.Algorithm)} is not specified");

            this.options = options;
            this.authenticationService = authenticationService;
            this.loggerFactory = loggerFactory;
        }

        private async Task<string> EncryptUsingKeyVault(byte[] secret)
        {
            logger.LogInformation($"{DateTime.UtcNow.ToString("O")} {Environment.MachineName} Encrypting secret using Key Vault Key: {options.KEKKeyVaultName}/{options.KeyId}");

            var client = new Clients.KeyVault.KeyVaultClient(options.KEKKeyVaultName, loggerFactory.CreateLogger<Clients.KeyVault.KeyVaultClient>(), authenticationService);
            return await client.EncryptAsync(options.KeyId.Name, options.KeyId.Version, options.Algorithm, secret);
        }

        private async Task<byte[]> DecryptUsingKeyVault(string base64encoded)
        {
            logger.LogInformation($"{DateTime.UtcNow.ToString("O")} {Environment.MachineName} Decrypting secret using Key Vault Key: {options.KEKKeyVaultName}/{options.KeyId}");

            var client = new Clients.KeyVault.KeyVaultClient(options.KEKKeyVaultName, loggerFactory.CreateLogger<Clients.KeyVault.KeyVaultClient>(), authenticationService);
            return await client.DecryptAsync(options.KeyId.Name, options.KeyId.Version, options.Algorithm, base64encoded);
        }

        public override async Task<string> Encrypt(string secret, string iv)
        {
            var firstStage = await base.Encrypt(secret, iv);
            return await EncryptUsingKeyVault(Encoding.UTF8.GetBytes(firstStage));
        }

        public override async Task<string> Decrypt(string encryptedSecret, string iv)
        {
            var firstStage = await DecryptUsingKeyVault(encryptedSecret);
            return await base.Decrypt(Encoding.UTF8.GetString(firstStage), iv);
        }
    }
}
