﻿using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Crypto.SecretEncryptor
{
    public interface ISecretEncryptor
    {
        Task<string> Encrypt(string secret, string iv);
        Task<string> Decrypt(string encryptedSecret, string iv);
    }
}
