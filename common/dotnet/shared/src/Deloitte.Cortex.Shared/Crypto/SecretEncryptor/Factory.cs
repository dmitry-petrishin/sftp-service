﻿using Deloitte.Cortex.Shared.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace Deloitte.Cortex.Shared.Crypto.SecretEncryptor
{
    public static class Factory
    {
        public static ISecretEncryptor Get(Serialization.Configuration.SecretEncryptor config, IServiceProvider provider)
        {
            var loggerFactory = provider.GetRequiredService<ILoggerFactory>();
            switch (config.Type)
            {
                case Serialization.Configuration.SecretEncryptor.TypeEnum.Static:
                    return new StaticKeyEncryptor((Serialization.Configuration.SecretEncryptor.StaticOptions)config.TypedOptions, loggerFactory.CreateLogger<StaticKeyEncryptor>());
                case Serialization.Configuration.SecretEncryptor.TypeEnum.TwoStageKeyVault:
                    return new TwoStageKeyVaultEncryptor((Serialization.Configuration.SecretEncryptor.TwoStageKeyVault)config.TypedOptions, provider.GetRequiredService<IAuthenticationService>(), loggerFactory);
                default:
                    throw new Exception($"Unknown type of secret encryptor: {config.Type}");
            }
        }
    }
}
