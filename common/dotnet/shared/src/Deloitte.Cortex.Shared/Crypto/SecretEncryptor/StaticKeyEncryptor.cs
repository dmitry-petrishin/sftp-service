﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Crypto.SecretEncryptor
{
    public class StaticKeyEncryptor : ISecretEncryptor
    {
        protected ILogger logger;
        protected byte[] key;

        public StaticKeyEncryptor(Serialization.Configuration.SecretEncryptor.StaticOptions options, ILogger logger)
        {
            this.logger = logger;
            if (options.Key?.Value != null)
            {
                this.key = Encoding.UTF8.GetBytes(options.Key.Value);
                logger.LogInformation($"Key encryptor has {key.Length} bytes key");
                if (key.Length > 0)
                {
                    logger.LogInformation($"Key starts with \"{Char.ConvertFromUtf32(key[0])}\"");
                }
            }
        }

        public virtual Task<string> Encrypt(string secret, string iv)
        {
            logger.LogInformation("Encrypting with internal secret");

            byte[] encrypted;

            using (var aes = Aes.Create())
            {
                aes.IV = Convert.FromBase64String(iv);
                aes.Key = key;

                var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(secret);
                        }
                        encrypted = msEncrypt.ToArray();
                        return Task.FromResult(Convert.ToBase64String(encrypted));
                    }
                }
            }
        }

        public virtual Task<string> Decrypt(string encryptedSecret, string iv)
        {
            logger.LogInformation("Decrypting with internal secret");

            var cipherText = Convert.FromBase64String(encryptedSecret);

            using (Aes aes = Aes.Create())
            {
                aes.IV = Convert.FromBase64String(iv);
                aes.Key = key;

                var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            return Task.FromResult(srDecrypt.ReadToEnd());
                        }
                    }
                }
            }
        }
    }
}
