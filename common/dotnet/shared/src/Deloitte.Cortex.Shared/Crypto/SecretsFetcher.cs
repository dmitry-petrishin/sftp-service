﻿using Deloitte.Cortex.Shared.Authorization;
using Deloitte.Cortex.Shared.Serialization.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Crypto
{
    public class SecretsFetcher
    {
        private readonly ILogger logger;
        private readonly IServiceProvider services;
        private readonly ILoggerFactory loggerFactory;
        private readonly SecretsStorage configStorage;

        public SecretsFetcher(ILogger<SecretsFetcher> logger, IServiceProvider services, ILoggerFactory loggerFactory)
        {
            this.logger = logger;
            this.services = services;
            this.loggerFactory = loggerFactory;
            this.configStorage = services.GetService<SecretsStorage>();
        }

        public async Task FetchValuesAsync(object configuration)
        {
            // For now support only one type of storage
            if (configStorage != null && !string.IsNullOrWhiteSpace(configStorage.KeyVault))
            {
                logger.LogDebug("Fetching secrets using Key Vault");
                var authenticationService = services.GetRequiredService<IAuthenticationService>();
                var keyVaultClient = new Clients.KeyVault.KeyVaultClient(configStorage.KeyVault, loggerFactory.CreateLogger<Clients.KeyVault.KeyVaultClient>(), authenticationService);

                await ProcessValues(keyVaultClient, configuration);

                logger.LogDebug("Done");
            }
        }

        private async Task ProcessValues(Clients.KeyVault.KeyVaultClient keyVaultClient, object configuration)
        {
            if (configuration == null)
                return;

            var type = configuration.GetType();
            foreach (var property in type.GetProperties())
            {
                if (property.GetIndexParameters().Length != 0) // Ensure that the property does not requires any parameter
                {
                    continue;
                }

                var propType = property.PropertyType;
                // Skip basic types, to avoid Stack Overflow exception in some cases
                if (!propType.IsClass)
                {
                    continue;
                }

                if (propType == typeof(Secret))
                {
                    var secret = (Secret)property.GetValue(configuration);
                    if (secret == null)
                        continue;

                    if (secret.Type == Secret.TypeEnum.SecretsStorage)
                    {
                        var keyId = new KeyVaultId(secret.Key);
                        logger.LogInformation($"Fetching {keyId}");
                        var secretValue = await keyVaultClient.GetSecretAsync(keyId.Name, keyId.Version, AuthenticationLevelEnum.SecretsStorage);
                        secretValue = Encoding.UTF8.GetString(Convert.FromBase64String(secretValue));
                        secretValue = secretValue.TrimEnd('\n');
                        secret.Value = secretValue;
                    }
                    continue;
                }
                else if (property.PropertyType == typeof(string)
                    || property.PropertyType == typeof(JObject)
                    || property.PropertyType == typeof(JToken))
                {
                    continue;
                }

                // Make it recursive
                await ProcessValues(keyVaultClient, property.GetValue(configuration));
            }
        }
    }
}
