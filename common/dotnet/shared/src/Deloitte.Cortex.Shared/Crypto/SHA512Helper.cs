﻿using System.Security.Cryptography;
using System.Text;

namespace Deloitte.Cortex.Shared.Crypto
{
    // ReSharper disable once InconsistentNaming
    public static class SHA512Helper
    {
        /// <summary> Compute the hash value for the specified byte array. </summary>
        /// <param name="buffer"> Byte array to be hashed. </param>
        /// <returns> Hashed byte array. </returns>
        public static byte[] ComputeHash(byte[] buffer)
        {
            byte[] hashedInputBytes;

            using (SHA512 sha512 = SHA512.Create())
            {
                hashedInputBytes = sha512.ComputeHash(buffer);
            }

            return hashedInputBytes;
        }

        /// <summary> Compute the hash value for the specified string. </summary>
        /// <param name="s"> String to be hashed. </param>
        /// <returns> Hashed string. </returns>
        public static string ComputeStringHash(string s)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(s);

            byte[] hashedInputBytes = SHA512Helper.ComputeHash(bytes);

            // Convert to text
            // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
            StringBuilder hashedInputStringBuilder = new StringBuilder(128);
            foreach (var b in hashedInputBytes)
            {
                hashedInputStringBuilder.Append(b.ToString("X2").ToLowerInvariant());
            }

            return hashedInputStringBuilder.ToString();
        }
    }
}
