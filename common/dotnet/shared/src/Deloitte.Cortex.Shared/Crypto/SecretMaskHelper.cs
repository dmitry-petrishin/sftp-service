﻿using System;

namespace Deloitte.Cortex.Shared.Crypto
{
    public static class SecretMaskHelper
    {
        public static string MaskHash512(string hash, sbyte startIndex = 31, sbyte endIndex = 95)
        {
            string result = string.Empty;

            if (startIndex < 0 || endIndex < 0)
            {
                throw new ArgumentOutOfRangeException(
                    $"Indexes out of range. startIndex: \"{startIndex}\", endIndex: \"{endIndex}\"");
            }

            result += hash.Substring(0, startIndex);
            result += new string('*', endIndex - startIndex);
            result += hash.Substring(endIndex);

            return result;
        }
    }
}