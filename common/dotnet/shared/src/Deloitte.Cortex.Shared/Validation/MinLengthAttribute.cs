﻿using System.Collections;
using System.Linq;

namespace Deloitte.Cortex.Shared.Validation
{
    /// <summary>
    /// Fixed <see cref="System.ComponentModel.DataAnnotations.MinLengthAttribute"/> accepting <see cref="System.Collections.IEnumerable"/>.
    /// </summary>
    public class MinLengthAttribute: System.ComponentModel.DataAnnotations.MinLengthAttribute
    {
        public MinLengthAttribute(int length): base(length) { }

        public override bool IsValid(object value)
        {
            if (value is string) return base.IsValid(value);
            return base.IsValid(((IEnumerable) value).Cast<object>().ToArray());
        }
    }
}