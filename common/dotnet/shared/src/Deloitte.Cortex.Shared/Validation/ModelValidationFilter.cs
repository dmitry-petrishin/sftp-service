﻿using Deloitte.Cortex.Shared.ExceptionHandling;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Deloitte.Cortex.Shared.Validation
{
    public class ModelValidationFilter: IActionFilter, IOrderedFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
                throw new BadRequestException(context.ModelState);
        }

        public void OnActionExecuted(ActionExecutedContext context) { }

        public int Order => 100;
    }
}