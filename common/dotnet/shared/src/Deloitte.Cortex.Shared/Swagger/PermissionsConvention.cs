﻿using System.Linq;
using Deloitte.Cortex.Shared.Authorization;
using Deloitte.Cortex.Shared.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    /// <summary>
    /// Adds corresponding error response to Swagger description in case if action/controller is marked with <see cref="SetupPermissionAttribute"/>.
    /// </summary>
    public class PermissionsConvention: IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var errorResponseCode = PermissionsAuthorizationFilter.NotAuthorizedStatusCode.ToString("d");
            var filtersMetadata = context.ApiDescription.ActionDescriptor.FilterDescriptors.Select(fd => fd.Filter);

            if (filtersMetadata.OfType<SetupPermissionAttribute>().Any()
                && !operation.Responses.ContainsKey(errorResponseCode))
            {
                operation.Responses[errorResponseCode] = new Response
                {
                    Description = "Users doesn't have required permissions.",
                    Schema = context.SchemaRegistry.GetOrRegister(typeof(ErrorsResponse))
                };
            }
        }
    }
}