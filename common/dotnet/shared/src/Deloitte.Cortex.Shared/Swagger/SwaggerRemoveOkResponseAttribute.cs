﻿using System;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    // https://github.com/domaindrivendev/Swashbuckle/issues/660
    // https://github.com/domaindrivendev/Swashbuckle/blob/master/Swashbuckle.Core/Swagger/Annotations/SwaggerResponseRemoveDefaultsAttribute.cs
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class SwaggerRemoveOkResponseAttribute: SwaggerOperationFilterAttribute, IOperationFilter
    {
        public SwaggerRemoveOkResponseAttribute(): base(typeof(SwaggerRemoveOkResponseAttribute)) { }

        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Responses.ContainsKey("200"))
                operation.Responses.Remove("200");
        }
    }
}