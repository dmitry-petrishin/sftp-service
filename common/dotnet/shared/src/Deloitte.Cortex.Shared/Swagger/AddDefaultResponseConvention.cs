using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    /// <summary>
    /// Adds response from Action return type to Swagger doc if not present.
    /// Use <see cref="SwaggerRemoveOkResponseAttribute"/> or <see cref="StatusCodeResult"/> to remove it explicitly.
    /// </summary>
    /// <remarks>
    /// <see cref="Microsoft.AspNetCore.Mvc.ApiExplorer.DefaultApiDescriptionProvider"/> won't include response based on return type if Action has any <see cref="SwaggerResponseAttribute"/> present
    /// (see https://github.com/aspnet/Mvc/issues/4823).
    /// </remarks>
    public class AddDefaultResponseConvention: IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Responses.ContainsKey("200")) return;

            var defaultResponseType = (context.ApiDescription.ActionDescriptor as ControllerActionDescriptor)
                ?.MethodInfo.ReturnType;
            if (defaultResponseType == null || defaultResponseType == typeof(Task)) return;

            if (defaultResponseType.IsConstructedGenericType && defaultResponseType.GetGenericTypeDefinition() == typeof(Task<>))
                defaultResponseType = defaultResponseType.GetGenericArguments().First();

            if (!typeof(StatusCodeResult).IsAssignableFrom(defaultResponseType) &&
                !context.ApiDescription.SupportedResponseTypes.Select(r => r.Type).Contains(defaultResponseType))
            {
                operation.Responses["200"] = new Response {Schema = context.SchemaRegistry.GetOrRegister(defaultResponseType)};
            }
        }
    }
}