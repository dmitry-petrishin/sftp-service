﻿using System;
using System.Net;
using Deloitte.Cortex.Shared.Serialization;
using Microsoft.AspNetCore.Mvc;

namespace Deloitte.Cortex.Shared.Swagger
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class ErrorResponseAttribute: ProducesResponseTypeAttribute
    {
        public int ErrorCode { get; }

        public string Message { get; }

        public HttpStatusCode HttpCode => (HttpStatusCode) StatusCode;

        public ErrorResponseAttribute(string message): this(HttpStatusCode.BadRequest, message, null) { }

        public ErrorResponseAttribute(string message, int errorCode): this(HttpStatusCode.BadRequest, message, errorCode) { }

        public ErrorResponseAttribute(HttpStatusCode httpCode, string message): this(httpCode, message, null) { }

        public ErrorResponseAttribute(HttpStatusCode httpCode, string message, int errorCode): this(httpCode, message, (int?) errorCode) { }

        public ErrorResponseAttribute(int statusCode, string message): this(statusCode, message, null) { }

        public ErrorResponseAttribute(int statusCode, string message, int errorCode): this(statusCode, message, (int?) errorCode) { }

        private ErrorResponseAttribute(HttpStatusCode httpCode, string message, int? errorCode = null): this((int) httpCode, message, errorCode) { }

        private ErrorResponseAttribute(int statusCode, string message, int? errorCode = null): base(statusCode)
        {
            Type = typeof(ErrorsResponse);
            ErrorCode = errorCode ?? 0;
            Message = errorCode != null
                ? $"{errorCode}: {message}"
                : message;
        }
    }
}