﻿using System;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    /// <summary>
    /// Adds default value to Guid fields in Swagger to notify about proper formating.
    /// </summary>
    public class GuidConvention: ISchemaFilter
    {
        public void Apply(Schema model, SchemaFilterContext context)
        {
            if (context.JsonContract.UnderlyingType == typeof(Guid)
                || context.JsonContract.UnderlyingType == typeof(Guid?))
            {
                model.Example = new Guid();
            }
        }
    }
}
