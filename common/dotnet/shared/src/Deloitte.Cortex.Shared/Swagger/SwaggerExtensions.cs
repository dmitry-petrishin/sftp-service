﻿using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    public static class SwaggerExtensions
    {
        public static void UseConventions(this SwaggerGenOptions options)
        {
            options.DescribeAllEnumsAsStrings();
            options.DescribeStringEnumsInCamelCase();

            options.OperationFilter<AddDefaultResponseConvention>();
            options.OperationFilter<AuthorizationHeaderParameterConvention>();
            options.OperationFilter<DefaultParameterConvention>();
            options.OperationFilter<CancellationTokenConvention>();
            options.OperationFilter<PermissionsConvention>();
            options.OperationFilter<ErrorResponseAttributeFilter>();

            options.SchemaFilter<GuidConvention>();
        }
    }
}