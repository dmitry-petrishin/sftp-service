﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.Collections;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    public class ErrorResponseAttributeFilter: IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var attributes = context.ApiDescription.ActionDescriptor.FilterDescriptors.Select(fd => fd.Filter)
                .OfType<ErrorResponseAttribute>().ToArray();
            if (!attributes.Any()) return;

            operation.Responses = operation.Responses ?? new Dictionary<string, Response>();

            var keyPostfix = new DefaultDictionary<HttpStatusCode, string>("");
            foreach (var group in attributes.GroupBy(a => new { StatusCode = a.HttpCode, a.Type }))
            {
                var statusCode = group.Key.StatusCode;
                var type = group.Key.Type;

                var messages = group.Where(a => a.Message != null).Select(a => a.Message).ToArray();
                var description = messages.Any()
                    ? string.Join("\n\n", messages)
                    : null;

                var key = statusCode.ToString("D") + keyPostfix[statusCode];
                keyPostfix[statusCode] += " "; // hack to allow status code repetition in Swagger description

                var response = operation.Responses.GetOrDefault(key) ?? new Response();
                Apply(response, type, description, context.SchemaRegistry);
                operation.Responses[key] = response;
            }
        }

        private static void Apply(Response response, Type type, string description,
            ISchemaRegistry schemaRegistry)
        {
            if (description != null)
                response.Description = description;

            if (type != null && type != typeof(void))
                response.Schema = schemaRegistry.GetOrRegister(type);
        }
    }
}