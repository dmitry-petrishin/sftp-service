﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    /// <summary>
    /// This convention adds uses presence of default parameter value in action method signature
    /// to determine if it should be marked as required in Swagger documentation.
    /// It also sets default value in documentation if present in signature.
    /// </summary>
    public class DefaultParameterConvention : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var descriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;
            if (descriptor == null)
                throw new NotSupportedException(
                    $"ActionDescriptor of type {context.ApiDescription.ActionDescriptor.GetType()} is not supported.");

            if (operation.Parameters == null) return;

            var parameters = operation.Parameters.ToDictionary(p => p.Name, p => p);
            foreach (var info in descriptor.MethodInfo.GetParameters())
            {
                if (parameters.ContainsKey(info.Name))
                {
                    var parameter = parameters[info.Name];
                    parameter.Required = !info.HasDefaultValue;
                    if (info.HasDefaultValue)
                    {
                        NonBodyParameter nonBodyParameter = parameter as NonBodyParameter;
                        if (nonBodyParameter != null) nonBodyParameter.Default = info.RawDefaultValue;
                    }
                }
            }
        }
    }
}
