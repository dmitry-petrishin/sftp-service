﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Deloitte.Cortex.Shared.Swagger
{
    /// <summary>
    /// This convention will automatically apply <see cref="AuthorizationHeaderParameter"/> to all actions that require authorization 
    /// via <see cref="AuthorizeFilter"/> and doesn't have <see cref="AllowAnonymousFilter"/>.
    /// It doesn't support explicit authorization in action code and in this case you should apply <see cref="AuthorizationHeaderParameter"/> manually.
    /// </summary>
    public class AuthorizationHeaderParameterConvention : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (ActionRequiresAuthorization(context))
                new AuthorizationHeaderParameter().Apply(operation, context);
        }

        private bool ActionRequiresAuthorization(OperationFilterContext context)
        {
            var filters = context.ApiDescription.ActionDescriptor.FilterDescriptors.Select(fd => fd.Filter).ToArray();
            return filters.OfType<AuthorizeFilter>().Any()
                && !filters.OfType<AllowAnonymousFilter>().Any();
        }
    }
}
