﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Deloitte.Cortex.Shared.Serialization
{
    // Doesn't support multiple comma-separated values
    public class EnumHyphenSeparatedConverter<T>: EnumConverter
        where T : struct
    {
        // Doesn't support digits and underscores in names
        private static readonly Regex ToHyphenSeparatedRegex = new Regex("([a-z])([A-Z])", RegexOptions.Compiled);
        private static readonly Regex FromHyphenSeparatedRegex = new Regex("([a-z])-([a-z])", RegexOptions.Compiled);

        static EnumHyphenSeparatedConverter()
        {
            if (!typeof(T).GetTypeInfo().IsEnum)
                throw new NotSupportedException($"{typeof(T).FullName} is not supported because it's not an enum type.");
        }

        public EnumHyphenSeparatedConverter(): base(typeof(T))
        {}

        // ? -> enum
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return base.CanConvertFrom(context, sourceType) || sourceType == typeof(string);
        }

        // enum -> ?
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return base.CanConvertTo(context, destinationType) || destinationType == typeof(string);
        }

        // string -> enum
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var stringValue = value as string;
            if (stringValue != null)
            {
                return Enum.Parse(typeof(T), FromHyphenSeparatedRegex.Replace(stringValue, "$1$2"), ignoreCase: true);
            }

            return base.ConvertFrom(context, culture, value);
        }

        // enum -> string
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value is T)
            {
                var enumValue = (T)value;
                return ToHyphenSeparatedRegex.Replace(enumValue.ToString(), "$1-$2").ToLower();
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
