﻿namespace Deloitte.Cortex.Shared.Serialization
{
    /// <summary>
    /// Can be used for returning results with pagination support
    /// </summary>
    /// <typeparam name="T">Type of item to be serialized</typeparam>
    public class Range<T> where T : class
    {
        public int TotalCount { get; set; }
        public T[] Items { get; set; }

        public Range()
        {
            Items = new T[0];
        }
    }
}
