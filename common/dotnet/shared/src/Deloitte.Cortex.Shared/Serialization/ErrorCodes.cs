﻿namespace Deloitte.Cortex.Shared.Serialization
{
    /// <summary>
    /// Enum containing global error codes, valid across all microservices
    /// </summary>
    public enum ErrorCodes
    {
        /// <summary>
        /// Call to another service has been failed. The following errors will be errors returned by another service, if they succesfully read
        /// </summary>
        ClientCallFailed = 1000,
        /// <summary>
        /// Some generic error which could be shown on UI
        /// </summary>
        UIGeneric = 1001,
    }
}
