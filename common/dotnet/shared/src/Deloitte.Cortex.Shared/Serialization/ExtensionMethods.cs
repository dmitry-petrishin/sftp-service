﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Deloitte.Cortex.Shared.Serialization
{
	public static class ExtensionMethods
	{
		public static Range<T> TakeRange<T, TProperty>(this ICollection<T> collection, int offset, int limit,
			Func<T, TProperty> orderBy, bool orderAsc = true) where T : class
		{
			limit = limit > 0 ? limit : int.MaxValue;

			IEnumerable<T> orderedCollection = orderAsc ? collection.OrderBy(orderBy) : collection.OrderByDescending(orderBy);

			return new Range<T>()
			{
				Items = orderedCollection.Skip(offset).Take(limit).ToArray(),
				TotalCount = collection.Count
			};
		}

		public static Range<T> TakeRange<T>(this ICollection<T> collection, int offset, int limit) where T : class
		{
			limit = limit > 0 ? limit : int.MaxValue;

			return new Range<T>()
			{
				Items = collection.Skip(offset).Take(limit).ToArray(),
				TotalCount = collection.Count
			};
		}
	}
}
