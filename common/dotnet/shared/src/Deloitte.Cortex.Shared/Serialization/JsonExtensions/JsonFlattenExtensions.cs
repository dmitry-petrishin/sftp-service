﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Serialization.JsonExtensions
{
    public static class JsonFlattenExtensions
    {
        public static IDictionary<string, string> Flatten(this JObject self)
        {
            var dictionary = new Dictionary<string, string>();

            self.FlattenValue(dictionary, null);

            return dictionary;
        }

        private static void FlattenValue(this JToken self, IDictionary<string, string> dictionary, string prefix)
        {
            if (self is JObject jo)
            {
                var propPrefix = prefix == null ? "" : prefix + "_";
                foreach (var prop in jo.Properties())
                {
                    FlattenValue(prop.Value, dictionary, propPrefix + prop.Name);
                }
                return;
            }

            if (self is JArray)
            {
                return;
            }

            if (self is JValue jv)
            {
                dictionary.Add(prefix, jv.Value?.ToString());
            }
        }
    }
}
