﻿using System;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Serialization.JsonExtensions
{
    public static class JsonDiffExtensions
    {
        public static Tuple<JObject, JObject> GetDiff(this JObject oldJson, JObject newJson)
        {
            return Tuple.Create(oldJson, newJson);
        }
    }
}
