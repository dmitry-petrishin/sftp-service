﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class Authentication
    {
        [Obsolete]
        public string Instance
        {
            get
            {
                return Main.Instance;
            }
            set
            {
                Main.Instance = value;
            }
        }
        
        [Obsolete]
        public string Tenant
        {
            get
            {
                return Main.Tenant;
            }
            set
            {
                Main.Tenant = value;
            }
        }

        [JsonIgnore]
        [Obsolete]
        public string Authority
        {
            get
            {
                return Main.Authority;
            }
        }

        [Obsolete]
        public string ClientId
        {
            get
            {
                return Main.ClientId;
            }
            set
            {
                Main.ClientId = value;
            }
        }

        /// <summary>
        /// Secret for 'service 2 service' authentication via Azure's Service Principal.
        /// </summary>
        [JsonProperty]
        [Obsolete]
        public string ClientSecret
        {
            get
            {
                return (Main.SPNSource.GetOptions() as SPNSource.SecretOptions)?.Value;
            }
            set
            {
                Main.SPNSource = new SPNSource
                {
                    Type = SPNSource.TypeEnum.Secret,
                    Options = JObject.FromObject(new { Value = value })
                };
            }
        }

        public AuthenticationLevel Initialisation { get; set; }
        public AuthenticationLevel Main { get; set; } = new AuthenticationLevel();
        public AuthenticationLevel SecretsStorage { get; set; }
        public AuthenticationLevel KEK { get; set; }

        public List<IncomingTokenClient> IncomingClients { get; set; } = new List<IncomingTokenClient>();
    }
}