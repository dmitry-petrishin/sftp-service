﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class SPNSource
    {
        public enum TypeEnum
        {
            Secret,
            LocalCertificate,
            KeyVaultCertificate,
        }

        public class SecretOptions
        {
            public string Value { get; set; }
        }

        public class LocalCertificateOptions
        {
            public string Path { get; set; }
            [JsonProperty("CertKey")]
            public string Password { get; set; }
        }

        public class KeyVaultCertificateOptions
        {
            public string KeyVaultName { get; set; }
            public KeyVaultId CertificateId { get; set; }
            [JsonProperty("CertKey")]
            public string Password { get; set; }
        }

        public TypeEnum Type { get; set; }
        public JObject Options { get; set; }

        public object GetOptions()
        {
            if (Options == null)
                throw new Exception($"Options are not specified for {nameof(SPNSource)}");

            switch (Type)
            {
                case TypeEnum.Secret:
                    return Options.ToObject<SecretOptions>();
                case TypeEnum.LocalCertificate:
                    return Options.ToObject<LocalCertificateOptions>();
                case TypeEnum.KeyVaultCertificate:
                    return Options.ToObject<KeyVaultCertificateOptions>();
                default:
                    throw new Exception($"Unknown options type {Type.ToString()}");
            }
        }
    }
}
