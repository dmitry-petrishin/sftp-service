﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class AuthenticationLevel
    {
        public string Instance { get; set; } = "https://login.microsoftonline.com/{0}";
        [JsonProperty(Required = Required.Always)]
        public string Tenant { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string ClientId { get; set; }
        [JsonIgnore]
        public string Authority => string.Format(Instance, Tenant);

        public SPNSource SPNSource { get; set; }
    }
}
