﻿namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class SecretsStorage
    {
        public string KeyVault { get; set; }
    }
}
