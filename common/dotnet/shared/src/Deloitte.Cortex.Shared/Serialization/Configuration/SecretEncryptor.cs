﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class SecretEncryptor
    {
        public enum TypeEnum
        {
            Static,
            TwoStageKeyVault,
        }

        public class StaticOptions
        {
            public Secret Key { get; set; }
        }

        public class TwoStageKeyVault : StaticOptions
        {
            public string KEKKeyVaultName { get; set; }
            public KeyVaultId KeyId { get; set; }
            public string Algorithm { get; set; }
        }

        private TypeEnum type;
        [JsonConverter(typeof(StringEnumConverter))]
        public TypeEnum Type
        {
            get
            {
                return type;
            }
            set
            {
                if (type != value)
                {
                    type = value;
                    typedOptions = null;
                }
            }
        }
        public JObject Options { get; set; }

        private object typedOptions = null;
        [JsonIgnore]
        public object TypedOptions
        {
            get
            {
                if (typedOptions == null)
                {
                    switch (Type)
                    {
                        case TypeEnum.Static:
                            typedOptions = Options.ToObject<StaticOptions>();
                            break;
                        case TypeEnum.TwoStageKeyVault:
                            typedOptions = Options.ToObject<TwoStageKeyVault>();
                            break;
                    }
                }
                return typedOptions;
            }
        }
    }
}
