﻿namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class KeyVaultId
    {
        public string Name { get; set; }
        public string Version { get; set; }

        public bool NotEmpty
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Name);
            }
        }

        public KeyVaultId()
        {
        }

        public KeyVaultId(string keyId)
        {
            int pos = keyId.IndexOf('@');
            if (pos >= 0)
            {
                Name = keyId.Substring(0, pos);
                Version = keyId.Substring(pos + 1);
            }
            else
            {
                Name = keyId;
            }
        }

        public static implicit operator KeyVaultId(string keyId)
        {
            return new KeyVaultId(keyId);
        }

        public override string ToString()
        {
            if (Version == null)
                return Name;
            else
                return $"{Name}@{Version}";
        }
    }
}
