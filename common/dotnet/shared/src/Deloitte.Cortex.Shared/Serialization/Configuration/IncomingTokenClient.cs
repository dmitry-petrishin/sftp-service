﻿using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class IncomingTokenClient
    {
        public string ClientId { get; set; }
        public string Authority { get; set; }
        public string SigningCertificatePath { get; set; }
        public JObject SigningKey { get; set; }
    }
}
