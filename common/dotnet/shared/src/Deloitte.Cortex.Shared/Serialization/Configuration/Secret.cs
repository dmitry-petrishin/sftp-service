﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Shared.Serialization.Configuration
{
    public class Secret
    {
        public enum TypeEnum
        {
            Static,
            SecretsStorage,
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public TypeEnum Type { get; set; } = TypeEnum.Static;
        public string Key { get; set; }
        public string Value { get; set; }

        public Secret()
        {
        }

        public static implicit operator Secret(string value)
        {
            return new Secret()
            {
                Value = value
            };
        }

        public Secret Clone()
        {
            return new Secret()
            {
                Type = Type,
                Key = Key,
                Value = Value,
            };
        }
    }
}
