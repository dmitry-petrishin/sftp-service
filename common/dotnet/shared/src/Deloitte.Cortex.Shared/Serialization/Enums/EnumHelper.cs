﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Deloitte.Cortex.Shared.Serialization.Enums
{
    public static class EnumHelper
    {
        public static IEnumerable<EnumView> BuildEnumViews<T>() where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(BuildView);
        }

        public static EnumView BuildView<T>(T enumValue) where T : struct
        {
            return new EnumView
            {
                Code = Enum.GetName(typeof(T), enumValue),
                DisplayName = enumValue.GetDescriptionValue(),
            };
        }
    }
}
