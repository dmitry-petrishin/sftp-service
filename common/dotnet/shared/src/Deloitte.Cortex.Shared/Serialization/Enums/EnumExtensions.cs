﻿using System.ComponentModel;

namespace Deloitte.Cortex.Shared.Serialization.Enums
{
    public static class EnumExtensions
    {
        public static string GetDescriptionValue<T>(this T value) where T : struct
        {
            //TODO: cache values
            var fieldInfo = value.GetType().GetField(value.ToString());

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                                            typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (descriptionAttributes == null) return string.Empty;

            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : value.ToString();
        }
    }
}
