﻿namespace Deloitte.Cortex.Shared.Serialization.Enums
{
    public class EnumView
    {
        public string Code { get; set; }
        public string DisplayName { get; set; }
    }
}
