﻿using Deloitte.Cortex.Shared.ExceptionHandling;
using System;
using System.Net;

namespace Deloitte.Cortex.Shared.Serialization
{
	public class SortOrder
	{
		private const char Asc = '+';
		private const char Desc = '-';

		private SortOrder()
		{
		}

		/// <summary>
		/// Field name. Null - use default sort order field.
		/// </summary>
		public string OrderBy { get; private set; } = null;

		public bool OrderAsc { get; private set; } = true;

		//sort= - sort by default field (individually defined for each object) asc
		//sort=name - sort by name asc
		//sort=+name - sort by name asc
		//sort=-name - sort by name desc
		public static SortOrder Parse(string sort)
		{
			if (String.IsNullOrWhiteSpace(sort)) //sort is not specified
				return new SortOrder(); //use default sort order

			bool hasPrefix = sort[0] == Asc || sort[0] == Desc;

			if (sort.Length == 1 && hasPrefix)
				throw new HttpException("Wrong sort order, missing field name.", HttpStatusCode.BadRequest);

			return new SortOrder()
			{
				OrderBy = hasPrefix ? sort.Substring(1) : sort,
				OrderAsc = sort[0] != Desc
			};
		}
	}
}
