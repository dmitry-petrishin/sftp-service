﻿namespace Deloitte.Cortex.Shared.Serialization
{
    /// <summary>
    /// Simple class for returning bool in json form from services with Message field for extra details, particularly when Success = false
    /// </summary>
    public class GenericResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public GenericResponse()
        {
            Success = false;
        }

        public GenericResponse(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}
