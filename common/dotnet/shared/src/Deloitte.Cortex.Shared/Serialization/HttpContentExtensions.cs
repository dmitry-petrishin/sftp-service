﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Serialization
{
    // Todo: remove when Microsoft.AspNet.WebApi.Client is available (https://github.com/aspnet/Home/issues/1558)
    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsAsync<T>(this HttpContent content)
        {
            var mediaType = content.Headers.ContentType?.MediaType;
            if (mediaType != "application/json")
                throw new NotSupportedException($"Media type \"{mediaType}\" is not supported.");

            var stringContent = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(stringContent);
        }
    }
}
