﻿namespace Deloitte.Cortex.Shared.Serialization
{
    public interface IIdentifier
    {
        string Id { get; set; }
    }
}
