﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Deloitte.Cortex.Shared.Serialization
{
    /// <summary>
    /// Use this generic response in backend to report about single or multiple errors.
    /// </summary>
    public class ErrorsResponse
    {
        public class Error
        {
            /// <summary>
            /// Specific for every service. Each code should be described in documentation or swagger. 0 means unknown error code.
            /// </summary>
            [JsonProperty(PropertyName = "code")]
            public int Code { get; set; }
            
            /// <summary>
            /// Property that caused an error. Used for model validation.
            /// </summary>
            [JsonProperty(PropertyName = "prop", NullValueHandling = NullValueHandling.Ignore)]
            public string Property { get; set; }
            
            /// <summary>
            /// Human readable error.
            /// </summary>
            [JsonProperty(PropertyName = "msg")]
            public string Message { get; set; }

            /// <summary>
            /// Empty ctor for deserialization
            /// </summary>
            public Error()
            {
                Code = 0;
            }

            public Error(string message): this()
            {
                Message = message;
            }

            public Error(string message, int code) :
                this(message)
            {
                Code = code;
            }

            public Error(string message, int code, string property) :
                this(message, code)
            {
                Property = property;
            }

            public override string ToString()
            {
                return $"#{Code}{ (!string.IsNullOrEmpty(Property) ? (" [" + Property + "]") : "") } {Message}";
            }
        }

        [JsonProperty(PropertyName = "errors")]
        public List<Error> Errors { get; } = new List<Error>();

        [JsonConstructor]
        internal ErrorsResponse()
        {}

        public ErrorsResponse(string errorMessage):
            this()
        {
            Errors.Add(new Error(errorMessage ?? "Internal Server Error"));
        }

        public ErrorsResponse(Exception exception):
            this(exception?.Message)
        {}

        public ErrorsResponse(string errorMessage, int code) :
            this()
        {
            Errors.Add(new Error(errorMessage, code));
        }

        public ErrorsResponse(IEnumerable<Error> errors) :
            this()
        {
            Errors.AddRange(errors ?? Enumerable.Empty<Error>());
        }
    }
}
