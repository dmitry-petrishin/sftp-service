﻿using Newtonsoft.Json;
using System;

namespace Deloitte.Cortex.Shared.Serialization
{
    public abstract class DocumentItemWithId : IIdentifier
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonIgnore]
        public bool IsIdEmpty => string.IsNullOrEmpty(Id);

        public void GenerateId()
        {
            Id = Guid.NewGuid().ToString("D");
        }

        public void GenerateIdWithoutDashes()
        {
            Id = Guid.NewGuid().ToString().Replace("-", "");
        }
    }
}
