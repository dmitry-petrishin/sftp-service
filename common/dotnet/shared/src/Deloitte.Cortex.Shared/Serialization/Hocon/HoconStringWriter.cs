﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Serialization.Hocon
{
    public class HoconStringWriter: JsonTextWriter
    {
        private static Action<JsonWriter, string> InternalWritePropertyName = (Action<JsonWriter, string>)
            typeof(JsonWriter).GetMethod("InternalWritePropertyName", BindingFlags.Instance | BindingFlags.NonPublic)
                .CreateDelegate(typeof(Action<JsonWriter, string>));

        private static Action<JsonTextWriter, string, bool> WriteEscapedString = (Action<JsonTextWriter, string, bool>)
            typeof(JsonTextWriter).GetMethod("WriteEscapedString", BindingFlags.Instance | BindingFlags.NonPublic)
                .CreateDelegate(typeof(Action<JsonTextWriter, string, bool>));

        private readonly MemoryStream _memoryStream;
        private readonly TextWriter _writer;

        public HoconStringWriter(): base(new StreamWriter(new MemoryStream()))
        {
            _writer = (TextWriter) typeof(JsonTextWriter).GetField("_writer", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
            _memoryStream = (MemoryStream) ((StreamWriter) _writer).BaseStream;
        }

        public override void WritePropertyName(string name)
        {
            InternalWritePropertyName(this, name);
            WriteEscapedString(this, name, true);
            _writer.Write('=');
        }

        public override void WritePropertyName(string name, bool escape)
        {
            WritePropertyName(name);
        }

        public string GetString()
        {
            _writer.Flush();
            var @string = Encoding.UTF8.GetString(_memoryStream.ToArray());
            return @string;
        }
    }
}
