﻿using System.Net.Http;

namespace Deloitte.Cortex.Shared.Serialization.Hocon
{
    public class HoconContent: StringContent
    {
        public HoconContent(object @object) : base(@object == null ? string.Empty : HoconSerializer.Serialize(@object))
        { }
    }
}
