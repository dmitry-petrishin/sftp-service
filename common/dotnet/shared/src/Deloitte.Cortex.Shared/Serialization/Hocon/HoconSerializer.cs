﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Serialization.Hocon
{
    public class HoconSerializer: JsonSerializer
    {
        public static string Serialize<T>(T @object)
        {
            var serializer = new HoconSerializer();

            string serializedString;
            using (var writer = new HoconStringWriter())
            {
                serializer.Serialize(writer, @object);
                serializedString = writer.GetString();
            }

            return serializedString;
        }
    }
}
