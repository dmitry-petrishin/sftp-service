﻿using Newtonsoft.Json;
using System;

namespace Deloitte.Cortex.Shared.Serialization
{
    /// <summary>
    /// Converts Guid to string. If Guid is empty, null is emitted to output JSON
    /// </summary>
    public class GuidToStringConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType != typeof(Guid))
                throw new Exception("Unsupported object type");
            var val = reader.Value;
            if (val == null)
                return Guid.Empty;
            return Guid.Parse(val.ToString());
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null || (value is Guid && (Guid)value == default))
                writer.WriteNull();
            else
                writer.WriteValue(value.ToString());
        }
    }
}
