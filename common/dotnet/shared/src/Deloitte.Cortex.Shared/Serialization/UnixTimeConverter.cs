﻿using Newtonsoft.Json;
using System;

namespace Deloitte.Cortex.Shared.Serialization
{
    public class UnixTimeConverter : JsonConverter
    {
        public enum KindEnum
        {
            Classic,
            Milliseconds,
            Microseconds
        }
        private KindEnum kind;

        public UnixTimeConverter(KindEnum kind)
        {
            this.kind = kind;
        }

        public UnixTimeConverter() : this(KindEnum.Classic)
        {
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) || objectType == typeof(DateTime?);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var t = (long)reader.Value;
            var res = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            switch (kind)
            {
                case KindEnum.Classic:
                    res = res.AddSeconds(t);
                    break;
                case KindEnum.Milliseconds:
                    res = res.AddMilliseconds(t);
                    break;
                case KindEnum.Microseconds:
                    res = res.AddMilliseconds(t / 1000);
                    break;
            }
            return res;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var epochBegin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var diff = (DateTime)value - epochBegin;
            long diffNumber = 0;
            switch (kind)
            {
                case KindEnum.Classic:
                    diffNumber = (long)diff.TotalSeconds;
                    break;
                case KindEnum.Milliseconds:
                    diffNumber = (long)diff.TotalMilliseconds;
                    break;
                case KindEnum.Microseconds:
                    diffNumber = (long)(diff.TotalMilliseconds * 1000);
                    break;
            }
            writer.WriteValue(diffNumber);
        }
    }
}
