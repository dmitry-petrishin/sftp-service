using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Deloitte.Cortex.Shared.Serialization
{
    public class KeyOrderedConverter: JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var jToken = JToken.FromObject(value, serializer);
            var jObject = jToken as JObject;
            if (jObject != null)
                jToken = new JObject(jObject.Properties().OrderBy(p => p.Name));

            jToken.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotSupportedException();
        }

        public override bool CanConvert(Type objectType) => true;
    }
}