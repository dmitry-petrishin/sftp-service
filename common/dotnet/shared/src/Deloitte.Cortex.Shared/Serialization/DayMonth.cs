﻿namespace Deloitte.Cortex.Shared.Serialization
{
    public class DayMonth
    {
        public int Day { get; set; }
        public int Month { get; set; }

        public DayMonth() { }

        public DayMonth(DayMonth dayMonth)
        {
            Day = dayMonth.Day;
            Month = dayMonth.Month;
        }

        public DayMonth(int day, int month)
        {
            Day = day;
            Month = month;
        }

        public override string ToString()
        {
            return $"{Month}/{Day}";
        }

        public DayMonth Clone()
        {
            return new DayMonth(Day, Month);
        }
    }
}
