﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Serialization
{
    public class JsonContent: StringContent
    {
        public JsonContent(object @object): base(@object == null ? string.Empty : JsonConvert.SerializeObject(@object),
            Encoding.UTF8, "application/json") { }
    }
}