# Shared code distributed as a NuGet package

## Services

### HTTP client for inter-service communication

For HTTP calls between services we use ```Deloitte.Cortex.Shared.Clients.BaseClient```.
It provides logging, authentication, exception forwarding, status code validation, etc.  
Custom service clients should inherit from it and use methods such as ```GetAsync```, ```PostAsync``` and similar.

This client is not thread-safe and is developed with ~1-2 request per instance in mind. As such all descendants should be injected with either scoped or transient lifetime 
(see [Service Lifetimes and Registration Options](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection#service-lifetimes-and-registration-options)).

#### Inter-service calls
Service can make authenticated calls to other services either on his behalf or on behalf of current user.
Both methods require access token in *Authorization* HTTP header.  

- To make **user-2-service** call use ```BaseClient.UseAuthTokenFromContext(IHttpContextAccessor)```.
  This will add current user token if present to all subsequent requests of current instance.  
  ```IHttpContextAccessor``` parameter can be injected after registration in *Startup.cs*:
  ```csharp
  services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
  ```

- To make **service-2-service** call without user involved use ```BaseClient.UseServiceAuthTokenAsync(IAuthenticationService)```.
  This one will add service token to all subsequent requests of current instance.  
  Method is asynchronous as it may involve separate HTTP call to obtain new service token.  
  ```IAuthenticationService``` implementation is thread safe and can be registered as singleton:
  ```csharp
  services.TryAddSingleton<IAuthenticationService, AuthenticationService>();
  ```

These methods can be used either before specific call or in client constructor to make it default for all calls. 
In either case token provided will be used in all subsequent HTTP calls of this client instance and only one token type can be in request.

### Security

#### Global ```AuthorizeAttribute```
To make all calls to MVC actions require authentication by default use ```MvcOptions.RequireAuthentication``` extension method:
```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddMvc(options =>
    {
        options.RequireAuthentication();
    });
    //...
}
```
You can then use ```AllowAnonymousAttribute``` to remove this requirement for specific controller or action.

#### Limit access for Service or User callers
You can allow access to action only to application users or only to other Cortex services.
By default calls from both services and users are allowed. To limit specific action or controller to allow calls
- only from another services - use ```ServiceOnlyAttribute```;
- only from application users - use ```UserOnlyAttribute```.   
```csharp
[Route("data"), Authorize]
public class AuthorizeExampleController: ControllerBase
{
    [HttpGet("user"), UserOnly]
    public string GetUserData() => "User only allowed.";

    [HttpGet("service"), ServiceOnly]
    public string GetServiceData() => "Service only allowed.";

    [HttpGet("user-or-service")]
    public string GetUserOrServiceData() => "User or Service allowed.";
}
```

## Database Migration

### DBMigrator
Solution contains DBMigrator project with tool for moving data between NOSQL databases.
At the moment of writing only **DocumentDB** and **MongoDB** were supported.

Command Line:  
```DBMigrator <source-name> <destination-name> <logging-level>```
Default values:
- `source-name` = "DocumentDB-Local"
- `destination-name` = "MongoDB-Local"
- `logging-level` = "Information"

Both `source-name` and `destination-name` are loaded from *appsettings.json* and *appsettings.Local.json*. Second one is .git-ignored and can be used for local runs.  
*appsettings&ast;.json* contains JSON dictionaries where keys are names and values are configurations for corresponding DBs.  
Part of key name before "-" is expected to contain database type (like "MongoDB" or "DocumentDB"). Spaces are ignored and "-" is not required to be present.  
**Example**:
```json
{
  "DocumentDB-Local": {
    "Endpoint": "https://localhost:8081/",
    "Key": "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw=="
  },
  "MongoDB-Local": {
    "Endpoint": "mongodb://localhost"
  }
}
```

If database name is specified in source configuration - only specified database will be replicated.  
**Example**:
```json
"DocumentDB-Local": {
  "Endpoint": "https://localhost:8081/",
  "Key": "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==",
  "DatabaseId": "Informatica" 
}
```

If both database and collection names are present - only specified collection will be replicated.  
**Example**:
```json
"MongoDB-Local": {
  "Endpoint": "mongodb://localhost",
  "DatabaseName": "Security",
  "CollectionName": "Users"
}
```

## Building & Using

### How that works?
Every push to origin/master will trigger already predefined build in VSTS. That build will do all the work for publishing to Nexus NuGet server
[more about build](https://usinnovation.visualstudio.com/Cortex/_build/index?context=Mine&path=%5Cshared&definitionId=8&_a=completed)
[more about Nexus NuGet](http://40.121.167.107:8081/nexus/#view-repositories;CortexNuget~browsestorage)

### How to version the package?
There is <version> section in a .nuspec file in solution (for example <version>1.0.2</version>). Before each push to origin/master It is necessary to manually increase version in .nuspec file. If version would not be increased publishing step would fail and build would complete with "partial succeeded" resolution.  

### How to include new just added library to NuGet package?
It is necessary to specify the <file> tag that points to your library release dll version. Also if library has 3rd party dependencies It is necessary to add them in the <dependencies> section

### Which source should be used for accessing Shared?
Please try http://nexus.usinnovation.io:8081/nexus/service/local/nuget/CortexNuget/ or http://40.121.167.107:8081/nexus/service/local/nuget/CortexNuget/
