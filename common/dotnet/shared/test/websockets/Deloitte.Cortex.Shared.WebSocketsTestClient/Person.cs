﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Deloitte.Cortex.Shared.WebSocketsTestClient
{
    public class Person
    {
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}
