﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.WebSocketsTestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            RunWebSockets().GetAwaiter().GetResult();
        }

        static async Task RunWebSockets()
        {
            ClientWebSocket client = new ClientWebSocket();

            await client.ConnectAsync(new Uri("ws://localhost:59458/ws"), CancellationToken.None);

            Console.WriteLine("Connected!");

            Task sending = Task.Run(async() => 
            {
                string line;
                while((line = Console.ReadLine()) != null && line != String.Empty)
                {
                    var person = new Person { Name = "Dmitry", Surname = "Petrishin" };

                    string string1 = JsonConvert.SerializeObject(person);

                    byte[] bytes = Encoding.UTF8.GetBytes(string1);

                    await client.SendAsync(new ArraySegment<byte>(bytes), WebSocketMessageType.Text, true, CancellationToken.None);
                }

                await client.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
            });

            Task receiving = Receiving(client);

            await Task.WhenAll(sending, receiving);
        }

        static async Task Receiving(ClientWebSocket client)
        {
            byte[] buffer = new byte[1024 * 4];

            while (true)
            {
                WebSocketReceiveResult result = await client.ReceiveAsync(
                                 new ArraySegment<byte>(buffer), 
                                 CancellationToken.None);

                if (result.MessageType == WebSocketMessageType.Text)
                {
                    Console.WriteLine(Encoding.UTF8.GetString(buffer, 0, result.Count));
                }
                else if (result.MessageType == WebSocketMessageType.Close)
                {
                    await client.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                    break;
                }
            }
        }
    }
}
