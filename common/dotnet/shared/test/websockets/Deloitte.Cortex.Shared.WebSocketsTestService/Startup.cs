﻿using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.WebSockets;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Deloitte.Cortex.Shared.WebSocketsTestService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IServiceProvider provider, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(LogLevel.Debug);
            loggerFactory.AddDebug(LogLevel.Debug);

            

            var webSocketOptions = new WebSocketOptions()
                                   {
                                       KeepAliveInterval = TimeSpan.FromSeconds(120),
                                       ReceiveBufferSize = 4 * 1024
                                   };
            app.UseWebSockets(webSocketOptions);

            app.MapWebSocketPath("/ws", provider.GetService<WebSocketManager<Person>>());


            app.UseFileServer();
            app.UseMvc();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddSingleton<WebSocketConnectionMemory>();
            services.AddSingleton<WebSocketManager<Person>>();
        }
    }
}