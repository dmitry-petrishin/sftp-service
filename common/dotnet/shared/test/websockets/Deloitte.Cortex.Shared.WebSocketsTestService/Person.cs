﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.WebSocketsTestService
{
    public class Person
    {
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}
