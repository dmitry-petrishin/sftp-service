﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Deloitte.Cortex.Shared.WebSocketsTestService
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder MapWebSocketPath1(
            this IApplicationBuilder app,
            PathString path)
        {
            return app.Map(path, a => a.UseMiddleware<WebSocketManagerMiddleware1>());
        }
    }
}
