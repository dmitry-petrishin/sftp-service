﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Deloitte.Cortex.Shared.WebSockets;

using Microsoft.AspNetCore.Mvc;

namespace Deloitte.Cortex.Shared.WebSocketsTestService.Controllers
{
    [Route("one")]
    public class OneController
    {
        private WebSocketManager<Person> manager;
        public OneController(WebSocketManager<Person> manager)
        {
            
            this.manager = manager;
        }

        [HttpGet]
        public async Task Get()
        {
            await this.manager.SendMessageToAllAsync("Helllooooo");
        }
    }
}
