﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Deloitte.Cortex.Shared.WebSocketsTestService
{
    public class WebSocketManagerMiddleware1
    {
        private readonly RequestDelegate next;

        public WebSocketManagerMiddleware1(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                context.Response.StatusCode = 400;
            }
            else
            {
                WebSocket socket = await context.WebSockets.AcceptWebSocketAsync();
                await this.Echo(socket);
            }

            await this.next(context);
        }

        private async Task Echo(WebSocket webSocket)
        {
            byte[] buffer = new byte[1024 * 4];
            WebSocketReceiveResult result = await webSocket.ReceiveAsync(
                    new ArraySegment<byte>(buffer), 
                    CancellationToken.None);

            string xx = Encoding.UTF8.GetString(buffer);

            Person person = JsonConvert.DeserializeObject<Person>(xx);

            while (!result.CloseStatus.HasValue)
            {
                await webSocket.SendAsync(
                    new ArraySegment<byte>(buffer, 0, result.Count), 
                    result.MessageType, 
                    result.EndOfMessage, 
                    CancellationToken.None);

                result = await webSocket.ReceiveAsync(
                             new ArraySegment<byte>(buffer), 
                             CancellationToken.None);
            }

            await webSocket.CloseAsync(
                result.CloseStatus.Value, 
                result.CloseStatusDescription, 
                CancellationToken.None);
        }
    }
}
