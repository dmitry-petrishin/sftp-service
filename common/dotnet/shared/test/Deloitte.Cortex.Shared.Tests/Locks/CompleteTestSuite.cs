﻿using Deloitte.Cortex.Shared.Clients.Caching;
using Deloitte.Cortex.Shared.Locks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Tests.Locks
{
    public class CompleteTestSuite : BaseTest
    {
        private CachingConfiguration cachingConfig;

        public CompleteTestSuite()
        {
            var configuration = GetConfiguration();
            cachingConfig = new CachingConfiguration();
            configuration.GetSection("Caching").Bind(cachingConfig);
        }

        [Test]
        public void Unit_Shared_Locks_SimpleTest()
        {
            var factory = new LoggerFactory();
            var logger = factory.CreateLogger<RedisClient>();
            var cachingClient = new RedisClient(cachingConfig.Endpoint.Value, "UnitTests", logger, cachingConfig.CA, cachingConfig.Crt);

            var objectId = Guid.NewGuid().ToString();
            using (CacheLock.AcquireLock(cachingClient, objectId))
            {
                Assert.Throws<LockException>(() =>
                {
                    CacheLock.AcquireLock(cachingClient, objectId, TimeSpan.FromSeconds(3), TimeSpan.FromMinutes(5));
                });
            }
        }

        [Test]
        public void Unit_Shared_Locks_SimultaneousEmptyTest()
        {
            var factory = new LoggerFactory();
            var logger = factory.CreateLogger<RedisClient>();
            var cachingClient = new RedisClient(cachingConfig.Endpoint.Value, "UnitTests", logger, cachingConfig.CA, cachingConfig.Crt);

            var lockId = Guid.NewGuid().ToString();
            var originalValue = 0;

            Action<int> action = (taskNumber) =>
            {
                var taskId = $"Task {taskNumber}";
                Thread.CurrentThread.Name = taskId;

                Console.WriteLine($"[{taskId}] {DateTime.UtcNow} Trying to aquire lock");
                using (var objectLock = CacheLock.AcquireLock(cachingClient, lockId))
                {
                    Console.WriteLine($"[{taskId}] {DateTime.UtcNow} Acquired");
                    var val = originalValue;
                    val++;
                    Thread.Sleep(3000);
                    originalValue = val;

                    Console.WriteLine($"[{taskId}] {DateTime.UtcNow} Job finished");
                }
            };

            int i = 0;
            var tasks = new List<Task>(2);
            while (i++ < 2)
            {
                var num = i;
                var task = new Task(() => action(num));
                tasks.Add(task);
            }

            foreach (var task in tasks)
                task.Start();

            Console.WriteLine($"Tasks: {tasks.Count}");
            Task.WaitAll(tasks.ToArray());

            Assert.AreEqual(2, originalValue);
        }

        [Test]
        public void Unit_Shared_Locks_SimultaneousExistingTest()
        {
            var factory = new LoggerFactory();
            var logger = factory.CreateLogger<RedisClient>();
            var cachingClient = new RedisClient(cachingConfig.Endpoint.Value, "UnitTests", logger, cachingConfig.CA, cachingConfig.Crt);

            var lockId = Guid.NewGuid().ToString();
            // Create and store lock
            using (var objectLock = CacheLock.AcquireLock(cachingClient, lockId))
            {
            }
            var originalValue = 0;

            Action<int> action = (taskNumber) =>
            {
                var taskId = $"Task {taskNumber}";
                Thread.CurrentThread.Name = taskId;

                Console.WriteLine($"[{taskId}] {DateTime.UtcNow} Trying to aquire lock");
                using (var objectLock = CacheLock.AcquireLock(cachingClient, lockId))
                {
                    Console.WriteLine($"[{taskId}] {DateTime.UtcNow} Acquired");
                    var val = originalValue;
                    val++;
                    Thread.Sleep(3000);
                    originalValue = val;

                    Console.WriteLine($"[{taskId}] {DateTime.UtcNow} Job finished");
                }
            };

            int i = 0;
            var tasks = new List<Task>(2);
            while (i++ < 2)
            {
                var num = i;
                var task = new Task(() => action(num));
                tasks.Add(task);
            }

            foreach (var task in tasks)
                task.Start();

            Console.WriteLine($"Tasks: {tasks.Count}");
            Task.WaitAll(tasks.ToArray());

            Assert.AreEqual(2, originalValue);
        }
    }
}
