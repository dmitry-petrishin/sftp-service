﻿using Deloitte.Cortex.Shared.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests
{
    [TestFixture]
    public class SerializationTests
    {
        private class Sample
        {
            public int Id;
            public string Name;

            public Sample() { }
            public Sample(int id, string name)
            {
                Id = id;
                Name = name;
            }
        }

        public SerializationTests()
        {
        }

        [Test]
        public void Unit_Shared_Serialization_ErrorsResponseTest()
        {
            var resp = new ErrorsResponse("Field Name is not specified", 100);
            var json = JsonConvert.SerializeObject(resp, Formatting.None);

            Assert.AreEqual("{\"errors\":[{\"code\":100,\"msg\":\"Field Name is not specified\"}]}", json);
        }

        [Test]
        public void Unit_Shared_Serialization_RangeTest()
        {
            Range<Sample> response = new Range<Sample>();
            response.TotalCount = 1000;
            response.Items = new Sample[]
            {
                new Sample(1, "sample 1"),
                new Sample(2, "sample 2")
            };

            var res = JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            // Wait for .NET Core 1.2 where this API is fixed
            //var expected = System.Text.Encoding.UTF8.GetString(Resource.RangeSample);
            //Assert.AreEqual(expected, res);
            Assert.IsNotNull(res);
        }
    }
}
