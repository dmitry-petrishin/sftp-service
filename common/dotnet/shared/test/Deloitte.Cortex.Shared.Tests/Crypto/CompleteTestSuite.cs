﻿using Deloitte.Cortex.Shared.Crypto;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests.Crypto
{
    public class CompleteTestSuite : BaseTest
    {
        [Test]
        public void Unit_Shared_Crypto_GenerateStrongPasswordTest()
        {
            var password = PasswordStore.GeneratePassword(20, 3);
            Assert.AreEqual(20, password.Length);
        }
    }
}
