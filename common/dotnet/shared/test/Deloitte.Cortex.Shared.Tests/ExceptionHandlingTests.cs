﻿using Deloitte.Cortex.Shared.Serialization;
using NUnit.Framework;
using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Tests
{
    [TestFixture]
    public class ExceptionHandlingTests
    {

        public ExceptionHandlingTests()
        {
        }

        [Test]
        public void Unit_Shared_ExceptionHandling_MessageTest()
        {
            var errors = new List<ErrorsResponse.Error>
            {
                new ErrorsResponse.Error("Value must be specified", 100, "ConnectorId"),
                new ErrorsResponse.Error("Something went wrong")
            };
            var exc = new ExceptionHandling.BadRequestException(errors);
            var msg = exc.Message;

            var expected = @"Errors are:
#100 [ConnectorId] Value must be specified
#0 Something went wrong";

            expected = expected.Replace("\r\n", "\n").Replace("\r", "\n");
            msg = msg.Replace("\r\n", "\n").Replace("\r", "\n");
            Assert.AreEqual(expected, msg);
        }
    }
}
