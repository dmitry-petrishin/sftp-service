﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using System.Text;

using Deloitte.Cortex.Shared.Clients;

using Newtonsoft.Json.Linq;
using Deloitte.Cortex.Shared.Serialization;
using Deloitte.Cortex.Shared.ExceptionHandling;

namespace Deloitte.Cortex.Shared.Tests
{
    [TestFixture]
    public class BaseClientTests : IDisposable
    {
        private const int port = 40080;
        private TcpListener server;
        public BaseClientTests()
        {
            server = new TcpListener(System.Net.IPAddress.Loopback, port);
            server.Start();
        }

        public void Dispose()
        {
            if (server != null)
            {
                server.Stop();
                server = null;
            }
        }

        private async Task DoPostTest(object postData)
        {
            using (var client = new BaseClient($"http://localhost:{port}"))
            {
                // Post
                var requestTask = client.PostAsync<GenericResponse>("/test", postData);

                // Read
                string actualRequest;
                using (var clientSocket = await server.AcceptSocketAsync())
                {
                    var buffer = new byte[1024];
                    Thread.Sleep(100); // Wait for all data to come into read buffer
                    int bytesRead = clientSocket.Receive(buffer);
                    actualRequest = Encoding.UTF8.GetString(buffer, 0, bytesRead);

                    // Validate POST request
                    if (postData != null)
                    {
                        int pos = actualRequest.IndexOf("\r\n\r\n");
                        if (pos > 0)
                        {
                            var obj = actualRequest.Substring(pos + 4);
                            var objDeserialized = JToken.Parse(obj);
                            var expected = JToken.FromObject(postData);
                            // Compare strings since NUnit shows more details on what's the difference for strings
                            Assert.AreEqual(expected.ToString(), objDeserialized.ToString());
                        }
                    }

                    // Build response
                    var content = JsonConvert.SerializeObject(new GenericResponse(true, ""));
                    StringBuilder sb = new StringBuilder();
                    // Use \r\n to make it cross-platform
                    sb.Append("HTTP/1.1 200 OK\r\n");
                    sb.Append($"Content-Length: {Encoding.UTF8.GetBytes(content).Length}\r\n");
                    sb.Append("Content-Type: application/json\r\n");
                    sb.Append("\r\n");
                    sb.Append(content);

                    // Send
                    var answer = sb.ToString();
                    clientSocket.Send(Encoding.UTF8.GetBytes(answer));
                }

                Assert.IsNotNull(actualRequest);

                var response = await requestTask;
                Assert.IsTrue(response.Success);
            }
        }

        [Test]
        public async Task Unit_Shared_BasicClient_PostObjectTest()
        {
            await DoPostTest(new { id = 10, name = "test name" });
        }

        [Test]
        public async Task Unit_Shared_BasicClient_PostArrayTest()
        {
            await DoPostTest(new[] { new { id = 10, name = "Hey" }, new { id = 11, name = "It's me" } });
        }

        [Test]
        public async Task Unit_Shared_BasicClient_PostNullTest()
        {
            await DoPostTest(null);
        }

        [Test]
        public async Task Unit_Shared_BasicClient_PassErrorsThroughTest()
        {
            using (var client = new BaseClient($"http://localhost:{port}"))
            {
                client.PassErrorsThrough = true;

                // Post
                var requestTask = client.GetAsync<GenericResponse>("/test");

                // Read
                using (var clientSocket = await server.AcceptSocketAsync())
                {
                    var buffer = new byte[1024];
                    Thread.Sleep(100); // Wait for all data to come into read buffer
                    clientSocket.Receive(buffer);

                    // Build response
                    var jsonResponse = new ErrorsResponse(new List<ErrorsResponse.Error>
                    {
                        new ErrorsResponse.Error("Something went wrong", 100),
                        new ErrorsResponse.Error("Something went wrong 2", 101),
                    });
                    var content = JsonConvert.SerializeObject(jsonResponse);
                    StringBuilder sb = new StringBuilder();
                    // Use \r\n to make it cross-platform
                    sb.Append($"HTTP/1.1 400 Bad request\r\n");
                    sb.Append($"Content-Length: {Encoding.UTF8.GetBytes(content).Length}\r\n");
                    sb.Append("Content-Type: application/json\r\n");
                    sb.Append("\r\n");
                    sb.Append(content);

                    // Send
                    var answer = sb.ToString();
                    clientSocket.Send(Encoding.UTF8.GetBytes(answer));
                }

                var exc = Assert.ThrowsAsync<BadRequestException>(() =>
                {
                    return requestTask;
                });
                Assert.AreEqual(3, exc.Response.Errors.Count);
                Assert.AreEqual((int)ErrorCodes.ClientCallFailed, exc.Response.Errors[0].Code);
                Assert.AreEqual(100, exc.Response.Errors[1].Code);
                Assert.AreEqual(101, exc.Response.Errors[2].Code);
            }
        }

        [Test]
        public async Task Unit_Shared_BasicClient_PassErrorsThroughBadContentTest()
        {
            using (var client = new BaseClient($"http://localhost:{port}"))
            {
                client.PassErrorsThrough = true;

                // Post
                var requestTask = client.GetAsync<GenericResponse>("/test");

                // Read
                using (var clientSocket = await server.AcceptSocketAsync())
                {
                    var buffer = new byte[1024];
                    Thread.Sleep(100); // Wait for all data to come into read buffer
                    clientSocket.Receive(buffer);

                    // Build response
                    // "{\"asd\": 0}" valid json is also works fine
                    var content = "{asdasdasd i'm bad kill me";

                    StringBuilder sb = new StringBuilder();
                    // Use \r\n to make it cross-platform
                    sb.Append($"HTTP/1.1 400 Bad request\r\n");
                    sb.Append($"Content-Length: {Encoding.UTF8.GetBytes(content).Length}\r\n");
                    sb.Append("Content-Type: application/json\r\n");
                    sb.Append("\r\n");
                    sb.Append(content);

                    // Send
                    var answer = sb.ToString();
                    clientSocket.Send(Encoding.UTF8.GetBytes(answer));
                }

                // No errors to chain so usual very "bad" exception should be thrown
                var exc = Assert.ThrowsAsync<Exception>(() =>
                {
                    return requestTask;
                });
            }
        }

        [Test]
        public async Task Unit_Shared_BasicClient_InternalServerErrorWithNoContentTest()
        {
            using (var client = new BaseClient($"http://localhost:{port}"))
            {
                client.PassErrorsThrough = true;

                // Post
                var requestTask = client.PostAsync("/test", null);

                // Read
                using (var clientSocket = await server.AcceptSocketAsync())
                {
                    var buffer = new byte[1024];
                    Thread.Sleep(100); // Wait for all data to come into read buffer
                    int bytesReceived = clientSocket.Receive(buffer);
                    var reqText = Encoding.UTF8.GetString(buffer, 0, bytesReceived);

                    // Build response
                    StringBuilder sb = new StringBuilder();
                    // Use \r\n to make it cross-platform
                    sb.Append($"HTTP/1.1 500 Internal Server Error\r\n");
                    sb.Append($"Content-Length: 0\r\n");
                    sb.Append("\r\n");

                    // Send
                    var answer = sb.ToString();
                    clientSocket.Send(Encoding.UTF8.GetBytes(answer));
                }

                var exc = Assert.ThrowsAsync<Exception>(() =>
                {
                    return requestTask;
                });
            }
        }

        [Test]
        public void Unit_Shared_BasicClient_BuildUrlTest()
        {
            // With or without trailing / for endpoint - it will work
            using (var client = new BaseClient($"http://localhost:{port}/api"))
            {
                var finalUrl = client.BuildUrl("/getSomething?doNotRemoveMe", new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
                {
                    { "csv", "escape,me&" },
                    { "ids", new string[] { "1", "2" } },
                    { "limit", "100" },
                    { "offset", "2000" }
                });

                Assert.AreEqual($"/getSomething?doNotRemoveMe&csv=escape,me%26&ids=1&ids=2&limit=100&offset=2000", finalUrl);
            }
        }
    }
}
