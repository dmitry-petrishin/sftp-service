﻿using System;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.Parallel;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests
{
    [TestFixture]
    public class ParallelTests
    {
        [Test]
        public async Task TestParallel()
        {
            var i = 0;
            var maxI = 0;
            Func<Task> worker = async () =>
            {
                i++;
                if (i > maxI)
                {
                    maxI = i;
                }
                await Task.Delay(500);
                i--;
            };

            var remnants = await worker.Repeat(20).DoInParallel(5);
            await Task.WhenAll(remnants);
            Assert.AreEqual(5, maxI);
        }
    }
}
