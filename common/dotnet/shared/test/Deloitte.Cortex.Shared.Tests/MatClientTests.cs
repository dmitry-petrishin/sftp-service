﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

using NUnit.Framework;

using Deloitte.Cortex.Shared.Clients.Mat;
using Deloitte.Cortex.Shared.Clients.Mat.Models;

namespace Deloitte.Cortex.Shared.Tests
{
    [TestFixture]
    public class MatClientTests
    {
        private readonly string connectionString;

        public MatClientTests()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Local";

            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, false)
                .AddJsonFile($"appsettings.{environmentName}.json", true, false)
                .AddEnvironmentVariables();
            var configuration = builder.Build();

            connectionString = configuration.GetValue<string>("MatConnectionString");

            CheckData();
        }

        private void CheckData()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    // Init clients
                    cmd.CommandText = "DELETE FROM MAT_CLIENT WHERE ClientId IN (13700000, 13700001)";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"INSERT INTO MAT_CLIENT (ClientId, FiscalYearEnd)
VALUES
    (13700000, '05/25'),
    (13700001, '06/25')";
                    cmd.ExecuteNonQuery();

                    // Init engagements
                    cmd.CommandText = "DELETE FROM MAT_ENGAGEMENT WHERE EngagementId IN (27950000, 27950001, 27950002, 27950003)";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"INSERT INTO MAT_ENGAGEMENT (ClientId, EngagementId, EngagementName)
VALUES
    (13700000, 27950000, 'Engagement alpha'),
    (13700000, 27950001, 'Engagement beta'),
    (13700000, 27950002, 'Engagement alpha 2'),
    (13700001, 27950003, 'Engagement theta')";
                    cmd.ExecuteNonQuery();

                    // Init entities
                    cmd.CommandText = "DELETE FROM MAT_ENTITY WHERE EntityId IN (39297000, 39297001)";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"INSERT INTO MAT_ENTITY (ClientId, EngagementId, EntityId, EntityName)
VALUES
    (13700000, 39299, 39297000, 'ADITI CORP'),
    (13700000, 39308, 39297000, 'ADITI CORP'),
    (13700000, 39312, 39297000, 'ADITI CORP'),
    (13700000, 42070, 39297000, 'ADITI CORP'),
    (13700000, 42072, 39297000, 'ADITI CORP'),
    (13700000, 45623, 39297000, 'ADITI CORP'),
    (13700000, 27950000, 39297000, 'ADITI CORP'),
    (13700000, 27950001, 39297000, 'ADITI CORP'),
    (13700000, 27950002, 39297000, 'ADITI CORP'),
    (13700000, 27950002, 39297001, 'ADITI CORP 2nd dep')";
                    cmd.ExecuteNonQuery();

                    // Init employees
                    cmd.CommandText = "DELETE FROM MDR_EMPLOYEES WHERE PersonId IN (2030000)";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"INSERT INTO MDR_EMPLOYEES (PersonnelNbr, PersonId)
VALUES
    (2030000, 2030000)";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        [Test]
        public async Task Unit_Shared_MatClient_BasicTest()
        {
            using (MatDbClient matClient = new MatDbClient(connectionString))
            {
                int clientId = 13700000;
                MatClient client = await matClient.GetClientAsync(clientId);
                Assert.NotNull(client);
                Assert.AreEqual(clientId, client.Id);
                Assert.NotNull(client.FiscalYearEnd);
                Assert.AreEqual(25, client.FiscalYearEnd.Day);
                Assert.AreEqual(5, client.FiscalYearEnd.Month);

                int engagementId = 27950000;
                MatEngagement engagement = await matClient.GetEngagementAsync(engagementId);
                Assert.NotNull(engagement);
                Assert.AreEqual(engagementId, engagement.Id);

                int clientId2 = 3625;
                IEnumerable<MatEngagement> engagementList = await matClient.ListEngagementByClientIdAsync(clientId2);
                
                int entityId = 39297000;
                MatEntity entity = await matClient.GetEntityAsync(entityId);
                Assert.NotNull(entity);
                Assert.AreEqual(entityId, entity.Id);

                int clientId3 = 3625;
                IEnumerable<MatEntity> entityList = await matClient.ListEntityByClientIdAsync(clientId3);

                int employeeId = 2030000;
                MatEmployee employee = await matClient.GetEmployeeAsync(employeeId);
                Assert.NotNull(entity);
                Assert.AreEqual(employeeId, employee.PersonId);
            }
        }

        [Test]
        public async Task Unit_Shared_MatClient_GetEngagementsTest()
        {
            using (MatDbClient matClient = new MatDbClient(connectionString))
            {
                var result = await matClient.GetEngagementsAsync(new int[] { 27950000, 27950001, 27950003, 37950000 });
                Assert.AreEqual(3, result.Count());

                // Order doesn't make sense here
                CollectionAssert.AreEquivalent(new[] { "Engagement alpha", "Engagement beta", "Engagement theta" }, result.Select(x => x.Name));
            }
        }

        [Test]
        public async Task Unit_Shared_MatClient_SearchEngagementsTest()
        {
            using (MatDbClient matClient = new MatDbClient(connectionString))
            {
                var result = await matClient.SearchEngagementsAsync(13700000, "alpha", 100);
                Assert.AreEqual(2, result.Count());

                // Order doesn't make sense here
                CollectionAssert.AreEquivalent(new[] { "Engagement alpha", "Engagement alpha 2" }, result.Select(x => x.Name));
            }
        }

        [Test]
        public async Task Unit_Shared_MatClient_GetEntitiesForClientTest()
        {
            using (MatDbClient matClient = new MatDbClient(connectionString))
            {
                var result = await matClient.GetEntitiesForClientAsync(13700000);
                Assert.AreEqual(2, result.Count());

                // Order doesn't make sense here
                CollectionAssert.AreEquivalent(new[] { "ADITI CORP", "ADITI CORP 2nd dep" }, result.Select(x => x.Name));
            }
        }

        [Test]
        public async Task Unit_Shared_MatClient_GetClientWithEntitiesTest()
        {
            int clientId = 13700000;
            using (MatDbClient matClient = new MatDbClient(connectionString))
            {
                var result = await matClient.GetClientWithEntitiesAsync(clientId);

                Assert.NotNull(result);
                Assert.AreEqual(clientId, result.Id);
                Assert.NotNull(result.Entities);
                CollectionAssert.AreEquivalent(new[] { 39297000, 39297001 }, result.Entities.Select(x => x.Id));
            }
        }

        [Test]
        public async Task Unit_Shared_MatClient_GetEngagementWithEntitiesTest()
        {
            int engagementId = 27950002;
            using (MatDbClient matClient = new MatDbClient(connectionString))
            {
                var result = await matClient.GetEngagementWithEntitiesAsync(engagementId);

                Assert.NotNull(result);
                Assert.AreEqual(engagementId, result.Id);
                Assert.NotNull(result.Entities);
                CollectionAssert.AreEquivalent(new[] { 39297000, 39297001 }, result.Entities.Select(x => x.Id));
            }
        }

        [Test]
        public async Task Unit_Shared_MatClient_GetEngagementsWithEntitiesTest()
        {
            var engagementIds = new int[] { 27950001, 27950002 };
            using (MatDbClient matClient = new MatDbClient(connectionString))
            {
                var result = await matClient.GetEngagementsWithEntitiesAsync(engagementIds);

                Assert.NotNull(result);
                Assert.AreEqual(engagementIds.Length, result.Count());

                var engagement = result.First(x => x.Id == 27950001);
                CollectionAssert.AreEquivalent(new[] { 39297000 }, engagement.Entities.Select(x => x.Id));
                engagement = result.First(x => x.Id == 27950002);
                CollectionAssert.AreEquivalent(new[] { 39297000, 39297001 }, engagement.Entities.Select(x => x.Id));
            }
        }
    }
}
