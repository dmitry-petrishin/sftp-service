using Deloitte.Cortex.Shared.Reflection.DynamicProxyUtils;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests.Reflection
{
	[TestFixture]
	public class OverrideTests
	{
        [Test]
	    public void OverrideMethodTest()
	    {
	        TestInterfaceOne impl = new TestClassOne();
	        TestInterfaceOne ovrd = TransparentProxyHelper.WrapToProxy(impl);
	        TransparentProxyHelper.Override<TestInterfaceOne, string, string>(
	            impl,
	            _ => _.MethodOne(null),
	            (obj, arg) =>
	            {
	                var result = obj.MethodOne(arg);
	                return result;
	            });

	        var rawResult = impl.MethodOne("1");
	        var ovrdResult = ovrd.MethodOne("1");

	        Assert.AreEqual(rawResult, ovrdResult);
	    }

	    public interface TestInterfaceOne
	    {
	        string MethodOne(string arg);
	    }

        public interface TestInterfaceTwo
	    {
	        string MethodOne(string arg);
	    }

	    public class TestClassOne : TestInterfaceOne
	    {
	        public string MethodOne(string arg)
	        {
	            return arg + " 1";
	        }
	    }
	}
}
