﻿using System;
using Deloitte.Cortex.Shared.Clients.Audit.Models;
using Deloitte.Cortex.Shared.Clients.Interfaces;
using Deloitte.Cortex.Shared.Tests.ServiceBus;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests
{
    [TestFixture]
    public class AuditLogClientTests
    {
        private readonly IMessagingClient _auditLogClient;

        public AuditLogClientTests()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Local";

            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, false)
                .AddJsonFile($"appsettings.{environmentName}.json", true, false)
                .AddEnvironmentVariables();
            var configuration = builder.Build();

            _auditLogClient = new FakeMessagingClient();
        }

        [Test]
        public void Unit_Shared_BusClient()
        {
            var log = new AuditLogDto
            {
                Type = AuditLogEventType.General
            };
            _auditLogClient.SendToQueueAsync(log);
            var logReturned = _auditLogClient.RecieveFromQueueAsync<AuditLogDto>().Result;

            Assert.AreEqual(log.Type, logReturned.Type);
        }
    }
}
