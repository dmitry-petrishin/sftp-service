using System;
using System.Collections.Generic;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Tests.Repository.Abstract;
using NUnit.Framework;
using Item = Deloitte.Cortex.Shared.Tests.Repository.Models.TestItemWithDictionary<System.DateTimeKind>;

namespace Deloitte.Cortex.Shared.Tests.Repository
{
    [TestFixtureSource(typeof(RepositorySource<Item>))]
    public class EnumDictionaryTests: DictionaryTests<DateTimeKind>
    {
        public EnumDictionaryTests(IRepository<Item> repository): base(repository) { }

        protected override IEnumerable<DateTimeKind> GenerateKeys()
        {
            return Enum<DateTimeKind>.Values;
        }
    }
}