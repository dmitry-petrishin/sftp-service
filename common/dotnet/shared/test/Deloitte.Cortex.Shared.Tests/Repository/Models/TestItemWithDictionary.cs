﻿using System.Collections.Generic;

namespace Deloitte.Cortex.Shared.Tests.Repository.Models
{
    public class TestItemWithDictionary<TKey>: TestItem
    {
        public IDictionary<TKey, int> Dictionary { get; set; }
    }
}