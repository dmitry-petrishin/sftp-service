﻿using System;
using System.Collections.Generic;
using Deloitte.Cortex.Shared.Serialization;

namespace Deloitte.Cortex.Shared.Tests.Repository.Models
{
    public class TestItem : DocumentItemWithId, IEquatable<TestItem>
    {
        public string ReferencedObjectId { get; set; }
        public string Name { get; set; }
        public List<int> SomeIds { get; set; }
        public int Order { get; set; }

        public TestItem()
        {
            SomeIds = new List<int>();
        }

        #region Equality members

        public bool Equals(TestItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Name, other.Name) && Order == other.Order;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TestItem)obj);
        }

        public override int GetHashCode()
        {
            unchecked { return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ Order; }
        }

        public static bool operator ==(TestItem left, TestItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TestItem left, TestItem right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}
