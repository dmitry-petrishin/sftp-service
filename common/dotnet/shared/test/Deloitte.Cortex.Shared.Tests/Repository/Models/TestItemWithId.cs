﻿using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Tests.Repository.Models
{
    /// <typeparam name="TId">Should be <see cref="System.Nullable{T}"/> or class.</typeparam>
    public class TestItemWithId<TId>
    {
        [JsonProperty("id")]
        public TId Id { get; set; }

        public string Name { get; set; }
    }
}