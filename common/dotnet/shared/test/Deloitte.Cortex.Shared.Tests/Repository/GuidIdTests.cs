﻿using System;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Tests.Repository.Abstract;
using NUnit.Framework;
using Item = Deloitte.Cortex.Shared.Tests.Repository.Models.TestItemWithId<System.Guid?>;

namespace Deloitte.Cortex.Shared.Tests.Repository
{
    [TestFixtureSource(typeof(RepositorySource<Item>))]
    public class GuidIdTests: IdTests<Guid?>
    {
        public GuidIdTests(IRepository<Item> repository): base(repository) { }

        protected override Guid? GenerateId()
        {
            return Guid.NewGuid();
        }

        protected override string IdToString(Guid? id)
        {
            return id?.ToString("D") ?? "";
        }
    }
}