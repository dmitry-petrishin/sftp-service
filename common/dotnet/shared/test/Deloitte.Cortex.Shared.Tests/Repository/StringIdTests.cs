﻿using System;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Tests.Repository.Abstract;
using NUnit.Framework;
using Item = Deloitte.Cortex.Shared.Tests.Repository.Models.TestItemWithId<string>;

namespace Deloitte.Cortex.Shared.Tests.Repository
{
    [TestFixtureSource(typeof(RepositorySource<Item>))]
    public class StringIdTests: IdTests<string>
    {
        public StringIdTests(IRepository<Item> repository): base(repository) { }

        protected override string GenerateId()
        {
            return Guid.NewGuid().ToString("D");
        }

        protected override string IdToString(string id)
        {
            return id;
        }
    }

}