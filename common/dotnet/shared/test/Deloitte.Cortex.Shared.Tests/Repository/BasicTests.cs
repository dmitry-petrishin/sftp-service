﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.AspNetCore;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Tests.Repository.Models;
using Microsoft.Azure.Documents.Client;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests.Repository
{
    [TestFixtureSource(typeof(RepositorySource<TestItem>))]
    public class BasicTests: BaseTest
    {
        private readonly IRepository<TestItem> _repository;

        public BasicTests(IRepository<TestItem> repository)
        {
            _repository = repository;
        }

        [TestCase]
        public async Task Unit_Shared_Repository_WriteAndReadTest()
        {
            var newItem = new TestItem { Name = "Test item 1", SomeIds = new List<int> { 1, 10, 15 } };
            await _repository.CreateItemAsync(newItem);
            Assert.IsTrue(!newItem.IsIdEmpty);

            try
            {
                var existingItem = await _repository.GetItemByIdAsync(newItem.Id);
                Assert.AreEqual(newItem.Name, existingItem.Name, "Name mismatch");
                Assert.AreEqual(newItem.Id, existingItem.Id, "Id mismatch");
                Assert.AreEqual(newItem.SomeIds, existingItem.SomeIds, "Id mismatch");

                //var name = await repo.GetItemByIdAsync(newItem.Id, x => new { Name = x.Name });
                //Assert.AreEqual(newItem.Name, name);
            }
            finally
            {
                await _repository.DeleteItemAsync(newItem.Id);
            }
        }

        [TestCase]
        public async Task Unit_Shared_Repository_GetItemsCountTest()
        {
            string referencedObjectId = Guid.NewGuid().ToString();
            List<TestItem> items = new List<TestItem>();
            int i = 0;
            while (++i <= 10)
            {
                var item = new TestItem
                {
                    Name = "Item #" + i.ToString(),
                    ReferencedObjectId = referencedObjectId
                };
                await _repository.CreateItemAsync(item);
                items.Add(item);
            }

            try
            {
                int count = await _repository.GetItemCountAsync(item => item.ReferencedObjectId == referencedObjectId);
                Assert.AreEqual(10, count);
            }
            finally
            {
                await _repository.DeleteItemsAsync(items.Select(item => item.Id));
            }
        }

        [TestCase]
        public async Task Unit_Shared_Repository_PaginationTest()
        {
            string testName = "Pagination Test " + Guid.NewGuid();

            var items = Enumerable.Range(0, 10).Select(n => new TestItem
            {
                Name = testName + " Item #" + n,
                Order = -n
            }).ToList();

            await _repository.CreateItemsAsync(items
                .Union(new[] { new TestItem { Name = "Other item" } }));

            try
            {
                Assert.AreEqual(10, items.Count);

                const int pageSize = 4, page = 2;
                var res = await _repository.GetItemsAsync(item => item.Name.StartsWith(testName), it => it.Order, pageSize * (page - 1), pageSize,
                    feedOptions: new FeedOptions { EnableScanInQuery = true });

                Assert.AreEqual(10, res.TotalCount, "Total items count is invalid");
                Assert.AreEqual(pageSize, res.Items.Length, "Expected full page of results");
                Assert.AreEqual(res.Items.First().Order, -5, "Invalid first item in results");
                CollectionAssert.AreEqual(res.Items, items.OrderBy(it => it.Order).Skip(pageSize * (page - 1)).Take(pageSize),
                    "Returned items are different or have different order.");
            }
            finally
            {
                await _repository.DeleteItemsAsync(items.Select(item => item.Id));
            }
        }

        [TestCase]
        public async Task Unit_Shared_Repository_DeleteItemsTest()
        {
            var objId = Guid.NewGuid().ToString();
            var createdItems = new List<TestItem>();
            for (int i = 1; i <= 50; i++)
            {
                var item = new TestItem
                {
                    ReferencedObjectId = objId,
                    Name = $"Test item {i}"
                };
                await _repository.CreateItemAsync(item);
                createdItems.Add(item);
            }

            var items = await _repository.GetItemsAsync(item => item.ReferencedObjectId == objId, item => item.Id);
            Assert.AreEqual(createdItems.Count, items.Count);
            await _repository.DeleteItemsAsync(items);
        }

        [TestCase]
        public async Task Unit_Shared_Repository_UpdateTest()
        {
            var item = new TestItem
            {
                Name = "test"
            };
            await _repository.CreateItemAsync(item);
            try
            {
                var itemToUpdate = new TestItem
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "test 1"
                };
                //Assert.ThrowsAsync<Shared.ExceptionHandling.NotFoundException>(() => { return repo.UpdateItemAsync(itemToUpdate); });
            }
            finally
            {
                await _repository.DeleteItemAsync(item.Id);
            }
        }

        [TestCase]
        public async Task Unit_Shared_Repository_FeedOptionsMaxItemCountTest()
        {
            var items = new[] { new TestItem { Name = "Item 1" }, new TestItem { Name = "Item 2" } };
            await _repository.CreateItemsAsync(items);

            try
            {
                var feedOptions = new FeedOptions { MaxItemCount = 1 };

                var getItems = await _repository.GetItemsAsync(null, feedOptions);
                Assert.AreEqual(feedOptions.MaxItemCount, getItems.Count);

                getItems = await _repository.GetItemsAsync(null, i => i, feedOptions);
                Assert.AreEqual(feedOptions.MaxItemCount, getItems.Count);
            }
            finally
            {
                await _repository.DeleteItemsAsync(items.Select(i => i.Id));
            }
        }
    }
}