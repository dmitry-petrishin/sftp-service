﻿using System;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.ExceptionHandling;
using Deloitte.Cortex.Shared.Tests.Repository.Models;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests.Repository
{
    [TestFixtureSource(typeof(RepositorySource<TestItem>))]
    public class ExceptionTests
    {
        private readonly IRepository<TestItem> _repository;

        public ExceptionTests(IRepository<TestItem> repository)
        {
            _repository = repository;
        }

        [TestCase]
        public void Unit_Shared_Repository_GetItemById_ThrowsNotFound_IfNoItemWithId()
        {
            Assert.ThrowsAsync<NotFoundException>(() =>
                _repository.GetItemByIdAsync(Guid.NewGuid()));

            // Todo: test with struct selector once supported
            Assert.ThrowsAsync<NotFoundException>(() =>
                _repository.GetItemByIdAsync(Guid.NewGuid(), item => item));
        }

        [TestCase]
        public void Unit_Shared_Repository_UpdateItem_ThrowsNotFound_IfNoItemWithId()
        {
            Assert.ThrowsAsync<NotFoundException>(() =>
                _repository.UpdateItemAsync(Guid.NewGuid().ToString(), new TestItem()));
        }

        [TestCase]
        public void Unit_Shared_Repository_DeleteItem_ThrowsNotFound_IfNoItemWithId()
        {
            Assert.ThrowsAsync<NotFoundException>(() =>
                _repository.DeleteItemAsync(Guid.NewGuid().ToString()));
        }

        [TestCase]
        public async Task Unit_Shared_Repository_CreateItem_ThrowsConflict_IfItemWithIdExists()
        {
            var item = new TestItem();
            await _repository.CreateItemAsync(item);

            try
            {
                var item2 = new TestItem { Id = item.Id };
                Assert.ThrowsAsync<ConflictException>(() =>
                    _repository.CreateItemAsync(item2));
            }
            finally
            {
                await _repository.DeleteItemAsync(item.Id);
            }
        }
    }
}
