using System;
using System.Collections.Generic;
using System.Linq;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Tests.Repository.Abstract;
using NUnit.Framework;
using Item = Deloitte.Cortex.Shared.Tests.Repository.Models.TestItemWithDictionary<System.Guid>;

namespace Deloitte.Cortex.Shared.Tests.Repository
{
    [TestFixtureSource(typeof(RepositorySource<Item>))]
    public class GuidDictionaryTests: DictionaryTests<Guid>
    {
        public GuidDictionaryTests(IRepository<Item> repository): base(repository) { }

        protected override IEnumerable<Guid> GenerateKeys()
        {
            return Enumerable.Range(0, 5).Select(_ => Guid.NewGuid());
        }
    }
}