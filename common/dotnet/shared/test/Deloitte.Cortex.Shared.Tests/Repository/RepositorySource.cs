using System.Collections;
using System.Collections.Generic;
using Deloitte.Cortex.Shared.DataAccess;
using Microsoft.Extensions.Configuration;

namespace Deloitte.Cortex.Shared.Tests.Repository
{
    public class RepositorySource<TItem>: BaseTest, IEnumerable<IRepository<TItem>>
        where TItem: class
    {
        public IEnumerator<IRepository<TItem>> GetEnumerator()
        {
            var configuration = BaseTest.GetConfiguration();

            var documentDBSettings = new DocumentDBSettings();
            configuration.GetSection("DocumentDB").Bind(documentDBSettings);
            yield return new DocumentDBRepository<TItem>(documentDBSettings);

            var mongoDBSettings = new MongoDBSettings();
            configuration.GetSection("Mongo").Bind(mongoDBSettings);
            yield return new MongoDBRepository<TItem>(mongoDBSettings);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}