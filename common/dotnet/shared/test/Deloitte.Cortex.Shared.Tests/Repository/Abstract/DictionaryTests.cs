﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Tests.Repository.Models;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests.Repository.Abstract
{
    public abstract class DictionaryTests<TKey>
    {
        private readonly IRepository<TestItemWithDictionary<TKey>> _repository;

        protected DictionaryTests(IRepository<TestItemWithDictionary<TKey>> repository)
        {
            _repository = repository;
        }

        protected abstract IEnumerable<TKey> GenerateKeys(); 

        [TestCase]
        public async Task CreateItem_ShouldSerializeDictionary()
        {
            int counter = 0;
            var item = new TestItemWithDictionary<TKey>
            {
                Dictionary = GenerateKeys().ToDictionary(key => key, key => ++counter)
            };

            try
            {
                await _repository.CreateItemAsync(item);
            }
            finally
            {
                await _repository.DeleteItemAsync(item.Id);
            }
        }
    }
}