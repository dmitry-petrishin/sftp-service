using System.Threading.Tasks;
using Deloitte.Cortex.Shared.DataAccess;
using Deloitte.Cortex.Shared.Tests.Repository.Models;
using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests.Repository.Abstract
{
    public abstract class IdTests<TId>
    {
        private readonly IRepository<TestItemWithId<TId>> _repository;

        protected IdTests(IRepository<TestItemWithId<TId>> repository)
        {
            _repository = repository;
        }

        protected abstract TId GenerateId();

        protected abstract string IdToString(TId id);

        [TestCase]
        public async Task CreateItem_ShouldPopulateEmptyId()
        {
            var item = new TestItemWithId<TId> { Id = default(TId) };

            try
            {
                await _repository.CreateItemAsync(item);
            }
            finally
            {
                await _repository.DeleteItemAsync(IdToString(item.Id));
            }

            Assert.IsNotNull(item.Id);
        }

        [TestCase]
        public async Task CreateItem_ShouldNotChangeNonemptyId()
        {
            var id = GenerateId();
            var item = new TestItemWithId<TId> { Id = id };

            try
            {
                await _repository.CreateItemAsync(item);
            }
            finally
            {
                await _repository.DeleteItemAsync(IdToString(item.Id));
            }

            Assert.AreEqual(id, item.Id);
        }
    }
}