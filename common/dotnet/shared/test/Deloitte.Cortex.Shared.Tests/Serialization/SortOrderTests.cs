﻿using NUnit.Framework;
using Deloitte.Cortex.Shared.Serialization;
using System.Net;
using Deloitte.Cortex.Shared.ExceptionHandling;

namespace Deloitte.Cortex.Shared.Tests.Serialization
{
	[TestFixture]
	public class SortOrderTests
	{
		[TestCase("name", "name", true)]
		[TestCase("+name", "name", true)]
		[TestCase("-name", "name", false)]
		[TestCase("", null, true)]
		[TestCase(" ", null, true)]
		[TestCase("\t", null, true)]
		[TestCase(null, null, true)]
		public void Unit_Shared_Serialization_SortOrder_ShouldParseValidParameter(string sort, string orderBy, bool orderAsc)
		{
			SortOrder sortOrder = SortOrder.Parse(sort);

			Assert.That(sortOrder.OrderBy, Is.EqualTo(orderBy), "Field name does not match.");
			Assert.That(sortOrder.OrderAsc, Is.EqualTo(orderAsc), "Order direction does not match.");
		}

		[TestCase("+")]
		[TestCase("-")]
		public void Unit_Shared_Serialization_SortOrder_ShouldFailForInvalidParameter(string sort)
		{
			Assert.Throws(
				Is.TypeOf<HttpException>().And.Property("HttpCode").EqualTo(HttpStatusCode.BadRequest),
				delegate { SortOrder.Parse(sort); });
		}
	}
}
