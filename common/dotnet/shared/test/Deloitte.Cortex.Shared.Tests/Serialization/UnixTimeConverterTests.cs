﻿using NUnit.Framework;
using Deloitte.Cortex.Shared.Serialization;
using System;
using Newtonsoft.Json;

namespace Deloitte.Cortex.Shared.Tests.Serialization
{
    [TestFixture]
    public class UnixTimeConverterTests
    {
        private class TestClass
        {
            [JsonConverter(typeof(UnixTimeConverter))]
            public DateTime Time { get; set; }

            [JsonConverter(typeof(UnixTimeConverter), new object[] { UnixTimeConverter.KindEnum.Milliseconds })]
            public DateTime? TimeMs { get; set; }
        }

        [Test]
        public void Unit_Shared_Serialization_UnixTimeConverter_FullTest()
        {
            var json = "{\"Time\": 1487278666, \"TimeMs\": 1487278666000}";
            var res = JsonConvert.DeserializeObject<TestClass>(json);
            Assert.IsNotNull(res);
            Assert.AreEqual(DateTime.Parse("16.02.2017 20:57:46"), res.Time);
            Assert.AreEqual(DateTime.Parse("16.02.2017 20:57:46"), res.TimeMs);

            json = "{\"Time\": 1487278666}";
            res = JsonConvert.DeserializeObject<TestClass>(json);
            Assert.IsFalse(res.TimeMs.HasValue);

            json = JsonConvert.SerializeObject(res);
            Assert.AreEqual("{\"Time\":1487278666,\"TimeMs\":null}", json);

            res.TimeMs = new DateTime(2017, 02, 16, 20, 57, 46, DateTimeKind.Utc);
            json = JsonConvert.SerializeObject(res);
            Assert.AreEqual("{\"Time\":1487278666,\"TimeMs\":1487278666000}", json);
        }
    }
}
