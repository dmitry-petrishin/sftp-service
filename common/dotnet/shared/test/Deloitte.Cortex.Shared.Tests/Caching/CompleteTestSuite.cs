﻿using Deloitte.Cortex.Shared.Clients.Caching;
using Deloitte.Cortex.Shared.Clients.Caching.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Deloitte.Cortex.Shared.Tests.Caching
{
    public class CompleteTestSuite : BaseTest
    {
        private class TestObject
        {
            public string TestField { get; set; }

            public override bool Equals(object obj)
            {
                if (obj == null || obj.GetType() != GetType())
                    return false;
                var specificObj = (TestObject)obj;

                return (TestField.Equals(specificObj.TestField));
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        private CachingConfiguration cachingConfig;

        public CompleteTestSuite()
        {
            var configuration = GetConfiguration();
            cachingConfig = new CachingConfiguration();
            configuration.GetSection("Caching").Bind(cachingConfig);
        }

        [Test]
        public async Task Unit_Shared_Caching_RedisSetGetTest()
        {
            var factory = new LoggerFactory();
            var logger = factory.CreateLogger<RedisClient>();
            var cachingClient = new RedisClient(cachingConfig.Endpoint.Value, "UnitTests", logger, cachingConfig.CA, cachingConfig.Crt);

            var key = Guid.NewGuid().ToString();
            var original = new TestObject { TestField = Guid.NewGuid().ToString() };
            await cachingClient.SetAsync(key, original);
            try
            {
                var read = await cachingClient.GetAsync<TestObject>(Guid.NewGuid().ToString());
                Assert.IsNull(read);
                read = await cachingClient.GetAsync<TestObject>(key);
                Assert.AreEqual(original, read);
            }
            finally
            {
                await cachingClient.DeleteAsync(key);
            }
        }

        [Test]
        public async Task Unit_Shared_Caching_FactoryTest()
        {
            var factory = new LoggerFactory();
            var client = Factory.GetClient(cachingConfig, "UnitTests", factory);

            var key = Guid.NewGuid().ToString();

            var original = new TestObject { TestField = Guid.NewGuid().ToString() };
            await client.SetAsync(key, original);
            try
            {
                var read = await client.GetAsync<TestObject>(key);
                Assert.AreEqual(original, read);
            }
            finally
            {
                await client.DeleteAsync(key);
            }
        }

        [Test]
        public void Unit_Shared_Caching_LockableTest()
        {
            var factory = new LoggerFactory();
            var client = Factory.GetClient(cachingConfig, "UnitTests", factory);

            var key = Guid.NewGuid().ToString();
            var obj = new object();
            using (var lockObject = Lockable<object>.SetAndLock(client, key, obj))
            {
                Assert.IsNotNull(lockObject);
                Assert.AreEqual(LockStatus.Locked, lockObject.Status);
                Assert.IsNotNull(lockObject.Object);

                var anotherTry = Lockable<object>.Get(client, key, TimeSpan.Zero, null, TimeSpan.FromMinutes(5));
                Assert.IsNotNull(anotherTry);
                Assert.AreEqual(LockStatus.LockFailed, anotherTry.Status);
                Assert.IsNull(anotherTry.Object);

                var delay = TimeSpan.FromSeconds(2);
                Thread.Sleep(delay);
                anotherTry = Lockable<object>.Get(client, key, TimeSpan.Zero, null, delay);
                Assert.IsNotNull(anotherTry);
                Assert.AreEqual(LockStatus.Locked, anotherTry.Status);
                Assert.IsNotNull(anotherTry.Object);
            }

            {
                var anotherTry = Lockable<object>.Get(client, key, TimeSpan.Zero, null, TimeSpan.FromMinutes(5));
                Assert.IsNotNull(anotherTry);
                anotherTry.Dispose();
            }
        }

        [Test]
        public async Task Unit_Shared_Caching_RedisLockTakeReleaseTest()
        {
            var factory = new LoggerFactory();
            var logger = factory.CreateLogger<RedisClient>();
            var cachingClient = new RedisClient(cachingConfig.Endpoint.Value, "UnitTests", logger, cachingConfig.CA, cachingConfig.Crt);

            var key = Guid.NewGuid().ToString();
            var original = new TestObject { TestField = Guid.NewGuid().ToString() };
            //await cachingClient.SetAsync(key, original);

            var token = Guid.NewGuid().ToString();
            try
            {
                var firstUserCase = cachingClient.LockTake(key, token, new TimeSpan(0, 60, 0));
                var firstQuery = cachingClient.LockQuery(key);
                var secondUserCase = cachingClient.LockTake(key, token, new TimeSpan(0, 60, 0));
                Assert.IsTrue(firstUserCase);
                Assert.IsFalse(secondUserCase);
                Assert.AreEqual(firstQuery, token); 
                var lockRelease = cachingClient.LockRelease(key, token);
                Assert.IsTrue(lockRelease);
                var secondQuery = cachingClient.LockQuery(key);
                Assert.IsNull(secondQuery);
            }
            finally
            {
                await cachingClient.DeleteAsync(key);
            }
        }
    }
}
