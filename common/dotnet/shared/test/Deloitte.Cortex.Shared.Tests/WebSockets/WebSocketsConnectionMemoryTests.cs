﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;

using Deloitte.Cortex.Shared.WebSockets;

using NUnit.Framework;

namespace Deloitte.Cortex.Shared.Tests.WebSockets
{
    [TestFixture]
    public class WebSocketsConnectionMemoryTests
    {

        [Test]
        public void AddSocketShouldBeAdded()
        {
            WebSocketConnectionMemory memory = new WebSocketConnectionMemory();
            WebSocket socket = new TestWebSocket();

            Assert.That(() => { memory.AddSocket(socket); }, Throws.Nothing);

            string id = memory.GetId(socket);

            Assert.That(id, Is.Not.Null);
        }

        [Test]
        public void GetIdOfNotExistedSocketShouldReturnNull()
        {
            WebSocketConnectionMemory memory = new WebSocketConnectionMemory();
            WebSocket socket = new TestWebSocket();
            string result = memory.GetId(socket);
            Assert.That(result, Is.Null);
        }

        [Test]
        public void RemoveNotExistedSocket()
        {
            WebSocketConnectionMemory memory = new WebSocketConnectionMemory();

            Assert.That(async () => { await memory.RemoveSocket(Guid.NewGuid().ToString()); }, Throws.Nothing);
        }
    }
}
