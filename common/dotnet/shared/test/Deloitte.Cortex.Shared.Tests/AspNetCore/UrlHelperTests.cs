﻿using System;
using System.Collections.Generic;
using System.Text;

using Deloitte.Cortex.Shared.AspNetCore.Formatting;

using Microsoft.Extensions.Primitives;

using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Deloitte.Cortex.Shared.Tests.AspNetCore
{
    public class UrlHelperTests
    {
        [Test]
        public void SimpleTest()
        {
            string id = Guid.NewGuid().ToString();
            string uri = $"datasets/{id}/field";
            string[] names = {"name1", "name2", "name3"};
            string[] practiceIds = { Guid.NewGuid().ToString() };

            IDictionary<string, StringValues> dict = new Dictionary<string, StringValues>
                           {
                               {"practiceIds", new StringValues(practiceIds)},
                               {"names", new StringValues(names)}
                           };

            string finalUri = UrlHelper.GetUriFromArrays(uri, dict);
            string expectedUri = uri + $"?practiceIds={practiceIds[0]}&names=name1&names=name2&names=name3";

            Assert.That(finalUri, Is.EqualTo(expectedUri));
        }

        [Test]
        public void ShouldEscapeSymbols()
        {
            string id = Guid.NewGuid().ToString();
            string uri = $"datasets/{id}/field";
            string[] names = {"name 1", "name 2", "name 3"};

            IDictionary<string, StringValues> dict = new Dictionary<string, StringValues>
                                                         {
                                                             {"names", new StringValues(names)}
                                                         };

            string finalUri = UrlHelper.GetUriFromArrays(uri, dict);
            string expectedUri = uri + $"?names=name%201&names=name%202&names=name%203";

            Assert.That(finalUri, Is.EqualTo(expectedUri));
        }
    }
}